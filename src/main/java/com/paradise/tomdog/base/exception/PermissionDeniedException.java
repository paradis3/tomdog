package com.paradise.tomdog.base.exception;

/**
 * 没有权限异常
 *
 * @author Paradise
 */
public class PermissionDeniedException extends Exception {
    public PermissionDeniedException() {
        super();
    }

    public PermissionDeniedException(String message) {
        super(message);
    }
}
