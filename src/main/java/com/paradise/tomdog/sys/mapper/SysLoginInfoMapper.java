package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysLoginInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author Paradise
 */
@Mapper
public interface SysLoginInfoMapper {
    /**
     * delete by primary key
     *
     * @param infoId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long infoId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysLoginInfo record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysLoginInfo record);

    /**
     * select by primary key
     *
     * @param infoId primary key
     * @return object by primary key
     */
    SysLoginInfo selectByPrimaryKey(Long infoId);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysLoginInfo record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysLoginInfo record);

    /**
     * 查询上次登录信息
     */
    SysLoginInfo selectLastLoginInfo(@Param("username") String username,
                                     @Param("domain") String domain);

    /**
     * 查询上次登录时间
     *
     * @param uid uid
     * @return {@link Date}
     */
    Date selectLastLoginTime(Long uid);
}