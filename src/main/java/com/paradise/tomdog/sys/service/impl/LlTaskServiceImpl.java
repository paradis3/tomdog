package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.rpc.constant.Task_Add_Type;
import com.paradise.tomdog.rpc.result.*;
import com.paradise.tomdog.rpc.utils.LlUtils;
import com.paradise.tomdog.sys.api.param.TaskCreateParam;
import com.paradise.tomdog.sys.api.res.ApiCreateResult;
import com.paradise.tomdog.sys.constant.*;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.LlTaskService;
import com.paradise.tomdog.sys.task.utils.LlTaskUtils;
import com.paradise.tomdog.sys.vo.TaskQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Paradise
 */
@Service
@Slf4j
public class LlTaskServiceImpl implements LlTaskService {

    @Resource
    private LlTaskMapper llTaskMapper;
    @Resource
    private TaskOperationLogMapper logMapper;
    @Resource
    private TaskItemMapper taskItemMapper;
    @Resource
    private TaskTbItemMapper taskTbItemMapper;
    @Resource
    private TaskPriceDepthMapper taskPriceDepthMapper;
    @Resource
    private TaskPriceBtnMapper taskPriceBtnMapper;
    @Resource
    private TaskPriceMapper taskPriceMapper;
    @Resource
    private UserLevelMapper userLevelMapper;
    @Resource
    private SysUserMapper userMapper;
    @Resource
    private TaskPermissionMerchantMapper taskPermissionMerchantMapper;
    @Resource
    private BalanceMapper balanceMapper;
    @Resource
    private BalanceService balanceService;
    @Resource
    private TaskRefundMapper taskRefundMapper;
    @Resource
    private PointsRecordMapper pointsRecordMapper;
    @Resource
    private TbShopMapper tbShopMapper;

    @Override
    public LlTask selectByPrimaryKey(Long id) {
        return llTaskMapper.selectByPrimaryKey(id);
    }

    @Override
    public LlTask selectByTaskId(String taskId) {
        return llTaskMapper.selectOneByTaskId(taskId);
    }

    @Override
    public PageData selectTaskPage(TaskQuery query, Integer pageNum, Integer pageSize, SysUser currentUser) {
        // 时间戳转 10位
        if (query.getBeginDate() != null) {
            query.setBeginDate(query.getBeginDate() / 1000);
        }
        if (query.getEndDate() != null) {
            query.setEndDate(query.getEndDate() / 1000);
        }
        List<LlTask> taskList;
        if (CommonUtils.isMerchantType(currentUser)) {
            PageHelper.startPage(pageNum, pageSize);
            taskList = llTaskMapper.selectByTaskQueryAndMerchantId(query, currentUser.getMerchantId());
        } else if (CommonUtils.isAdminType(currentUser)) {
            PageHelper.startPage(pageNum, pageSize);
            taskList = llTaskMapper.selectByTaskQuery(query);
        } else if (CommonUtils.isApiType(currentUser) || CommonUtils.isMerchantUserType(currentUser)) {
            query.setUserId(currentUser.getId());
            PageHelper.startPage(pageNum, pageSize);
            taskList = llTaskMapper.selectByTaskQuery(query);
        } else {
            taskList = new ArrayList<>();
        }
        return new PageData(new PageInfo<>(taskList));
    }

    @Override
    public void exportTaskList(TaskQuery query, HttpServletRequest request, HttpServletResponse response, SysUser currentUser) {
        // 时间戳转 10位
        if (query.getBeginDate() != null) {
            query.setBeginDate(query.getBeginDate() / 1000);
        }
        if (query.getEndDate() != null) {
            query.setEndDate(query.getEndDate() / 1000);
        }
        List<LlTask> taskList;
        if (CommonUtils.isMerchantType(currentUser)) {
            taskList = llTaskMapper.selectByTaskQueryAndMerchantId(query, currentUser.getMerchantId());
        } else {
            if (CommonUtils.isApiType(currentUser)) {
                query.setUserId(currentUser.getId());
            }
            taskList = llTaskMapper.selectByTaskQuery(query);
        }
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .dataList(getDataList(taskList)).title("任务数据列表")
                .rowName(new String[]{"序号", "任务ID", "用户ID", "任务类型", "数量/已完成", "消耗积分",
                        "任务状态", "开始时间", "商品/店铺", "关键字", "创建时间"})
                .request(request).response(response)
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    private List<Object[]> getDataList(List<LlTask> taskList) {
        List<Object[]> list = new ArrayList<>();
        Object[] arr;
        int index = 1;
        for (LlTask task : taskList) {
            arr = new Object[11];
            arr[0] = index++;
            arr[1] = task.getTaskId();
            arr[2] = task.getUserId();
            arr[3] = task.getType();
            arr[4] = task.getCompletedCount();
            arr[5] = task.getCostPoints();
            arr[6] = task.getStatus();
            arr[7] = task.getBeginTime();
            arr[8] = task.getTarget();
            arr[9] = task.getKeywords();
            arr[10] = DateFormatUtils.format(task.getCreateTime());
            list.add(arr);
        }
        return list;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R changeTaskStatus(Long id, String status, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除！");
        }
        if (!task.getStatus().equals(Task_Status.ING.getCode())) {
            return Rx.fail("任务不在进行中状态，无法完成操作！");
        }
        int x = llTaskMapper.updateStatusById(status, id, null);
        if (x == 1) {
            return Rx.success();
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return Rx.fail();
    }

    /**
     * 记录任务操作日志
     *
     * @param taskId   任务主键
     * @param operator 操作人id
     * @param type     操作类型{@link Task_Operate_Type}
     * @param result   LL api 返回结果
     */
    private void recordOperation(Long taskId, Long operator, Task_Operate_Type type, LlResultInterface result) {
        TaskOperationLog operationLog = new TaskOperationLog();
        operationLog.setCreateTime(System.currentTimeMillis());
        operationLog.setOperatorId(operator);
        operationLog.setTaskId(taskId);
        operationLog.setOperateType(type.getType());
        if (result != null) {
            if (result.isSuccess()) {
                operationLog.setResult(1);
            } else {
                operationLog.setResult(0);
                operationLog.setResultMsg(result.toString());
            }
        }
        logMapper.insert(operationLog);
    }

    @Override
    public R taskPause(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除");
        }
        if (!task.getStatus().equals(Task_Status.ING.getCode())) {
            return Rx.fail("任务状态不是进行中，无法操作！");
        }
        LlTaskPauseResult result = LlTaskUtils.pause(task);
        if (!result.isSuccess()) {
            log.error("任务 {} 暂停失败： {}", task.getTaskId(), result.toString());
            recordOperation(id, user.getId(), Task_Operate_Type.PAUSE, result);
            return Rx.fail("任务申请暂停失败，请稍后再试");
        } else {
            int x = llTaskMapper.updateStatusById(Task_Status.PAUSE_ING.getCode(), id, null);
            if (x == 1) {
                // 记录操作日志
                recordOperation(id, user.getId(), Task_Operate_Type.PAUSE, result);
                return Rx.success();
            }
            log.error("申请暂停任务 {}，更新任务状态失败", task.getTaskId());
            return Rx.fail();
        }
    }

    @Override
    public R taskCancel(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除");
        }
        if (!task.getStatus().equals(Task_Status.ING.getCode()) &&
                task.getStatus().equals(Task_Status.PAUSED.getCode())) {
            return Rx.fail("任务状态不是进行中，无法操作！");
        }
        // 分站用户角色 or API 角色 只能操作自己的任务
        boolean isCurrentUser = task.getUserId().equals(user.getId());
        if ((CommonUtils.isApiType(user) || CommonUtils.isMerchantUserType(user)) && !isCurrentUser) {
            return Rx.permissionDenied();
        }
        List<TaskItem> taskItemList = taskItemMapper.selectByTaskId(id);
        if (taskItemList.isEmpty()) {
            LlTaskCancelResult result = LlTaskUtils.cancel(task.getTaskId());
            if (!result.isSuccess()) {
                log.error("任务 {} 取消失败： {}", task.getTaskId(), result.toString());
                recordOperation(id, user.getId(), Task_Operate_Type.CANCEL, result);
                return Rx.fail("任务申请取消失败，请稍后再试");
            } else {
                int x = llTaskMapper.updateStatusById(Task_Status.CANCEL_ING.getCode(), id, null);
                if (x == 1) {
                    // 记录操作日志
                    recordOperation(id, user.getId(), Task_Operate_Type.CANCEL, result);
                    return Rx.success(llTaskMapper.selectByPrimaryKey(id));
                }
            }
        } else {
            for (TaskItem item : taskItemList) {
                LlTaskCancelResult result = LlTaskUtils.cancel(item.getTaskItemId());
                if (!result.isSuccess()) {
                    log.error("任务 {} 取消失败： {}", task.getTaskId(), result.toString());
                    recordOperation(id, user.getId(), Task_Operate_Type.CANCEL, result);
                    return Rx.fail("任务申请取消失败，请稍后再试");
                }
            }
            int x = llTaskMapper.updateStatusById(Task_Status.CANCEL_ING.getCode(), id, null);
            if (x == 1) {
                // 记录操作日志
                recordOperation(id, user.getId(), Task_Operate_Type.CANCEL, null);
                return Rx.success(llTaskMapper.selectByPrimaryKey(id));
            }
        }
        log.error("申请取消任务 {}，更新任务状态失败", task.getTaskId());
        return Rx.fail();
    }

    @Override
    public R taskComplete(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除");
        }
        if (!task.getStatus().equals(Task_Status.ING.getCode())) {
            return Rx.fail("任务状态不是进行中，无法操作！");
        }
        int x = llTaskMapper.updateStatusById(Task_Status.COMPLETED.getCode(), id, null);
        if (x == 1) {
            // 记录操作日志
            recordOperation(id, user.getId(), Task_Operate_Type.COMPLETE, null);
            return Rx.success();
        }
        log.error("申请完成任务 {}，更新任务状态失败", task.getTaskId());
        return Rx.fail();
    }

    @Override
    public R taskQuery(Long id) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除");
        }
        if (task.getStatus().equals(Task_Status.INIT.getCode())) {
            return Rx.fail("任务未发布！");
        }
        List<TaskItem> taskItemList = taskItemMapper.selectByTaskId(id);
        List<LlTaskQueryResult> results = new ArrayList<>();
        if (taskItemList.size() > 0) {
            int totalCompletedCount = 0;
            boolean isCompleted = false;
            for (TaskItem item : taskItemList) {
                LlTaskQueryResult result = LlTaskUtils.query(item.getTaskItemId());
                if (!result.isSuccess()) {
                    return Rx.error("任务查询失败", result);
                } else {
                    // 数据更新逻辑
                    if (taskNeedRefund(result)) {
                        this.insertIntoRefund(task.getTaskId(), result, task.getId());
                    }
                    Integer completedCount = result.getData().getCompletedCount();
                    if (item.getTotalCount() > completedCount && completedCount > item.getCompletedCount()) {
                        taskItemMapper.updateCompletedCountById(completedCount, item.getId());
                        isCompleted = false;
                    }
                    if (completedCount >= item.getTotalCount()) {
                        taskItemMapper.completeTask(item.getId(), Task_Status.COMPLETED.getCode());
                        isCompleted = true;
                    }
                    totalCompletedCount += completedCount;
                }
                results.add(result);
            }
            // 如果子任务全部完成，则标记任务为完成状态
            if (isCompleted) {
                llTaskMapper.completeTask(task.getId(), Task_Status.COMPLETED.getCode());
            } else {
                // 否则更新任务完成数量
                llTaskMapper.updateCompletedCountById(totalCompletedCount, task.getId());
            }
            return Rx.success(results);
        } else {
            LlTaskQueryResult result = LlTaskUtils.query(task.getTaskId());
            if (result.isSuccess()) {
                // 数据更新逻辑
                if (taskNeedRefund(result)) {
                    this.insertIntoRefund(task.getTaskId(), result, task.getId());
                }
                Integer completedCount = result.getData().getCompletedCount();
                if (task.getCount() > completedCount && completedCount > task.getCompletedCount()) {
                    llTaskMapper.updateCompletedCountById(completedCount, task.getId());
                }
                if (completedCount >= task.getCount()) {
                    llTaskMapper.completeTask(task.getId(), Task_Status.COMPLETED.getCode());
                }
                return Rx.success(result);
            } else {
                return Rx.error("任务查询失败", result);
            }
        }
    }

    private boolean taskNeedRefund(LlTaskQueryResult result) {
        return result.getData().getStatusCode().equals(10) || result.getData().getStatusCode().equals(9);
    }

    private void insertIntoRefund(String taskId, LlTaskQueryResult result, Long id) {
        if (result.getData().getStatusCode().equals(10)) {
            llTaskMapper.updateStatusById(Task_Status.CANCELED.getCode(), id, null);
        } else if (result.getData().getStatusCode().equals(9)) {
            llTaskMapper.updateStatusById(Task_Status.PART_COMPLETED.getCode(), id, null);
        } else {
            return;
        }
        TaskRefund taskRefund = new TaskRefund();
        taskRefund.setTaskId(taskId);
        taskRefund.setStatus("0");
        taskRefund.setStatusCode(String.valueOf(result.getData().getStatusCode()));
        taskRefund.setStatusDesc(result.getData().getStatusDesc());
        taskRefundMapper.insert(taskRefund);
    }

    /**
     * @param task 任务信息
     * @param user 登录人
     * @return {@link R}
     */
    @Override
    public R createTask(LlTask task, SysUser user) {
        task.setTarget(task.getTarget().replaceAll(" ", ""));
        // 参数校验
        Task_Add_Type type = TaskTypeTools.getType(task.getType());
        if (type.equals(Task_Add_Type.NULL)) {
            return Rx.fail("任务类型不正确");
        }
        // 查询任务权限
        boolean hasTaskPermission = taskPermissionMerchantMapper
                .selectByTaskCodeAndMerchantId(TaskTypeTools.getTypePrefix(type), user.getMerchantId());
        if (!hasTaskPermission) {
            return Rx.fail("没有权限创建此类型任务");
        }
        R res = LlTaskUtils.checkTaskParam(task);
        if (Rx.isSuccess(res)) {
            resolveTaskCreateInit(task, user);
            return Rx.success(task);
        } else {
            return res;
        }
    }

    private void resolveTaskCreateInit(LlTask task, SysUser user) {
        String[] keys = task.getKeywords().split("[,，;；]");
        // 生成 taskId
        task.setTaskId(LlUtils.generateTaskId());
        task.setUserId(user.getId());
        task.setStatus(Task_Status.INIT.getCode());
        task.setCreateTime(System.currentTimeMillis());
        task.setIsDel(0);
        task.setCompletedCount(0);
        task.setCount(task.getCount());
        task.setCostPoints(calculatorUserCostPoints(task, user));
        llTaskMapper.insert(task);
        // 判断是否有子任务（多个搜索词）
        if (keys.length > 1) {
            int index = 0;
            for (String key : keys) {
                if (StrUtil.isNotEmpty(key)) {
                    TaskItem item = new TaskItem();
                    item.setTaskId(task.getId());
                    item.setTaskItemId(task.getTaskId() + index++);
                    item.setKeyword(key);
                    item.setTotalCount(task.getCount() / keys.length);
                    item.setCompletedCount(0);
                    item.setStatus(Task_Status.INIT.getCode());
                    taskItemMapper.insert(item);
                }
            }
        }
        // 商品信息
    }

    private void resolveApiTaskCreateInit(LlTask task, SysUserApi user) {
        task.setUserId(user.getUserId());
        task.setStatus(Task_Status.ING.getCode());
        task.setCreateTime(System.currentTimeMillis());
        task.setIsDel(0);
        task.setCompletedCount(0);
        llTaskMapper.insert(task);
    }

    /**
     * 计算分站浏览时长积分消耗
     *
     * @param browserTime 浏览时长
     * @return 积分消耗
     */
    @Deprecated
    private int calculatorBrowserTimeCost(Integer browserTime, List<TaskPriceBrowserTime> list) {
        if (list.isEmpty()) {
            return 0;
        }
        int cost = 0;
        for (TaskPriceBrowserTime tbt : list) {
            if (tbt.getEnd() <= browserTime) {
                cost += (tbt.getEnd() - tbt.getStart()) / 10 * tbt.getPriceUnit();
            }
            if (tbt.getEnd() > browserTime) {
                cost += (browserTime - tbt.getStart()) / 10 * tbt.getPriceUnit();
            }
        }
        return cost;
    }

    private Integer calculatorMerchantCostPoints(LlTask task) {
        int btCost = 0;
        int depthCost = 0;
        // 分站商户、API商户消耗积分公式 = 基础消耗积分+浏览时长增加对应积分+浏览深度增加对应积分
        int basicPrice;
        try {
            Map<Integer, TaskPrice> map = taskPriceMapper.selectAllByIsMaster();
            basicPrice = map.get(task.getType()).getPrice();
            if (TaskTypeTools.needDeptBrowserTime(task.getType())) {
//                List<TaskPriceBrowserTime> list = taskPriceBrowserTimeMapper.selectByBrowserTimeForMaster(Integer.valueOf(task.getBrowseTime()));
//                btCost = calculatorBrowserTimeCost(Integer.valueOf(task.getBrowseTime()), list);
                int browserTime = Integer.parseInt(task.getBrowseTime());
                if (browserTime > 90) {
                    TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectMaster();
                    btCost = taskPriceBtn.getPrice() * ((browserTime - 90) / 10);
                }
                // 浏览深度积分计算
                if (task.getDepth() > 0) {
                    TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectMasterByType(task.getDepth());
                    depthCost = taskPriceDepth.getPrice();
                }
            }
            log.info("分站任务积分消耗计算结果，基础消耗{}；浏览时长消耗{}；浏览深度消耗{}", basicPrice, btCost, depthCost);
            return (basicPrice + btCost + depthCost) * task.getCount();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            DentalCabotClient.sendText("计算分站积分消耗异常：" + e.getLocalizedMessage());
        }
        return 0;
    }

    /**
     * 计算分站用户积分消耗
     *
     * @param task 任务
     * @param user 任务创建人
     * @return 积分消耗
     */
    private Integer calculatorUserCostPoints(LlTask task, SysUser user) {
        int browserTime = 180;
        try {
            browserTime = Integer.parseInt(task.getBrowseTime());
        } catch (NumberFormatException e) {
            log.error(e.getLocalizedMessage() + "BrowserTime : " + task.getBrowseTime());
        }
        // 分站用户消耗积分公式 = （基础消耗积分+浏览时长增加对应积分+浏览深度增加对应积分）*用户等级折扣
        Integer basicCost = taskPriceMapper.selectPriceByTypeAndMerchantId(task.getType(), user.getMerchantId());
        if (basicCost == null) {
            basicCost = taskPriceMapper.selectPriceByTypeAndMerchantId(task.getType(), null);
            if (basicCost == null) {
                log.error("任务类型{}未配置积分消耗价格", task.getType());
                throw new RuntimeException("分站商户未配置积分消耗价格");
            }
        }
        int depthCost = 0;
        int btCost = 0;
        if (TaskTypeTools.needDeptBrowserTime(task.getType())) {
            // 浏览时长
//            List<TaskPriceBrowserTime> list = taskPriceBrowserTimeMapper.selectByBrowserTimeForMerchant(browserTime, null);
//            btCost = calculatorBrowserTimeCost(browserTime, list);
            if (browserTime > 90) {
                TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectOneByMerchantId(user.getMerchantId());
                if (taskPriceBtn == null) {
                    taskPriceBtn = taskPriceBtnMapper.selectDefault();
                }
                btCost = taskPriceBtn.getPrice() * ((browserTime - 90) / 10);
            }
            // 浏览深度积分计算
            if (task.getDepth() > 0) {
                TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectOneByMerchantId(user.getMerchantId(), task.getDepth());
                if (taskPriceDepth == null) {
                    taskPriceDepth = taskPriceDepthMapper.selectDefault(task.getDepth());
                }
                depthCost = taskPriceDepth.getPrice();
            }
        }
        // 查询会员等级 discount
        BigDecimal discount = BigDecimal.ONE;
        UserLevel userLevel = userLevelMapper.selectByPrimaryKey(user.getUserLevel());
        if (userLevel != null) {
            discount = userLevel.getDiscount();
        }
        BigDecimal result = discount.multiply(new BigDecimal(task.getCount()))
                .multiply(new BigDecimal(basicCost + depthCost + btCost));
        log.info("用户积分消耗计算结果，基础消耗{}；浏览时长消耗{}；浏览深度消耗{}；会员折扣{}", basicCost, btCost, depthCost, discount);
        return result.intValue();
    }

    @Override
    public R updateTask(LlTask task, SysUser user) {
        LlTask originTask = llTaskMapper.selectByPrimaryKey(task.getId());
        // 判断状态
        if (!originTask.getStatus().equals(Task_Status.INIT.getCode())) {
            return Rx.fail("任务状态无法修改");
        }
        R res = LlTaskUtils.checkTaskParam(task);
        if (!Rx.isSuccess(res)) {
            return res;
        }
        llTaskMapper.updateByPrimaryKeySelective(task);
        // 判断keywords 是否发生更改
        if (!originTask.getKeywords().equals(task.getKeywords())) {
            // 根据 task 主键删除原有子任务
            taskItemMapper.deleteByTaskId(task.getId());
            // 重新添加子任务
            // 判断是否有子任务（多个搜索词）
            String split = "[,，;；]";
            String[] keys = task.getKeywords().split(split);
            if (keys.length > 1) {
                int index = 0;
                for (String key : keys) {
                    if (StrUtil.isNotEmpty(key)) {
                        TaskItem item = new TaskItem();
                        item.setTaskId(task.getId());
                        item.setTaskItemId(task.getTaskId() + index++);
                        item.setKeyword(key);
                        taskItemMapper.insert(item);
                    }
                }
            }
        }
        return Rx.success(llTaskMapper.selectByPrimaryKey(task.getId()));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public R publishTask(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null || !task.getStatus().equals(Task_Status.INIT.getCode())) {
            return Rx.fail("任务不存在或者任务状态无法发布");
        }
        SysUser taskUser = userMapper.selectByPrimaryKey(task.getUserId());
        // 计算积分消耗，扣减积分
        int uc = calculatorUserCostPoints(task, taskUser);
        BalanceUpdateParam param = BalanceUpdateParam.builder()
                .businessId(task.getUserId()).userTypeEnum(UserTypeEnum.MERCHANT_USER_TYPE)
                .createBy(task.getUserId()).taskId(task.getTaskId())
                .amount(-uc).recordType(Points_Record_Type.TASK_CONSUME)
                .build();
        R<Balance> res = balanceService.updateBalance(param);
        if (!Rx.isSuccess(res)) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return res;
        }
        int mc = calculatorMerchantCostPoints(task);
        BalanceUpdateParam param2 = BalanceUpdateParam.builder()
                .businessId(taskUser.getMerchantId()).userTypeEnum(UserTypeEnum.MERCHANT_TYPE)
                .createBy(task.getUserId()).taskId(task.getTaskId())
                .amount(-mc).recordType(Points_Record_Type.TASK_CONSUME)
                .build();
        res = balanceService.updateBalance(param2);
        if (!Rx.isSuccess(res)) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Rx.fail("扣减商户积分余额异常：" + res.getMsg());
        }
        List<TaskItem> taskItemList = taskItemMapper.selectByTaskId(id);
        if (taskItemList.size() > 0) {
            for (TaskItem item : taskItemList) {
                task.setTaskId(item.getTaskItemId());
                task.setKeywords(item.getKeyword());
                task.setCount(item.getTotalCount());
                LlTaskAddResult result = LlTaskUtils.create(task, tbShopMapper);
                if (!result.isSuccess()) {
                    log.error("任务{}发布失败：返回结果{}", id, result.toString());
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return Rx.fail(result.getTips());
                }
            }
            // 更新子任务状态
            taskItemMapper.updateStatusByTaskId(Task_Status.ING.getCode(), id);
        } else {
            LlTaskAddResult result = LlTaskUtils.create(task, tbShopMapper);
            if (!result.isSuccess()) {
                log.error("任务{}发布失败：返回结果{}", id, result.toString());
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return Rx.fail(result.getTips());
            }
        }
        // 更新任务状态
        int x = llTaskMapper.updateStatusById(Task_Status.ING.getCode(), id, System.currentTimeMillis());
        if (x == 1) {
            return Rx.success("任务发布成功");
        } else {
            log.error("任务{}发布成功，更新任务状态失败", id);
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Rx.fail("任务发布成功，更新任务状态失败");
        }
    }

    @Override
    public TaskDetail getTaskDetail(Long id, SysUser currentUser) {
        TaskDetail detail = new TaskDetail();
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return null;
        }
        if (CommonUtils.isMerchantUserType(currentUser) && !task.getUserId().equals(currentUser.getId())) {
            return null;
        }
        List<TaskItem> itemList = taskItemMapper.selectByTaskId(id);
        if (!itemList.isEmpty()) {
            detail.setItemList(itemList);
        }
        detail.setTask(task);
        // 如果是淘宝任务
        if (TaskTypeTools.isTbTask(TaskTypeTools.getType(task.getType()))) {
            detail.setTaskTbItem(taskTbItemMapper.selectByPrimaryKey(task.getId()));
        }
        return detail;
    }

    @Override
    public R queryMerchantCostPoints(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task != null) {
            int mc = calculatorMerchantCostPoints(task);
            int uc = calculatorUserCostPoints(task, userMapper.selectByPrimaryKey(task.getUserId()));
            return Rx.success(uc);
        } else {
            return Rx.fail("任务不存在或已被删除");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public R apiCreateTask(TaskCreateParam task, SysUserApi userApi) {
        Long userId = userApi.getUserId();
        LlTask llTask = task.toLlTask(userId);
        R res = LlTaskUtils.checkTaskParam(llTask);
        if (Rx.isSuccess(res)) {
            // 发布任务
            llTask.setCostPoints(calculatorMerchantCostPoints(llTask));
            res = this.publishApiTask(llTask, userApi);
            if (Rx.isSuccess(res)) {
                this.resolveApiTaskCreateInit(llTask, userApi);
                return Rx.apiSuccess("操作成功", new ApiCreateResult(llTask.getCostPoints(),
                        balanceMapper.selectByBusinessIdAndType(userApi.getId(), UserTypeEnum.API_TYPE.getType()).getBalance()));
            }
        }
        return Rx.apiFail(res.getMsg());
    }

    @Override
    public R cancelApiTask(LlTask task) {
        LlTaskCancelResult result = LlTaskUtils.cancel(task.getTaskId());
        // 记录操作日志
        recordOperation(task.getId(), task.getUserId(), Task_Operate_Type.CANCEL, result);
        if (!result.isSuccess()) {
            log.error("任务 {} 取消失败： {}", task.getTaskId(), result.toString());
            return Rx.apiFail("任务申请取消失败：" + result.getTips());
        } else {
            int x = llTaskMapper.updateStatusById(Task_Status.CANCEL_ING.getCode(), task.getId(), null);
            if (x == 1) {
                return Rx.apiSuccess("任务取消成功", "");
            }
        }
        return Rx.apiFail("任务取消失败，请稍后再试！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R resolveRefund(TaskRefund refund) {
        // 查询
        LlTask task = llTaskMapper.selectOneByTaskId(refund.getTaskId());
        PointsRecord record = pointsRecordMapper.selectByTaskIdAndTypeNotMerchant(task.getTaskId(),
                Points_Record_Type.TASK_CONSUME.getCode());
        SysUser taskUser = userMapper.selectByPrimaryKey(Optional.of(task).map(LlTask::getUserId).get());
        if (taskUser == null || record == null) {
            return Rx.fail("查询记录异常或者查询用户信息异常");
        }
        BigDecimal percent;
        // 全额退款
        if (Task_Status.CANCELED.getCode().equals(task.getStatus())) {
            percent = BigDecimal.ONE;
        } else if (Task_Status.PART_COMPLETED.getCode().equals(task.getStatus())) {
            percent = new BigDecimal(task.getCount() - task.getCompletedCount())
                    .divide(new BigDecimal(task.getCount()), 4, BigDecimal.ROUND_HALF_DOWN);
        } else {
            return Rx.success();
        }
        UserTypeEnum userTypeEnum = UserTypeEnum.API_TYPE.getType() == record.getUserType() ?
                UserTypeEnum.API_TYPE : UserTypeEnum.MERCHANT_USER_TYPE;
        BalanceUpdateParam param = BalanceUpdateParam.builder()
                .amount(-percent.multiply(new BigDecimal(record.getAmount())).intValue()).createBy(task.getUserId())
                .recordType(Points_Record_Type.TASK_CANCEL).taskId(task.getTaskId())
                .businessId(record.getBusinessId()).userTypeEnum(userTypeEnum)
                .build();
        R<Balance> res = balanceService.updateBalance(param);
        if (Rx.isSuccess(res)) {
            // 商户退款
            if (userTypeEnum.equals(UserTypeEnum.MERCHANT_USER_TYPE)) {
                Integer amount = pointsRecordMapper.selectAmountByCondition(taskUser.getMerchantId(), UserTypeEnum.MERCHANT_TYPE.getType(),
                        task.getTaskId(), Points_Record_Type.TASK_CONSUME.getCode());
                BalanceUpdateParam param2 = BalanceUpdateParam.builder()
                        .businessId(taskUser.getMerchantId()).userTypeEnum(UserTypeEnum.MERCHANT_TYPE)
                        .createBy(task.getUserId()).taskId(task.getTaskId())
                        .amount(-percent.multiply(new BigDecimal(amount)).intValue()).recordType(Points_Record_Type.TASK_CANCEL)
                        .build();
                res = balanceService.updateBalance(param2);
            }
            if (Rx.isSuccess(res)) {
                // 标记退款状态处理完成
                int x = taskRefundMapper.updateStatusByRefundId("1", refund.getRefundId());
                if (x == 1) {
                    return Rx.success();
                }
            }
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return Rx.fail();
    }

    @Override
    public R deleteTask(Long id, SysUser user) {
        LlTask task = llTaskMapper.selectByPrimaryKey(id);
        if (task == null) {
            return Rx.fail("任务不存在或已被删除");
        }
        if (!task.getUserId().equals(user.getId())) {
            return Rx.permissionDenied();
        }
        if (!Task_Status.INIT.getCode().equals(task.getStatus())) {
            return Rx.fail("任务状态不是未发布，无法删除");
        }
        if (llTaskMapper.deleteByPrimaryKey(id) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public Integer queryTaskCount(String status, SysUser currentUser, String type) {
        List<Integer> list = new ArrayList<>();
        if (StrUtil.isNotEmpty(type) && type.length() > 1) {
            for (Task_Add_Type addType : Task_Add_Type.values()) {
                if (addType.name().startsWith(type)) {
                    list.add(addType.getType());
                }
            }
        }
        return llTaskMapper.countByStatusAndUserId(status, currentUser.getId(), list);
    }

    @Override
    public Map<String, Integer> queryAllTaskCount(SysUser currentUser) {
        Map<String, Integer> resMap = new HashMap<>();
        resMap.put("tb1", queryTaskCount(Task_Status.ING.getCode(), currentUser, "TB"));
        resMap.put("tb2", queryTaskCount(Task_Status.COMPLETED.getCode(), currentUser, "TB"));
        resMap.put("jd1", queryTaskCount(Task_Status.ING.getCode(), currentUser, "JD"));
        resMap.put("jd2", queryTaskCount(Task_Status.COMPLETED.getCode(), currentUser, "JD"));
        resMap.put("pdd1", queryTaskCount(Task_Status.ING.getCode(), currentUser, "PDD"));
        resMap.put("pdd2", queryTaskCount(Task_Status.COMPLETED.getCode(), currentUser, "PDD"));
        resMap.put("dy1", queryTaskCount(Task_Status.ING.getCode(), currentUser, "DY"));
        resMap.put("dy2", queryTaskCount(Task_Status.COMPLETED.getCode(), currentUser, "DY"));
        return resMap;
    }

    @Transactional(rollbackFor = Exception.class)
    R publishApiTask(LlTask task, SysUserApi userApi) {
        Long userApiId = userApi.getId();
        int cost = task.getCostPoints();

        BalanceUpdateParam param = BalanceUpdateParam.builder()
                .businessId(userApiId).userTypeEnum(UserTypeEnum.API_TYPE)
                .createBy(task.getUserId()).taskId(task.getTaskId())
                .amount(-cost).recordType(Points_Record_Type.TASK_CONSUME)
                .build();
        R<Balance> res = balanceService.updateBalance(param);
        if (!Rx.isSuccess(res)) {
            return res;
        }
        // 发布任务，失败回滚
        LlTaskAddResult result = LlTaskUtils.create(task, tbShopMapper);
        if (!result.isSuccess()) {
            log.error("Api任务{}发布失败：返回结果{}", task.getTaskId(), result.toString());
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Rx.fail(result.getTips());
        }
        return Rx.success();
    }

}
