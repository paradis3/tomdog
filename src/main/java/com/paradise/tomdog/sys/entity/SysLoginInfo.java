package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * 用户登录日志表
 *
 * @author Paradise
 */
@Data
@Builder
public class SysLoginInfo {
    /**
     * 访问ID
     */
    @ApiModelProperty(value = "访问ID")
    private Long infoId;
    
    /**
     * 用户账号
     */
    @ApiModelProperty(value = "用户账号")
    private String userName;
    
    /**
     * 域名
     */
    @ApiModelProperty(value = "域名")
    private String domain;
    
    /**
     * 登录IP地址
     */
    @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;
    
    /**
     * 登录地点
     */
    @ApiModelProperty(value = "登录地点")
    private String loginLocation;
    
    /**
     * 浏览器类型
     */
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    
    /**
     * 操作系统
     */
    @ApiModelProperty(value = "操作系统")
    private String os;
    
    /**
     * 登录状态（0成功 1失败）
     */
    @ApiModelProperty(value = "登录状态（0成功 1失败）")
    private String status;
    
    /**
     * 提示消息
     */
    @ApiModelProperty(value = "提示消息")
    private String msg;

    /**
     * 访问时间
     */
    @ApiModelProperty(value = "访问时间")
    private Date loginTime;

    @ApiModelProperty(value = "用户id")
    private Long uid;

    public static com.paradise.tomdog.sys.entity.SysLoginInfo.SysLoginInfoBuilder builder() {
        return new com.paradise.tomdog.sys.entity.SysLoginInfo.SysLoginInfoBuilder();
    }
}