package com.paradise.tomdog.sys.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.paradise.tomdog.sys.entity.SysRolePermission;
import com.paradise.tomdog.sys.mapper.SysRolePermissionMapper;
import com.paradise.tomdog.sys.service.SysRolePermissionService;

import java.util.List;

@Service
public class SysRolePermissionServiceImpl implements SysRolePermissionService{

    @Resource
    private SysRolePermissionMapper sysRolePermissionMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysRolePermissionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SysRolePermission record) {
        return sysRolePermissionMapper.insert(record);
    }

    @Override
    public int insertSelective(SysRolePermission record) {
        return sysRolePermissionMapper.insertSelective(record);
    }

    @Override
    public SysRolePermission selectByPrimaryKey(Long id) {
        return sysRolePermissionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SysRolePermission record) {
        return sysRolePermissionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysRolePermission record) {
        return sysRolePermissionMapper.updateByPrimaryKey(record);
    }
    @Override
    public int insertList(List<SysRolePermission> list) {
        return sysRolePermissionMapper.insertList(list);
    }
}
