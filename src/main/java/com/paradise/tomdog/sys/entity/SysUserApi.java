package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.SysUserApi")
@Data
public class SysUserApi {
    /**
     * api 商户主键
     */
    @ApiModelProperty(value = "api 商户主键", example = "201")
    private Long id;

    /**
     * 关联账号id
     */
    @ApiModelProperty(value = "关联账号id", hidden = true)
    private Long userId;

    /**
     * APP_ID
     */
    @ApiModelProperty(value = "APP_ID", hidden = true)
    private String appId;

    /**
     * APP_KEY
     */
    @ApiModelProperty(value = "APP_KEY", hidden = true)
    private String appKey;

    @ApiModelProperty(value = "APP_SECRET", hidden = true)
    private String appSecret;

    @NotBlank(message = "API商户手机号码不能为空")
    @Length(min = 11, max = 11, message = "商户手机号码为11位数字")
    @ApiModelProperty(value = "API商户手机号码", example = "15512345678")
    private String telephone;

    @NotBlank
    @Length(message = "分账商户账号长度3-20", min = 3, max = 20)
    @ApiModelProperty(value = "API商户USERNAME", example = "USER_NAME")
    private String username;

    /**
     * 申请时间
     */
    @ApiModelProperty(value = "申请时间", hidden = true)
    private Date applyTime;

    /**
     * 余额
     */
    @ApiModelProperty(value = "余额", hidden = true)
    private BigDecimal balance;

    /**
     * 积分余额
     */
    @ApiModelProperty(value = "积分余额", hidden = true)
    private Integer points;

    /**
     * 备注
     */
    @Length(max = 255, message = "备注最长为255个字符")
    @ApiModelProperty(value = "备注", example = "api 商户 备注信息~")
    private String remark;

    @ApiModelProperty(hidden = true)
    private Long createBy;

    @ApiModelProperty(hidden = true)
    private Long updateBy;

    @ApiModelProperty(hidden = true)
    private Date createTime;

    @ApiModelProperty(hidden = true)
    private Date updateTime;

    @ApiModelProperty(hidden = true)
    private Integer isDel;

    @ApiModelProperty(hidden = true)
    private String status;

    public interface Add {

    }

    public interface Edit {

    }
}