package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.cwmutils.CmwAPI;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.base.utils.IdWorkByTwitter;
import com.paradise.tomdog.sys.constant.IsDel;
import com.paradise.tomdog.sys.constant.Recharge_Status;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.BranchRechargeRecordService;
import com.paradise.tomdog.sys.service.SysUserPayInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * @author Paradise
 */
@Service
@Slf4j
public class BranchRechargeRecordServiceImpl implements BranchRechargeRecordService {

    @Resource
    private BranchRechargeRecordMapper branchRechargeRecordMapper;
    @Resource
    private UserLevelMapper userLevelMapper;
    @Resource
    private BranchRechargePackageMapper branchRechargePackageMapper;
    @Resource
    private SysUserMapper userMapper;
    @Resource
    private TbsBalanceMapper tbsBalanceMapper;
    @Resource
    private TbsBalanceRecordMapper tbsBalanceRecordMapper;
    @Autowired
    private IdWorkByTwitter idWorkByTwitter;
    @Autowired
    private SysUserPayInfoService sysUserPayInfoService;
    @Resource
    private SysUserPayInfoMapper sysUserPayInfoMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Resource
    private UserExpandMapper userExpandMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return branchRechargeRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(BranchRechargeRecord record) {
        return branchRechargeRecordMapper.insertSelective(record);
    }

    @Override
    public BranchRechargeRecord selectByPrimaryKey(Long id) {
        return branchRechargeRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BranchRechargeRecord record) {
        return branchRechargeRecordMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<BranchRechargeRecord> selectByAll(BranchRechargeRecord branchRechargeRecord) {
        return branchRechargeRecordMapper.selectByAll(branchRechargeRecord);
    }

    @Override
    public R createOrder(Long packageId, SysUser user, String payType) {
        BranchRechargePackage rechargePackage = branchRechargePackageMapper.selectByPrimaryKey(packageId);
        if (rechargePackage == null
                || StringUtils.equals(rechargePackage.getIsDel(), "1")
                || StringUtils.equals(rechargePackage.getStatus(), "0")
                || !user.getMerchantId().equals(rechargePackage.getMerchantId())) {
            return Rx.fail("套餐不存在或已被删除");
        }
        BranchRechargeRecord record = new BranchRechargeRecord();
        record.setUserId(user.getId());
        record.setRechargePoints(rechargePackage.getPoints() + rechargePackage.getGivePoints());
        record.setPayAmount(rechargePackage.getPrice());
        record.setPayType(payType);
        record.setOrderNo(String.valueOf(idWorkByTwitter.nextId()));
        record.setPackageId(packageId);
        record.setMerchantId(rechargePackage.getMerchantId());
        record.setCreateTime(new Date());
        record.setCreateBy(user.getId());
        record.setUpdateTime(new Date());
        record.setUpdateBy(user.getId());
        record.setStatus(Recharge_Status.ORDER_CREATE.getCode());
        record.setIsDel(IsDel.NO.getCode());
        int num = branchRechargeRecordMapper.insertSelective(record);
        if (num == 1) {
            try {
                //查询用户商户付款信息
                SysUserPayInfo sysUserPayInfo = sysUserPayInfoMapper.selectOneByMerchantId(user.getMerchantId());
                if (sysUserPayInfo == null) {
                    return Rx.fail("商户首付款信息未设置");
                }
                JSONObject object = JSON.parseObject(CmwAPI.orderCreate(record, sysUserPayInfo));
                if (object != null && StringUtils.equals(object.getString("status"), "1")) {
                    String payNo = object.getJSONObject("data").getString("id");
//                    record.setPayNo(payNo);
                    record.setStatus(Recharge_Status.UN_PAYED.getCode());
                    branchRechargeRecordMapper.updateByPrimaryKey(record);
                    String payUrl = sysUserPayInfoService.getPayUrl(user, payNo, sysUserPayInfo);
                    return Rx.success(payUrl);
                } else {
                    record.setStatus(Recharge_Status.PAY_FAIL.getCode());
                    branchRechargeRecordMapper.updateByPrimaryKey(record);
                    return Rx.fail("订单创建失败.");
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                record.setStatus(Recharge_Status.PAY_FAIL.getCode());
                branchRechargeRecordMapper.updateByPrimaryKey(record);
            }
        }
        return Rx.fail("订单创建失败");
    }

    /**
     * 手动补单
     *
     * @param recordId
     * @param user
     * @param status
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R replaceOrder(Long recordId, SysUser user, String status) {
        BranchRechargeRecord branchRechargeRecord = branchRechargeRecordMapper.selectByPrimaryKey(recordId);
        if (branchRechargeRecord == null
                || StringUtils.equals(branchRechargeRecord.getIsDel(), "1")
                || StringUtils.equals(branchRechargeRecord.getStatus(), "2")) {
            return Rx.fail("订单不存在或充值成功");
        }
        if (StringUtils.equals("2", status)) {
            //1重新查询财务 2 直接置为成功
            branchRechargeRecord.setUpdateBy(1L);
            branchRechargeRecord.setStatus("1");
            branchRechargeRecord.setUpdateTime(new Date());
            branchRechargeRecordMapper.updateByPrimaryKey(branchRechargeRecord);
            //向kafka发送充值成功消息
            HashMap<String, String> paramMap = new HashMap<>();
            //商户订单号
            paramMap.put("orderid", branchRechargeRecord.getOrderNo());
            //1：收款成功；0：待支付；-1：二维码生成中；11：二维码生成失败；12：订单超时未支付'
            paramMap.put("status", "1");
            //支付金额 单位：元。精确小数点后2位
            paramMap.put("facevalue", branchRechargeRecord.getPayAmount().toString());
            //财务猫平台生成的唯一支付流水订单号
            paramMap.put("id", "123");
            //时间戳
            paramMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
            //扩展字段代表使用商户ID
            paramMap.put("extends_param", branchRechargeRecord.getMerchantId().toString());
            kafkaTemplate.send("cwmCallBack", JSON.toJSONString(paramMap));
        } else if (StringUtils.equals("1", status)) {
            //1重新查询财务 2 直接置为成功
            branchRechargeRecord.setUpdateBy(1L);
            branchRechargeRecord.setStatus("1");
            branchRechargeRecordMapper.updateByPrimaryKey(branchRechargeRecord);
        }
        return Rx.success("设置成功");
    }

    /**
     * 处理上级推广人推广积分逻辑
     * + 增加处理拓展信息：
     * + 如果是第一次充值，上级推广人+1
     * + 增加上级的充值金额，累计充值积分
     *
     * @param record 充值订单记录
     * @param user   当前充值用户
     */
    @Override
    public void userSpreadPointsHandler(BranchRechargeRecord record, SysUser user) {
        log.info("推广用户积分返利处理逻辑---开始");
        log.info("充值信息{}", record.toString());
        BigDecimal points = BigDecimal.ZERO;
        if (user.getSuperiorId() != null) {
            // 查询上级推广人
            SysUser superiorUser = userMapper.selectByPrimaryKey(user.getSuperiorId());
            if (superiorUser != null) {
                // 查询上级推广人等级信息
                UserLevel superiorUserLevel = userLevelMapper.selectByPrimaryKey(superiorUser.getUserLevel());
                // 推广返比大于0
                if (superiorUserLevel != null && superiorUserLevel.getPercent().compareTo(BigDecimal.ZERO) > 0) {
                    points = superiorUserLevel.getPercent().multiply(new BigDecimal(record.getRechargePoints()));
                    // 推广积分入账记录
                    TbsBalanceRecord tbsBalanceRecord = new TbsBalanceRecord(superiorUser.getId(), points.intValue(), "1");
                    tbsBalanceRecord.setIsDel("0");
                    tbsBalanceRecord.setSourceId(record.getId());
                    tbsBalanceRecord.setCreateTime(new Date());
                    tbsBalanceRecord.setCreateBy(user.getId());
                    tbsBalanceRecordMapper.insert(tbsBalanceRecord);
                    if (tbsBalanceRecord.getId() != null) {
                        // 修改用户 推广积分余额
                        TbsBalance tbsBalance = tbsBalanceMapper.selectByPrimaryKey(superiorUser.getId());
                        log.info("推广人当前推广积分余额信息{}", tbsBalance);
                        if (tbsBalance == null) {
                            tbsBalance = new TbsBalance(superiorUser.getId(), points.intValue());
                            tbsBalance.setVersion(1);
                            tbsBalanceMapper.insert(tbsBalance);
                        } else {
                            tbsBalanceMapper.updateSpreadBalanceByUserId(points.intValue(),
                                    tbsBalance.getVersion(), tbsBalance.getUserId());
                        }
                        log.info("推广人{}最新积分余额信息{}", tbsBalance.getUserId(), tbsBalance.getSpreadBalance());
                    }
                }
            }
        }
        dealUserExpandUpdate(user, record, points);
        log.info("推广用户积分返利处理逻辑---结束");
    }

    /**
     * 处理 用户充值推广信息扩展
     *
     * @param user   充值用户信息
     * @param record 充值记录
     * @param points 推广积分
     */
    private void dealUserExpandUpdate(SysUser user, BranchRechargeRecord record, BigDecimal points) {
        // 判断用户是否第一次充值
        log.info(">>> 进入用户充值推广积分拓展逻辑");
        try {
            // 更新上级扩展信息
            if (user.getSuperiorId() != null) {
                UserExpand userExpand = userExpandMapper.selectByPrimaryKey(user.getSuperiorId());
                if (userExpand == null) {
                    userExpand = new UserExpand(user.getSuperiorId());
                    userExpandMapper.insert(userExpand);
                }
                if (branchRechargeRecordMapper.countByUserId(user.getId()) == 1) {
                    userExpand.setRechargeCount(userExpand.getRechargeCount() + 1);
                }
                userExpand.setRechargeAmount(record.getPayAmount().add(userExpand.getRechargeAmount()));
                userExpand.setSpreadPoints(points.add(new BigDecimal(userExpand.getSpreadPoints())).intValue());
                userExpandMapper.updateByPrimaryKeySelective(userExpand);
            }

            // 更新用户自己的充值信息，推广积分信息
            log.info("开始更新用户自己的拓展信息：>>>");
            UserExpand selfUserExpand = userExpandMapper.selectByPrimaryKey(user.getId());
            if (selfUserExpand == null) {
                selfUserExpand = new UserExpand(user.getId());
                userExpandMapper.insert(selfUserExpand);
            }
            selfUserExpand.setSelfRechargeAmount(record.getPayAmount().add(selfUserExpand.getSelfRechargeAmount()));
            selfUserExpand.setParentSpreadPoints(selfUserExpand.getParentSpreadPoints() + points.intValue());
            userExpandMapper.updateByPrimaryKeySelective(selfUserExpand);
            log.info("个人累计充值积分，给上级带来的推广积分:{}", selfUserExpand);
            log.info(" 用户充值推广积分拓展逻辑完成 <<<");
        } catch (RuntimeException e) {
            log.error(e.getLocalizedMessage(), e);
            DentalCabotClient.sendText("处理充值推广扩展信息异常");
        }
    }

    /**
     * 用户升级等级处理逻辑
     *
     * @param record 充值订单
     * @param user   充值用户
     */
    @Override
    public void userLevelHandler(BranchRechargeRecord record, SysUser user) {
        // 判断当前等级，以及是否能够直接升级 -> 是否有可供升级的等级 -> 如果有，选择可以升级的最高level 的等级；
        log.info("进入用户充值升级判断逻辑：用户ID：{}；充值金额：{}", user.getId(), record.getPayAmount());
        UserLevel currentLevel = userLevelMapper.selectByPrimaryKey(user.getUserLevel());
        // 查询累计充值金额，并根据累计充值金额判断能否升级
        BigDecimal rechargeAmount = record.getPayAmount();
        // 需求变更：不需要累计充值 2020-3-28
       /* try {
            UserExpand userExpand = userExpandMapper.selectByPrimaryKey(user.getId());
            if (userExpand != null) {
                rechargeAmount = rechargeAmount.add(userExpand.getRechargeAmount());
            }
        } catch (RuntimeException e) {
            log.error(e.getLocalizedMessage(), e);
            DentalCabotClient.sendText(e.getLocalizedMessage());
        }*/

        UserLevel canUpdateLevel = userLevelMapper.selectCanUpdateLevel(user.getMerchantId(), rechargeAmount, currentLevel.getLevel());
        if (canUpdateLevel != null) {
            int x = userMapper.updateUserLevelById(canUpdateLevel.getId(), user.getId());
            if (x == 1) {
                log.info("用户通过直接充值升级成功，当前等级为：{}", canUpdateLevel.getName());
            }
        } else {
            log.info("此次充值无升级");
        }
    }

    @Override
    public PageData getRechargePage(BranchRechargeRecordQueryBean queryBean, SysUser user) {
        PageHelper.startPage(queryBean.getPageNum(), queryBean.getPageSize());
        if (CommonUtils.isMerchantType(user)) {
            queryBean.setMerchantId(user.getMerchantId());
        } else if (CommonUtils.isMerchantUserType(user)) {
            queryBean.setUserId(user.getId());
        } else if (!CommonUtils.isAdminType(user)) {
            throw new RuntimeException("非法访问");
        }
        List<BranchRechargeRecordWithSysUserWithSysBranchMerchant> rechargeRecords
                = branchRechargeRecordMapper.selectAllByQueryBean(queryBean);
        return new PageData(new PageInfo<>(rechargeRecords));
    }

    @Override
    public int insert(BranchRechargeRecord record) {
        return branchRechargeRecordMapper.insert(record);
    }

    @Override
    public int updateByPrimaryKey(BranchRechargeRecord record) {
        return branchRechargeRecordMapper.updateByPrimaryKey(record);
    }

    /**
     * 查找待支付的订单
     *
     * @param minUpdateTime
     * @return
     */
    @Override
    public List<BranchRechargeRecord> selectAllByUpdateTimeBefore(Date minUpdateTime) {
        return branchRechargeRecordMapper.selectAllByUpdateTimeBefore(minUpdateTime);
    }

    /**
     * 导出充值订单
     */
    @Override
    public void exportMasterRechargeRecord(BranchRechargeRecordQueryBean queryBean, SysUser user, HttpServletRequest request,
                                           HttpServletResponse response) {
        if (CommonUtils.isMerchantType(user) || CommonUtils.isApiType(user)) {
            queryBean.setMerchantId(user.getMerchantId());
        } else if (CommonUtils.isApiType(user)) {
            queryBean.setUserId(user.getId());
        } else if (!CommonUtils.isAdminType(user)) {
            throw new RuntimeException("非法访问");
        }
        // 根据条件查询分站商户充值记录
        List<BranchRechargeRecordWithSysUserWithSysBranchMerchant> list
                = branchRechargeRecordMapper.selectAllByQueryBean(queryBean);
        String[] rowNames = new String[]{"序号", "用户名", "商户名", "支付金额", "充值积分", "订单号", "支付流水号", "支付状态",
                "创建时间", "支付时间",};
        String title = "充值记录数据列表";
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .request(request).response(response).title(title)
                .rowName(rowNames)
                .dataList(getDataList(list))
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 解析Excel需要的数据列表
     *
     * @param list 记录数据
     */
    private List<Object[]> getDataList(List<BranchRechargeRecordWithSysUserWithSysBranchMerchant> list) {
        List<Object[]> resultList = new ArrayList<>();
        int index = 1;
        for (BranchRechargeRecordWithSysUserWithSysBranchMerchant record : list) {
            Object[] arr = new Object[11];
            arr[0] = index++;
            arr[1] = record.getSysUser().getUsername();
            arr[2] = record.getSysBranchMerchant().getName();
            arr[3] = record.getPayAmount();
            arr[4] = record.getRechargePoints();
            arr[5] = record.getOrderNo();
            arr[6] = record.getPayNo();
            arr[7] = Recharge_Status.getNameBycode(record.getStatus());
            arr[8] = DateFormatUtils.format(record.getCreateTime());
            arr[9] = DateFormatUtils.format(record.getPayTime());
            arr[10] = record.getRemark();
            resultList.add(arr);
        }
        return resultList;
    }
}