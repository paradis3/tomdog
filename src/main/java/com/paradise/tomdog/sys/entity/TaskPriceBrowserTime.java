package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "任务阶梯价格配置")
@Data
public class TaskPriceBrowserTime {
    @ApiModelProperty(value = "null")
    private Long id;

    /**
     * 序列
     */
    @ApiModelProperty(value = "序列")
    private Integer index;

    /**
     * 区间开始
     */
    @ApiModelProperty(value = "区间开始")
    private Integer start;

    /**
     * 区间结束
     */
    @ApiModelProperty(value = "区间结束")
    private Integer end;

    /**
     * 价格/每10S
     */
    @ApiModelProperty(value = "价格/每10S")
    private Integer priceUnit;

    @ApiModelProperty(value = "null")
    private Integer isMaster;

    @ApiModelProperty(value = "null")
    private Integer isDefault;

    @ApiModelProperty(value = "null")
    private Long merchantId;
}