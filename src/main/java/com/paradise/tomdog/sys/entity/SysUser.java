package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * 系统用户表
 *
 * @author Paradise
 */
@ApiModel(value = "系统用户")
@Data
public class SysUser {

    public interface Add {

    }

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", example = "1", hidden = true)
    private Long id;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id", example = "1", hidden = true)
    private Long roleId;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @Length(max = 20, min = 5, message = "用户名长度在5-20个字符之间")
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    @NotEmpty(message = "密码不能为空", groups = Add.class)
    @Size(max = 20, min = 8, message = "密码长度在8-20位之间", groups = Add.class)
    private String password;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    @NotBlank(message = "手机号码不能为空")
    private String telephone;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名")
    private String realName;

    /**
     * 用户积分
     */
    @ApiModelProperty(value = "用户积分", example = "0", hidden = true)
    private Integer userPoints;

    /**
     * 用户等级
     */
    @ApiModelProperty(value = "用户等级", hidden = true)
    private Integer userLevel;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", hidden = true)
    private String status;

    /**
     * 上级推广人id
     */
    @ApiModelProperty(value = "上级推广人id", example = "1", hidden = true)
    private Long superiorId;

    /**
     * 注册时间
     */
    @ApiModelProperty(value = "注册时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registrationTime;

    /**
     * 注册ip
     */
    @ApiModelProperty(value = "注册ip", hidden = true)
    private String registrationIp;

    /**
     * 注册来源
     */
    @ApiModelProperty(value = "注册来源", hidden = true)
    private String registrationSource;

    @ApiModelProperty(value = "余额", example = "0", hidden = true)
    private transient Long balance;

    @ApiModelProperty(value = "QQ")
    private String qq;

    @ApiModelProperty(value = "wechat")
    private String wechat;

    @ApiModelProperty(value = "旺旺")
    private String wangwang;

    @ApiModelProperty(value = "邮箱")
    @Email
    private String email;

    /**
     * 性别 1 男 0 女
     */
    @ApiModelProperty(value = "性别 1 男 0 女", example = "1")
    private String sex;

    @ApiModelProperty(value = "所属分站id", example = "1")
    private Long merchantId;

    @ApiModelProperty(hidden = true)
    private Long creator;

    @ApiModelProperty(hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(hidden = true)
    private Long updater;

    @ApiModelProperty(hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(hidden = true)
    private Integer isDel;

    @ApiModelProperty(hidden = true)
    private Integer invitationCode;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "用户类型：0 超管 1 总站 2 分站 3 分站商户 4 API商户")
    private Integer userType;
}