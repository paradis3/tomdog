package com.paradise.tomdog.base.component;

import com.paradise.tomdog.sys.entity.SysUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * 〈一句话功能简述〉<br>
 * 〈mytoken〉
 *
 * @author Administrator
 * @date 2019/12/27/027
 * @since 1.0.0
 */
public class MyToken extends UsernamePasswordAuthenticationToken {


    private Long userId;

    private SysUser user;

    public MyToken(Object principal, Object credentials, SysUser user) {
        super(principal, credentials);
        setUserId(user.getId());
        setUser(user);
    }

    public MyToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, SysUser user) {
        super(principal, credentials, authorities);
        setUserId(user.getId());
        setUser(user);
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public SysUser getUser() {
        return user;
    }
}
