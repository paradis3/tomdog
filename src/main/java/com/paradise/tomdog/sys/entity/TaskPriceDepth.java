package com.paradise.tomdog.sys.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
public class TaskPriceDepth {
    /**
     * 自增主键
     */
    private Long id;

    /**
     * 分站商户id，总站为null
     */
    private Long merchantId;

    /**
     * 1：固定浏览；2：随机浏览
     */
    private Integer type;

    /**
     * 价格
     */
    private Integer price;

    /**
     * 是否默认 1：是； 0：否
     */
    private Integer isDefault;

    private Date updateTime;

    /**
     * 是否总站 1是 0否
     */
    private Integer isMaster;

    public TaskPriceDepth(Long merchantId, Integer type, Integer price, Integer isDefault, Integer isMaster) {
        this.merchantId = merchantId;
        this.type = type;
        this.price = price;
        this.isDefault = isDefault;
        this.isMaster = isMaster;
    }

    public TaskPriceDepth() {
    }
}