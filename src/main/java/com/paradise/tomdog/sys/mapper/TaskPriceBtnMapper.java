package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPriceBtn;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Paradise
 */
@Mapper
public interface TaskPriceBtnMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPriceBtn record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskPriceBtn selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskPriceBtn record);

    /**
     * 查询总站
     *
     * @return select res
     */
    TaskPriceBtn selectMaster();

    /**
     * 查询分站商户 非默认
     *
     * @param merchantId 分站商户id
     * @return select res
     */
    TaskPriceBtn selectOneByMerchantId(@Param("merchantId") Long merchantId);

    /**
     * 查询分站商户默认
     *
     * @return select res
     */
    TaskPriceBtn selectDefault();


}