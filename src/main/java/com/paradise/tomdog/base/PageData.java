package com.paradise.tomdog.base;

import com.github.pagehelper.PageInfo;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 封装的分页查询返回结果
 *
 * @author Paradise
 * @version 1.0
 */
@Getter
@Setter
public class PageData {
    private List list;
    private Long count;

    public PageData(List list, Long count) {
        this.list = list;
        this.count = count;
    }

    public PageData(PageInfo pageInfo) {
        this.list = pageInfo.getList();
        this.count = pageInfo.getTotal();
    }
}
