package com.paradise.tomdog.rpc.bean;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 查询拼多多店铺信息
 *
 * @author Paradise
 */
@Getter
@Setter
@Slf4j
public class PddShopQuery extends AbstractLlQueryBean {
    private String shopid;

    public PddShopQuery(String shopid) {
        this.shopid = shopid;
    }

    public static void main(String[] args) {
        PddShopQuery query = new PddShopQuery("953589");
        R r = query.doQuery();
        System.out.println(r.getData());
    }

    @Override
    public void initSign() {
        String url = "/api/query_pinduoduo_shop?appid=1049&shopid=" + this.shopid
                + "&timestamp="
                + this.timestamp + "&ver=4";
        try {
            url = URLEncoder.encode(url, CharsetUtil.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.sign = SecureUtil.md5(url);
    }

    @Override
    public R doQuery() {
        this.initSign();
        HttpResponse httpResponse = HttpUtil
                .createPost("http://www.liuliangjunheng.com/api/query_pinduoduo_shop")
                .body(this.toJson()).execute();
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            log.info("查询拼多多店铺信息Result：");
            log.info(httpResponse.body());
            JSONObject model = new JSONObject(httpResponse.body()).getJSONObject("data");
            return Rx.success(model);
        }
        return Rx.fail(httpResponse.toString());
    }

    public String toJson() {
        return JSONUtil.toJsonStr(this);
    }

}
