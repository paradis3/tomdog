package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysAccount;
import com.paradise.tomdog.sys.entity.SysRole;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.service.SysAccountService;
import com.paradise.tomdog.sys.service.SysRoleService;
import com.paradise.tomdog.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统参数配置 - 价格表 - 配置 控制器
 *
 * @author Paradise
 */
@Api(tags = "后台管理：管理员管理")
@RestController
@RequestMapping("/sys/account/")
@Slf4j
@Valid
public class SysAccountController {

    private final SysRoleService roleService;
    private final SysUserService userService;
    private final SysAccountService accountService;

    public SysAccountController(SysRoleService roleService, SysUserService userService, SysAccountService accountService) {
        this.roleService = roleService;
        this.userService = userService;
        this.accountService = accountService;
    }

    @ApiOperation("新增管理员")
    @PostMapping("/add")
    @ControllerWebLog(name = "新增管理员")
    @PreAuthorize("hasAuthority('pms:mb:account:add')")
    public R add(@Validated @RequestBody SysAccount account,
                 @ApiIgnore Principal principal) {
        if (!ParamCheckUtils.telephoneCheck(account.getTelephone())) {
            return Rx.fail("手机号码格式不正确");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        // 判断用户名，手机号是否在商户下已经存在
        List<SysUser> userList = userService.selectAllByPhoneAndMerchantId(account.getTelephone(), currentUser.getMerchantId());
        if (!userList.isEmpty()) {
            return Rx.fail("手机号码已存在");
        }
        userList = userService.selectAllByUsernameAndMerchantId(account.getUsername(), currentUser.getMerchantId());
        if (!userList.isEmpty()) {
            return Rx.fail("用户名已存在");
        }
        // 参数校验1：roleId 必须在当前角色的下级
        if (roleCheckInvalid(currentUser, account.getRoleId())) {
            return Rx.fail("上级角色选择错误");
        }
        account.setCreateBy(currentUser.getId());
        account.setUpdateBy(currentUser.getId());
        account.setStatus(DictConstant.ENABLE);
        account.setMerchantId(currentUser.getMerchantId());
        account.setUserType(currentUser.getUserType());
        if (accountService.insert(account) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    private boolean roleCheckInvalid(SysUser currentUser, Long roleId) {
        // 参数校验1：roleId 必须在当前角色的下级
        boolean flag = false;
        List<SysRole> roles = roleService.selectAllByPid(currentUser.getRoleId(), null);
        for (SysRole r : roles) {
            if (r.getId().equals(roleId)) {
                flag = true;
                break;
            }
        }
        return !flag;
    }

    @ApiOperation("编辑管理员")
    @PostMapping("/edit")
    @ControllerWebLog(name = "编辑管理员")
    @PreAuthorize("hasAuthority('pms:mb:account:edit')")
    public R edit(@ApiIgnore Principal principal, @Validated @RequestBody SysAccount account) {
        if (account.getId() == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        if (roleCheckInvalid(currentUser, account.getRoleId())) {
            return Rx.fail("上级角色选择错误");
        }
        if (!ParamCheckUtils.telephoneCheck(account.getTelephone())) {
            return Rx.fail("手机号码格式不正确");
        }
        SysUser originUser = userService.selectByPrimaryKey(account.getId());
        // 判断用户名，手机号是否在商户下已经存在
        if (!originUser.getTelephone().equals(account.getTelephone())) {
            List<SysUser> userList = userService.selectAllByPhoneAndMerchantId(account.getTelephone(), currentUser.getMerchantId());
            if (!userList.isEmpty()) {
                return Rx.fail("手机号码已存在");
            }
        }
        if (!originUser.getUsername().equals(account.getUsername())) {
            List<SysUser> userList = userService.selectAllByUsernameAndMerchantId(account.getUsername(), currentUser.getMerchantId());
            if (!userList.isEmpty()) {
                return Rx.fail("用户名已存在");
            }
        }
        account.setUpdateBy(currentUser.getId());
        if (accountService.updateByPrimaryKey(account) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @ApiOperation("删除管理员")
    @PostMapping("/del")
    @ControllerWebLog(name = "删除管理员")
    @PreAuthorize("hasAuthority('pms:mb:account:del')")
    public R del(@ApiIgnore Principal principal, Long userId) {
        if (userId == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        if (userService.deleteByPrimaryKey(userId, currentUser.getId()) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @ApiOperation("查询管理员列表")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('pms:mb:account:list')")
    public R list(@ApiIgnore Principal principal, SysAccount account) {
        SysUser currentUser = CommonUtils.getUser(principal);
        account.setMerchantId(currentUser.getMerchantId());
        // 默认管理员查看全部角色列表
        // 根据商户id 查询全部 or 根据角色id 查询自角色下的全部管理员
        List<SysAccount> accountList = new ArrayList<>();
        // 总站type
        if (CommonUtils.isAdminType(currentUser)) {
            // 递归查询下级
            accountList = accountService.selectAllByRoleId(currentUser.getRoleId(), account);
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            SysRole role = roleService.selectByPrimaryKey(currentUser.getRoleId());
            // 超管查看全部
            if (role.getIsSuper() == 1) {
                accountList = accountService.selectAllByMerchantId(account);
            } else {
                accountList = accountService.selectAllByRoleId(currentUser.getRoleId(), account);
            }
        }
        return Rx.success(accountList);
    }


    @ApiOperation("启用、禁用管理员")
    @PostMapping("/enable")
    @PreAuthorize("hasAuthority('pms:mb:account:enable')")
    public R enable(@NotNull(message = "管理员id不能为空") Long id,
                    @ApiIgnore Principal principal) {
        return userService.modifyEnableAccount(id, CommonUtils.getUser(principal));
    }
}
