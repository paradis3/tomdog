package com.paradise.tomdog.base.utils;

import com.paradise.tomdog.rpc.constant.Sms_Service_Type;
import com.paradise.tomdog.sys.entity.RegisterBean;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 短信验证接口
 *
 * @author xdd
 * @date 2017/11/27 0027
 */

@Component
@Slf4j
public class MobileMsg {
    @Autowired
    RedisUtil redisUtil;

    @Value("${isTest}")
    private String isTest;

    public static final String MSG_URL = "http://api.1cloudsp.com/api/v2/send";
    public static String ACCESSKEY = "HkF5azgnIxt1NDov";
    public static String ACCESSKEY_SECRET = "RsVin4x6vq2w0vqTuUklr7IVEnVCOQST";

    private static final Map<String, List<String>> msgMap = new HashMap<>();

    /**
     * 获取随机手机验证码
     */
    public static String getSmsCode(int size) {
        String base = "0123456789";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 根据模板发送天瑞云短信共同方法
     */
    public void sendSmsFromTemp(String phone, String content, String signId, String templateId) throws Exception {
        if (StringUtils.isAnyBlank(phone, content, signId, templateId)) {
            throw new RuntimeException("输入的短信参数错误");
        }
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = new PostMethod(MobileMsg.MSG_URL);
        postMethod.getParams().setContentCharset("UTF-8");
        postMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());
        NameValuePair[] data = {
                //是否需要加密？有待商榷
                new NameValuePair("accesskey", MobileMsg.ACCESSKEY),
                new NameValuePair("secret", MobileMsg.ACCESSKEY_SECRET),
                new NameValuePair("content", URLEncoder.encode(content, "utf-8")),
                new NameValuePair("mobile", phone),
                new NameValuePair("sign", signId),
                new NameValuePair("templateId", templateId),
        };
        postMethod.setRequestBody(data);
        /*并发测试取消短信验证*/
        int statusCode = httpClient.executeMethod(postMethod);
        log.info("Phone:{};TemplateId:{}", phone, templateId);
        log.info("statusCode: " + statusCode + ", body: "
                + postMethod.getResponseBodyAsString());
        /*并发测试取消短信验证*/
    }

    /**
     * 从缓存获取验证码
     * 每获取一次移除一次计数（最多三次）
     */
    private String getSms(String phone) {
        List<String> o = msgMap.get(phone);
        String sms = null;
        if (o != null) {
            if (!"0".equals(o.get(1))) {
                sms = o.get(0);
                o.add(1, String.valueOf(Integer.parseInt(o.get(1)) - 1));
            } else {
                msgMap.remove(phone);
            }
        }
        return sms;
    }

    /**
     * 验证码手机验证码
     *
     * @return true 验证通过 false 验证不通过
     */
    public boolean checkSms(String phone, String sms) {
        String num = getSms(phone);
        String code = redisUtil.getString(phone);
        log.info("Java缓存中的验证码：{}", num);
        log.info("Redis缓存中的验证码：{}", code);
        log.info("待验证的验证码：{}", sms);
        if (!StringUtils.equals(sms, num) && !StringUtils.equals(sms, code)) {
            return false;
        }
        //验证成功移除验证码
        msgMap.remove(phone);
        redisUtil.delete(phone);
        return true;
    }

    /**
     * 普通短信
     *
     * @param bean 域名，手机号，商户信息
     * @throws Exception e
     */
    @EventListener
    @Async
    public void sendNormalSms(RegisterBean bean) throws Exception {
        String code;
        String signId;
        String templateId;
        if (StringUtils.equals(isTest, "true")) {
            code = "123456";
        } else {
            code = getSmsCode(6);
            log.info("生成的短信验证码为：{}", code);
            Object signObject = redisUtil.get("sms_sign_" + bean.getMerchantId());
            if (StringUtils.isNotBlank((String) signObject)) {
                signId = (String) signObject;
            } else {
                signId = redisUtil.getString("sms_sign_1");
            }
            log.info("获取到的签名id：{}", signId);
            Object templateObject = redisUtil.get("sms_temp_" + bean.getMerchantId() + "_" + Sms_Service_Type.NORMALCODE.getCode());
            if (StringUtils.isNotBlank((String) templateObject)) {
                templateId = (String) templateObject;
            } else {
                templateId = redisUtil.getString("sms_temp_1_" + Sms_Service_Type.NORMALCODE.getCode());
            }
            log.info("获取到的模板id：{}", templateId);
            sendSmsFromTemp(bean.getTelephone(), code, signId, templateId);
        }
        //添加到缓存
        List<String> list = new ArrayList<>();
        list.add(code);
        list.add("3");
        msgMap.put(bean.getTelephone(), list);
        // 替换成 redis 存储
        redisUtil.set(bean.getTelephone(), code, 5, TimeUnit.MINUTES);
    }

}
