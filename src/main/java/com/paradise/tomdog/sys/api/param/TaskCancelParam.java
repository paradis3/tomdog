package com.paradise.tomdog.sys.api.param;

import com.paradise.tomdog.rpc.bean.BaseParamBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Paradise
 */
@Getter
@Setter
public class TaskCancelParam extends BaseParamBean implements ApiParamInterface {
    @ApiModelProperty(value = "API用户名")
    private String username;
    @ApiModelProperty("appId")
    private String appId;
    @ApiModelProperty(value = "时间戳")
    private Long timestamp;
    @ApiModelProperty("任务ID")
    private String taskId;
    @ApiModelProperty(value = "签名")
    private String sign;

    public TaskCancelParam() {
    }
}
