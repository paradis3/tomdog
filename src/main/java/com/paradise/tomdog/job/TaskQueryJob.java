package com.paradise.tomdog.job;

import chatbot.DentalCabotClient;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.constant.Task_Status;
import com.paradise.tomdog.sys.entity.LlTask;
import com.paradise.tomdog.sys.entity.TaskRefund;
import com.paradise.tomdog.sys.mapper.LlTaskMapper;
import com.paradise.tomdog.sys.mapper.TaskRefundMapper;
import com.paradise.tomdog.sys.service.LlTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.*;

/**
 * 定时任务
 *
 * @author Paradise
 */
@Component
@Slf4j
public class TaskQueryJob {

    @Resource
    private LlTaskService taskService;
    @Resource
    private TaskRefundMapper taskRefundMapper;
    @Resource
    private LlTaskMapper taskMapper;

    /**
     * 每小时执行一次，查询进行中的任务执行状态；并进行完成数量的更新操作
     */
    @Scheduled(cron = "0 0 0/1 * * ? ")
    public void taskItemQuery() {
        //Common Thread Pool
        ExecutorService pool = new ThreadPoolExecutor(5, 200,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(1024), new ThreadPoolExecutor.AbortPolicy());
        log.info("定时任务 TaskItemQuery 执行中... ");
        CompletionService<Integer> completionService = new ExecutorCompletionService<>(pool);
        List<LlTask> taskList = taskMapper.selectByStatus(Task_Status.ING.getCode());
        for (LlTask task : taskList) {
            completionService.submit(() -> {
                taskService.taskQuery(task.getId());
                return null;
            });
        }
    }

    /**
     * 每 10 min执行一次，查询申请取消中的任务执行状态；积分退还逻辑处理
     */
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void taskCancelQuery() {
        //Common Thread Pool
        ExecutorService pool = new ThreadPoolExecutor(5, 200,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(1024), new ThreadPoolExecutor.AbortPolicy());
        log.info("定时任务 taskCancelQuery 执行中... ");
        CompletionService<Integer> completionService = new ExecutorCompletionService<>(pool);
        List<LlTask> taskList = taskMapper.selectByStatus(Task_Status.CANCEL_ING.getCode());
        for (LlTask task : taskList) {
            completionService.submit(() -> {
                taskService.taskQuery(task.getId());
                return null;
            });
        }
    }

    /**
     * 每 10 min执行一次，查询申请取消中的任务执行状态；积分退还逻辑处理
     */
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void dealRefund() {
        // 查询全部未处理的退款任务
        List<TaskRefund> refundList = taskRefundMapper.selectByStatus("0");
        if (!refundList.isEmpty()) {
            for (TaskRefund refund : refundList) {
                R res = taskService.resolveRefund(refund);
                if (!Rx.isSuccess(res)) {
                    log.error("任务{}处理退款失败", refund.getTaskId());
                    DentalCabotClient.sendText("任务{" + refund.getTaskId() + "}处理退款失败:" + res.getData());
                }
            }
        }
    }
}
