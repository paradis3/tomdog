package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 京东商品信息
 *
 * @author Paradise
 */
@ApiModel(value = "京东商品信息")
@Data
public class JdGoods {
    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id")
    private Long id;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String name;

    /**
     * 图片url
     */
    @ApiModelProperty(value = "图片url")
    private String chaturl;

    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    /**
     * 店铺id
     */
    @ApiModelProperty(value = "店铺id")
    private Long shopId;

    /**
     * 店铺关注数量
     */
    @ApiModelProperty(value = "店铺关注数量")
    private Integer shopFollowCount;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    public JdGoods(Long id, String name, String chaturl, String shopName,
                   Long shopId, Integer shopFollowCount) {
        this.id = id;
        this.name = name;
        this.chaturl = chaturl;
        this.shopName = shopName;
        this.shopId = shopId;
        this.shopFollowCount = shopFollowCount;
    }
}