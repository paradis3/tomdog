package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@ApiModel(value = "com-paradise-tomdog-sys-entity-SysMenu")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class SysMenu {
    /**
     * 菜单主键
     */
    @ApiModelProperty(value = "菜单主键")
    private Long id;

    /**
     * 上级菜单ID
     */
    @ApiModelProperty(value = "上级菜单ID")
    private Long pid;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;
    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String title;

    /**
     * 菜单值
     */
    @ApiModelProperty(value = "菜单值")
    private String value;

    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    /**
     * 菜单类型：0 目录 1菜单 2 按钮
     */
    @ApiModelProperty(value = "菜单类型：0 目录 1菜单 2 按钮")
    private String type;

    /**
     * 菜单级别
     */
    @ApiModelProperty(value = "菜单级别")
    private Integer level;

    /**
     * 前端资源，组件
     */
    @ApiModelProperty(value = "前端资源，组件")
    private String uri;
    @ApiModelProperty(value = "前端资源，组件")
    private String path;

    /**
     * 状态:1有效，0无效
     */
    @ApiModelProperty(value = "状态:1有效，0无效")
    private String status;

    private Long creator;

    private Date createTime;

    private Long updater;

    private Date updateTime;

    @ApiModelProperty(hidden = true)
    private List<SysMenu> children;

    @ApiModelProperty(hidden = true)
    private Integer isMaster;
}