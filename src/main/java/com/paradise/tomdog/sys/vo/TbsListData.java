package com.paradise.tomdog.sys.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author Paradise
 */
@Getter
@Setter
public class TbsListData {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "充值时间")
    private String rechargeTime;
    @ApiModelProperty(value = "充值用户")
    private String rechargeUser;
    @ApiModelProperty(value = "充值单号")
    private String rechargeId;
    @ApiModelProperty(value = "充值积分")
    private String rechargePoints;
    @ApiModelProperty(value = "返利积分")
    private String spreadPoints;
}
