package com.paradise.tomdog.rpc.bean;

import lombok.Builder;
import lombok.Getter;

/**
 * 取消任务接口参数
 *
 * @author Paradise
 */
@Builder
@Getter
public class TaskCancelBean extends BaseParamBean {
    private String username;
    private String id;
    private String format;
    private Long timestamp;
    private Integer ver;

}
