package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface TbsWithdrawRecordService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insert(TbsWithdrawRecord record);
    
    int insertSelective(TbsWithdrawRecord record);
    
    TbsWithdrawRecord selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(TbsWithdrawRecord record);
    
    int updateByPrimaryKey(TbsWithdrawRecord record);
    
    R createPointsWithdrawRecord(SysUser user, TbsWithdrawRecord record);
    
    PageData pagePointsWithdrawRecord(Integer pageNum, Integer pageSize, TbsWithdrawRecord record);
    
    R auditPointsWithdraw(TbsWithdrawRecord record);
    
    /**
     * 导出提现列表单
     */
    void exportPointsWithdraw(TbsWithdrawRecord record, SysUser user, HttpServletRequest request,
                              HttpServletResponse response);
}

