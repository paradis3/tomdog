package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserApi;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.SysUserApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Paradise
 */
@Api(tags = "后台管理：首页接口 #充值相关的待完善")
@Slf4j
@RestController
@RequestMapping("/sys/index")
public class HelloController {

    @Resource
    private BalanceService balanceService;
    @Resource
    private SysUserApiMapper sysUserApiMapper;
    @Resource
    private SysBranchMerchantMapper branchMerchantMapper;
    @Resource
    private SysUserMapper userMapper;
    @Resource
    private LlTaskMapper taskMapper;
    @Resource
    private SysUserApiService sysUserApiService;
    @Resource
    private MasterRechargeRecordMapper masterRechargeRecordMapper;
    @Resource
    private BranchRechargeRecordMapper branchRechargeRecordMapper;

    @ApiOperation("用户数统计（总站/分站商户后台）")
    @GetMapping("/getUserCount")
    @PreAuthorize("hasAuthority('pms:mb:index:getUserCount')")
    public R getUserCount(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isAdminType(currentUser)) {
            //  用户数统计：分站商户，API商户，分站用户
            Map<String, Object> resMap = new HashMap<>(4);
            resMap.put("merchantCount", branchMerchantMapper.countBy());
            resMap.put("apiCount", sysUserApiMapper.countByIsDel());
            resMap.put("userCount", userMapper.countByUserTypeAndMerchantId(UserTypeEnum.MERCHANT_USER_TYPE.getType(), null));
            return Rx.success(resMap);
        } else if (CommonUtils.isMerchantType(currentUser)) {
            // 查询分站商户用户 数量
            return Rx.success(userMapper.countByUserTypeAndMerchantId(UserTypeEnum.MERCHANT_USER_TYPE.getType(), currentUser.getMerchantId()));
        }
        return Rx.permissionDenied();

    }

    @ApiOperation("库存积分")
    @GetMapping("/getBalance")
    @PreAuthorize("hasAuthority('pms:mbau:index:getBalance')")
    public R getBalance(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isAdminType(currentUser)) {
            // 总站库存积分，如何维护？
            return Rx.success(balanceService.queryBalance(1L, UserTypeEnum.ADMIN_TYPE));
        } else if (CommonUtils.isMerchantType(currentUser)) {
            // 查询分站商户 库存积分
            return Rx.success(balanceService.queryBalance(currentUser.getMerchantId(), UserTypeEnum.MERCHANT_TYPE));
        } else if (CommonUtils.isApiType(currentUser)) {
            SysUserApi sysUserApi = sysUserApiService.selectByUserId(currentUser.getId());
            return Rx.success(balanceService.queryBalance(sysUserApi.getId(), UserTypeEnum.API_TYPE));
        } else if (CommonUtils.isMerchantUserType(currentUser)) {
            return Rx.success(balanceService.queryBalance(currentUser.getId(), UserTypeEnum.MERCHANT_USER_TYPE));
        }
        return Rx.permissionDenied();
    }

    @ApiOperation("查询近几日充值曲线记录")
    @GetMapping("/getRechargeRecord")
    @PreAuthorize("hasAuthority('pms:mb:index:rechargeRecord')")
    public R rechargeRecord(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isAdminType(currentUser)) {
            // 查询 分站商户充值记录，API商户充值记录
            return Rx.success(masterRechargeRecordMapper.selectPayAmount());
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            return Rx.success(branchRechargeRecordMapper.selectPayAmount(currentUser.getMerchantId()));
        }
        return Rx.permissionDenied();
    }

    private static Date getToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private static Date getToMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static void main(String[] args) {
        Date date = getToday();
        System.out.println(date.getTime());
    }

    @ApiOperation("今日任务数，今日充值笔数，今日充值金额，本月充值")
    @GetMapping("/getSomeInfo")
    public R getSomeInfo(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        Date today = getToday();
        Date toMonth = getToMonth();
        if (CommonUtils.isAdminType(currentUser)) {
            // 查询 分站商户充值记录，API商户充值记录
            Long taskCount = taskMapper.countByCondition(null, null, today.getTime());
            Map<String, Object> map = masterRechargeRecordMapper.countByStatusAndPayTime(today);
            map.put("taskCount", taskCount);
            map.put("monthCount", masterRechargeRecordMapper.selectPayAmountByPayTime(toMonth));
            return Rx.success(map);
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            Long count = taskMapper.countByCondition(currentUser.getMerchantId(), null, today.getTime());
            Map<String, Object> map = branchRechargeRecordMapper.countByStatusAndCreateTime(today, currentUser.getMerchantId());
            map.put("taskCount", count);
            map.put("monthCount", branchRechargeRecordMapper.selectPayAmountByCreateTime(toMonth, currentUser.getMerchantId()));
            return Rx.success(map);
        }
        return Rx.permissionDenied();
    }
}
