package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Api(tags = "充值相关接口", description = "充值相关接口")
@RestController
@RequestMapping("/user/recharge")
@Slf4j
public class RechargeController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private MasterRechargeRecordService masterRechargeRecordService;
    @Autowired
    private BranchRechargeRecordService branchRechargeRecordService;
    @Autowired
    private BranchRechargePackageService branchRechargePackageService;
    @Autowired
    private MasterRechargePackageService masterRechargePackageService;
    @Autowired
    private TbsBalanceRecordService tbsBalanceRecordService;
    @Autowired
    SysUserPayInfoService sysUserPayInfoService;

    @ApiOperation("添加/修改商户充值收款信息")
    @PostMapping("/updateRechargeInfo")
    @PreAuthorize("hasAuthority('pms:mb:recharge:update')")
    @ControllerWebLog(name = "添加/修改商户充值收款信息")
    public R updateRechargeInfo(@Validated SysUserPayInfo sysUserPayInfo, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (CommonUtils.isMerchantType(user) && !sysUserPayInfo.getMerchantId().equals(user.getMerchantId())) {
            throw new RuntimeException("分站商户只可设置自己的收款信息");
        }
        sysUserPayInfo.setCreateBy(user.getId());
        sysUserPayInfo.setUpdateBy(user.getId());
        sysUserPayInfo.setCreateTime(new Date());
        sysUserPayInfo.setUpdateTime(new Date());
        return sysUserPayInfoService.updateSysUserPayInfo(sysUserPayInfo);
    }

    @ApiOperation("根据id获取商户充值收款信息")
    @PostMapping("/getRchargeInfo")
    @PreAuthorize("hasAuthority('pms:mb:recharge:get')")
    @ControllerWebLog(name = "根据id获取商户充值收款信息")
    public R getRchargeInfo(Long merchantId, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isAdminType(user) && !user.getMerchantId().equals(merchantId)) {
            throw new RuntimeException("分站商户只能查看自己的收款信息");
        }
        List<SysUserPayInfo> sysUserPayInfos = sysUserPayInfoService.selectByUserIdAndIsDelNot(merchantId, "1");
        if (sysUserPayInfos.size() == 1) {
            return Rx.success(sysUserPayInfos.get(0));
        } else {
            return Rx.success();
        }
    }

    @ApiOperation("添加/修改总站套餐信息")
    @PostMapping("/updateMasterPackageInfo")
    @PreAuthorize("hasAuthority('pms:m:package:update')")
    @ControllerWebLog(name = "添加/修改商户套餐信息")
    public R updateMasterPackageInfo(@Validated MasterRechargePackage rechargePackage, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (rechargePackage.getId() == null) {
            rechargePackage.setCreateBy(user.getId());
            rechargePackage.setCreateTime(new Date());
        }
        rechargePackage.setUpdateBy(user.getId());
        rechargePackage.setUpdateTime(new Date());
        return masterRechargePackageService.updateMasterRechargePackage(rechargePackage);
    }

    @ApiOperation("添加/修改分站套餐信息")
    @PostMapping("/updateBranchPackageInfo")
    @PreAuthorize("hasAuthority('pms:mb:package:update')")
    @ControllerWebLog(name = "添加/修改分站套餐信息")
    public R updateBranchPackageInfo(@Validated BranchRechargePackage rechargePackage, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!user.getMerchantId().equals(rechargePackage.getMerchantId())) {
            throw new RuntimeException("分站商户只能修改自己的套餐信息");
        }
        if (rechargePackage.getId() == null) {
            rechargePackage.setCreateBy(user.getId());
            rechargePackage.setCreateTime(new Date());
        }
        rechargePackage.setUpdateBy(user.getId());
        rechargePackage.setUpdateTime(new Date());
        return branchRechargePackageService.updateBranchRechargePackage(rechargePackage);
    }

    @ApiOperation("查询总站充值套餐列表")
    @GetMapping("/getMasterRechargePackageList")
    @PreAuthorize("hasAuthority('pms:mba:package:get')")
    public R getMasterRechargePackageList(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return Rx.success(masterRechargePackageService.selectByAllAndValid(user));
    }

    @ApiOperation("查询分站充值套餐列表")
    @GetMapping("/getBranchRechargePackageList")
    @PreAuthorize("hasAuthority('pms:mbu:package:get')")
    public R getBranchRechargePackageList(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return Rx.success(branchRechargePackageService.selectByAllAndValid(user));
    }

    @ApiOperation("创建总站用户充值订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "packageId", value = "套餐ID", defaultValue = "201"),
            @ApiImplicitParam(name = "payType", value = "支付方式（1：支付宝 2：微信）", defaultValue = "1"),
    })
    @PostMapping("/createMasterRechargeOrder")
    @PreAuthorize("hasAuthority('pms:ba:rechargeOrder:create')")
    @ControllerWebLog
    public R createMasterRechargeOrder(Long packageId, String payType, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return masterRechargeRecordService.createOrder(packageId, user, payType);
    }

    @ApiOperation("手动为总站用户补单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "recordId", value = "订单ID", defaultValue = "201"),
            @ApiImplicitParam(name = "status", value = "1重新查询财务 2 直接置为成功", defaultValue = "1"),
    })
    @PostMapping("/replaceMasterRechargeOrder")
    @PreAuthorize("hasAuthority('pms:m:rechargeOrder:replace')")
    @ControllerWebLog
    public R replaceMasterRechargeOrder(Long recordId, String status, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return masterRechargeRecordService.replaceOrder(recordId, user, status);
    }

    @ApiOperation("创建分站用户充值订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "packageId", value = "套餐ID", defaultValue = "1"),
            @ApiImplicitParam(name = "payType", value = "支付方式（1：支付宝 2：微信）", defaultValue = "1"),
    })
    @PostMapping("/createBranchRechargeOrder")
    @PreAuthorize("hasAuthority('pms:u:createBranchRechargeOrder:create')")
    @ControllerWebLog
    public R createBranchRechargeOrder(Long packageId, String payType, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return branchRechargeRecordService.createOrder(packageId, user, payType);

    }

    @ApiOperation("手动为分站用户补单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "recordId", value = "订单ID", defaultValue = "201"),
            @ApiImplicitParam(name = "status", value = "1重新查询财务 2 直接置为成功", defaultValue = "1"),
    })
    @PostMapping("/replaceBranchRechargeOrder")
    @PreAuthorize("hasAuthority('pms:mb:replaceBranchRechargeOrder:replace')")
    @ControllerWebLog
    public R replaceBranchRechargeOrder(Long recordId, String status, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return branchRechargeRecordService.replaceOrder(recordId, user, status);
    }

    @ApiOperation("分页查询总站用户充值记录")
    @GetMapping("/getMasterRechargeOrderPage")
    @PreAuthorize("hasAuthority('pms:mba:rechargeOrder:get')")
    public R getMasterRechargeOrderPage(MasterRechargeRecordQueryBean queryBean, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        PageData pageData = masterRechargeRecordService.getRechargePage(queryBean, user);
        return Rx.success(pageData);
    }

    @ApiOperation("导出总站用户充值记录")
    @GetMapping("/exportMasterRechargeOrderPage")
    @PreAuthorize("hasAuthority('pms:mba:rechargeOrder:exportMaster')")
    public void exportMasterRechargeOrderPage(MasterRechargeRecordQueryBean queryBean, @ApiIgnore Principal principal,
                                              @ApiIgnore HttpServletResponse response, @ApiIgnore HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        masterRechargeRecordService.exportMasterRechargeRecord(queryBean, currentUser, request, response);
    }

    @ApiOperation("分页查询分站用户充值记录")
    @GetMapping("/getBranchRechargeOrderPage")
    @PreAuthorize("hasAuthority('pms:mbu:rechargeOrder:get')")
    public R getBranchRechargeOrderPage(BranchRechargeRecordQueryBean queryBean, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        PageData pageData = branchRechargeRecordService.getRechargePage(queryBean, user);
        return Rx.success(pageData);
    }

    @ApiOperation("导出分站用户充值记录")
    @GetMapping("/exportBranchRechargeOrderPage")
    @PreAuthorize("hasAuthority('pms:mbu:rechargeOrder:exportBranch')")
    public void exportBranchRechargeOrderPage(BranchRechargeRecordQueryBean queryBean, @ApiIgnore Principal principal,
                                              @ApiIgnore HttpServletResponse response, @ApiIgnore HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        branchRechargeRecordService.exportMasterRechargeRecord(queryBean, currentUser, request, response);
    }

    @ApiOperation("分页查询推广收益记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", defaultValue = "10")
    })
    @GetMapping("/getSpreadRechargePage")
    @PreAuthorize("hasAuthority('pms:u:SpreadRechargePage:get')")
    public R getSpreadRechargePage(Integer pageNum, Integer pageSize,
                                   @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        PageData pageData = tbsBalanceRecordService.getSpreadRechargePage(pageNum, pageSize, user);
        return Rx.success(pageData);
    }
}

