package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPermissionApi;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * CmwAPI 商户任务权限
 *
 * @author Paradise
 */
@Mapper
public interface TaskPermissionApiMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPermissionApi record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskPermissionApi selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskPermissionApi record);

    /**
     * 初始化 CmwAPI 权限
     *
     * @param apiId api 商户ID
     * @return insert count
     */
    int initPermission(@Param("apiId") Long apiId);

    /**
     * 查询Api 商户任务权限
     *
     * @param apiId api id
     * @return select result
     */
    List<TaskPermissionApi> selectAllByApiId(@Param("apiId") Long apiId);

    /**
     * 查询是否有权限
     *
     * @param taskCode 任务类型
     * @param apiId    api userId
     * @return true 存在 false 不存在
     */
    boolean selectByTaskCodeAndApiId(@Param("taskCode") String taskCode,
                                     @Param("apiId") Long apiId);


}