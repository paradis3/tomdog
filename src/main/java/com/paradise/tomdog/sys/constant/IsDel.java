package com.paradise.tomdog.sys.constant;

/**
 * 删除状态枚举
 *
 * @author Paradise
 */
public enum IsDel {
    /**
     * 删除
     */
    NO("0", "未删除"),
    YES("1", "已删除");

    private String code;
    private String name;

    IsDel(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
