package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

public interface BranchRechargeRecordService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insertSelective(BranchRechargeRecord record);
    
    BranchRechargeRecord selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(BranchRechargeRecord record);
    
    /**
     * 分站用户创建积分充值订单
     *
     * @param packageId 选择的套餐id
     * @param user      当前登录用户
     * @param payType   支付方式
     * @return {@link R}
     */
    R createOrder(Long packageId, SysUser user, String payType);
    
    R replaceOrder(Long recordId, SysUser user, String status);
    
    /**
     * 分页查询 充值记录

     * @param user     当前登录用户
     * @return {@link PageData}
     */
    PageData getRechargePage(BranchRechargeRecordQueryBean queryBean, SysUser user);
    
    int insert(BranchRechargeRecord record);
    
    int updateByPrimaryKey(BranchRechargeRecord record);
    
    List<BranchRechargeRecord> selectByAll(BranchRechargeRecord branchRechargeRecord);
    
    /**
     * 返利
     *
     * @param record
     * @param user
     */
    void userSpreadPointsHandler(BranchRechargeRecord record, SysUser user);
    
    /**
     * 升级
     *
     * @param record
     * @param user
     */
    void userLevelHandler(BranchRechargeRecord record, SysUser user);
    
    List<BranchRechargeRecord> selectAllByUpdateTimeBefore(Date minUpdateTime);
    /**
     * 导出充值订单
     */
    void exportMasterRechargeRecord(BranchRechargeRecordQueryBean queryBean, SysUser user, HttpServletRequest request,
                                    HttpServletResponse response);
}








