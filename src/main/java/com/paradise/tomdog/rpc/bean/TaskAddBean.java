package com.paradise.tomdog.rpc.bean;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 添加淘宝流量任务
 *
 * @author Paradise
 */
@EqualsAndHashCode(callSuper = true)
@Getter
@Builder
public class TaskAddBean extends BaseParamBean {
    /**
     * 猎流用户名 Y
     */
    private String username;
    /**
     * 任务id/订单号 Y
     */
    private String id;
    /**
     * 发布量 Y
     */
    private Integer count;
    /**
     * Y
     * 0点到23点的发布量，用逗号隔开，如某个点不需要则以0代替
     */
    private String hour;
    /**
     * Y
     * 任务开始时间
     * 格式：yyyy-MM-dd
     * 默认当天日期
     **/
    private String begin_time;
    /**
     * Y
     * 0=APP搜索
     * 1=PC搜索
     * 2=直访店铺
     * 3=直访商品
     */
    private Integer type;
    /**
     * Y
     * 商品链接、店铺链接
     */
    @Setter
    private String target;
    /**
     * Y
     * 搜索关键字
     */
    @Setter
    private String keyword;
    /**
     * Y
     * 流量时间，如100-180,180-280,280-380380-480
     */
    private String browse_time;
    /**
     * Y
     * 浏览其他宝贝
     * 0=不浏览
     * 1=随机浏览
     * 2=深度浏览
     */
    private Integer depth;
    /**
     * 店铺访问模式
     * sales:按销量
     * favos:按收藏
     * cates:按全部品类
     * no:不访问,不传默认为no
     * 仅针对PC流量有效	N
     */
    private String shopVisit;
    /**
     * xml or json N
     */
    private String format;
    /**
     * 时间戳
     */
    private Long timestamp;
    /**
     * 接口版本号
     */
    private Integer ver;

    /**
     * 抖音视频id
     */
    @Setter
    private Long aweme_id;
    /**
     * 淘宝直播id
     */
    @Setter
    private Long liveid;
    @Setter
    private Long userid;
    @Setter
    private String itemid;
    @Setter
    private String juid;
}
