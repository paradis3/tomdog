package com.paradise.tomdog.sys.entity;

import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Paradise
 */
@Getter
@Setter
@Builder
public class BalanceUpdateParam {
    private Long businessId;
    private UserTypeEnum userTypeEnum;
    private int amount;
    private Points_Record_Type recordType;

    private Long rechargeId;
    private String taskId;
    private String remark;
    private SysUser currentUser;
    private Long createBy;
}
