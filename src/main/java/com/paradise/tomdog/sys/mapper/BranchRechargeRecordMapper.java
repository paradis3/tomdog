package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.BranchRechargeRecord;
import com.paradise.tomdog.sys.entity.BranchRechargeRecordQueryBean;
import com.paradise.tomdog.sys.entity.BranchRechargeRecordWithSysUserWithSysBranchMerchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 分站用户充值记录
 *
 * @author Paradise
 */
@Mapper
public interface BranchRechargeRecordMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(BranchRechargeRecord record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(BranchRechargeRecord record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    BranchRechargeRecord selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(BranchRechargeRecord record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(BranchRechargeRecord record);

    List<BranchRechargeRecord> selectByAll(BranchRechargeRecord branchRechargeRecord);

    /**
     * 查询用户累计充值积分
     *
     * @param userId 用户ID
     * @return 累计充值积分
     */
    Integer selectSumPointsByUserId(@Param("userId") Long userId);

    List<BranchRechargeRecord> selectAllByUpdateTimeBefore(@Param("minUpdateTime") Date minUpdateTime);

    List<BranchRechargeRecordWithSysUserWithSysBranchMerchant> selectAllByQueryBean(BranchRechargeRecordQueryBean queryBean);

    /**
     * 统计 目标日期 之后的充值记录，充值金额
     *
     * @param createTime 查询开始时间
     * @param merchantId 分站商户id
     * @return 金额，记录数
     */
    Map<String, Object> countByStatusAndCreateTime(@Param("createTime") Date createTime,
                                                   @Param("merchantId") Long merchantId);

    /**
     * 统计 目标日期 之后的累计充值金额
     *
     * @param createTime 查询开始时间
     * @param merchantId 分站商户id
     * @return SUM
     */
    BigDecimal selectPayAmountByCreateTime(@Param("createTime") Date createTime,
                                           @Param("merchantId") Long merchantId);

    /**
     * 查询30d的充值金额
     *
     * @param merchantId 商户id
     * @return ListMap
     */
    List<Map<String, Object>> selectPayAmount(@Param("merchantId") Long merchantId);

    /**
     * 查询充值数量
     *
     * @param userId userId
     * @return 成功充值的数量
     */
    Integer countByUserId(@Param("userId") Long userId);


}