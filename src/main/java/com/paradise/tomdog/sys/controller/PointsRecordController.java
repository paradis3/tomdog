package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.PointsRecordQueryBean;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.service.PointsRecordService;
import com.paradise.tomdog.sys.service.SysUserApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * @author Paradise
 */
@Api(tags = "后台管理：积分记录")
@RestController
@RequestMapping("/sys/points")
@Slf4j
public class PointsRecordController {

    private final PointsRecordService pointsRecordService;
    private final SysUserApiService sysUserApiService;

    @Autowired
    public PointsRecordController(PointsRecordService pointsRecordService,SysUserApiService sysUserApiService) {
        this.pointsRecordService = pointsRecordService;
        this.sysUserApiService = sysUserApiService;
    }

    /**
     * 分页查询分站商户积分记录
     * 1. 总站可以看到全部的
     * 2. 分站商户看自己的
     *
     * @param bean      查询条件
     * @param principal 登录信息
     * @return R
     */
    @ApiOperation("分页查询分站商户积分记录")
    @GetMapping("/getBranchMerchantPointsRecordPage")
    @PreAuthorize("hasAuthority('pms:mb:points:getBranchMerchantPointsRecordPage')")
    public R getBranchMerchantPointsRecordPage(@Validated PointsRecordQueryBean bean,
                                               @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        boolean noPermission = CommonUtils.isApiType(currentUser) || CommonUtils.isMerchantUserType(currentUser);
        if (noPermission) {
            return Rx.permissionDenied();
        }
        // 分站商户查询自己分站下的记录；
        if (CommonUtils.isMerchantType(currentUser)) {
            bean.setMerchantId(currentUser.getMerchantId());
        }
        PageData pageData = pointsRecordService.getPointsRecordPage(bean, currentUser, UserTypeEnum.MERCHANT_TYPE);
        return Rx.success(pageData);
    }

    @ApiOperation("分页查询分站用户积分记录")
    @GetMapping("/getBranchMerchantUserPointsRecordPage")
    @PreAuthorize("hasAuthority('pms:mb:points:getUserPointsRecordPage')")
    public R getBranchMerchantUserPointsRecordPage(@Validated PointsRecordQueryBean bean,
                                                   @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        boolean hasNoPermission = CommonUtils.isMerchantUserType(currentUser);
        if (hasNoPermission) {
            return Rx.permissionDenied();
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            bean.setMerchantId(currentUser.getMerchantId());
        }
        PageData pageData = pointsRecordService.getPointsRecordPage(bean, currentUser, UserTypeEnum.MERCHANT_USER_TYPE);
        return Rx.success(pageData);
    }

    @ApiOperation("分页查询API商户积分记录")
    @GetMapping("/getApiUserPointsRecordPage")
    @PreAuthorize("hasAuthority('pms:ma:points:getApiUserPointsRecordPage')")
    public R getApiUserPointsRecordPage(@Validated PointsRecordQueryBean bean,
                                        @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        boolean hasNoPermission = CommonUtils.isMerchantUserType(currentUser) || CommonUtils.isMerchantType(currentUser);
        if (hasNoPermission) {
            return Rx.permissionDenied();
        }
        if (CommonUtils.isApiType(currentUser)) {
            bean.setApiUserId(sysUserApiService.selectByUserId(currentUser.getId()).getId());
        }
        PageData pageData = pointsRecordService.getPointsRecordPage(bean, currentUser, UserTypeEnum.API_TYPE);
        return Rx.success(pageData);
    }

    @ApiOperation("导出分站商户积分记录")
    @GetMapping("/exportBranchMerchantPointsRecord")
    @PreAuthorize("hasAuthority('pms:mb:points:exportBranchMerchantPointsRecord')")
    public void exportBranchMerchantPointsRecord(PointsRecordQueryBean bean, @ApiIgnore Principal principal,
                                                 HttpServletResponse response, HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isMerchantType(currentUser)) {
            bean.setMerchantId(currentUser.getMerchantId());
        }
        pointsRecordService.exportPointsRecord(bean, request, response, currentUser, UserTypeEnum.MERCHANT_TYPE);
    }

    @ApiOperation("导出API商户积分记录")
    @GetMapping("/exportApiUserPointsRecord")
    @PreAuthorize("hasAuthority('pms:ma:points:exportBranchMerchantPointsRecord')")
    public void exportApiUserPointsRecord(PointsRecordQueryBean bean, @ApiIgnore Principal principal,
                                          HttpServletResponse response,
                                          HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isApiType(currentUser)) {
            bean.setApiUserId(sysUserApiService.selectByUserId(currentUser.getId()).getId());
        }
        pointsRecordService.exportPointsRecord(bean, request, response, currentUser, UserTypeEnum.API_TYPE);
    }

    @ApiOperation("导出分站用户积分记录")
    @GetMapping("/exportBranchMerchantUserPointsRecord")
    @PreAuthorize("hasAuthority('pms:mbu:points:exportBranchMerchantUserPointsRecord')")
    public void exportBranchMerchantUserPointsRecord(PointsRecordQueryBean bean, @ApiIgnore Principal principal,
                                                     HttpServletResponse response, HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isMerchantType(currentUser)) {
            bean.setMerchantId(currentUser.getMerchantId());
        }
        pointsRecordService.exportPointsRecord(bean, request, response, currentUser, UserTypeEnum.MERCHANT_USER_TYPE);
    }
}
