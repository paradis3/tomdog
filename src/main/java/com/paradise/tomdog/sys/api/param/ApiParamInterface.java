package com.paradise.tomdog.sys.api.param;

/**
 * Api 查询 参数 接口
 *
 * @author Paradise
 */
public interface ApiParamInterface {
    /**
     * 获取 username
     *
     * @return username
     */
    String getUsername();

    /**
     * 获取调用方签名
     *
     * @return 签名信息
     */
    String getSign();

    /**
     * 获取参数中的 appId
     *
     * @return appId
     */
    String getAppId();

    /**
     * 服务端签名
     *
     * @param appSecret api secret
     * @param prefix    前缀 “”
     * @return 签名
     */
    String createSign(String appSecret, String prefix);

    /**
     * 获取参数列表中的时间戳
     *
     * @return 时间戳
     */
    Long getTimestamp();
}
