package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.PointsRecord;
import com.paradise.tomdog.sys.entity.PointsRecordQueryBean;
import com.paradise.tomdog.sys.entity.SysUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Paradise
 */
public interface PointsRecordService {

    /**
     * 逻辑删除
     *
     * @param recordId id
     * @return update count
     */
    int deleteByPrimaryKey(Long recordId);

    /**
     * insert
     *
     * @param record record
     * @return insert count
     */
    int insert(PointsRecord record);

    /**
     * select
     *
     * @param recordId id
     * @return select result
     */
    PointsRecord selectByPrimaryKey(Long recordId);

    /**
     * 分页查询 积分记录
     *
     * @param bean         查询参数 {@linkplain PointsRecordQueryBean}
     * @param currentUser  当前登录用户
     * @param userTypeEnum 类型
     * @return {@linkplain PageData}
     */
    PageData getPointsRecordPage(PointsRecordQueryBean bean, SysUser currentUser, UserTypeEnum userTypeEnum);

    /**
     * 查询导出 Excel 积分记录
     *
     * @param bean         查询条件
     * @param request      请求
     * @param response     响应
     * @param currentUser  当前登录用户
     * @param userTypeEnum 查询目标类型
     */
    void exportPointsRecord(PointsRecordQueryBean bean,
                            HttpServletRequest request,
                            HttpServletResponse response,
                            SysUser currentUser, UserTypeEnum userTypeEnum);

    /**
     * 查询退回积分
     *
     * @param taskId       任务ID
     * @param type         type 变更类型
     * @param userTypeEnum 用户类型
     * @return amount
     */
    @Deprecated
    PointsRecord getAmountByTaskIdAndType(String taskId, Points_Record_Type type, UserTypeEnum userTypeEnum);

    /**
     * 查询变更积分数量
     *
     * @param businessId   业务id
     * @param userTypeEnum 业务类型
     * @param taskId       任务id
     * @param type         变更类型
     * @return amount
     */
    Integer getAmountByCondition(Long businessId, UserTypeEnum userTypeEnum,
                                 String taskId, Points_Record_Type type);

}
