package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 淘宝任务宝贝信息
 *
 * @author Paradise
 */
@ApiModel(value = "淘宝任务宝贝信息")
@Data
public class TbItemInfo {
    /**
     * 任务主键ID
     */
    @ApiModelProperty(value = "任务主键ID", hidden = true)
    private Long taskId;

    @ApiModelProperty(value = "宝贝链接")
    private String target;

    /**
     * 商品名称
     */
    @ApiModelProperty(value = "商品名称")
    private String itemName;

    /**
     * 图片链接
     */
    @ApiModelProperty(value = "图片链接")
    private String picUrl;

    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    /**
     * 收藏数量
     */
    @ApiModelProperty(value = "收藏数量", example = "100000")
    private Integer collectionCount;
}