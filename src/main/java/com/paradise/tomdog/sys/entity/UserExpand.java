package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Mybatis Generator 2020/03/09
 */
@ApiModel(value = "用户推广信息拓展表")
@Data
public class UserExpand {
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long uid;

    /**
     * 推广人数
     */
    @ApiModelProperty(value = "推广人数")
    private Integer spreadCount;

    /**
     * 充值人数
     */
    @ApiModelProperty(value = "充值人数")
    private Integer rechargeCount;

    /**
     * 下级累计充值金额
     */
    @ApiModelProperty(value = "充值金额")
    private BigDecimal rechargeAmount;

    /**
     * 累计获得推广积分
     */
    @ApiModelProperty(value = "累计推广积分")
    private Integer spreadPoints;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 个人累计充值
     */
    private BigDecimal selfRechargeAmount;

    /**
     * 个人累计带来的推广积分
     */
    private Integer parentSpreadPoints;

    public UserExpand() {
    }

    public UserExpand(Long uid) {
        this.uid = uid;
    }
}