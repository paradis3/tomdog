package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.BranchRechargePackage;
import com.paradise.tomdog.sys.entity.MasterRechargePackage;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.List;

/**
 * @author Paradise
 */
public interface BranchRechargePackageService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insertSelective(BranchRechargePackage record);
    
    BranchRechargePackage selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(BranchRechargePackage record);
    
    /**
     * 查询分站用户套餐列表
     *
     * @param user 当前用户
     * @return select result
     */
    List<BranchRechargePackage> getRechargePackageList(SysUser user);
    
    R updateBranchRechargePackage(BranchRechargePackage rechargePackage);
    
    int insert(BranchRechargePackage record);
    
    int updateByPrimaryKey(BranchRechargePackage record);
    
    List<BranchRechargePackage> selectByAllAndValid(SysUser user);
    
    /**
     * 初始化分站商户套餐信息
     */
    void initMerchantPackage(Long MerchantId);
}


