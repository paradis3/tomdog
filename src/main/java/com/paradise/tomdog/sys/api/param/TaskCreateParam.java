package com.paradise.tomdog.sys.api.param;

import com.paradise.tomdog.rpc.bean.BaseParamBean;
import com.paradise.tomdog.sys.entity.LlTask;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author Paradise
 */
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "Api任务参数")
@Data
public class TaskCreateParam extends BaseParamBean implements ApiParamInterface {
    /**
     * api 用户名
     */
    @ApiModelProperty(value = "用户名", example = "")
    private String username;
    /**
     * app id
     */
    @ApiModelProperty(value = "appId", example = "")
    private String appId;
    /**
     * 签名
     */
    @ApiModelProperty(value = "签名")
    private String sign;
    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳-秒数")
    private Long timestamp;
    /**
     * 任务ID
     */
    @ApiModelProperty(value = "任务ID", example = "201912260000123456abc")
    private String taskId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", hidden = true)
    private Long userId;
    /**
     * 任务类型
     */
    @ApiModelProperty(value = "任务类型", example = "0")
    private Integer type;
    /**
     * 总发布量
     */
    @ApiModelProperty(value = "总发布量", example = "1")
    private Integer count;
    /**
     * 任务天数
     */
    @ApiModelProperty(value = "任务天数", example = "1")
    private Integer days;
    /**
     * 每日任务数
     */
    @ApiModelProperty(value = "每天任务量", example = "1")
    private Integer dayCount;
    /**
     * 每小时任务执行量
     */
    @ApiModelProperty(value = "每小时任务执行量", example = "0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0")
    private String hour;
    /**
     * 任务开始时间 yyyy-MM-dd
     */
    @ApiModelProperty(value = "任务开始时间 yyyy-MM-dd", example = "2019-12-31")
    private String beginTime;
    /**
     * 商品链接 or 店铺连接
     */
    @ApiModelProperty(value = "商品链接 or 店铺连接", example = "https://item.taobao.com/item.htm?id=24281572477")
    private String target;
    /**
     * 搜索关键字
     */
    @ApiModelProperty(value = "搜索关键字", example = "搜索词")
    private String keyword;
    /**
     * 浏览时长
     */
    @ApiModelProperty(value = "浏览时长", example = "180")
    private String browseTime;
    /**
     * * 0=不浏览* 1=随机浏览* 2=深度浏览
     */
    @ApiModelProperty(value = "0:不浏览;1:随机浏览;2:深度浏览", example = "2")
    private Integer depth;

    public LlTask toLlTask(Long userId) {
        LlTask task = new LlTask();
        task.setUserId(userId);
        task.setType(this.type);
        task.setTarget(this.target);
        task.setTaskId(this.taskId);
        task.setBeginTime(this.beginTime);
        task.setCount(this.count);
        task.setDays(this.days);
        task.setDayCount(this.dayCount);
        task.setHour(this.hour);
        task.setKeywords(this.keyword);
        task.setBrowseTime(this.browseTime);
        task.setDepth(this.depth);
        return task;
    }
}