package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysSmsInfo;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.Date;
import java.util.List;

public interface SysSmsInfoService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insert(SysSmsInfo record);
    
    int insertSelective(SysSmsInfo record);
    
    SysSmsInfo selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(SysSmsInfo record);
    
    int updateByPrimaryKey(SysSmsInfo record);
    
    List<SysSmsInfo> selectByAll(SysSmsInfo sysSmsInfo);
    
    List<SysSmsInfo> selectAllByUpdateTimeAfter(Date minUpdateTime);
    
    R updateSmsInfo(SysUser user, SysSmsInfo info);
    
    void initSmsInfo(Long id);
}

