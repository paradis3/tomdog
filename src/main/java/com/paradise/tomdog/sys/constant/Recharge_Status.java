package com.paradise.tomdog.sys.constant;

/**
 * 任务状态枚举
 *
 * @author Paradise
 */
public enum Recharge_Status {
    /**
     * 未发布
     */
    ORDER_CREATE("0", "订单创建"),
    UN_PAYED("1", "待支付"),
    PAY_SUCCESS("2", "支付成功"),
    PAY_FAIL("3", "支付失败");

    private String code;
    private String name;

    Recharge_Status(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
    
    public  static String getNameBycode(String code) {
        if ("0".equals(code)) {
            return Recharge_Status.ORDER_CREATE.getName();
        }else if("1".equals(code)){
            return Recharge_Status.UN_PAYED.getName();
        }else if("2".equals(code)){
            return Recharge_Status.PAY_SUCCESS.getName();
        }else if("3".equals(code)){
            return Recharge_Status.PAY_FAIL.getName();
        }else {
            return  "未知状态";
        }
    }
}
