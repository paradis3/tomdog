package com.paradise.tomdog.sys.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author Paradise
 */
@Getter
@Setter
@ToString
public class TaskPriceBtn {
    /**
     * 自增主键
     */
    private Long id;

    /**
     * 分站商户id，总站为null
     */
    private Long merchantId;

    /**
     * 价格
     */
    private Integer price;

    /**
     * 是否总站 1：是； 0：否
     */
    private Integer isMaster;

    /**
     * 是否默认 1：是； 0：否
     */
    private Integer isDefault;

    private Date updateTime;

    public TaskPriceBtn() {
    }

    public TaskPriceBtn(Long merchantId, Integer price, Integer isMaster, Integer isDefault) {
        this.merchantId = merchantId;
        this.price = price;
        this.isMaster = isMaster;
        this.isDefault = isDefault;
    }
}