package com.paradise.tomdog.sys.controller;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.MobileMsg;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.sys.constant.GlobalConfig;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.SysBranchMerchantMapper;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.SysGlobalConfigService;
import com.paradise.tomdog.sys.service.SysSmsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Api(tags = "消息短信相关接口", description = "消息短信相关接口")
@RestController
@RequestMapping("/msg")
@Slf4j
public class MsgController {
    @Autowired
    MobileMsg mobileMsg;
    @Autowired
    private SysBranchMerchantMapper merchantMapper;
    @Autowired
    private SysSmsInfoService sysSmsInfoService;
    @Autowired
    private SysGlobalConfigService sysGlobalConfigService;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    ApplicationContext context;

    @ApiOperation("添加或修改短信模板")
    @PostMapping("/updateSmsInfo")
    @PreAuthorize("hasAuthority('pms:m:sms:update')")
    @ControllerWebLog(name = "添加或修改短信模板")
    public R updateSmsInfo(SysSmsInfo info, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isAdminType(user)) {
            info.setMerchantId(user.getId());
        }
        return sysSmsInfoService.updateSmsInfo(user, info);
    }

    @ApiOperation("查看短信模板信息")
    @PostMapping("/getSmsInfo")
    @PreAuthorize("hasAuthority('pms:mb:sms:get')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "merchantId", value = "商户id", defaultValue = "1", paramType = "query"),
    })
    public R getSmsInfo(Long merchantId, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        SysSmsInfo sysSmsInfo = new SysSmsInfo();
        if (!CommonUtils.isAdminType(user)) {
            sysSmsInfo.setMerchantId(user.getMerchantId());
        } else {
            sysSmsInfo.setMerchantId(merchantId);
        }
        List<SysSmsInfo> sysSmsInfos = sysSmsInfoService.selectByAll(sysSmsInfo);
        return Rx.success(sysSmsInfos);
    }

    @ApiOperation("修改短信扣积分配置")
    @PostMapping("/updateSmsConfig")
    @PreAuthorize("hasAuthority('pms:m:smsConfig:update')")
    @ControllerWebLog(name = "修改短信扣积分配置")
    public R updateSmsConfig(SysGlobalConfig config, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        config.setUpdater(user.getId());
        config.setUpdateTime(new Date());
        sysGlobalConfigService.updateByPrimaryKeySelective(config);
        return Rx.success("修改成功");
    }

    @ApiOperation("查看短信扣积分配置")
    @PostMapping("/getSmsConfig")
    @PreAuthorize("hasAuthority('pms:m:smsConfig:get')")
    @ControllerWebLog(name = "查看短信扣积分配置")
    public R getSmsConfig() {
        List<SysGlobalConfig> sysGlobalConfigs = sysGlobalConfigService.selectAllByConfigId(GlobalConfig.SMS.getCode());
        if (sysGlobalConfigs.size() > 0) {
            return Rx.success(sysGlobalConfigs.get(0));
        } else {
            return Rx.fail("获取失败");
        }
    }

    @ApiOperation("获取注册/忘记密码验证码(不用登录)")
    @PostMapping("/getNormalCode")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "domain", value = "域名", paramType = "query"),
            @ApiImplicitParam(name = "telephone", value = "手机号", paramType = "query"),
    })
    @ControllerWebLog(name = "获取注册/忘记密码验证码")
    public R getRegisterCode(@ApiIgnore RegisterBean bean) {
        if (StrUtil.isBlank(bean.getTelephone()) || !ParamCheckUtils.telephoneCheck(bean.getTelephone())) {
            return Rx.fail("请填写正确的手机号");
        }
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(bean.getDomain());
        if (merchant == null) {
            log.error("根据域名{}查询商户信息不存在", bean.getDomain());
            return Rx.fail("域名错误，查询商户信息不存在");
        }
        R<Balance> balanceR = balanceService.updateBalanceForSms(merchant.getId());
        if (!Rx.isSuccess(balanceR)) {
            return Rx.fail("商户积分余额异常，验证码发送失败！");
        }
        bean.setMerchantId(merchant.getId().toString());
        context.publishEvent(bean);
        return Rx.success("验证码发送成功");
    }

    @ApiOperation("获取修改手机号验证码(需登录)")
    @PostMapping("/getModificationCode")
    @PreAuthorize("hasAuthority('pms:bu:modificationCode:get')")
    @ControllerWebLog(name = "获取修改手机号验证码")
    public R getModificationCode(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        R<Balance> balanceR = balanceService.updateBalanceForSms(user.getMerchantId());
        if (!Rx.isSuccess(balanceR)) {
            return Rx.success("商户积分余额异常，验证码发送失败！");
        }
        RegisterBean bean = new RegisterBean();
        bean.setTelephone(user.getTelephone());
        bean.setMerchantId(user.getMerchantId().toString());
        context.publishEvent(bean);
        return Rx.success("验证码发送成功");
    }

}

