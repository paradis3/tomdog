package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.Banner;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.List;

/**
 * Banner Service
 *
 * @author Ynagjian
 */
public interface BannerService {

    /**
     * delete by primary key
     *
     * @param id   primaryKey
     * @param user 操作人
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer id, SysUser user);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(Banner record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(Banner record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    Banner selectByPrimaryKey(Integer id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Banner record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Banner record);

    /**
     * 更新banner
     *
     * @param user   操作人
     * @param banner 更新 record
     * @return update result
     */
    R<String> updateBanner(SysUser user, Banner banner);

    /**
     * 条件查询
     *
     * @param banner 查询条件
     * @return select result
     */
    List<Banner> selectByAll(Banner banner);

}
