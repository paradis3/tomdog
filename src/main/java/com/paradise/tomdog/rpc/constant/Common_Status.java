package com.paradise.tomdog.rpc.constant;

/**
 * 通用状态码 - 枚举
 *
 * @author Paradise
 */

public enum Common_Status {
    /**
     * 失败 - 订单未提交，未扣款（提示各种异常）
     */
    FAIL("-9", "订单未提交，未扣款（提示各种异常）"),
    /**
     * 成功
     */
    SUCCESS("1", "成功"),
    /**
     * 处理中
     */
    DEALING("0", "处理中"),
    /**
     * 取消任务中
     */
    TASK_CANCELING("8", "取消任务中"),
    /**
     * 部分退款
     */
    PARTIAL_REFUND("9", "部分退款"),
    /**
     * 全额退款
     */
    FULL_REFUND("10", "全额退款");

    Common_Status(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
