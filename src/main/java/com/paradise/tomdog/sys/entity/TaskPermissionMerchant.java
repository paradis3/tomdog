package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分站商户任务权限
 *
 * @author Paradise
 */
@Data
@ApiModel(value = "分站商户任务权限")
public class TaskPermissionMerchant {

    @ApiModelProperty(value = "权限ID", example = "201")
    private Long id;

    /**
     * 任务类型编码 TB:淘宝 JD DY PDD
     */
    @ApiModelProperty(hidden = true)
    private String taskCode;

    /**
     * 分站商户ID
     */
    @ApiModelProperty(hidden = true)
    private Long merchantId;

    /**
     * 1 有权限 0 没有权限
     */
    @ApiModelProperty(value = "权限配置(1-有权限;0-没有权限)", example = "1")
    private Integer permission;
}