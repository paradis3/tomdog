package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.UserLevel")
@Data
public class UserLevel {
    /**
     * 用户等级表主键
     */
    @ApiModelProperty(value = "用户等级表主键")
    private Integer id;
    
    /**
     * 等级名称
     */
    @ApiModelProperty(value = "等级名称")
    @NotNull(message = "等级名称不能为空")
    private String name;
    
    /**
     * 所属分站id
     */
    @ApiModelProperty(value = "所属分站id")
    @NotNull(message = "所属分站id不能为空")
    private Long merchantId;
    
    /**
     * 等级数字，越高越nb
     */
    @ApiModelProperty(value = "等级数字，越高越nb",hidden = true)
    private Integer level;
    
    /**
     * 消费折扣
     */
    @ApiModelProperty(value = "消费折扣")
    @NotNull(message = "消费折扣不能为空")
    @Max(value = 1,message = "折扣不能大于1")
    private BigDecimal discount;
    
    /**
     * 推广返比
     */
    @ApiModelProperty(value = "推广返比")
    @NotNull(message = "推广返比不能为空")
    private BigDecimal percent;
    
    /**
     * 充值升级所需价格
     */
    @ApiModelProperty(value = "充值升级所需价格")
    @NotNull(message = "充值升级所需价格不能为空")
    private Integer rechargePrice;
    
    /**
     * 推广积分累计升级标准
     */
    @ApiModelProperty(value = "推广积分累计升级标准")
    @NotNull(message = "推广积分累计升级标准不能为空")
    private Integer pointsPrice;
    
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    
    @ApiModelProperty(value = "",hidden = true)
    private Long creator;
    
    @ApiModelProperty(value = "",hidden = true)
    private Date createTime;
    
    @ApiModelProperty(value = "",hidden = true)
    private Long updater;
    
    @ApiModelProperty(value = "",hidden = true)
    private Date updateTime;
    
    /**
     * 是否允许自助升级 0禁用 1启用
     */
    @ApiModelProperty(value = "是否允许自助升级 0禁用 1启用")
    @NotNull(message = "是否允许自助升级不能为空")
    private String canUpdate;
    
    /**
     * 是否默认 0不是默认 1默认
     */
    @ApiModelProperty(value = "是否默认 0不是默认 1默认")
    @NotNull(message = "是否默认不能为空")
    private String isDefault;
    
    /**
     * 前端是否显示 0不显示 1显示
     */
    @ApiModelProperty(value = "前端是否显示 0不显示 1显示")
    @NotNull(message = "前端是否显示不能为空")
    private String isShow;
    
    /**
     * 删除标记 0正常 1删除
     */
    @ApiModelProperty(value = "删除标记 0正常 1删除")
    @NotNull(message = "删除标记不能为空")
    private String isDel;
    
    /**
     * 是否有效 0无效 1有效
     */
    @ApiModelProperty(value = "是否有效 0无效 1有效")
    @NotNull(message = "是否有效不能为空")
    private String status;
}