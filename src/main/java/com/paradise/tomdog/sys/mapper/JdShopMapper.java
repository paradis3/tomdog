package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.JdShop;
import org.apache.ibatis.annotations.Mapper;

/**
 * 京东商铺 mapper
 *
 * @author Paradise
 */
@Mapper
public interface JdShopMapper {
    /**
     * delete by primary key
     *
     * @param shopId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long shopId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(JdShop record);

    /**
     * select by primary key
     *
     * @param shopId primary key
     * @return object by primary key
     */
    JdShop selectByPrimaryKey(Long shopId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(JdShop record);
}