package com.paradise.tomdog.sys.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.paradise.tomdog.sys.entity.SysLoginInfo;
import com.paradise.tomdog.sys.mapper.SysLoginInfoMapper;
import com.paradise.tomdog.sys.service.SysLoginInfoService;

@Service
public class SysLoginInfoServiceImpl implements SysLoginInfoService {
    
    @Resource
    private SysLoginInfoMapper sysLoginInfoMapper;
    
    @Override
    public int deleteByPrimaryKey(Long infoId) {
        return sysLoginInfoMapper.deleteByPrimaryKey(infoId);
    }
    
    @Override
    public int insert(SysLoginInfo record) {
        return sysLoginInfoMapper.insert(record);
    }
    
    @Override
    public int insertSelective(SysLoginInfo record) {
        return sysLoginInfoMapper.insertSelective(record);
    }
    
    @Override
    public SysLoginInfo selectByPrimaryKey(Long infoId) {
        return sysLoginInfoMapper.selectByPrimaryKey(infoId);
    }
    
    @Override
    public int updateByPrimaryKeySelective(SysLoginInfo record) {
        return sysLoginInfoMapper.updateByPrimaryKeySelective(record);
    }
    
    @Override
    public int updateByPrimaryKey(SysLoginInfo record) {
        return sysLoginInfoMapper.updateByPrimaryKey(record);
    }
    
}


