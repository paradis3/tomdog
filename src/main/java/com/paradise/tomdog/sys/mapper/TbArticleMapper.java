package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TbArticle;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Paradise
 */
@Mapper
public interface TbArticleMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbArticle record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TbArticle selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbArticle record);
}