package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysSmsInfo;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.Date;import java.util.List;

@Mapper
public interface SysSmsInfoMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysSmsInfo record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysSmsInfo record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysSmsInfo selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysSmsInfo record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysSmsInfo record);
    
    List<SysSmsInfo> selectByAll(SysSmsInfo sysSmsInfo);
    
    List<SysSmsInfo> selectAllByUpdateTimeAfter(@Param("minUpdateTime") Date minUpdateTime);
}