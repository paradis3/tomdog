package com.paradise.tomdog.rpc.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 淘宝店铺信息
 *
 * @author Paradise
 */
@Setter
@Getter
@ApiModel
public class TbShopInfo {
    @ApiModelProperty(value = "店铺图片链接", example = "//img.alicdn.com/imgextra/i2/TB1iNwfoeL2gK0jSZFmwu37iXXa.png")
    private String picUrl;
    @ApiModelProperty(value = "店铺收藏数量", example = "25074424")
    private Integer fansNum;
    @ApiModelProperty(value = "店铺掌柜", example = "adidas官方旗舰店")
    private String seller;
    @ApiModelProperty(value = "店铺名称", example = "adidas官方旗舰店")
    private String shopName;
    @ApiModelProperty(value = "店铺ID", example = "62147762")
    private String shopId;
    private Long sellerId;

    public TbShopInfo(String picUrl, Integer fansNum, String seller, String shopName, String shopId, Long sellerId) {
        this.picUrl = picUrl;
        this.fansNum = fansNum;
        this.seller = seller;
        this.shopName = shopName;
        this.shopId = shopId;
        this.sellerId = sellerId;
    }

    @Override
    public String toString() {
        return "TbShopInfo{" +
                "picUrl='" + picUrl + '\'' +
                ", fansNum=" + fansNum +
                ", seller='" + seller + '\'' +
                ", shopName='" + shopName + '\'' +
                ", shopId='" + shopId + '\'' +
                '}';
    }
}
