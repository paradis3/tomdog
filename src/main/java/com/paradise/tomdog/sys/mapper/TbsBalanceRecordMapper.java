package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TbsBalanceRecord;
import com.paradise.tomdog.sys.vo.TbsListData;import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface TbsBalanceRecordMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbsBalanceRecord record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(TbsBalanceRecord record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TbsBalanceRecord selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(TbsBalanceRecord record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbsBalanceRecord record);
    
    /**
     * 分页查询 推广收益列表
     *
     * @param tbsBalanceRecord 查询条件
     * @return {@link TbsListData}
     */
    List<TbsListData> selectSpreadByPage(TbsBalanceRecord tbsBalanceRecord);
    

    
    /**
     * 根据userId 查询累计推广积分
     *
     * @param userId 用户ID
     * @return 累计推广积分
     */
    Integer selectSumSpreadPointsByUserId(@Param("userId") Long userId);
}