package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 分站用户推广积分余额
 *
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.TbsBalance")
@Data
@NoArgsConstructor
public class TbsBalance {
    /**
     * 用户主键
     */
    @ApiModelProperty(value = "用户主键")
    private Long userId;
    /**
     * 推广积分余额
     */
    @ApiModelProperty(value = "推广积分余额")
    private Integer spreadBalance;
    @ApiModelProperty(value = "乐观锁")
    private Integer version;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    public TbsBalance(Long userId, Integer spreadBalance) {
        this.userId = userId;
        this.spreadBalance = spreadBalance;
        this.updateTime = new Date();
    }
}