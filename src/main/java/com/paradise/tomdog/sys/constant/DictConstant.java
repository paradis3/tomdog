package com.paradise.tomdog.sys.constant;

/**
 * 字典常量定义
 *
 * @author Paradise
 */
public class DictConstant {

    public static final String ENABLE = "1";
    public static final String DISABLE = "0";
    public static final Integer ADD = 1;
    public static final Integer MINUS = 0;
}
