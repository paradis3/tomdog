package com.paradise.tomdog.sys.init;

import com.paradise.tomdog.sys.entity.TaskPrice;
import com.paradise.tomdog.sys.mapper.TaskPriceMapper;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Paradise
 */
@Slf4j
public class TaskPriceInit {

    private static Map<Integer, TaskPrice> masterTaskPriceMap = new ConcurrentHashMap<>(100);

    @Resource
    private static TaskPriceMapper taskPriceMapper;

    public static Integer getMasterPrice(int type) {
        if (masterTaskPriceMap == null || masterTaskPriceMap.size() < 1) {
            masterTaskPriceMap = taskPriceMapper.selectAllByIsMaster();
        }
        return masterTaskPriceMap.get(type).getPrice();
    }

    @PostConstruct
    public void init() {
        masterTaskPriceMap = taskPriceMapper.selectAllByIsMaster();
        log.info("总站基础价格表Map：{}", masterTaskPriceMap.size());
    }
}
