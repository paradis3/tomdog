package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.TbItemInfo;

public interface TaskTbItemService {


    int deleteByPrimaryKey(Long taskId);

    int insert(TbItemInfo record);

    TbItemInfo selectByPrimaryKey(Long taskId);

    int updateByPrimaryKey(TbItemInfo record);

}
