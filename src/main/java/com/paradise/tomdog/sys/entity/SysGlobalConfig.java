package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com-paradise-tomdog-sys-entity-SysGlobalConfig")
@Data
public class SysGlobalConfig {
    @ApiModelProperty(value = "")
    private Long id;
    
    /**
     * 配置id
     */
    @ApiModelProperty(value = "配置id")
    private String configId;
    
    /**
     * 配置名
     */
    @ApiModelProperty(value = "配置名")
    private String name;
    
    /**
     * 值
     */
    @ApiModelProperty(value = "值")
    private BigDecimal value;
    
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
     * 删除标记 1=删除
     */
    @ApiModelProperty(value = "删除标记 1=删除",hidden = true)
    private String isDelete;
    
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",hidden = true)
    private Date updateTime;
    
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人",hidden = true)
    private Long updater;
}