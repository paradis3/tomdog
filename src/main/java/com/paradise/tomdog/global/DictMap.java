package com.paradise.tomdog.global;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 项目启动后，查询数据库字典数据，初始化Map容器
 * 提供修改Map内字典数据的功能
 * 定时或者实时同步Map数据到数据库
 * 提供查询字典数据的接口
 *
 * @author Paradise
 */
@AutoConfigureAfter
public class DictMap {

    public static ConcurrentHashMap dictMap;

}
