package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Paradise
 */
@ApiModel(value = "任务信息")
@Data
public class LlTask {
    /**
     * 自增主键
     */
    @ApiModelProperty(value = "自增主键", example = "201")
    @NotNull(groups = Edit.class)
    private Long id;
    /**
     * 任务ID
     */
    @ApiModelProperty(value = "任务ID", hidden = true)
    private String taskId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", hidden = true)
    private Long userId;
    /**
     * 任务类型
     */
    @ApiModelProperty(value = "任务类型", example = "1")
    @NotNull(message = "任务类型必填")
    private Integer type;
    /**
     * 总发布量
     */
    @ApiModelProperty(value = "总发布量", example = "20")
    @NotNull(message = "总发布量必填")
    private Integer count;
    /**
     * 任务天数
     */
    @ApiModelProperty(value = "任务天数", example = "5")
    @NotNull(message = "任务天数必填")
    private Integer days;
    /**
     * 每天任务量
     */
    @ApiModelProperty(value = "每天任务量", example = "4")
    @NotNull(message = "每天任务量必填")
    private Integer dayCount;
    /**
     * 每小时任务执行量
     */
    @ApiModelProperty(value = "每小时任务执行量", example = "0,0,0,0,0,0,0,0,2,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0")
    @NotBlank(message = "每小时执行量必填")
    private String hour;
    /**
     * 任务开始时间 yyyy-MM-dd
     */
    @ApiModelProperty(value = "任务开始时间 yyyy-MM-dd", example = "2020-12-31")
    @NotBlank(message = "任务开始时间不能为空")
    private String beginTime;
    /**
     * 商品链接 or 店铺连接
     */
    @ApiModelProperty(value = "商品链接 or 店铺连接", example = "https://item.taobao.com/item.htm?id=24281572477")
    @NotBlank(message = "商品/店铺链接不能为空")
    private String target;
    /**
     * 搜索关键字
     */
    @ApiModelProperty(value = "搜索关键字", example = "Hello;world")
    private String keywords;
    /**
     * 流量时间
     */
    @ApiModelProperty(value = "流量时间", example = "180")
    private String browseTime;
    /**
     * * 0=不浏览* 1=随机浏览* 2=深度浏览
     */
    @ApiModelProperty(value = "0:不浏览;1:随机浏览;2:深度浏览", example = "2")
    private Integer depth;
    /**
     * 消耗积分
     */
    @ApiModelProperty(value = "消耗积分", example = "100")
    private Integer costPoints;
    /**
     * 任务发布时间
     */
    @ApiModelProperty(value = "任务发布时间", hidden = true)
    private Long releaseTime;
    /**
     * 完成数量
     */
    @ApiModelProperty(value = "完成数量", hidden = true)
    private Integer completedCount;
    /**
     * 任务状态
     */
    @ApiModelProperty(value = "任务状态", hidden = true)
    private String status;
    /**
     * 是否删除（1：是）
     */
    @ApiModelProperty(value = "是否删除（1：是）", hidden = true)
    private Integer isDel;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    private Long createTime;

    @ApiModelProperty(value = "用户手机号")
    private String telephone;

    public interface Add {

    }

    public interface Edit {

    }
}