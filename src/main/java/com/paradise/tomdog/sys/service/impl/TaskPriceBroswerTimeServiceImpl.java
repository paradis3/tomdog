package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.TaskPriceBrowserTime;
import com.paradise.tomdog.sys.mapper.TaskPriceBrowserTimeMapper;
import com.paradise.tomdog.sys.service.TaskPriceBroswerTimeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TaskPriceBroswerTimeServiceImpl implements TaskPriceBroswerTimeService {

    @Resource
    private TaskPriceBrowserTimeMapper taskPriceBroswerTimeMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return taskPriceBroswerTimeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(TaskPriceBrowserTime record) {
        return taskPriceBroswerTimeMapper.insert(record);
    }

    @Override
    public TaskPriceBrowserTime selectByPrimaryKey(Long id) {
        return taskPriceBroswerTimeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(TaskPriceBrowserTime record) {
        return taskPriceBroswerTimeMapper.updateByPrimaryKeySelective(record);
    }

}
