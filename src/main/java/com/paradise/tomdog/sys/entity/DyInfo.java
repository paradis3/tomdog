package com.paradise.tomdog.sys.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * 抖音信息实体类
 *
 * @author Paradise
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class DyInfo {

    /**
     * 主键 -短链接简码
     */
    private String id;

    /**
     * 视频-短链接
     */
    private String url;

    /**
     * 标题
     */
    private String title;

    /**
     * 缩略图url
     */
    private String picUrl;

    /**
     * 短视频描述
     */
    private String desc;

    /**
     * 视频作者
     */
    private String author;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 视频id
     */
    private String videoId;

    public DyInfo() {
    }
}