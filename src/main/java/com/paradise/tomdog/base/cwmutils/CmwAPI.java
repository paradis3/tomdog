package com.paradise.tomdog.base.cwmutils;


import com.paradise.tomdog.base.utils.IdWorkByTwitter;
import com.paradise.tomdog.sys.entity.BranchRechargeRecord;
import com.paradise.tomdog.sys.entity.MasterRechargeRecord;
import com.paradise.tomdog.sys.entity.SysUserPayInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class CmwAPI {
    
    public static String execute(String uri, Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        int timestamp = (int) (System.currentTimeMillis() / 1000);
        param.put("appid", info.getAppId());
        param.put("secret", info.getAppSecret());
        param.put("timestamp", String.valueOf(timestamp));
        String url = CwmUtils.createUrl(uri, param);
        String data = CwmUtils.httpget(info.getAppServer() + url);
        return data;
    }
    
    //创建订单
    public static String orderCreate(Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        String uri = "/api/order_create";
        return execute(uri, param, info);
    }
    
    //创建订单
    public static String orderCreate(BranchRechargeRecord record, SysUserPayInfo info) throws UnsupportedEncodingException {
        Map<String, String> param = new HashMap<String, String>();
        //商户订单号
        param.put("orderid", record.getOrderNo());
        //1支付宝 2微信
        param.put("channel", record.getPayType());
        // 1跳转到我们定义的收款页  0返回json
        param.put("redirect", "0");
        //金额
        param.put("price", String.valueOf(record.getPayAmount()));
        //付款成功后跳转URL
        param.put("return_url", "");
        //标题
        param.put("subject", "积分充值");
        //自定义扩展参数(扩展字段使用商户ID)
        param.put("extends_param", String.valueOf(record.getMerchantId()));
        log.info("param:" + param);
        String data = orderCreate(param, info);
        log.info("data:" + data);
        return data;
    }
    
    //创建订单
    public static String orderCreate(MasterRechargeRecord record, SysUserPayInfo info) throws UnsupportedEncodingException {
        Map<String, String> param = new HashMap<String, String>();
        param.put("orderid", record.getOrderNo());
        param.put("channel", record.getPayType());
        param.put("redirect", "0");
        param.put("price", String.valueOf(record.getPayAmount()));
        param.put("return_url", "");
        param.put("subject", "积分充值");
        //扩展字段使用商户ID,master 默认为1
        param.put("extends_param", "1");
        log.info("param:" + param);
        String data = orderCreate(param, info);
        log.info("data:" + data);
        return data;
    }
    
    //获取二维码(移动端)
    public static String qrcodeGet(Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        String uri = "/api/qrcode_get";
        return execute(uri, param, info);
    }
    
    //获取订单信息
    public static String orderQuery(Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        String uri = "/api/order_query";
        return execute(uri, param, info);
    }
    
    //刷新订单,刷新二维码有效期
    public static String orderRefresh(Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        String uri = "/api/order_refresh";
        return execute(uri, param, info);
    }
    
    //交易补单
    public static String orderAttach(Map<String, String> param, SysUserPayInfo info) throws UnsupportedEncodingException {
        //补单仅限于支付宝
        String uri = "/api/order_attach";
        return execute(uri, param, info);
    }
    
    public static void main(String[] args) throws UnsupportedEncodingException {
        IdWorkByTwitter idWorkByTwitter = new IdWorkByTwitter(0, 0);
        SysUserPayInfo sysUserPayInfo = new SysUserPayInfo();
        //流量均衡
//        sysUserPayInfo.setAppId("20191006226");
//        sysUserPayInfo.setAppSecret("41c53d493703ba88b3a5bdeca18ee969");
        //测试
        sysUserPayInfo.setAppId("20191016480");
        sysUserPayInfo.setAppSecret("bca60b00b95308985b5deb8f4f40db7a");
        sysUserPayInfo.setAppServer("http://www.caiwumao.com");
        //下单
//		Map<String, String> param=new HashMap<String, String>();
//		param.put("orderid", idWorkByTwitter.nextId()+"");//商户订单号
//		param.put("channel","1");//1支付宝 2微信
//		param.put("redirect", "0");// 1跳转到我们定义的收款页  0返回json
//		param.put("price", "0.01");//金额
//		param.put("return_url", "");//付款成功后跳转URL
//		param.put("subject", "测试");//标题
//		param.put("extends_param", "");//自定义扩展参数
//		String data=orderCreate(param,sysUserPayInfo);
//		System.out.println("param:"+param);
//		System.out.println("data:"+data);
//
////        对应的支付码为http://www.caiwumao.com/payment?id=2019100622615772748901265777
//        JSONObject result = JSON.parseObject(data);
//        if ("1".equals(result.getString("status"))) {
//            String QRurl = CwmConfig.apiHost+"/payment?id="+ result.getJSONObject("data").getString("id");
//            System.out.println(QRurl);
//        }
        
        
        //获取二维码(移动端)
//		Map<String, String> param=new HashMap<String, String>();
//		param.put("orderid", "1577276692215");//商户订单号
//		String data=qrcodeGet(param);
//		System.out.println("data:"+data);
        
        //获取订单信息
        Map<String, String> param = new HashMap<String, String>();
        param.put("orderid", "6620203302854328320");//商户订单号
        String data = orderQuery(param, sysUserPayInfo);
        System.out.println("data:" + data);
//
        //刷新订单,刷新二维码有效期
//		Map<String, String> param=new HashMap<String, String>();
//		param.put("orderid", "1577276692215");//商户订单号
//		String data=orderRefresh(param);
//      System.out.println("data:"+data);
        
        //交易补单
//		Map<String, String> param=new HashMap<String, String>();
//		param.put("orderid", "1577276692215");//商户订单号
//		param.put("pay_orderid", "1544609701796");//支付流水号，支付宝交易号
//		String data=orderAttach(param);
//		System.out.println(data);
    
    
    }
}
