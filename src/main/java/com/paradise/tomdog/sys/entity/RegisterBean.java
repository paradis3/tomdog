package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * @author Paradise
 */
@ApiModel("管理员登录实体信息")
@Data
public class RegisterBean {

    @Length(max = 20, min = 5, message = "用户名长度在5-20个字符之间")
    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    @NotEmpty(message = "密码不能为空")
    @Size(max = 20, min = 8, message = "密码长度在8-20位之间")
    private String password;

    @NotBlank(message = "手机号码不能为空")
    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "验证码")
    private String smsCode;

    @ApiModelProperty(value = "域名")
    private String domain;

    @ApiModelProperty(value = "邀请码")
    private String uid;

    @ApiModelProperty(value = "商户id", hidden = true)
    private String merchantId;

}
