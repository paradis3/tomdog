package com.paradise.tomdog.rpc.bean;

import cn.hutool.json.JSONUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * 查询淘宝店铺信息
 *
 * @author Paradise
 */
@Getter
@Setter
public class TbShopQuery {
    private String url;
    private String client;
    private String format;
    private String appid;
    private String ver;
    private String timestamp;
    private String sign;

    public TbShopQuery(String url) {
        this.url = url;
        this.client = "web";
        this.format = "json";
        this.appid = "1049";
        this.ver = "4";
        this.timestamp = "1576738962";
        this.sign = "4cc87362bbf77856f24f9fd8e7991219";
    }

    @Override
    public String toString() {
        return "TbShopQuery{" +
                "url='" + url + '\'' +
                ", client='" + client + '\'' +
                ", format='" + format + '\'' +
                ", appid='" + appid + '\'' +
                ", ver='" + ver + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }

    public String toJson() {
        return JSONUtil.toJsonStr(this);
    }
}
