package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.BranchRechargePackage;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.BranchRechargePackageMapper;
import com.paradise.tomdog.sys.service.BranchRechargePackageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class BranchRechargePackageServiceImpl implements BranchRechargePackageService {

    @Resource
    private BranchRechargePackageMapper branchRechargePackageMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return branchRechargePackageMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(BranchRechargePackage record) {
        return branchRechargePackageMapper.insertSelective(record);
    }

    @Override
    public BranchRechargePackage selectByPrimaryKey(Long id) {
        return branchRechargePackageMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(BranchRechargePackage record) {
        return branchRechargePackageMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<BranchRechargePackage> getRechargePackageList(SysUser user) {
        BranchRechargePackage branchRechargePackage = new BranchRechargePackage();
        branchRechargePackage.setMerchantId(user.getMerchantId());
        return branchRechargePackageMapper.selectByAll(new BranchRechargePackage());
    }

    /**
     * 修改分站套餐信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R updateBranchRechargePackage(BranchRechargePackage rechargePackage) {
        int num = 0;
        if (rechargePackage.getId() == null) {
            num = insertSelective(rechargePackage);
        } else {
            num = updateByPrimaryKeySelective(rechargePackage);
        }
        if (num == 1) {
            return Rx.success("添加/修改套餐信息---成功");
        } else {
            throw new RuntimeException("添加/修改套餐信息---失败");
        }
    }

    @Override
    public int insert(BranchRechargePackage record) {
        return branchRechargePackageMapper.insert(record);
    }

    @Override
    public int updateByPrimaryKey(BranchRechargePackage record) {
        return branchRechargePackageMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<BranchRechargePackage> selectByAllAndValid(SysUser user) {
        BranchRechargePackage branchRechargePackage = new BranchRechargePackage();
        if (CommonUtils.isMerchantUserType(user)) {
            branchRechargePackage.setStatus("1");
        }
        branchRechargePackage.setIsDel("0");
        if (CommonUtils.isAdminType(user)) {
            branchRechargePackage.setMerchantId(1L);
        } else {
            branchRechargePackage.setMerchantId(user.getMerchantId());
        }
        return branchRechargePackageMapper.selectByAll(branchRechargePackage);
    }

    /**
     * 初始化分站商户套餐信息
     */
    @Override
    public void initMerchantPackage(Long MerchantId) {
        //查找默认套餐
        BranchRechargePackage branchRechargePackage = new BranchRechargePackage();
        branchRechargePackage.setMerchantId(1L);
        branchRechargePackage.setStatus("1");
        branchRechargePackage.setIsDel("0");
        List<BranchRechargePackage> branchRechargePackages = branchRechargePackageMapper.selectByAll(branchRechargePackage);
        for (BranchRechargePackage rechargePackage : branchRechargePackages) {
            rechargePackage.setId(null);
            rechargePackage.setMerchantId(MerchantId);
            rechargePackage.setCreateBy(1L);
            rechargePackage.setUpdateBy(1L);
            rechargePackage.setCreateTime(new Date());
            rechargePackage.setUpdateTime(new Date());
            branchRechargePackageMapper.insertSelective(rechargePackage);
        }
    }
}



