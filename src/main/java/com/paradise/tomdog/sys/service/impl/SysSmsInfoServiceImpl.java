package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.SysSmsInfo;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.SysSmsInfoMapper;
import com.paradise.tomdog.sys.service.SysSmsInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SysSmsInfoServiceImpl implements SysSmsInfoService {

    @Resource
    private SysSmsInfoMapper sysSmsInfoMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysSmsInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SysSmsInfo record) {
        return sysSmsInfoMapper.insert(record);
    }

    @Override
    public int insertSelective(SysSmsInfo record) {
        return sysSmsInfoMapper.insertSelective(record);
    }

    @Override
    public SysSmsInfo selectByPrimaryKey(Long id) {
        return sysSmsInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SysSmsInfo record) {
        return sysSmsInfoMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysSmsInfo record) {
        return sysSmsInfoMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SysSmsInfo> selectByAll(SysSmsInfo sysSmsInfo) {
        sysSmsInfo.setIsDel("0");
        return sysSmsInfoMapper.selectByAll(sysSmsInfo);
    }

    @Override
    public List<SysSmsInfo> selectAllByUpdateTimeAfter(Date minUpdateTime) {
        return sysSmsInfoMapper.selectAllByUpdateTimeAfter(minUpdateTime);
    }

    /**
     * 修改商户短信信息
     */
    @Override
    public R updateSmsInfo(SysUser user, SysSmsInfo info) {
        int num;
        if (StringUtils.isNotEmpty(info.getTemplateId())) {
            info.setTemplateId(info.getTemplateId().replaceAll(" ", ""));
        }
        info.setUpdateBy(user.getId());
        info.setUpdateTime(new Date());
        if (info.getId() != null) {
            info.setCreateBy(user.getId());
            info.setCreateTime(new Date());
            num = updateByPrimaryKey(info);
        } else {
            num = insertSelective(info);
        }
        if (num == 1) {
            return Rx.success("添加/修改商户短信信息---成功");
        }
        throw new RuntimeException("添加/修改商户短信信息---异常");
    }

    /**
     * 初始化短信签名模板信息(暂未写，可与总站共用)
     */
    @Override
    public void initSmsInfo(Long id) {

    }
}

