package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.PointsRecord;
import com.paradise.tomdog.sys.entity.PointsRecordQueryBean;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.PointsRecordMapper;
import com.paradise.tomdog.sys.service.PointsRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paradise
 */
@Slf4j
@Service
public class PointsRecordServiceImpl implements PointsRecordService {

    @Resource
    private PointsRecordMapper pointsRecordMapper;

    @Override
    public int deleteByPrimaryKey(Long recordId) {
        return pointsRecordMapper.deleteByPrimaryKey(recordId);
    }

    @Override
    public int insert(PointsRecord record) {
        return pointsRecordMapper.insert(record);
    }

    @Override
    public PointsRecord selectByPrimaryKey(Long recordId) {
        return pointsRecordMapper.selectByPrimaryKey(recordId);
    }

    @Override
    public PageData getPointsRecordPage(PointsRecordQueryBean bean, SysUser currentUser,
                                        UserTypeEnum userTypeEnum) {
        List<PointsRecord> list = new ArrayList<>();
        bean.setUserType(userTypeEnum.getType());
        if (bean.getBeginDate() != null) {
            bean.setBeginDate(bean.getBeginDate() / 1000);
        }
        if (bean.getEndDate() != null) {
            bean.setEndDate(bean.getEndDate() / 1000);
        }
        // 查询分站商户积分记录
        if (userTypeEnum.equals(UserTypeEnum.MERCHANT_TYPE)) {
            bean.setBusinessId(bean.getMerchantId());
            PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
            list = pointsRecordMapper.selectMerchantRecord(bean);
        } else
            // 查询分站用户积分记录
            if (userTypeEnum.equals(UserTypeEnum.MERCHANT_USER_TYPE)) {
                bean.setBusinessId(bean.getUserId());
                PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
                list = pointsRecordMapper.selectUserRecord(bean);
            } else
                // 查询API商户积分记录
                if (userTypeEnum.equals(UserTypeEnum.API_TYPE)) {
                    bean.setBusinessId(bean.getApiUserId());
                    PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
                    list = pointsRecordMapper.selectApiRecord(bean);
                }
        for (PointsRecord record : list) {
            record.setTypeName(getTypeName(record.getRecordType()));
        }
        return new PageData(new PageInfo<>(list));
    }

    private String getTypeName(String type) {
        for (Points_Record_Type rt : Points_Record_Type.values()) {
            if (rt.getCode().equals(type)) {
                return rt.getDesc();
            }
        }
        return "";
    }

    @Override
    public void exportPointsRecord(PointsRecordQueryBean bean, HttpServletRequest request,
                                   HttpServletResponse response, SysUser currentUser,
                                   UserTypeEnum userTypeEnum) {
        List<PointsRecord> list = new ArrayList<>();
        bean.setUserType(userTypeEnum.getType());
        String[] rowNames = new String[0];
        String title = " ";
        if (bean.getBeginDate() != null) {
            bean.setBeginDate(bean.getBeginDate() / 1000);
        }
        if (bean.getEndDate() != null) {
            bean.setEndDate(bean.getEndDate() / 1000);
        }
        // 查询分站商户积分记录
        if (userTypeEnum.equals(UserTypeEnum.MERCHANT_TYPE)) {
            bean.setBusinessId(bean.getMerchantId());
            list = pointsRecordMapper.selectMerchantRecord(bean);
            rowNames = new String[]{"序号", "分站商户USERNAME", "分站商户名称", "记录类型", "变更数量", "生成时间", "备注"};
            title = "分站商户积分记录数据列表";
        } else
            // 查询分站用户积分记录
            if (userTypeEnum.equals(UserTypeEnum.MERCHANT_USER_TYPE)) {
                bean.setBusinessId(bean.getUserId());
                list = pointsRecordMapper.selectUserRecord(bean);
                rowNames = new String[]{"序号", "USERNAME", "手机号码", "记录类型", "变更数量", "生成时间", "备注"};
                title = "分站用户积分记录数据列表";
            } else
                // 查询API商户积分记录
                if (userTypeEnum.equals(UserTypeEnum.API_TYPE)) {
                    bean.setBusinessId(bean.getApiUserId());
                    list = pointsRecordMapper.selectApiRecord(bean);
                    rowNames = new String[]{"序号", "API商户USERNAME", "记录类型", "变更数量", "生成时间", "备注"};
                    title = "API商户积分记录数据列表";
                }

        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .request(request).response(response).title(title)
                .rowName(rowNames)
                .dataList(getDataList(list, userTypeEnum))
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }

    }

    @Override
    public PointsRecord getAmountByTaskIdAndType(String taskId, Points_Record_Type type, UserTypeEnum userTypeEnum) {
        return pointsRecordMapper.selectByTaskIdAndType(taskId, type.getCode(), userTypeEnum.getType());
    }

    @Override
    public Integer getAmountByCondition(Long businessId, UserTypeEnum userTypeEnum, String taskId, Points_Record_Type type) {
        return pointsRecordMapper.selectAmountByCondition(businessId, userTypeEnum.getType(), taskId, type.getCode());
    }


    /**
     * 解析Excel需要的数据列表
     *
     * @param list     记录数据
     * @param typeEnum 类型
     * @return List<Object [ ]>
     */
    private List<Object[]> getDataList(List<PointsRecord> list, UserTypeEnum typeEnum) {
        List<Object[]> resultList = new ArrayList<>();
        Object[] arr;
        int index = 1;
        if (typeEnum.equals(UserTypeEnum.MERCHANT_TYPE)) {
            for (PointsRecord record : list) {
                arr = resolveMerchantData(record);
                arr[0] = index++;
                resultList.add(arr);
            }
        }
        if (typeEnum.equals(UserTypeEnum.MERCHANT_USER_TYPE)) {
            for (PointsRecord record : list) {
                arr = resolveUserData(record);
                arr[0] = index++;
                resultList.add(arr);
            }
        }
        if (typeEnum.equals(UserTypeEnum.API_TYPE)) {
            for (PointsRecord record : list) {
                arr = resolveApiData(record);
                arr[0] = index++;
                resultList.add(arr);
            }
        }
        return resultList;
    }

    /**
     * 处理用户积分记录数据
     *
     * @param record 记录
     * @return object[]
     */
    private Object[] resolveUserData(PointsRecord record) {
        Object[] arr = new Object[7];
        arr[1] = record.getUserName();
        arr[2] = record.getTelephone();
        arr[3] = record.getRemark();
        arr[4] = record.getAmount();
        arr[5] = DateFormatUtils.format(record.getCreateTime());
        arr[6] = record.getRemark();
        return arr;
    }

    private Object[] resolveMerchantData(PointsRecord record) {
        Object[] arr = new Object[7];
        arr[1] = record.getUserName();
        arr[2] = record.getMerchantName();
        arr[3] = record.getRemark();
        arr[4] = record.getAmount();
        arr[5] = DateFormatUtils.format(record.getCreateTime());
        arr[6] = record.getRemark();
        return arr;
    }

    private Object[] resolveApiData(PointsRecord record) {
        Object[] arr = new Object[6];
        arr[1] = record.getUserName();
        arr[2] = record.getRemark();
        arr[3] = record.getAmount();
        arr[4] = DateFormatUtils.format(record.getCreateTime());
        arr[5] = record.getRemark();
        return arr;
    }

    private String dealRecordType(String type) {
        for (Points_Record_Type recordType : Points_Record_Type.values()) {
            if (recordType.getCode().equals(type)) {
                return recordType.getDesc();
            }
        }
        return "";
    }
}
