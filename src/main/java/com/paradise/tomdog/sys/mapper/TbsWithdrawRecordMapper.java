package com.paradise.tomdog.sys.mapper;

import java.util.Date;

import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecordWithSysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Mapper
public interface TbsWithdrawRecordMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbsWithdrawRecord record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(TbsWithdrawRecord record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TbsWithdrawRecord selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(TbsWithdrawRecord record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbsWithdrawRecord record);
    
    List<TbsWithdrawRecordWithSysUser> selectByAllJoinUser(TbsWithdrawRecord record);

    
    
}