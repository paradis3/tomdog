//package com.paradise.tomdog.base.utils;
//
//import com.alibaba.fastjson.JSON;
//import com.ztuo.bc.wallet.component.SysCode;
//import com.ztuo.bc.wallet.interceptor.SignCheckException;
//import com.ztuo.bc.wallet.mapper.BusSystemMapper;
//import com.ztuo.bc.wallet.model.BusSystem;
//import com.ztuo.bc.wallet.model.BusSystemExample;
//import com.ztuo.bc.wallet.util.RedisUtil;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.net.URLDecoder;
//import java.net.URLEncoder;
//import java.util.TreeMap;
//
///**
// * 用于接口安全验证，主要防止重放攻击，和接口鉴权
// */
//@Service
//public class SecurityService {
//
//	@Autowired
//	private RedisUtil redisUtil;
//
//	@Autowired
//	private BusSystemMapper busSystemMapper;
//
//	/**
//	 * 签名校验
//	 * @Title: sign
//	 * @param map 返回值
//	 * @return ResultDto    返回类型
//	 * @lastModify 2018年4月22日
//	 */
//	public boolean verifySign(TreeMap<String, Object> map,String sysId) {
//		if(sysId == null){
//			return false;
//		}
//		// 签名私钥
//		String publicKey = null;
//		// REDIS中有的场合
//		if (redisUtil.hasKey(SysCode.RedisStr.SYSID_PUBLIC + sysId)
//				&& StringUtils.isNoneBlank(redisUtil
//				.getString(SysCode.RedisStr.SYSID_PUBLIC + sysId))) {
//			//
//			publicKey = redisUtil.getString(SysCode.RedisStr.SYSID_PUBLIC + sysId);
//		} else {
//			BusSystemExample busSystemExample = new BusSystemExample();
//			BusSystemExample.Criteria criteria = busSystemExample.createCriteria();
//			criteria.andSysIdEqualTo(sysId);
//			BusSystem busSystem = this.busSystemMapper.selectOneByExample(busSystemExample);
//			publicKey = busSystem.getPubKey();
//			redisUtil.set(SysCode.RedisStr.SYSID_PUBLIC + sysId, publicKey);
//		}
//		// 返回签名值
//		byte[] sign = SHAUtil.decryptBASE64(URLDecoder.decode((String) map.get("sign")));
//		map.remove("sign");
//		System.out.println("publicKey->" + publicKey);
//		System.out.println("signString->" + JSON.toJSONString(map));
//		return SHAUtil.verify(JSON.toJSONString(map), SHAUtil.decryptBASE64(publicKey), sign);
//	}
//
//	/**
//	 * 对返回值进行签名
//	 * @Title: sign
//	 * @param map 返回值
//	 * @param sysId 系统ID
//	 * @return map    返回类型
//	 */
//	public  TreeMap<String, Object> sign(TreeMap<String, Object> map, String sysId) {
//		// 签名私钥
//		String privateKey = null;
//		// REDIS中有的场合
//		if (redisUtil.hasKey(SysCode.RedisStr.SYSID_PRIVATE + sysId)
//				&& StringUtils.isNoneBlank(redisUtil
//				.getString(SysCode.RedisStr.SYSID_PRIVATE + sysId))) {
//			//
//			privateKey = redisUtil.getString(SysCode.RedisStr.SYSID_PRIVATE + sysId);
//		} else {
//			BusSystemExample busSystemExample = new BusSystemExample();
//			BusSystemExample.Criteria criteria = busSystemExample.createCriteria();
//			criteria.andSysIdEqualTo(sysId);
//			BusSystem busSystem = this.busSystemMapper.selectOneByExample(busSystemExample);
//			privateKey = busSystem.getPriKey();
//			redisUtil.set(SysCode.RedisStr.SYSID_PRIVATE + sysId, privateKey);
//		}
//		// 签名
//		String sign = SHAUtil
//				.encryptBASE64(SHAUtil.sign(JSON.toJSONString(map),
//						SHAUtil.decryptBASE64(privateKey)));
//		// 设定签名
//		map.put("sign", sign);
//		// 返回签名值
//		return map;
//	}
//
//	/**
//	 * 重放攻击与时间戳验证
//	 * @Title: replayAttackCheck
//	 * @param parmsDto 设定文件
//	 * @return void    返回类型
//	 * @lastModify 2018年5月1日
//	 */
//	public void replayAttackCheck(String timeStamp, String nonce) {
//		long nowTime = System.currentTimeMillis();
//		if(Math.abs(nowTime- Long.valueOf(timeStamp)) > 1000 * 600) {
//			throw new SignCheckException("请求过期");
//		}
//		// 重放攻击check
//		if (redisUtil.hHasKey(SysCode.RedisStr.NONCE_STR, nonce)) {
//			throw new SignCheckException("请勿重复请求");
//		}
//		redisUtil.hset(SysCode.RedisStr.NONCE_STR, nonce,
//				nonce);
//		redisUtil.expire(SysCode.RedisStr.NONCE_STR,
//				3600);
//
//	}
//	public static void main(String[] args) {
//		TreeMap<String, Object> map = new TreeMap<>();
//		map.put("account","1");
//		map.put("nonce","2");
//		String timeStamp = String.valueOf(System.currentTimeMillis());
//		System.out.println("timeStamp:" + timeStamp);
//		map.put("timestamp",timeStamp);
//		map.put("sysId","app");
//		System.out.println("signString->" + JSON.toJSONString(map));
//		try {
//			byte[] signautre = SHAUtil.sign(JSON.toJSONString(map), SHAUtil.decryptBASE64("MIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCHTx3F0qNXuNy70v/UfOZWijtP/7wi1j1F8Pzbv72iLF7O60U52kiJlECOH3d7IHZzmfae/vM3xEpSneS71kViyrK6U9Mm3o8HzEeNlTPfkA/s1g8RePdeyJUKZ00TV8B4UtuClHcJtGIDGyy17R8u+gm+0RYYxJ2l6MXQ5UwZAJ7pjpRs4vMwyuQq2PFULGi4hgXSWOQPJcDduRnFM5a0weii28KwyNMA/yA4ltAdkCskq1FlybJNqpw3QMHrrsqfoSWWeqJR8OaoqNqbhC+HMYUHOz4+Miw4Kken700DL4Ty60x/5RtdD4CL/a0scVXnMe8TD0LEOsv1H5DHGB3nAgMBAAECggEAZl0oPPx5rmegNkgOWwaSi23QKVljT0ec20jRQr3wDLxcjVXX7UiCD/MkU2Di1zdb5WDY/rgJ8GqDf7UL0j7sBy0pwWShHYrJ0jBtAWOa9sraAWZ/x8wn+IDuoAw+dx+v8Fde0Y7X73OTop3wWUGmkbd/n+g2O06jpVxQKxUNWOvQ4pINkrO/7nghrfOQgiCTICcu0v8Gurnz2SXTabA6SaoBTlP+XIqnUrp8RxOHJMSEbK5Gl13vCxGvYsBhinl0uQ/SFSph5qAtuvuBBPKqGZv3PXeIwha0gDFEbdMLUpmwjwwdv6gik0cIOue2OQXteE3gC6eE6cEU5pLhmMC2YQKBgQDIycB6vmOz0BMBSoh8k/U7ZpazIQO3xWqSdMKY18/Ny7J+nPDsd4fymsxdenim3fFonuytQJxAcJy6kgHUVenotfuUWPlRg6dBd6yocApQDuBlEkXMmduknUOHzS/nMRg5gAnSi34eytPGrS2N9SMdeBYmfeg7/W1KSAfIM/5sCwKBgQCshAq8/CBvOHGUu7sD7siXfvKSJk30veIEZ8wzz0di6efOWyXHP0IjGlQkUoOeyTcn4jJIqZWWf2MLMrPobXVpeByZCsxlWc/ZjWHqFjmqiJ9bGd91BU8UwbiGEhoKPA8O7iMmfsATAQKkcVRB09Zq1nD6NH8RwfWkqjkjU/NjFQJ/IoKKko4mlMEugwpMax0DCTKYtDD4cp17s7BdwedV0AkypJBU2E+zDf+NIIPsOMHsvA6UnzJ7qJeyWF6/8b7SdSzSEK06LMhz9sya4FhhbWEhMD3zwTawiZp+ANlDYnkNsNRQ298dzi0OOReKvNtlLat2IyeAws7L+fhnXgGRuQKBgBxmfHISO7CzpRcsNKR4GfO+xWZEAg9pdxzsjZR16jWhdjTlknmTs2qd+f4ky2BHldnuBgMqT+L/w6ljRnTcGDktud+EBJQmze3ttZHY6NNnPHy97KiSICu1nJANyNWof34gvA6pTSuTvgCeW5WeryzVcrHuFIEy4iKil1d2iZnlAoGBAK4+xbK9GZOdSJ0KclM3t/Stg3nfCD78pOmNuRH3D/Eub6Tf5Cv2rtw5113qBl7cwCWWI3F/AgeAGI5O/pwvdlHkJ3uEA8OQvIuTkz3jWk+wV6kLTBVI9JguzutUsd8YpYU7HcwBGV0LvauLTtu5pfemr4mX2M+kAAPKO/fIKUyF"));
//			String signatureStr = SHAUtil.encryptBASE64(signautre);
////			signatureStr = "Cy7vuldt0P81Byr7GYq1Se2r/RlO4O91hucdV4abJWVop+oZ4LSSIQYrp5ygCNU9HQ8Z6gyHuBI07kyoayXAwFszAkAHYKBllv5kc6FeeJYgVPTl96NOW6Ez9aE7MHEF1TGgnJlPUZadoYLZZEou5gW8D1fP9/RTiTl8NdMsbeI=";
//			System.out.println("signautre->" + URLEncoder.encode(signatureStr));
//			byte[] signatureBytes = SHAUtil.decryptBASE64(signatureStr);
//
//			boolean b = SHAUtil.verify(JSON.toJSONString(map), SHAUtil.decryptBASE64("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh08dxdKjV7jcu9L/1HzmVoo7T/+8ItY9RfD827+9oixezutFOdpIiZRAjh93eyB2c5n2nv7zN8RKUp3ku9ZFYsqyulPTJt6PB8xHjZUz35AP7NYPEXj3XsiVCmdNE1fAeFLbgpR3CbRiAxsste0fLvoJvtEWGMSdpejF0OVMGQCe6Y6UbOLzMMrkKtjxVCxouIYF0ljkDyXA3bkZxTOWtMHootvCsMjTAP8gOJbQHZArJKtRZcmyTaqcN0DB667Kn6EllnqiUfDmqKjam4QvhzGFBzs+PjIsOCpHp+9NAy+E8utMf+UbXQ+Ai/2tLHFV5zHvEw9CxDrL9R+Qxxgd5wIDAQAB"), signatureBytes);
//			System.out.print("result->" + b);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
