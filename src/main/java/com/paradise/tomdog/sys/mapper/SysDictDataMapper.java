package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysDictData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Paradise
 */
@Mapper
public interface SysDictDataMapper {
    /**
     * delete by primary key
     *
     * @param dictCode primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long dictCode);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysDictData record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysDictData record);

    /**
     * select by primary key
     *
     * @param dictCode primary key
     * @return object by primary key
     */
    SysDictData selectByPrimaryKey(Long dictCode);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysDictData record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysDictData record);
}