package com.paradise.tomdog.sys.mapper;
import java.math.BigDecimal;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.paradise.tomdog.sys.entity.MasterRechargePackage;
import org.apache.ibatis.annotations.Mapper;

/**
 * 总站充值套餐
 *
 * @author Paradise
 */
@Mapper
public interface MasterRechargePackageMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(MasterRechargePackage record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(MasterRechargePackage record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    MasterRechargePackage selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(MasterRechargePackage record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(MasterRechargePackage record);
    
    List<MasterRechargePackage> selectByAll(MasterRechargePackage masterRechargePackage);

	
}