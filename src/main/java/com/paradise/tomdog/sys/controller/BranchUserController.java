package com.paradise.tomdog.sys.controller;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.MobileMsg;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.TaskPriceBtnMapper;
import com.paradise.tomdog.sys.mapper.TaskPriceDepthMapper;
import com.paradise.tomdog.sys.service.PointsRecordService;
import com.paradise.tomdog.sys.service.SysBranchMerchantService;
import com.paradise.tomdog.sys.service.SysUserService;
import com.paradise.tomdog.sys.service.TbsBalanceRecordService;
import com.paradise.tomdog.sys.vo.BranchUserVo;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Paradise
 */
@Api(tags = "分站用户：我的")
@RestController
@RequestMapping("/user")
@Slf4j
public class BranchUserController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TbsBalanceRecordService tbsBalanceRecordService;
    @Autowired
    private PointsRecordService pointsRecordService;
    @Autowired
    private SysBranchMerchantService branchMerchantService;
    @Autowired
    MobileMsg mobileMsg;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Resource
    private TaskPriceDepthMapper taskPriceDepthMapper;
    @Resource
    private TaskPriceBtnMapper taskPriceBtnMapper;

    @PostMapping("/register")
    @ApiOperation("分站用户注册（需要获取分站域名信息）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", paramType = "query", required = true),
            @ApiImplicitParam(name = "password", value = "密码", paramType = "query", required = true),
            @ApiImplicitParam(name = "telephone", value = "手机号", paramType = "query", required = true),
            @ApiImplicitParam(name = "smsCode", value = "验证码", paramType = "query", required = true),
            @ApiImplicitParam(name = "domain", value = "域名", paramType = "query", required = true),
            @ApiImplicitParam(name = "uid", value = "邀请码", paramType = "query"),
    })
    @ControllerWebLog
    public R registry(@ApiIgnore RegisterBean registerBean) {
        if (StrUtil.isBlank(registerBean.getDomain())) {
            return Rx.fail("domain不能为空");
        }
        if (StringUtils.contains(registerBean.getUsername(), ",")) {
            return Rx.fail("用户名含有非法字符");
        }
        if (StringUtils.equalsAny(registerBean.getUsername(), "admin", "branch", "api", "branch_user1", "branch_user2")) {
            return Rx.fail("用户名不能为admin/branch/api/user");
        }
        SysUser pUser = null;
        if (StringUtils.isNotBlank(registerBean.getUid())) {
            // 判断 uid 是否有效
            pUser = sysUserService.selectByInvitationCode(registerBean.getUid());
            if (pUser == null) {
                log.info("用户注册：邀请码{}无效", registerBean.getUid());
            }
        }
        return sysUserService.registerBranchUser(registerBean, pUser);
    }

    @PostMapping("/registerParamCheck")
    @ApiOperation("分站用户注册参数校验")
    public R registerParamCheck(@RequestBody RegisterParamCheckBean paramCheckBean) {
        if (StrUtil.isBlank(paramCheckBean.getDomain())) {
            return Rx.fail("domain不能为空");
        }
        if (StringUtils.contains(paramCheckBean.getUsername(), "[,|$]")) {
            return Rx.fail("用户名含有非法字符");
        }
        // 查询商户
        SysBranchMerchant merchant = branchMerchantService.selectByDomain(paramCheckBean.getDomain());
        if (merchant == null) {
            log.error("根据域名{}查询商户信息不存在", paramCheckBean.getDomain());
            return Rx.fail("根据域名{" + paramCheckBean.getDomain() + "}查询商户信息不存在");
        }

        //查询此商户是否有相同用户名的用户
        if (StrUtil.isNotEmpty(paramCheckBean.getUsername())) {
            if (!sysUserService.selectAllByUsernameAndMerchantId(paramCheckBean.getUsername(),
                    merchant.getId()).isEmpty()) {
                return Rx.fail("用户名已存在");
            }
        }
        //查询此商户是否有相同手机号的用户
        if (StrUtil.isNotEmpty(paramCheckBean.getTelephone())) {
            if (!ParamCheckUtils.telephoneCheck(paramCheckBean.getTelephone())) {
                return Rx.fail("手机号码格式不正确");
            }
            if (!sysUserService.selectAllByPhoneAndMerchantId(paramCheckBean.getTelephone(),
                    merchant.getId()).isEmpty()) {
                return Rx.fail("手机号已存在");
            }
        }
        return Rx.success();
    }


    @ApiOperation(value = "分站用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ControllerWebLog
    public R login(@ApiParam(value = "用户名密码") @RequestBody LoginBean loginBean,
                   @ApiIgnore HttpServletRequest request) {
        R<String> res = sysUserService.login(loginBean.getDomain(), loginBean.getUsername(),
                loginBean.getPassword(), request);
        if (!Rx.isSuccess(res)) {
            return res;
        }
        // 兼容手机号码登录
        SysUser user = sysUserService.selectOneByUsernameAndDomain(loginBean.getUsername(), loginBean.getDomain());
        if (user == null) {
            user = sysUserService.selectAllByPhoneAndDomain(loginBean.getUsername(), loginBean.getDomain()).get(0);
        }
        if (!CommonUtils.isMerchantUserType(user)) {
            return Rx.fail("账号不存在");
        }
        Map<String, String> tokenMap = new HashMap<>(2);
        tokenMap.put("token", res.getData());
        tokenMap.put("tokenHead", tokenHead);
        return Rx.success(tokenMap);
    }

    @ApiOperation(value = "用户修改登录密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newPassword", value = "新密码", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "旧密码", paramType = "query"),
    })
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('pms:u:user:changePassword')")
    @ControllerWebLog
    public R changePassword(String newPassword, String password, @ApiIgnore Principal principal) {
        if (StrUtil.isBlank(newPassword) || StrUtil.isBlank(password)) {
            return Rx.fail("密码不能为空");
        }
        return sysUserService.changePassword(newPassword, password, principal);
    }

    @ApiOperation(value = "忘记密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "telephone", value = "手机号", paramType = "query"),
            @ApiImplicitParam(name = "newPassword", value = "新密码", paramType = "query"),
            @ApiImplicitParam(name = "smsCode", value = "验证码", paramType = "query"),
            @ApiImplicitParam(name = "domain", value = "域名", paramType = "query"),
    })
    @RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
    @ControllerWebLog
    public R forgetPassword(String telephone, String newPassword, String smsCode, String domain) {
        if (StringUtils.isAnyBlank(telephone, newPassword, smsCode, domain)) {
            return Rx.fail("参数不完整");
        }
        return sysUserService.forgetPassword(newPassword, telephone, smsCode, domain);
    }

    @ApiOperation(value = "修改绑定手机号码")
    @GetMapping("/changeBindTelephone")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "telephone", value = "新手机号码", defaultValue = "15512345678"),
            @ApiImplicitParam(name = "code", value = "验证码", defaultValue = ">>>>>"),
    })
    @PreAuthorize("hasAuthority('pms:u:user:changeBindTelephone')")
    @ControllerWebLog
    public R changeBindTelephone(String telephone, String code, @ApiIgnore Principal principal) {
        return sysUserService.
                updateUserTelephone(telephone, code, CommonUtils.getUser(principal));
    }

    @ApiIgnore
    @ApiOperation(value = "管理员重置密码（禁用）")
    @ApiImplicitParams({@ApiImplicitParam(name = "password", value = "新密码", defaultValue = "password")})
    @RequestMapping(value = "/changePassword2", method = RequestMethod.POST)
    public R changePassword2(String password, Long id) {
        if (StringUtils.isBlank(password)) {
            return Rx.error("密码不能为空");
        }
        return sysUserService.resetPassword(password, id);
    }

    @ApiIgnore
    @ApiOperation("获取用户所有权限（包括+-权限）")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户id", defaultValue = "1")})
    @RequestMapping(value = "/permission/{id}", method = RequestMethod.GET)
    public R getPermissionList(@PathVariable Long id) {
        List<SysPermission> permissionList = sysUserService.getPermissionList(id);
        return Rx.success(permissionList);
    }

    @ApiOperation("拉取用户信息")
    @GetMapping(value = "/info")
    @PreAuthorize("hasAuthority('pms:u:user:info')")
    public R getUserInfo(@ApiIgnore Principal principal) {
        // 根据 域名查询
        // 手机号码，头像信息，会员等级，积分余额，推广用户，推广收益，推广链接
        BranchUserVo user = sysUserService.getUserInfoDetail(CommonUtils.getUserId(principal));
        if (user == null) {
            return Rx.fail("用户不存在或已被删除");
        }
        Map<String, Object> resultMap = new HashMap<>(6);
        // 设置累计充值金额为 0
        user.setSelfRechargeAmount(BigDecimal.ZERO);
        user.setRechargeAmount(BigDecimal.ZERO);
        resultMap.put("userInfo", user);
        resultMap.put("invitationCount", sysUserService.getInvitationUserCount(user.getId()));
        resultMap.put("invitationLink", getLink(user.getMerchantId(), user.getInvitationCode()));
        resultMap.put("invitationGain", tbsBalanceRecordService.selectSumSpreadPointsByUserId(user.getId()));
        resultMap.put("lastLoginInfo", sysUserService.getLastLoginInfo(user.getUsername(), user.getDomain()));
        // 查询分站权限配置信息
        resultMap.put("taskPermissionList", branchMerchantService.getTaskPermissionList(user.getMerchantId()));
        SysBranchMerchant merchant = branchMerchantService.selectById(user.getMerchantId());
        resultMap.put("merchantInfo", merchant);
        resultMap.put("QRCodeUrl", merchant.getCsUrl());

        TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectOneByMerchantId(user.getMerchantId());
        if (taskPriceBtn == null) {
            taskPriceBtn = taskPriceBtnMapper.selectDefault();
        }
        resultMap.put("taskPriceBt", taskPriceBtn);
        List<TaskPriceDepth> taskPriceDepthList = taskPriceDepthMapper.selectByMerchantId(user.getMerchantId());
        if (taskPriceDepthList == null || taskPriceDepthList.isEmpty()) {
            taskPriceDepthList = taskPriceDepthMapper.selectByIsDefault();
        }
        resultMap.put("taskPriceDepth", taskPriceDepthList);
        return Rx.success(resultMap);
    }

    private String getLink(Long merchantId, Integer code) {
        String domain = branchMerchantService.selectById(merchantId).getDomain();
        return domain + "/home/#/register?uid=" + code;
    }

    @ApiOperation("获取推广用户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", defaultValue = "10")
    })
    @GetMapping("/getInvitationUserList")
    @PreAuthorize("hasAuthority('pms:u:user:getInvitationUserList')")
    public R getInvitationUserList(@ApiIgnore Principal principal,
                                   Integer pageNum,
                                   Integer pageSize) {
        SysUser user = CommonUtils.getUser(principal);
        if (user == null || user.getStatus().equals(DictConstant.DISABLE)) {
            return Rx.fail("没有操作权限");
        }
        return sysUserService.getInvitationUserList(user, pageNum, pageSize);
    }

    @ApiOperation("用户修改自己资料")
    @PostMapping(value = "/modifyUserInfo")
    @PreAuthorize("hasAuthority('pms:u:user:modifyUserInfo')")
    public R modifyUserInfo(@Validated @RequestBody SysUserEdit user,
                            @ApiIgnore Principal principal) {
        SysUser sysUser = CommonUtils.getUser(principal);
        if (!sysUser.getId().equals(user.getId())) {
            return Rx.fail("没有操作权限");
        }
        int x = sysUserService.updateByPrimaryKeySelective(user.toUser());
        if (x == 1) {
            return Rx.success();
        } else {
            return Rx.fail();
        }
    }

    @ApiOperation("分页查询积分记录")
    @GetMapping("/getUserPointsRecordPage")
    @PreAuthorize("hasAuthority('pms:u:user:getUserPointsRecordPage')")
    public R getBranchMerchantUserPointsRecordPage(PointsRecordQueryBean bean,
                                                   @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        bean.setUserId(currentUser.getId());
        PageData pageData = pointsRecordService.getPointsRecordPage(bean, currentUser, UserTypeEnum.MERCHANT_USER_TYPE);
        return Rx.success(pageData);
    }

    @ApiOperation("查询导出分站用户积分记录")
    @GetMapping("/exportPointsRecord")
    @PreAuthorize("hasAuthority('pms:u:user:exportPointsRecord')")
    public void exportPointsRecord(PointsRecordQueryBean bean, @ApiIgnore Principal principal,
                                   HttpServletResponse response, HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        bean.setUserId(currentUser.getId());
        pointsRecordService.exportPointsRecord(bean, request, response, currentUser, UserTypeEnum.MERCHANT_USER_TYPE);
    }


}

