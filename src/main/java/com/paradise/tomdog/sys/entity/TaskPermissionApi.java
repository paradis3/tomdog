package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * API商户权限
 *
 * @author Paradise
 */
@Data
@ApiModel(value = "API商户权限配置")
public class TaskPermissionApi {
    @ApiModelProperty(value = "权限主键", example = "201")
    private Long id;

    /**
     * 任务类型编码 TB:淘宝 JD DY PDD
     */
    @ApiModelProperty(hidden = true)
    private String taskCode;

    /**
     * API用户ID
     */
    @ApiModelProperty(hidden = true)
    private Long apiId;

    /**
     * 1 有权限 0 没有权限
     */
    @ApiModelProperty(value = "权限配置(1-有权限;0-没有权限)", example = "1")
    private Integer permission;
}