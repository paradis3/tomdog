package com.paradise.tomdog.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.UserLevel;
import com.paradise.tomdog.sys.mapper.UserLevelMapper;
import com.paradise.tomdog.sys.service.UserLevelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Paradise
 */
@Service
public class UserLevelServiceImpl implements UserLevelService {

    @Resource
    private UserLevelMapper userLevelMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userLevelMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insertSelective(UserLevel record) {
        return userLevelMapper.insertSelective(record);
    }

    @Override
    public UserLevel selectByPrimaryKey(Integer id) {
        return userLevelMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(UserLevel record) {
        return userLevelMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int insert(UserLevel record) {
        return userLevelMapper.insert(record);
    }

    @Override
    public int updateByPrimaryKey(UserLevel record) {
        return userLevelMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<UserLevel> getDefaultLevelList(SysUser user) {
        UserLevel userLevel = new UserLevel();
        userLevel.setIsDel("0");
        if (CommonUtils.isAdminType(user)) {
            userLevel.setMerchantId(1L);
        } else {
            userLevel.setMerchantId(user.getMerchantId());
        }
        return userLevelMapper.selectByAll(userLevel);
    }

    @Override
    public List<UserLevel> getUserLevelList(SysUser user) {
        UserLevel userLevel = new UserLevel();
        userLevel.setIsDel("0");
        userLevel.setMerchantId(user.getMerchantId());
        if (CommonUtils.isMerchantUserType(user)) {
            userLevel.setStatus("1");
            userLevel.setIsShow("1");
        }
        return userLevelMapper.selectByAll(userLevel);
    }

    @Override
    public UserLevel getDefaultUserLevel(Long merchantId) {
        UserLevel userLevel = new UserLevel();
        userLevel.setIsDel("0");
        userLevel.setIsDefault("1");
        userLevel.setMerchantId(merchantId);
        List<UserLevel> userLevels = userLevelMapper.selectByAll(userLevel);
        if (userLevels.size() > 0) {
            return userLevels.get(0);
        } else {
            return null;
        }
    }

    @Override
    public PageData getUserLevelPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        UserLevel userLevel = new UserLevel();
        userLevel.setIsDel("0");
        return new PageData(new PageInfo<>(userLevelMapper.selectByAll(userLevel)));
    }

    /**
     * 修改用户权限
     *
     * @return select result
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R updateSysUserPayInfo(UserLevel userLevel, SysUser sysUser) {
        //如果是删除直接更新
        if (userLevel.getId() != null && "1".equals(userLevel.getIsDel())) {
            updateByPrimaryKey(userLevel);
            //查询
            UserLevel userLevelQuery = new UserLevel();
            userLevelQuery.setIsDel("0");
            userLevelQuery.setMerchantId(userLevel.getMerchantId());
            List<UserLevel> userLevels = userLevelMapper.selectByAll(userLevelQuery);
            //判断是否有默认等级
            List<UserLevel> collect = userLevels.stream().filter(u -> "1".equals(u.getIsDefault())).collect(Collectors.toList());
            if (collect.size() == 0) {
                return Rx.fail("至少需有一个默认等级");
            }
            return Rx.success("success");
        }
        //查询
        UserLevel userLevelQuery = new UserLevel();
        userLevelQuery.setIsDel("0");
        userLevelQuery.setMerchantId(userLevel.getMerchantId());
        List<UserLevel> userLevels = userLevelMapper.selectByAll(userLevelQuery);
        for (UserLevel level : userLevels) {
            boolean bool = (userLevel.getPointsPrice() >= level.getPointsPrice() && userLevel.getRechargePrice() >= level.getRechargePrice())
                    || (userLevel.getPointsPrice() <= level.getPointsPrice() && userLevel.getRechargePrice() <= level.getRechargePrice());
            if (!bool) {
                return Rx.fail("充值升级所需价格和推广积分累计升级积分，应同时比下一等级高，且同时比上一等级低");
            }
        }
        //过滤
        List<UserLevel> userLevelsFilter = new ArrayList<UserLevel>();
        if (userLevel.getId() != null) {
            userLevelsFilter.addAll(userLevels.stream().filter(u -> !u.getId().equals(userLevel.getId())).collect(Collectors.toList()));
        } else {
            userLevelsFilter.addAll(userLevels);
        }
        //添加
        userLevelsFilter.add(userLevel);
        //判断是否有默认等级
        List<UserLevel> collect = userLevelsFilter.stream().filter(u -> "1".equals(u.getIsDefault())).collect(Collectors.toList());
        if (collect.size() == 0) {
            return Rx.fail("至少需有一个默认等级");
        }
        //排序
        List<UserLevel> userLevelsSort = userLevelsFilter.stream().sorted(Comparator.comparing(UserLevel::getPointsPrice))
                .sorted(Comparator.comparing(UserLevel::getRechargePrice)).collect(Collectors.toList());
        for (int i = 0; i < userLevelsSort.size(); i++) {
            userLevelsSort.get(i).setLevel(i + 1);
            userLevelsSort.get(i).setUpdater(sysUser.getId());
            userLevelsSort.get(i).setUpdateTime(new Date());
            if (userLevelsSort.get(i).getId() != null) {
                updateByPrimaryKey(userLevelsSort.get(i));
            } else {
                userLevelsSort.get(i).setCreator(sysUser.getId());
                userLevelsSort.get(i).setCreateTime(new Date());
                insertSelective(userLevelsSort.get(i));
            }
        }
        return Rx.success("success");
    }

    @Override
    public List<UserLevel> initMerchantLevel(Long MerchantId) {
        //查询
        UserLevel userLevelQuery = new UserLevel();
        userLevelQuery.setIsDel("0");
        userLevelQuery.setMerchantId(1L);
        List<UserLevel> userLevels = userLevelMapper.selectByAll(userLevelQuery);
        for (UserLevel userLevel : userLevels) {
            userLevel.setCreator(1L);
            userLevel.setCreateTime(new Date());
            userLevel.setUpdater(1L);
            userLevel.setUpdateTime(new Date());
            userLevel.setMerchantId(MerchantId);
            userLevel.setId(null);
            userLevelMapper.insertSelective(userLevel);
        }
        return userLevels;
    }

    @Override
    public List<UserLevel> getMerchantLevelList(Long merchantId) {
        UserLevel userLevel = new UserLevel();
        userLevel.setMerchantId(merchantId);
        userLevel.setIsDel("0");
        userLevel.setStatus("1");
        userLevel.setIsShow("1");
        return userLevelMapper.selectByAll(userLevel);

    }
}



