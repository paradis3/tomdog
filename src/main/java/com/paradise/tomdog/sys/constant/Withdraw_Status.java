package com.paradise.tomdog.sys.constant;

/**
 * 任务状态枚举
 *
 * @author Paradise
 */
public enum Withdraw_Status {
    /**
     * 未发布
     */
    WITHDRAW_CREATE("0", "提现中"),
    WITHDRAW_FAIL("1", "失败"),
    WITHDRAW_SUCCESS("2", "成功");

    private String code;
    private String name;

    Withdraw_Status(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
    
    public  static String getNameBycode(String code) {
        if ("0".equals(code)) {
            return Withdraw_Status.WITHDRAW_CREATE.getName();
        }else if("1".equals(code)){
            return Withdraw_Status.WITHDRAW_FAIL.getName();
        }else if("2".equals(code)){
            return Withdraw_Status.WITHDRAW_SUCCESS.getName();
        }else {
            return  "未知状态";
        }
    }
}
