package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Paradise
 */
@ApiModel("管理员登录实体信息")
@Data
public class LoginBean {
    @ApiModelProperty(value = "用户名", required = true, example = "paradise")
    private String username;
    @ApiModelProperty(value = "密码", required = true, example = "123456")
    private String password;
    @ApiModelProperty(value = "手机号", required = true, example = "17709694505")
    private String telephone;
    @ApiModelProperty(value = "验证码", required = true, example = "123456")
    private String smsCode;
    @ApiModelProperty(value = "域名", required = true, example = "http://61.132.228.55:18081/")
    private String domain;
    @ApiModelProperty(value = "商户id", hidden = true)
    private String merchantId;
}
