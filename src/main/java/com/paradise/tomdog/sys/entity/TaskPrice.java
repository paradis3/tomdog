package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 任务价格配置表
 *
 * @author Paradise
 */
@ApiModel(value = "任务价格配置信息")
@Data
public class TaskPrice {
    /**
     * 任务价格配置主键
     */
    @ApiModelProperty(value = "任务价格配置主键")
    private Long priceId;

    /**
     * 任务类型
     */
    @ApiModelProperty(value = "任务类型")
    private Integer type;

    /**
     * 价格
     */
    @ApiModelProperty(value = "价格")
    private Integer price;

    /**
     * 任务类型名称
     */
    @ApiModelProperty(value = "任务类型名称")
    private String typeName;

    /**
     * 分站商户ID
     */
    @ApiModelProperty(value = "分站商户ID")
    private Long merchantId;

    /**
     * 是否是总站 1是 0否
     */
    @ApiModelProperty(value = "是否是总站 1是 0否")
    private Integer isMaster;
}