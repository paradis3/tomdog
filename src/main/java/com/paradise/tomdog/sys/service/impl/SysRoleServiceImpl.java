package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.SysRole;
import com.paradise.tomdog.sys.mapper.SysRoleMapper;
import com.paradise.tomdog.sys.service.SysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Paradise
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public int deleteByPrimaryKey(Long roleId, Long userId) {
        // 递归查询子节点 id 数组 - 批量更新
        return sysRoleMapper.deleteAllById(roleId, userId);
    }

    @Override
    public int insert(SysRole record) {
        return sysRoleMapper.insert(record);
    }

    @Override
    public int insertSelective(SysRole record) {
        return sysRoleMapper.insertSelective(record);
    }

    @Override
    public SysRole selectByPrimaryKey(Long id) {
        return sysRoleMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SysRole record) {
        return sysRoleMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysRole record) {
        return sysRoleMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SysRole> selectAllByPid(Long roleId, String name) {
        return sysRoleMapper.selectAllByPidAndMerchantId(roleId, null, name);
    }

    @Override
    public List<SysRole> selectAllByMerchantId(Long merchantId, String name) {
        return sysRoleMapper.selectAllByPidAndMerchantId(null, merchantId, name);
    }

    @Override
    public SysRole selectSuperRole(Long merchantId) {
        return sysRoleMapper.selectSuperRole(merchantId);
    }

    @Override
    public SysRole selectByNameAndMerchantId(String name, Long merchantId) {
        return sysRoleMapper.selectByNameAndMerchantId(name, merchantId);
    }

    @Override
    public List<SysRole> selectByAll(SysRole role) {
        return sysRoleMapper.selectByAll(role);
    }

}
