package com.paradise.tomdog.rpc.bean;

import lombok.Builder;
import lombok.Getter;

/**
 * 暂停任务 参数
 *
 * @author Paradise
 */
@Builder
@Getter
public class TaskPauseBean extends BaseParamBean {
    private String username;
    private String id;
    private Integer status;
    private String format;
    private Long timestamp;
    private Integer ver;
}
