package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-Banner")
@Data
public class Banner {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Integer id;

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 所属分站id
     */
    @ApiModelProperty(value = "所属分站id")
    private Long merchantId;

    /**
     * 类型（text：文本；url：URL）
     */
    @ApiModelProperty(value = "类型（text：文本；url：URL）")
    private String type;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String imgUrl;

    /**
     * 文本内容
     */
    @ApiModelProperty(value = "文本内容")
    private String content;

    /**
     * 是否显示
     */
    @ApiModelProperty(value = "0:不显示，1：显示")
    private String isEnable;

    /**
     * 开始时间
     */
    @ApiModelProperty(hidden = true)
    private Date startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(hidden = true)
    private Date endTime;

    /**
     * 创建时间
     */
    @ApiModelProperty(hidden = true)
    private Date createTime;

    /**
     * 创建者
     */
    @ApiModelProperty(hidden = true)
    private String createUser;

    /**
     * 更新时间
     */
    @ApiModelProperty(hidden = true)
    private Date updateTime;

    /**
     * 更新者
     */
    @ApiModelProperty(hidden = true)
    private String updateUser;

    /**
     * 排序，默认0，序号越大越靠前
     */
    @ApiModelProperty(value = "排序，默认0，序号越大越靠前")
    private Integer sort;

    @ApiModelProperty(hidden = true)
    private Integer isDel;
}