package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TaskPermissionMerchant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
public interface SysBranchMerchantService {

    /**
     * 新增分站商户信息
     *
     * @param record      分站商户信息
     * @param currentUser 当前用户信息
     * @return 处理结果 {@link R}
     */
    R addBranchMerchant(SysBranchMerchant record, SysUser currentUser);

    /**
     * 修改分站商户信息
     *
     * @param branchMerchant 商户信息
     * @param currentUser    当前用户
     * @return 处理结果 {@link R}
     */
    R modifyBranchMerchant(SysBranchMerchant branchMerchant, SysUser currentUser);

    /**
     * 根据id逻辑删除 商户
     *
     * @param id          商户id
     * @param currentUser 当前用户
     * @return 处理结果 {@link R}
     */
    R deleteById(Long id, SysUser currentUser);

    /**
     * 分页查询 分店商户数据列表
     *
     * @param branchMerchant 查询条件
     * @param pageNum        pageNum
     * @param pageSize       pageSize
     * @return 数据列表
     */
    PageData selectByPage(SysBranchMerchant branchMerchant, Integer pageNum, Integer pageSize);

    /**
     * 修改商户管理员密码
     *
     * @param id          商户id
     * @param password    新密码
     * @param currentUser 当前登录用户
     * @return 处理结果 {@link R}
     */
    R changePassword(Long id, String password, SysUser currentUser);

    /**
     * 修改商户启用禁用状态
     *
     * @param id          商户id
     * @param currentUser 当前登录用户
     * @return 处理结果 {@link R}
     */
    R modifyEnableMerchant(Long id, SysUser currentUser);

    /**
     * 根据主键查询
     *
     * @param id 主键
     * @return {@link SysBranchMerchant}
     */
    SysBranchMerchant selectById(Long id);

    /**
     * 修改商户积分
     *
     * @param id          商户主键
     * @param type        1 +积分 0 -积分
     * @param amount      数量
     * @param currentUser 当前登录用户
     * @param remark      备注
     * @return 处理结果 {@link R}
     */
    R modifyBranchMerchantPoints(Long id, Integer type, Integer amount, SysUser currentUser, String remark);

    /**
     * 导出数据列表
     *
     * @param branchMerchant 查询条件
     * @param request        请求
     * @param response       响应
     */
    void exportBranchMerchantList(SysBranchMerchant branchMerchant,
                                  HttpServletRequest request,
                                  HttpServletResponse response);

    /**
     * 查询任务权限列表
     *
     * @param merchantId 分站商户Id
     * @return {@link TaskPermissionMerchant}
     */
    List<TaskPermissionMerchant> getTaskPermissionList(Long merchantId);

    /**
     * 修改分站商户权限
     *
     * @param permissionList 权限配置
     * @return {@link R}
     */
    R modifyMerchantTaskPermission(TaskPermissionMerchant[] permissionList);

    /**
     * 根据域名查询分站商户
     *
     * @param domain 域名
     * @return {@link SysBranchMerchant}
     */
    SysBranchMerchant selectByDomain(String domain);

    List<SysBranchMerchant> selectByAll(SysBranchMerchant sysBranchMerchant);

    List<SysBranchMerchant> selectAllByUpdateTimeAfter(Date minUpdateTime);
}
