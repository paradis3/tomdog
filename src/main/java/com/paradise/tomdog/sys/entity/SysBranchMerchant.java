package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Paradise
 */
@ApiModel(value = "分站商户 - Model")
@Data
public class SysBranchMerchant {

    public interface Add {

    }

    public interface Edit {

    }

    /**
     * 分站商户主键（自增id）
     */
    @NotNull(groups = Edit.class, message = "分站商户主键不能为空")
    @ApiModelProperty(value = "分站商户主键（自增id）", example = "1")
    private Long id;

    @ApiModelProperty(hidden = true)
    private Long pid;

    @ApiModelProperty(value = "分站商户名称", example = "分站商户测试")
    private String name;

    @NotBlank(message = "分站商户手机号码不能为空")
    @Length(min = 11, max = 11, message = "商户手机号码为11位数字")
    @ApiModelProperty(value = "商户手机号码", example = "15512345678")
    private String telephone;

    /**
     * 分站商户账号
     */
    @NotBlank(message = "分站商户账号不能为空")
    @Length(message = "分账商户账号长度5-20", min = 5, max = 20)
    @ApiModelProperty(value = "分站商户账号", example = "branch_test")
    private String username;

    /**
     * 积分余额
     */
    @ApiModelProperty(value = "积分余额", hidden = true)
    private Integer points;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型", example = "1", hidden = true)
    private String type;

    /**
     * 备注
     */
    @Length(max = 255, message = "备注最长为255个字符")
    @ApiModelProperty(value = "备注", example = "分站商户测试信息")
    private String remark;

    /**
     * 绑定域名
     */
    @Length(max = 255, message = "绑定域名长度最长为255个字符")
    @ApiModelProperty(value = "绑定域名", example = "www.google.com.cn")
    private String domain;

    /**
     * 天瑞云签名id
     */
    @ApiModelProperty(value = "天瑞云签名id")
    private String smsSignId;

    /**
     * 天瑞云签名内容
     */
    @ApiModelProperty(value = "天瑞云签名内容")
    private String smsSignContent;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态", hidden = true)
    private String status;

    @ApiModelProperty(hidden = true)
    private Integer isDel;

    /**
     * 创建人id
     */
    @ApiModelProperty(value = "创建人id", hidden = true)
    private Long createBy;

    /**
     * 更新人id
     */
    @ApiModelProperty(value = "更新人id", hidden = true)
    private Long updateBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 版权信息
     */
    @ApiModelProperty(value = "版权信息")
    private String copyright;

    /**
     * ICP备案信息
     */
    @ApiModelProperty(value = "备案信息")
    private String icp;

    @ApiModelProperty(value = "首页logo图片路径")
    private String homeLogoUrl;
    @ApiModelProperty(value = "登录页logo图片路径")
    private String loginLogoUrl;
    @ApiModelProperty(value = "小图标路径")
    private String smallLogoUrl;
    @ApiModelProperty(value = "客服二维码图片")
    private String csUrl;

    @ApiModelProperty(value = "分站商户标题")
    @NotBlank(message = "分站名称不能为空")
    @Length(message = "分站名称最多50个字符", max = 50)
    private String merchantTitle;

    @ApiModelProperty(value = "登录页侧边图片url")
    private String homePhotoUrl;

    /**
     * 扩展json
     */
    @ApiModelProperty(value = "扩展json")
    private String expandJson;
}