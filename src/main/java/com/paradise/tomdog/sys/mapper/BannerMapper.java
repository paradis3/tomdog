package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.Banner;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author machine
 */
@Mapper
public interface BannerMapper {
    /**
     * delete by primary key
     *
     * @param id     primaryKey
     * @param userId 操作人ID
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer id, Long userId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(Banner record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(Banner record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    Banner selectByPrimaryKey(Integer id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(Banner record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Banner record);

    /**
     * 条件查询
     *
     * @param banner 查询条件
     * @return 列表数据
     */
    List<Banner> selectByAll(Banner banner);


}