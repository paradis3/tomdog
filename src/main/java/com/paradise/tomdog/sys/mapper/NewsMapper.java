package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.News;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NewsMapper {
    /**
     * delete by primary key
     *
     * @param id     primaryKey
     * @param userId updater
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("id") Integer id, @Param("userId") Long userId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(News record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(News record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    News selectByPrimaryKey(Integer id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(News record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(News record);

    List<News> selectByAll(News news);

    List<News> selectByAllWithoutContent(News news);


}