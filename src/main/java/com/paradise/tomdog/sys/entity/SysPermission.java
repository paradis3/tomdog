package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import lombok.Data;

/**
 * 权限表
 *
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.SysPermission")
@Data
public class SysPermission {
    /**
     * 权限主键
     */
    @ApiModelProperty(value = "权限主键", example = "1")
    private Long id;

    /**
     * 父级权限id
     */
    @ApiModelProperty(value = "父级权限id", example = "1")
    private Long pid;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 权限值
     */
    @ApiModelProperty(value = "权限值")
    private String value;

    /**
     * 菜单图标
     */
    @ApiModelProperty(value = "菜单图标")
    private String icon;

    /**
     * 类型：0 目录 1菜单 2 按钮，接口
     */
    @ApiModelProperty(value = "类型：0 目录 1菜单 2 按钮，接口")
    private String type;

    /**
     * 前端资源，组件
     */
    @ApiModelProperty(value = "前端资源，组件")
    private String uri;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态",example = "1")
    private String status;

    @ApiModelProperty(hidden = true)
    private Long creator;

    @ApiModelProperty(hidden = true)
    private Date createTime;

    @ApiModelProperty(hidden = true)
    private Long updater;

    @ApiModelProperty(hidden = true)
    private Date updateTime;
}