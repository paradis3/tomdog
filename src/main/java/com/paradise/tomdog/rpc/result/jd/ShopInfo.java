package com.paradise.tomdog.rpc.result.jd;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShopInfo {
    private String name;
    private Long shopId;
    private Integer followCount;
}
