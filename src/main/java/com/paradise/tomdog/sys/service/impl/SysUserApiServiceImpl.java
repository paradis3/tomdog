package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.AppUtils;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.global.SystemConfig;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.BalanceMapper;
import com.paradise.tomdog.sys.mapper.SysUserApiMapper;
import com.paradise.tomdog.sys.mapper.SysUserMapper;
import com.paradise.tomdog.sys.mapper.TaskPermissionApiMapper;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.SysUserApiService;
import com.paradise.tomdog.sys.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Service
@Slf4j
public class SysUserApiServiceImpl implements SysUserApiService {

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysUserApiMapper sysUserApiMapper;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private TaskPermissionApiMapper taskPermissionApiMapper;
    @Resource
    private BalanceMapper balanceMapper;
    @Resource
    private BalanceService balanceService;
    @Resource
    private SysUserService sysUserService;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysUserApiMapper.deleteByPrimaryKey(id, null);
    }

    @Override
    public int insert(SysUserApi record) {
        return sysUserApiMapper.insert(record);
    }

    @Override
    public int insertSelective(SysUserApi record) {
        return sysUserApiMapper.insertSelective(record);
    }

    @Override
    public SysUserApi selectByPrimaryKey(Long id) {
        return sysUserApiMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SysUserApi record) {
        return sysUserApiMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysUserApi record) {
        return sysUserApiMapper.updateByPrimaryKey(record);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R addApiUser(SysUserApi userApi, SysUser currentUser) {
        // 校验数据存在性 手机号码唯一，username 唯一
        if (!ParamCheckUtils.telephoneCheck(userApi.getTelephone())) {
            return Rx.fail("手机号码格式不正确");
        }
        boolean exist1 = sysUserMapper.selectByUsernameMerIsNull(userApi.getUsername()).size() > 0;
        if (exist1) {
            return Rx.fail("API商户账号: " + userApi.getUsername() + " 已存在");
        }
        boolean exist2 = sysUserMapper.selectAllByPhoneAndDomainIsNull(userApi.getTelephone()).size() > 0;
        if (exist2) {
            return Rx.fail("手机号码: " + userApi.getTelephone() + " 已存在");
        }
        // 新增API商户管理员账户信息
        SysUser user = new SysUser();
        user.setPassword(passwordEncoder.encode(SystemConfig.DEFAULT_PASSWORD));
        user.setUsername(userApi.getUsername());
        user.setRegistrationSource("注册来源：新增API商户创建默认关联账户");
        user.setRegistrationTime(new Date());
        user.setUserType(UserTypeEnum.API_TYPE.getType());
        user.setStatus(DictConstant.ENABLE);
        user.setTelephone(userApi.getTelephone());
        user.setCreateTime(new Date());
        user.setUpdater(1L);
        user.setCreator(1L);
        // 分站角色 - 设定
        user.setRoleId(null);
        int r = sysUserMapper.insert(user);
        if (r == 1 && user.getId() != null) {
            // 新增API商户信息
            userApi.setUserId(user.getId());
            userApi.setAppId(AppUtils.getAppId());
            userApi.setAppKey(userApi.getAppId());
            userApi.setAppSecret(AppUtils.getAppSecret(userApi.getAppId()));
            userApi.setUpdateBy(currentUser.getId());
            userApi.setCreateBy(currentUser.getId());
            userApi.setCreateTime(new Date());
            userApi.setUpdateTime(new Date());
            userApi.setUpdateBy(currentUser.getId());
            userApi.setCreateBy(currentUser.getId());
            int x = sysUserApiMapper.insert(userApi);
            if (x == 1) {
                if (userApi.getId() != null) {
                    // 初始化任务权限 = 1
                    taskPermissionApiMapper.initPermission(userApi.getId());
                    // 初始化积分余额
                    balanceMapper.insert(new Balance(userApi.getId(), UserTypeEnum.API_TYPE.getType(), 0));
                }
                return Rx.success(userApi);
            }
        }
        return Rx.fail();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R modifyApiUser(SysUserApi sysUserApi, SysUser currentUser) {
        // 校验数据存在性 手机号码唯一，username 唯一
        if (!ParamCheckUtils.telephoneCheck(sysUserApi.getTelephone())) {
            return Rx.fail("手机号码格式不正确");
        }
        SysUserApi originApiUser = sysUserApiMapper.selectByPrimaryKey(sysUserApi.getId());
        boolean usernameChanged = !originApiUser.getUsername().equals(sysUserApi.getUsername());
        if (usernameChanged) {
            if (sysUserMapper.selectByUsernameMerIsNull(sysUserApi.getUsername()) != null) {
                return Rx.fail("API商户账号: " + sysUserApi.getUsername() + " 已存在");
            }
        }
        boolean telChanged = !originApiUser.getTelephone().equals(sysUserApi.getTelephone());
        if (telChanged) {
            if (sysUserMapper.selectAllByPhoneAndDomainIsNull(sysUserApi.getTelephone()) != null) {
                return Rx.fail("手机号码: " + sysUserApi.getTelephone() + " 已存在");
            }
        }
        sysUserApi.setUpdateBy(currentUser.getId());
        if (sysUserApiMapper.updateByPrimaryKeySelective(sysUserApi) == 1) {
            if (usernameChanged || telChanged) {
                SysUser sysUser = new SysUser();
                sysUser.setId(sysUserApi.getUserId());
                sysUser.setTelephone(sysUserApi.getTelephone());
                sysUser.setUsername(sysUserApi.getUsername());
                sysUser.setUpdater(currentUser.getId());
                sysUser.setUpdateTime(new Date());
                sysUserMapper.updateByPrimaryKeySelective(sysUser);
            }
            return Rx.success(sysUserApi);
        }
        return Rx.fail();
    }

    @Override
    public R resetApiKeySecret(SysUser currentUser) {
        SysUserApi api = sysUserApiMapper.selectByUserId(currentUser.getId());
        if (api == null) {
            return Rx.fail("API商户不存在");
        }
        SysUserApi newApi = new SysUserApi();
        newApi.setId(api.getId());
        newApi.setAppId(AppUtils.getAppId());
        newApi.setAppKey(newApi.getAppId());
        newApi.setAppSecret(AppUtils.getAppSecret(newApi.getAppId()));
        newApi.setUpdateTime(new Date());
        newApi.setUpdateBy(currentUser.getId());

        if (sysUserApiMapper.updateByPrimaryKeySelective(newApi) == 1) {
            return Rx.success(sysUserApiMapper.selectByUserId(currentUser.getId()));
        }
        return Rx.fail();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R delApiUser(Long id, SysUser currentUser) {
        SysUserApi sysUserApi = sysUserApiMapper.selectByPrimaryKey(id);
        if (sysUserApi != null) {
            sysUserMapper.deleteByPrimaryKey(sysUserApi.getUserId(), currentUser.getId());
        }
        if (sysUserApiMapper.deleteByPrimaryKey(id, currentUser.getId()) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public SysUserApi selectById(Long id) {
        return sysUserApiMapper.selectByPrimaryKey(id);
    }

    @Override
    public R modifyEnableApiUser(Long id, SysUser currentUser) {
        SysUserApi userApi = sysUserApiMapper.selectByPrimaryKey(id);
        if (userApi == null) {
            return Rx.error("API商户不存在或已被删除");
        }
        String status = DictConstant.ENABLE.equals(userApi.getStatus()) ? DictConstant.DISABLE : DictConstant.ENABLE;
        SysUserApi sysUserApi = new SysUserApi();
        sysUserApi.setId(id);
        sysUserApi.setStatus(status);
        sysUserApi.setUpdateBy(currentUser.getId());
        sysUserApi.setUpdateTime(new Date());
        if (sysUserApiMapper.updateByPrimaryKeySelective(sysUserApi) == 1) {
            // 同时更新 用户表 status
            sysUserMapper.updateStatusById(status, userApi.getUserId(), currentUser.getId());
            return Rx.success(sysUserApi);
        }
        return Rx.fail();
    }

    @Override
    public R changePassword(Long id, String password, SysUser currentUser) {
        SysUserApi userApi = sysUserApiMapper.selectByPrimaryKey(id);
        if (userApi == null) {
            return Rx.fail("API商户不存在或已被删除");
        }
        SysUser user = sysUserMapper.selectByPrimaryKey(userApi.getUserId());
        if (user == null) {
            return Rx.fail("API商户账号信息不存在或已被删除");
        }
        int x = sysUserMapper.changePassword(userApi.getUserId(), passwordEncoder.encode(password), currentUser.getId());
        if (x == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public PageData selectByPage(SysUserApi userApi, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<SysUserApi> list = sysUserApiMapper.selectByAll(userApi);
        return new PageData(new PageInfo<>(list));
    }

    @Override
    public void exportApiUserList(SysUserApi userApi, HttpServletRequest request, HttpServletResponse response) {
        List<SysUserApi> list = sysUserApiMapper.selectByAll(userApi);
        String[] rowNames = new String[]{"序号", "API商户 ID", "USERNAME", "手机号码", "APP_ID", "APP_KEY",
                "状态（1：启用 0：禁用）", "积分余额", "创建时间", "备注"};
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .dataList(getExcelDataList(list)).rowName(rowNames).title("API商户数据列表")
                .request(request).response(response)
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    private List<Object[]> getExcelDataList(List<SysUserApi> list) {
        List<Object[]> resultList = new ArrayList<>();
        Object[] arr;
        int index = 1;
        for (SysUserApi apiUser : list) {
            arr = new Object[10];
            arr[0] = index++;
            arr[1] = apiUser.getId();
            arr[2] = apiUser.getUsername();
            arr[3] = apiUser.getTelephone();
            arr[4] = apiUser.getAppId();
            arr[5] = apiUser.getAppKey();
            arr[6] = apiUser.getStatus();
            arr[7] = apiUser.getPoints();
            arr[8] = DateFormatUtils.format(apiUser.getApplyTime());
            arr[9] = apiUser.getRemark();
            resultList.add(arr);
        }
        return resultList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R modifyApiUserPoints(Long id, Integer type, Integer amount, SysUser currentUser) {
        Points_Record_Type recordType;
        if (type.equals(DictConstant.MINUS) && amount > 0) {
            amount = -amount;
            recordType = Points_Record_Type.MASTER_MODIFY_MINUS;
        } else {
            recordType = Points_Record_Type.MASTER_MODIFY_ADD;
        }
        BalanceUpdateParam param = BalanceUpdateParam.builder().businessId(id).userTypeEnum(UserTypeEnum.API_TYPE)
                .recordType(recordType).amount(amount).createBy(currentUser.getId())
                .build();
        return balanceService.updateBalance(param);
    }

    @Override
    public List<TaskPermissionApi> getTaskPermissionList(Long apiUserId) {
        return taskPermissionApiMapper.selectAllByApiId(apiUserId);
    }

    @Override
    public R modifyMerchantTaskPermission(TaskPermissionApi[] permissionList) {
        for (TaskPermissionApi permission : permissionList) {
            if (taskPermissionApiMapper.updateByPrimaryKey(permission) != 1) {
                return Rx.fail();
            }
        }
        return Rx.success();
    }

    @Override
    public SysUserApi selectByUsername(String username) {
        return sysUserApiMapper.selectByUsername(username);
    }

    @Override
    public Integer getApiUserBalance(Long id) {
        return balanceService.queryBalance(id, UserTypeEnum.API_TYPE);
    }

    @Override
    public SysUserApi selectByUserId(Long userId) {
        return sysUserApiMapper.selectByUserId(userId);
    }
}
