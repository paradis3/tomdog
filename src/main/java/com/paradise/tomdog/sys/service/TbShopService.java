package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.TbShop;

/**
 * 淘宝店铺信息
 *
 * @author Paradise
 */
public interface TbShopService {

    /**
     * insert 1
     *
     * @param record tb_shop
     * @return insert count
     */
    int insert(TbShop record);

    /**
     * 根据url 查询
     *
     * @param url 店铺链接
     * @return select result
     */
    TbShop selectByUrl(String url);
}
