package com.paradise.tomdog.sys.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.paradise.tomdog.sys.entity.SysRolePermission;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Paradise
 */
@Mapper
public interface SysRolePermissionMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysRolePermission record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysRolePermission record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysRolePermission selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysRolePermission record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysRolePermission record);
    
    int insertList(@Param("list")List<SysRolePermission> list);

	
}