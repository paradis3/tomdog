package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-TbArticle")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class TbArticle {
    /**
     * 文章id
     */
    @ApiModelProperty(value = "文章id")
    private Long id;

    /**
     * 作者
     */
    @ApiModelProperty(value = "作者")
    private String author;

    /**
     * 阅读量
     */
    @ApiModelProperty(value = "阅读量")
    private Integer readCount;

    @ApiModelProperty(value = "")
    private String json;

    @ApiModelProperty(value = "")
    private Date updateTime;
}