package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserApi;
import com.paradise.tomdog.sys.entity.TaskPermissionApi;
import com.paradise.tomdog.sys.service.SysUserApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.security.Principal;
import java.util.List;

/**
 * @author Paradise
 */
@Api(tags = "总站：API商户管理")
@RestController
@RequestMapping("/sys/apiUser")
@Slf4j
@Validated
public class MasterApiUserController {
    private final SysUserApiService userApiService;

    @Autowired
    public MasterApiUserController(SysUserApiService userApiService) {
        this.userApiService = userApiService;
    }

    @ApiOperation("新增API商户")
    @PostMapping("/addApiUser")
    @PreAuthorize("hasAuthority('pms:m:apiUser:addApiUser')")
    public R addApiUser(@Validated({Default.class, SysUserApi.Add.class})
                        @RequestBody SysUserApi userApi,
                        @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (StringUtils.equalsAny(userApi.getUsername(),"admin","branch","api","branch_user1","branch_user2")) {
            return Rx.fail("用户名不能为admin/branch/api/user");
        }
        return userApiService.addApiUser(userApi, currentUser);
    }

    @ApiOperation("修改API商户(账号，手机号码，备注)")
    @PostMapping("/modifyApiUser")
    @PreAuthorize("hasAuthority('pms:m:apiUser:modifyApiUser')")
    public R modifyApiUser(@Validated({Default.class, SysUserApi.Edit.class})
                           @RequestBody SysUserApi sysUserApi,
                           @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return userApiService.modifyApiUser(sysUserApi, currentUser);
    }

    @ApiOperation(("删除API商户"))
    @ApiImplicitParams(@ApiImplicitParam(name = "id", value = "id", defaultValue = "201", paramType = "query"))
    @DeleteMapping("/delApiUser")
    @PreAuthorize("hasAuthority('pms:m:apiUser:delApiUser')")
    public R delApiUser(@NotNull(message = "API商户id不能为空") Long id, @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return userApiService.delApiUser(id, currentUser);
    }

    @ApiOperation(("查询API商户详情"))
    @ApiImplicitParams(@ApiImplicitParam(name = "id", value = "id", defaultValue = "201", paramType = "query"))
    @GetMapping("/getApiUser")
    @PreAuthorize("hasAuthority('pms:m:apiUser:getApiUser')")
    public R getApiUser(@NotNull(message = "API商户id不能为空") Long id) {
        return Rx.success(userApiService.selectById(id));
    }

    @ApiOperation("启用、禁用API商户（状态切换）")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", value = "id", defaultValue = "201", paramType = "query"))
    @PostMapping("/enableApiUser")
    @PreAuthorize("hasAuthority('pms:m:apiUser:enableApiUser')")
    public R enableApiUser(@NotNull(message = "API商户id不能为空") Long id,
                           @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return userApiService.modifyEnableApiUser(id, currentUser);
    }

    @ApiOperation("修改API商户管理员密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "API商户id", defaultValue = "101", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "新密码", defaultValue = "new_pw_2019", paramType = "query")
    })
    @PostMapping("/changeApiUserPassword")
    @PreAuthorize("hasAuthority('pms:m:apiUser:changeApiUserPassword')")
    public R changeApiUserPassword(@NotNull(message = "API商户id不能为空") Long id,
                                   @NotBlank(message = "密码不能为空")
                                   @Length(max = 20, min = 5, message = "密码长度5-20位字符") String password,
                                   @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return userApiService.changePassword(id, password, currentUser);
    }

    @ApiOperation("分页查询API商户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "pageNum", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "pageSize", defaultValue = "10", paramType = "query")
    })
    @GetMapping("/getApiUserPage")
    @PreAuthorize("hasAuthority('pms:m:apiUser:getApiUserPage')")
    public R getApiUserPage(SysUserApi userApi,
                            @NotNull(message = "pageNum为空！") Integer pageNum,
                            @NotNull(message = "pageSize为空！") Integer pageSize) {
        return Rx.success(userApiService.selectByPage(userApi, pageNum, pageSize));
    }

    @ApiOperation("查询导出API商户列表")
    @GetMapping("/exportApiUserList")
    @PreAuthorize("hasAuthority('pms:m:apiUser:exportApiUserList')")
    public void exportApiUserList(SysUserApi userApi,
                                  HttpServletResponse response,
                                  HttpServletRequest request) {
        userApiService.exportApiUserList(userApi, request, response);
    }

    @ApiOperation("修改API商户积分")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "修改类型：1 增加积分；0 减少积分；", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "修改积分数量", defaultValue = "2", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "API商户 ID", defaultValue = "201", paramType = "query")
    })
    @PostMapping("/modifyApiUserPoints")
    @PreAuthorize("hasAuthority('pms:m:apiUser:modifyApiUserPoints')")
    public R modifyApiUserPoints(@NotNull(message = "type 不能为空") Integer type,
                                 @NotNull(message = "amount 不能为空") Integer amount,
                                 @NotNull(message = "id 不能为空") Long id,
                                 @ApiIgnore Principal principal) {
        if (amount <= 0) {
            return Rx.fail("amount 积分格式错误");
        }
        if (!DictConstant.ADD.equals(type) && !DictConstant.MINUS.equals(type)) {
            return Rx.fail("type 参数格式错误");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        return userApiService.modifyApiUserPoints(id, type, amount, currentUser);
    }

    @ApiOperation("查询API商户任务权限列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "apiUserId", value = "API商户ID", defaultValue = "201")})
    @GetMapping("/getTaskPermissionList")
    @PreAuthorize("hasAuthority('pms:m:apiUser:getTaskPermissionList')")
    public R getTaskPermissionList(Long apiUserId) {
        List<TaskPermissionApi> permissionList = userApiService.getTaskPermissionList(apiUserId);
        return Rx.success(permissionList);
    }

    @ApiOperation("修改API商户任务权限")
    @PostMapping("/modifyApiTaskPermission")
    @PreAuthorize("hasAuthority('pms:m:apiUser:modifyApiTaskPermission')")
    public R modifyApiTaskPermission(@RequestBody TaskPermissionApi[] permissionList) {
        return userApiService.modifyMerchantTaskPermission(permissionList);
    }
}
