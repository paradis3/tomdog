package com.paradise.tomdog.base;

/**
 * 校验结果
 *
 * @author Paradise
 */
public class CheckResults {
    private boolean checkRes;
    private String msg;

    public CheckResults(boolean checkRes, String msg) {
        this.checkRes = checkRes;
        this.msg = msg;
    }

    public CheckResults() {
        this.checkRes = true;
    }

    public boolean hasErrors() {
        return !this.checkRes;
    }

    public String getMsg() {
        return msg;
    }
}
