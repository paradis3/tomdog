package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author Paradise
 */
@Api(tags = "上传相关接口")
@RestController
@RequestMapping("/upload")
@Slf4j
public class UploadController {

    @ApiOperation("上传图片")
    @PostMapping("/uploadImg")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "img", value = "图片", dataType = "__file", paramType = "form"),
    })
    public R<Map<String, String>> uploadImgPhoto(@ApiIgnore MultipartHttpServletRequest request) {
        return Rx.success(uploadImg(request));
    }

    /**
     * 上传照片到oss
     *
     * @param request 文件上传请求
     * @return 上传文件路径访问url
     */
    public Map<String, String> uploadImg(MultipartHttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        MultiValueMap<String, MultipartFile> multiFileMap = request.getMultiFileMap();
        for (String key : multiFileMap.keySet()) {
            List<MultipartFile> value = multiFileMap.get(key);
            if (value.size() > 0) {
                for (MultipartFile file : value) {
                    if (file != null && !StringUtils.isEmpty(file.getOriginalFilename())) {
                        String originalFilename = file.getOriginalFilename();
                        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
                        Random random = new Random();
                        int a = random.nextInt(10000);
                        log.info("随机数：{}", a);
                        String fileName = String.valueOf(a) + System.currentTimeMillis() + substring;
                        log.info("文件名：{}", fileName);
                        String filePath = "/home/tomdog/img/"; // 上传后的路径
                        File dest = new File(filePath + fileName);
                        if (!dest.getParentFile().exists()) {
                            boolean dirRes = dest.getParentFile().mkdirs();
                            log.info("创建文件夹res:{}", dirRes);
                        }
                        try {
                            file.transferTo(dest);
                        } catch (IOException e) {
                            log.error("文件上传报错：" + e.getLocalizedMessage(), e);
                        }
                        String filename = "https://www.baijinliuliang.com/tomdog/img/" + fileName;
                        map.put(key, filename);
                    }
                }
            }
        }
        return map;
    }
}

