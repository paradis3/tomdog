package com.paradise.tomdog.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.SysMenu;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.SysMenuMapper;
import com.paradise.tomdog.sys.service.SysMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paradise
 */
@Slf4j
@Service
public class SysMenuServiceImpl implements SysMenuService {

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysMenuMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(SysMenu record) {
        return sysMenuMapper.insert(record);
    }

    @Override
    public SysMenu selectByPrimaryKey(Long id) {
        return sysMenuMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(SysMenu record) {
        return sysMenuMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<SysMenu> selectByRole(Long roleId, Integer isMaster) {
        return sysMenuMapper.selectByRole(roleId, isMaster);
    }

    @Override
    public int initSuperRoleMenu(Long roleId, Integer isMaster) {
        return sysMenuMapper.initSuperRoleMenu(roleId, isMaster);
    }

    @Override
    public R cfgMenu(String menuIds, SysUser currentUser, Long roleId) {
        if (StrUtil.isBlank(menuIds)) {
            return Rx.success();
        }
        String[] ids = menuIds.split(",");
        Long[] idLs = new Long[ids.length];
        int i = 0;
        for (String id : ids) {
            idLs[i] = Long.parseLong(id);
            i++;
        }
        int c = sysMenuMapper.deleteCfgByRoleId(roleId);
        log.info("删除原有配置数量：{}", c);
        int x = sysMenuMapper.cfgMenu(idLs, roleId, currentUser.getId());
        if (x == ids.length) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public void listToTree(List<SysMenu> menuList) {
        // 解析出二级菜单，同时分离原始列表，只保留一级菜单
        List<SysMenu> childrenList = getChildrenList(menuList);
        for (SysMenu menu : menuList) {
            if (menu.getLevel() == 1) {
                List<SysMenu> list = getChildren(menu.getId(), childrenList);
                if (!list.isEmpty()) {
                    menu.setChildren(list);
                }
            }
        }
    }

    private List<SysMenu> getChildren(Long pid, List<SysMenu> menuList) {
        List<SysMenu> list = new ArrayList<>();
        for (SysMenu menu : menuList) {
            if (menu.getPid().equals(pid)) {
                list.add(menu);
            }
        }
        return list;
    }

    private List<SysMenu> getChildrenList(List<SysMenu> menuList) {
        List<SysMenu> list = new ArrayList<>();
        for (SysMenu menu : menuList) {
            if (menu.getLevel() == 2) {
                list.add(menu);
            }
        }
        menuList.removeAll(list);
        return list;
    }

}
