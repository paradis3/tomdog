package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysPasswordLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 密码修改记录
 *
 * @author Paradise
 */
@Mapper
public interface SysPasswordLogMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysPasswordLog record);

    /**
     * select by primary key
     *
     * @param logId primary key
     * @return object by primary key
     */
    SysPasswordLog selectByPrimaryKey(Long logId);
}