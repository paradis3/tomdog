package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.rpc.constant.Task_Add_Type;
import com.paradise.tomdog.sys.constant.Task_Status;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TaskDetail;
import com.paradise.tomdog.sys.service.LlTaskService;
import com.paradise.tomdog.sys.vo.TaskQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 后台管理：任务模块接口
 *
 * @author Paradise
 */
@Api(tags = "后台管理：任务模块接口")
@Slf4j
@RestController
@RequestMapping("/sys/task")
@Validated
public class MasterTaskController {

    private final LlTaskService taskService;

    public MasterTaskController(LlTaskService taskService) {
        this.taskService = taskService;
    }

    @ApiOperation("返回任务状态列表")
    @GetMapping("/getTaskStatusList")
    public R getTaskStatusList() {
        List<Map> list = new ArrayList<>();
        Map<String, String> map;
        for (Task_Status status : Task_Status.values()) {
            if (status.name().contains("PAUSE")) {
                continue;
            }
            map = new HashMap<>(3);
            map.put("code", status.getCode());
            map.put("name", status.getName());
            map.put("desc", status.name());
            list.add(map);
        }
        return Rx.success(list);
    }

    @ApiOperation("返回任务类型列表")
    @GetMapping("/getTaskTypeList")
    public R getTaskTypeList() {
        List<Map> list = new ArrayList<>();
        Map<String, Object> map;
        for (Task_Add_Type type : Task_Add_Type.values()) {
            map = new HashMap<>(3);
            map.put("type", type.getType());
            map.put("name", type.getName());
            map.put("code", type.name());
            list.add(map);
        }
        return Rx.success(list);
    }


    @ApiOperation("分页查询任务列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", defaultValue = "10")
    })
    @GetMapping("/getTaskPage")
    @PreAuthorize("hasAuthority('pms:mba:task:page')")
    public R getTaskPage(TaskQuery query, Integer pageNum, Integer pageSize,
                         @ApiIgnore Principal principal) {
        // 如果当前登录用户是分站商户，只查询自己分站下的用户信息
        SysUser currentUser = CommonUtils.getUser(principal);
        PageData pageData = taskService.selectTaskPage(query, pageNum, pageSize, currentUser);
        return Rx.success(pageData);
    }

    @ApiOperation("查询导出任务列表Excel")
    @GetMapping("/exportTaskList")
    @PreAuthorize("hasAuthority('pms:mba:task:export')")
    public void exportTaskList(TaskQuery query, @ApiIgnore Principal principal,
                               HttpServletRequest request,
                               HttpServletResponse response) {
        taskService.exportTaskList(query, request, response, CommonUtils.getUser(principal));
    }

    @ApiIgnore
    @ApiOperation("暂停任务# 需求删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/taskPause")
    public R taskPause(@NotNull(message = "任务主键不能为空") Long id, @ApiIgnore Principal principal) {
        return taskService.taskPause(id, CommonUtils.getUser(principal));
    }

    @ApiOperation("取消任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/taskCancel")
    @ControllerWebLog
    @PreAuthorize("hasAuthority('pms:mba:task:taskCancel')")
    public R taskCancel(@NotNull(message = "任务主键不能为空") Long id, @ApiIgnore Principal principal) {
        return taskService.taskCancel(id, CommonUtils.getUser(principal));
    }

    @ApiIgnore
    @ApiOperation("完成任务 # 需求待删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @ControllerWebLog
    @GetMapping("/taskComplete")
    public R taskComplete(@NotNull(message = "任务主键不能为空") Long id, @ApiIgnore Principal principal) {
        return taskService.taskComplete(id, CommonUtils.getUser(principal));
    }

    @ApiOperation("查询任务详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", required = true, defaultValue = "201", paramType = "query"))
    @GetMapping("/getTaskDetail")
    @PreAuthorize("hasAuthority('pms:mba:task:getTaskDetail')")
    public R getTaskDetail(@NotNull(message = "ID不能为空") Long id, @ApiIgnore Principal principal) {
        TaskDetail detail = taskService.getTaskDetail(id, CommonUtils.getUser(principal));
        if (detail == null) {
            return Rx.fail("任务不存在或已删除");
        }
        return Rx.success(detail);
    }

    @ApiOperation("查询任务 # 刷新任务状态||测试用")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @ControllerWebLog
    @GetMapping("/taskQuery")
    public R taskQuery(@NotNull(message = "任务主键不能为空") Long id) {
        return taskService.taskQuery(id);
    }
}
