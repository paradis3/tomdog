package com.paradise.tomdog.sys.entity;

public class TbsWithdrawRecordWithSysUser extends TbsWithdrawRecord {
    private SysUser sysUser;

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }
}