package com.paradise.tomdog.sys.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.Date;

import com.paradise.tomdog.sys.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface SysPermissionMapper {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(SysPermission record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysPermission record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
    SysPermission selectByPrimaryKey(Long id);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysPermission record);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysPermission record);
    /**
     * 获取指定角色权限
     */
    @Select({"<script>",
            " SELECT p.*  FROM sys_permission p " +
                    " LEFT JOIN sys_role_permission r ON r.permission_id = p.id " +
                    " WHERE r.role_id = #{roleId} " +
                    " and p.status=1 ",
            "</script>"})
    List<SysPermission> getPermissionListByRoleId(Long roleId);
    
    List<SysPermission> selectAllValue();
	
}