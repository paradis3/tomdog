package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.BranchRechargePackage;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface BranchRechargePackageMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(BranchRechargePackage record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(BranchRechargePackage record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    BranchRechargePackage selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(BranchRechargePackage record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(BranchRechargePackage record);
    
    /**
     * 条件查询套餐列表
     *
     * @param branchRechargePackage 查询条件
     * @return select result
     */
    List<BranchRechargePackage> selectByAll(BranchRechargePackage branchRechargePackage);
}