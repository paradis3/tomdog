package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author Paradise
 */
@ApiModel(value = "积分变更记录")
@Data
public class PointsRecord {
    /**
     * 记录自增主键
     */
    @ApiModelProperty(value = "记录自增主键")
    private Long recordId;

    /**
     * 业务主键
     */
    @ApiModelProperty(value = "业务主键")
    private Long businessId;

    /**
     * 业务类型
     */
    @ApiModelProperty(value = "业务类型")
    private Integer userType;

    /**
     * 记录变更类型
     */
    @ApiModelProperty(value = "记录变更类型")
    private String recordType;

    /**
     * 积分变更数量
     */
    @ApiModelProperty(value = "积分变更数量")
    private Integer amount;

    /**
     * 状态（1=有效；0：无效）
     */
    @ApiModelProperty(value = "状态（1=有效；0：无效）")
    private String status;

    /**
     * =1删除
     */
    @ApiModelProperty(value = "=1删除")
    private Integer isDel;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 关联任务id
     */
    @ApiModelProperty(value = "关联任务id")
    private String taskId;

    /**
     * 充值ID
     */
    @ApiModelProperty(value = "充值ID")
    private Long rechargeId;

    /**
     * 备注信息
     */
    @ApiModelProperty(value = "备注信息")
    private String remark;

    private transient Long userId;
    private String userName;
    private String merchantName;
    private String typeName;
    private Long telephone;
}