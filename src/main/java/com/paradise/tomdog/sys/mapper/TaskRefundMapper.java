package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskRefund;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务积分退款记录
 *
 * @author Paradise
 */
@Mapper
public interface TaskRefundMapper {
    /**
     * delete by primary key
     *
     * @param refundId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long refundId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskRefund record);

    /**
     * select by primary key
     *
     * @param refundId primary key
     * @return object by primary key
     */
    TaskRefund selectByPrimaryKey(Long refundId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskRefund record);

    /**
     * 查询全部未处理的退款信息
     *
     * @param status 状态
     * @return select result
     */
    List<TaskRefund> selectByStatus(@Param("status") String status);

    /**
     * 更新状态
     *
     * @param updatedStatus 目标状态
     * @param refundId      id
     * @return update count
     */
    int updateStatusByRefundId(@Param("updatedStatus") String updatedStatus,
                               @Param("refundId") Long refundId);


}