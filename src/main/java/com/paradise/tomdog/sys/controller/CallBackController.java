package com.paradise.tomdog.sys.controller;

import com.alibaba.fastjson.JSON;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.cwmutils.CwmUtils;
import com.paradise.tomdog.sys.service.SysUserPayInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

/**
 * @author Paradise
 */
@Api(tags = "财务猫回调接口")
@RestController
@RequestMapping("/callback")
@Slf4j
public class CallBackController {
    @Autowired
    SysUserPayInfoService sysUserPayInfoService;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    
    @ApiOperation("财务猫充值回调")
    @RequestMapping("/rechargeCallBack")
    @ApiIgnore
    @ControllerWebLog(name = "财务猫充值回调")
    public Integer rechargeCallBack(HttpServletRequest request) throws UnsupportedEncodingException {
        if (CwmUtils.verifyCwmSign(request)) {
            kafkaTemplate.send("cwmCallBack", JSON.toJSONString(CwmUtils.getParamMap(request)));
            return 1;
        }else {
            return 0;
        }
    }
    
}

