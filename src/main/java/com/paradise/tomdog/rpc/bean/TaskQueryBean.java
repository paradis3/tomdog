package com.paradise.tomdog.rpc.bean;

import lombok.Builder;
import lombok.Getter;

/**
 * 任务查询 bean
 *
 * @author Paradise
 */
@Builder
@Getter
public class TaskQueryBean extends BaseParamBean {

    private String username;
    private String id;
    private String param;
    private Integer status;
    private String date1;
    private String date2;
    private Integer page;
    private String format;
    private Long timestamp;
    private Integer ver;

}
