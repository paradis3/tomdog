package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 用户修改资料
 *
 * @author Paradise
 */
@ApiModel(value = "用户资料修改")
@Data
public class SysUserEdit {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", example = "1")
    @NotNull(message = "ID 不能为空")
    private Long id;

    @ApiModelProperty(value = "手机号码")
    @NotBlank(message = "手机号码不能为空")
    private String telephone;

    @ApiModelProperty(value = "用户等级")
    private Integer userLevel;

    @ApiModelProperty(value = "备注信息")
    private String remark;

    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名", example = "张三")
    @Length(max = 10, min = 2, message = "真实姓名字符长度2-10")
    private String realName;

    @ApiModelProperty(value = "QQ", example = "2124507210")
    private String qq;

    @ApiModelProperty(value = "微信", example = "1568897441")
    private String wechat;

    @ApiModelProperty(value = "旺旺", example = "wang123456")
    private String wangwang;

    @ApiModelProperty(value = "邮箱", example = "12457895@qq.com")
    @Email
    private String email;

    /**
     * 性别 1 男 0 女
     */
    @ApiModelProperty(value = "性别 1 男 0 女", example = "1")
    private String sex;

    public SysUser toUser() {
        SysUser user = new SysUser();
        user.setId(this.id);
        user.setQq(this.qq);
        user.setEmail(this.email);
        user.setWangwang(this.wangwang);
        user.setWechat(this.wechat);
        user.setSex(this.sex);
        user.setRealName(this.realName);
        user.setTelephone(this.telephone);
        user.setRemark(this.remark);
        user.setUserLevel(this.userLevel);
        return user;
    }

}