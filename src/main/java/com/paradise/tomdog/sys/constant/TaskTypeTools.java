package com.paradise.tomdog.sys.constant;

import com.paradise.tomdog.rpc.constant.Task_Add_Type;

/**
 * 任务类型工具
 *
 * @author Paradise
 */
public class TaskTypeTools {

    public static Task_Add_Type getType(int type) {
        for (Task_Add_Type taskAddType : Task_Add_Type.values()) {
            if (taskAddType.getType() == type) {
                return taskAddType;
            }
        }
        return Task_Add_Type.NULL;
    }

    public static boolean equal(int type, Task_Add_Type taskAddType) {
        return getType(type).equals(taskAddType);
    }

    public static boolean isTbTask(Task_Add_Type type) {
        return type.name().startsWith("TB");
    }

    public static boolean isTbItemTask(Task_Add_Type type) {
        return type.name().startsWith("TB_ITEM");
    }

    public static boolean isTbShopTask(Task_Add_Type type) {
        return type.name().startsWith("TB_SHOP");
    }

    public static boolean isPddShopTask(Task_Add_Type type) {
        return type.name().startsWith("PDD_SHOP");
    }

    public static boolean isDyTask(Task_Add_Type type) {
        return type.name().startsWith("DY_");
    }

    public static boolean isTbArticleTask(Task_Add_Type type) {
        return type.name().startsWith("TB_ARTICLE");
    }

    public static boolean isPddItemTask(Task_Add_Type type) {
        return type.name().startsWith("PDD_ITEM");
    }

    public static boolean isJdItemTask(Task_Add_Type type) {
        return type.name().startsWith("JD_ITEM");
    }

    public static boolean isJdShopTask(Task_Add_Type type) {
        return type.name().startsWith("JD_SHOP");
    }

    public static String getTypePrefix(Task_Add_Type type) {
        return type.name().split("_")[0];
    }

    public static boolean needKeyword(Task_Add_Type type) {
        return type.name().contains("SEARCH");
    }

    public static boolean needDeptAndBrowseTime(Task_Add_Type type) {
        return type.name().contains("BT");
    }

    public static boolean needDeptBrowserTime(int type) {
        return needDeptAndBrowseTime(getType(type));
    }

    public static void main(String[] args) {
        System.out.println(getTypePrefix(Task_Add_Type.DY_COMMENT));
    }
}
