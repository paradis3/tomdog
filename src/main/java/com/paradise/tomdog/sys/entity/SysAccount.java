package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Paradise
 */
@ApiModel("管理员实体信息")
@Data
public class SysAccount {
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;

    @NotBlank(message = "用户名不能为空")
    @Length(max = 20, min = 5, message = "用户名长度在5-20个字符之间")
    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "手机号码")
    @NotBlank(message = "手机号码不能为空")
    private String telephone;

    @ApiModelProperty(value = "密码")
    @NotEmpty(message = "密码不能为空")
    @Size(max = 20, min = 8, message = "密码长度在8-20位之间")
    private String password;

    @ApiModelProperty(value = "性别 1 男 0 女", example = "1")
    private String sex;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @ApiModelProperty(hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(hidden = true)
    private Long updateBy;

    @ApiModelProperty(hidden = true)
    private Long createBy;

    @ApiModelProperty(hidden = true)
    private String status;

    @ApiModelProperty(hidden = true)
    private Long merchantId;
    @ApiModelProperty(hidden = true)
    private String roleName;
    @ApiModelProperty(hidden = true)
    private String createByUserName;
    @ApiModelProperty(hidden = true)
    private Integer userType;

}
