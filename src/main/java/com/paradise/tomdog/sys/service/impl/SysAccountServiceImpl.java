package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.SysAccount;
import com.paradise.tomdog.sys.mapper.SysUserMapper;
import com.paradise.tomdog.sys.service.SysAccountService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Paradise
 */
@Service
public class SysAccountServiceImpl implements SysAccountService {
    @Resource
    private SysUserMapper userMapper;
    @Resource
    private PasswordEncoder passwordEncoder;

    /**
     * 新增
     *
     * @param account 账户信息
     * @return insert count
     */
    @Override
    public int insert(SysAccount account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        return userMapper.insertAccount(account);
    }

    /**
     * 更新
     *
     * @param account 账户信息
     * @return update count
     */
    @Override
    public int updateByPrimaryKey(SysAccount account) {
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        return userMapper.updateAccount(account);
    }

    @Override
    public List<SysAccount> selectAllByRoleId(Long roleId, SysAccount account) {
        return userMapper.selectAllByRoleId(roleId, account);
    }

    @Override
    public List<SysAccount> selectAllByMerchantId(SysAccount account) {
        return userMapper.selectAllByRoleId(null, account);
    }
}
