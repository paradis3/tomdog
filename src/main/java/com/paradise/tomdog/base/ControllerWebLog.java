package com.paradise.tomdog.base;

import java.lang.annotation.*;

/**
 * @author Paradise
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ControllerWebLog {
    String name() default "";

    boolean intoDb() default false;

}

