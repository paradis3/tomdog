package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserApi;
import com.paradise.tomdog.sys.entity.TaskPermissionApi;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * api商户管理Service
 *
 * @author Paradise
 */
public interface SysUserApiService {


    int deleteByPrimaryKey(Long id);

    int insert(SysUserApi record);

    int insertSelective(SysUserApi record);

    SysUserApi selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserApi record);

    int updateByPrimaryKey(SysUserApi record);

    /**
     * 新增api商户
     *
     * @param userApi     api 信息
     * @param currentUser 当前用户
     * @return {@link R}
     */
    R addApiUser(SysUserApi userApi, SysUser currentUser);

    /**
     * 修改api商户
     *
     * @param sysUserApi  修改api商户信息
     * @param currentUser 当前用户
     * @return {@link R}
     */
    R modifyApiUser(SysUserApi sysUserApi, SysUser currentUser);

    /**
     * 重置 api key
     *
     * @param currentUser 当前api商户
     * @return {@link R}
     */
    R resetApiKeySecret(SysUser currentUser);

    /**
     * 删除 api商户信息
     *
     * @param id          api商户id
     * @param currentUser 当前登录人
     * @return {@link R}
     */
    R delApiUser(Long id, SysUser currentUser);

    /**
     * 查询api商户信息
     *
     * @param id 主键
     * @return {@link SysUserApi}
     */
    SysUserApi selectById(Long id);

    /**
     * 修改 api商户 状态
     *
     * @param id          主键
     * @param currentUser 当前登录人
     * @return {@link R}
     */
    R modifyEnableApiUser(Long id, SysUser currentUser);

    /**
     * 修改密码
     *
     * @param id          api商户主键
     * @param password    新密码
     * @param currentUser 当前登录人
     * @return {@link R}
     */
    R changePassword(Long id, String password, SysUser currentUser);

    /**
     * 分页查询
     *
     * @param userApi  查询参数
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @return {@linkplain PageData}
     */
    PageData selectByPage(SysUserApi userApi, Integer pageNum, Integer pageSize);

    /**
     * 导出excel
     *
     * @param userApi  查询参数
     * @param request  请求
     * @param response 响应
     */
    void exportApiUserList(SysUserApi userApi, HttpServletRequest request, HttpServletResponse response);

    /**
     * 修改api商户积分信息
     *
     * @param id          api商户主键
     * @param type        +- 积分
     * @param amount      积分数量
     * @param currentUser 当前登录人
     * @return {@linkplain R}
     */
    R modifyApiUserPoints(Long id, Integer type, Integer amount, SysUser currentUser);

    /**
     * 查询任务权限列表
     *
     * @param apiUserId Api商户Id
     * @return {@link TaskPermissionApi}
     */
    List<TaskPermissionApi> getTaskPermissionList(Long apiUserId);

    /**
     * 修改分站商户权限
     *
     * @param permission 权限配置
     * @return {@link R}
     */
    R modifyMerchantTaskPermission(TaskPermissionApi[] permission);

    /**
     * 根据username 查询 api user
     *
     * @param username 用户名
     * @return {@link SysUserApi}
     */
    SysUserApi selectByUsername(String username);

    /**
     * 查询Api 商户积分余额
     *
     * @param id app user id
     * @return balance
     */
    Integer getApiUserBalance(Long id);

    /**
     * 根据用户id 查询API商户信息
     *
     * @param userId 用户id
     * @return {@link SysUserApi}
     */
    SysUserApi selectByUserId(Long userId);
}
