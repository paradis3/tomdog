package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.UserLevel;

import java.util.List;

public interface UserLevelService {


    int deleteByPrimaryKey(Integer id);

    int insertSelective(UserLevel record);

    UserLevel selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserLevel record);

    int insert(UserLevel record);

    int updateByPrimaryKey(UserLevel record);

    /**
     * 查询默认用户等级列表
     *
     * @return select result
     */

    List<UserLevel> getDefaultLevelList(SysUser user);

    /**
     * 查询用户等级列表
     *
     * @return select result
     */
    List<UserLevel> getUserLevelList(SysUser user);

    /**
     * 查找商户用户默认等级
     *
     * @return select result
     */
    UserLevel getDefaultUserLevel(Long merchantId);

    /**
     * 分页查询用户等级列表
     *
     * @return select result
     */
    PageData getUserLevelPage(Integer pageNum, Integer pageSize);

    /**
     * 修改用户权限
     *
     * @return select result
     */
    R updateSysUserPayInfo(UserLevel userLevel, SysUser sysUser);

    /**
     * 初始化分站商户等级（升级、返利）列表
     *
     * @param MerchantId 商户id
     */
    List<UserLevel> initMerchantLevel(Long MerchantId);

    /**
     * 查询商户等级列表
     *
     * @param merchantId 商户id
     * @return List<UserLevel>
     */
    List<UserLevel> getMerchantLevelList(Long merchantId);
}



