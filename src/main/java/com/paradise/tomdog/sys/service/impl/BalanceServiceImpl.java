package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.constant.GlobalConfig;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.Balance;
import com.paradise.tomdog.sys.entity.BalanceUpdateParam;
import com.paradise.tomdog.sys.entity.SysGlobalConfig;
import com.paradise.tomdog.sys.mapper.BalanceMapper;
import com.paradise.tomdog.sys.mapper.PointsRecordMapper;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.SysGlobalConfigService;
import com.paradise.tomdog.sys.task.utils.LlTaskUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * 余额操作封装
 *
 * @author Paradise
 */
@Service
public class BalanceServiceImpl implements BalanceService {

    @Resource
    private BalanceMapper balanceMapper;
    @Resource
    private PointsRecordMapper pointsRecordMapper;
    @Resource
    private SysGlobalConfigService sysGlobalConfigService;

    @Override
    public Integer queryBalance(Long businessId, UserTypeEnum userTypeEnum) {
        Balance balance = balanceMapper.selectByBusinessIdAndType(businessId, userTypeEnum.getType());
        if (balance != null) {
            return balance.getBalance();
        } else {
            balanceMapper.insert(new Balance(businessId, userTypeEnum.getType(), 0));
        }
        return 0;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public R<Balance> updateBalance(Long businessId, UserTypeEnum userTypeEnum,
                                    int amount, Points_Record_Type recordType) {
        BalanceUpdateParam param = BalanceUpdateParam.builder().amount(amount).
                businessId(businessId).recordType(recordType)
                .userTypeEnum(userTypeEnum).build();
        return this.updateBalance(param);
    }

    /**
     * 扣除发短信的费用
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R<Balance> updateBalanceForSms(Long businessId) {
        List<SysGlobalConfig> sysGlobalConfigs = sysGlobalConfigService.selectAllByConfigId(GlobalConfig.SMS.getCode());
        if (sysGlobalConfigs.size() > 0) {
            SysGlobalConfig sysGlobalConfig = sysGlobalConfigs.get(0);
            BalanceUpdateParam param = BalanceUpdateParam.builder().amount(-sysGlobalConfig.getValue().intValue()).
                    businessId(businessId).recordType(Points_Record_Type.SMS_CONSUME)
                    .userTypeEnum(UserTypeEnum.MERCHANT_TYPE).build();
            return this.updateBalance(param);
        } else {
            return Rx.fail("短信发送失败！");
        }

    }
    @Transactional(rollbackFor = Exception.class)
    @Override
    public R<Balance> updateBalance(BalanceUpdateParam param) {
        Long businessId = param.getBusinessId();
        int userType = param.getUserTypeEnum().getType();
        Balance balance = balanceMapper.selectByBusinessIdAndType(businessId, userType);
        if (balance == null) {
            balanceMapper.insert(new Balance(businessId, userType, 0));
            balance = balanceMapper.selectByBusinessIdAndType(businessId, userType);
        }
        if (balance.getBalance() + param.getAmount() < 0) {
            return Rx.fail("积分余额不足");
        }
        // 记录积分变更日志
        pointsRecordMapper.insert(LlTaskUtils.initPointsRecord(param));
        balance.setAmount(param.getAmount());
        int x = balanceMapper.updateByBalanceIdWithCas(balance);
        if (x == 1) {
            balance = balanceMapper.selectByPrimaryKey(balance.getBalanceId());
            return Rx.success(balance);
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return Rx.fail();
    }
    
}

