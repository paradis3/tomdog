package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysDictType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Paradise
 */
@Mapper
public interface SysDictTypeMapper {
    /**
     * delete by primary key
     *
     * @param dictId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long dictId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysDictType record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysDictType record);

    /**
     * select by primary key
     *
     * @param dictId primary key
     * @return object by primary key
     */
    SysDictType selectByPrimaryKey(Long dictId);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysDictType record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysDictType record);
}