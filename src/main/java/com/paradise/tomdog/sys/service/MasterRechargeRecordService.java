package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.MasterRechargeRecord;
import com.paradise.tomdog.sys.entity.MasterRechargeRecordQueryBean;
import com.paradise.tomdog.sys.entity.SysUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

public interface MasterRechargeRecordService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insert(MasterRechargeRecord record);
    
    int insertSelective(MasterRechargeRecord record);
    
    MasterRechargeRecord selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(MasterRechargeRecord record);
    
    int updateByPrimaryKey(MasterRechargeRecord record);
    
    List<MasterRechargeRecord> selectByAll(MasterRechargeRecord masterRechargeRecord);
    
    /**
     * 分站用户创建积分充值订单
     * @param packageId 选择的套餐id
     * @param user      当前登录用户
     * @param payType   支付方式
     * @return {@link R}
     */
    R createOrder(Long packageId, SysUser user, String payType);
    
    R replaceOrder(Long recordId, SysUser user, String status);
    
    PageData getRechargePage(MasterRechargeRecordQueryBean queryBean , SysUser user);
    
    List<MasterRechargeRecord> selectAllByUpdateTimeBefore(Date minUpdateTime);
    /**
     * 导出充值订单
     */
     void exportMasterRechargeRecord(MasterRechargeRecordQueryBean queryBean,SysUser user, HttpServletRequest request,
                                           HttpServletResponse response);
    
}




