package com.paradise.tomdog.sys.constant;

/**
 * 任务状态枚举
 *
 * @author Paradise
 */
public enum Task_Status {
    /**
     * 未发布
     */
    INIT("0", "未发布"),
    ING("1", "进行中"),
    CANCEL_ING("2", "申请取消中"),
    CANCELED("3", "已取消"),
    COMPLETED("4", "已完成"),
    PAUSE_ING("5", "申请暂停中"),
    PAUSED("6", "已暂停"),
    PART_COMPLETED("7", "部分完成"),
    ;

    private String code;
    private String name;

    Task_Status(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
