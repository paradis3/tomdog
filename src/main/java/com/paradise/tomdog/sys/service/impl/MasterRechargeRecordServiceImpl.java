package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.cwmutils.CmwAPI;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.base.utils.IdWorkByTwitter;
import com.paradise.tomdog.sys.constant.IsDel;
import com.paradise.tomdog.sys.constant.Recharge_Status;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.MasterRechargePackageMapper;
import com.paradise.tomdog.sys.mapper.MasterRechargeRecordMapper;
import com.paradise.tomdog.sys.mapper.SysUserPayInfoMapper;
import com.paradise.tomdog.sys.service.MasterRechargeRecordService;
import com.paradise.tomdog.sys.service.SysUserPayInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
@Slf4j
public class MasterRechargeRecordServiceImpl implements MasterRechargeRecordService {

    @Resource
    private MasterRechargeRecordMapper masterRechargeRecordMapper;
    @Resource
    private MasterRechargePackageMapper masterRechargePackageMapper;
    @Autowired
    private IdWorkByTwitter idWorkByTwitter;
    @Autowired
    private SysUserPayInfoService sysUserPayInfoService;
    @Resource
    private SysUserPayInfoMapper sysUserPayInfoMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public int deleteByPrimaryKey(Long id) {
        return masterRechargeRecordMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(MasterRechargeRecord record) {
        return masterRechargeRecordMapper.insert(record);
    }

    @Override
    public int insertSelective(MasterRechargeRecord record) {
        return masterRechargeRecordMapper.insertSelective(record);
    }

    @Override
    public MasterRechargeRecord selectByPrimaryKey(Long id) {
        return masterRechargeRecordMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(MasterRechargeRecord record) {
        return masterRechargeRecordMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(MasterRechargeRecord record) {
        return masterRechargeRecordMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<MasterRechargeRecord> selectByAll(MasterRechargeRecord masterRechargeRecord) {
        return masterRechargeRecordMapper.selectByAll(masterRechargeRecord);
    }

    @Override
    public R createOrder(Long packageId, SysUser user, String payType) {
        MasterRechargePackage rechargePackage = masterRechargePackageMapper.selectByPrimaryKey(packageId);
        if (rechargePackage == null
                || StringUtils.equals(rechargePackage.getIsDel(), "1")
                || StringUtils.equals(rechargePackage.getStatus(), "0")) {
            return Rx.fail("套餐不存在或已被删除");
        }
        MasterRechargeRecord record = new MasterRechargeRecord();
        record.setUserId(user.getId());
        record.setMerchantId(user.getMerchantId());
        record.setRechargePoints(rechargePackage.getPoints() + rechargePackage.getGivePoints());
        record.setPayAmount(rechargePackage.getPrice());
        record.setPayType(payType);
        record.setOrderNo(String.valueOf(idWorkByTwitter.nextId()));
        record.setPackageId(packageId);
        record.setCreateTime(new Date());
        record.setCreateBy(user.getId());
        record.setUpdateTime(new Date());
        record.setUpdateBy(user.getId());
        record.setStatus(Recharge_Status.ORDER_CREATE.getCode());
        record.setIsDel(IsDel.NO.getCode());
        int num = masterRechargeRecordMapper.insertSelective(record);
        if (num == 1) {
            try {
                SysUserPayInfo sysUserPayInfo = sysUserPayInfoMapper.selectOneByMerchantId(1L);
                JSONObject object = JSON.parseObject(CmwAPI.orderCreate(record, sysUserPayInfo));
                if (object != null && StringUtils.equals(object.getString("status"), "1")) {
                    String payNo = object.getJSONObject("data").getString("id");
//                    record.setPayNo(payNo);
                    record.setStatus(Recharge_Status.UN_PAYED.getCode());
                    masterRechargeRecordMapper.updateByPrimaryKey(record);
                    String payUrl = sysUserPayInfoService.getPayUrl(user, payNo, sysUserPayInfo);
                    return Rx.success(payUrl);
                } else {
                    log.error("订单创建失败：{}", object);
                    DentalCabotClient.sendText("订单创建失败：");
                    return Rx.fail("订单创建失败");
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return Rx.fail("订单创建失败");
    }

    /**
     * 手动补单
     *
     * @param recordId
     * @param user
     * @param status
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R replaceOrder(Long recordId, SysUser user, String status) {
        MasterRechargeRecord masterRechargeRecord = masterRechargeRecordMapper.selectByPrimaryKey(recordId);
        if (masterRechargeRecord == null
                || StringUtils.equals(masterRechargeRecord.getIsDel(), "1")
                || StringUtils.equals(masterRechargeRecord.getStatus(), "2")) {
            return Rx.fail("订单不存在或充值成功");
        }
        if (StringUtils.equals("2", status)) {
            //1重新查询财务 2 直接置为成功
            masterRechargeRecord.setUpdateBy(1L);
            masterRechargeRecord.setStatus("1");
            masterRechargeRecord.setUpdateTime(new Date());
            masterRechargeRecordMapper.updateByPrimaryKey(masterRechargeRecord);
            //向kafka发送充值成功消息
            HashMap<String, String> paramMap = new HashMap<>();
            //商户订单号
            paramMap.put("orderid", masterRechargeRecord.getOrderNo());
            //1：收款成功；0：待支付；-1：二维码生成中；11：二维码生成失败；12：订单超时未支付'
            paramMap.put("status", "1");
            //支付金额 单位：元。精确小数点后2位
            paramMap.put("facevalue", masterRechargeRecord.getPayAmount().toString());
            //财务猫平台生成的唯一支付流水订单号
            paramMap.put("id", "123");
            //时间戳
            paramMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
            //扩展字段代表使用商户ID
            paramMap.put("extends_param", "1");
            kafkaTemplate.send("cwmCallBack", JSON.toJSONString(paramMap));
        } else if (StringUtils.equals("1", status)) {
            //1重新查询财务 2 直接置为成功
            masterRechargeRecord.setUpdateBy(1L);
            masterRechargeRecord.setStatus("1");
            masterRechargeRecordMapper.updateByPrimaryKey(masterRechargeRecord);
        }
        return Rx.success("设置成功");
    }


    @Override
    public PageData getRechargePage(MasterRechargeRecordQueryBean queryBean, SysUser user) {
        PageHelper.startPage(queryBean.getPageNum(), queryBean.getPageSize());
        if (CommonUtils.isApiType(user)) {
            queryBean.setUserId(user.getId());
        }
        if (CommonUtils.isMerchantType(user)) {
            queryBean.setMerchantId(user.getMerchantId());
        } else if (!CommonUtils.isAdminType(user)) {
            throw new RuntimeException("非法访问");
        }
        List<MasterRechargeRecordWithSysUserWithSysBranchMerchant> rechargeRecords
                = masterRechargeRecordMapper.selectAllByQueryBean(queryBean);
        return new PageData(new PageInfo<>(rechargeRecords));

    }

    /**
     * 查找待支付的订单
     *
     * @param minUpdateTime
     * @return
     */
    @Override
    public List<MasterRechargeRecord> selectAllByUpdateTimeBefore(Date minUpdateTime) {
        return masterRechargeRecordMapper.selectAllByUpdateTimeBefore(minUpdateTime);
    }

    /**
     * 导出充值订单
     */
    @Override
    public void exportMasterRechargeRecord(MasterRechargeRecordQueryBean queryBean, SysUser user, HttpServletRequest request,
                                           HttpServletResponse response) {
        if (CommonUtils.isMerchantType(user) || CommonUtils.isApiType(user)) {
            queryBean.setMerchantId(user.getMerchantId());
        } else if (CommonUtils.isApiType(user)) {
            queryBean.setUserId(user.getId());
        } else if (!CommonUtils.isAdminType(user)) {
            throw new RuntimeException("非法访问");
        }
        // 根据条件查询分站商户充值记录
        List<MasterRechargeRecordWithSysUserWithSysBranchMerchant> list
                = masterRechargeRecordMapper.selectAllByQueryBean(queryBean);
        String[] rowNames = new String[]{"序号", "用户名", "商户名", "支付金额", "充值积分", "订单号", "支付流水号", "支付状态",
                "创建时间", "支付时间",};
        String title = "充值记录数据列表";
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .request(request).response(response).title(title)
                .rowName(rowNames)
                .dataList(getDataList(list))
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    /**
     * 解析Excel需要的数据列表
     *
     * @param list 记录数据
     */
    private List<Object[]> getDataList(List<MasterRechargeRecordWithSysUserWithSysBranchMerchant> list) {
        List<Object[]> resultList = new ArrayList<>();
        int index = 1;
        for (MasterRechargeRecordWithSysUserWithSysBranchMerchant record : list) {
            Object[] arr = new Object[11];
            arr[0] = index++;
            arr[1] = record.getSysUser().getUsername();
            arr[2] = record.getSysBranchMerchant().getName();
            arr[3] = record.getPayAmount();
            arr[4] = record.getRechargePoints();
            arr[5] = record.getOrderNo();
            arr[6] = record.getPayNo();
            arr[7] = Recharge_Status.getNameBycode(record.getStatus());
            arr[8] = DateFormatUtils.format(record.getCreateTime());
            arr[9] = DateFormatUtils.format(record.getPayTime());
            arr[10] = record.getRemark();
            resultList.add(arr);
        }
        return resultList;
    }

}




