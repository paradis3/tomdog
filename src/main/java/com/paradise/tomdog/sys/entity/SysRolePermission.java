package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "com.paradise.tomdog.sys.entity.SysRolePermission")
@Data
public class SysRolePermission {
    /**
     * 角色权限主键
     */
    @ApiModelProperty(value = "角色权限主键", example = "1")
    private Long id;

    @ApiModelProperty(value = "角色id", example = "1")
    private Long roleId;

    /**
     * 权限id
     */
    @ApiModelProperty(value = "权限id", example = "2")
    private Long permissionId;
}