package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.MasterRechargePackage;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.MasterRechargePackageMapper;
import com.paradise.tomdog.sys.service.MasterRechargePackageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MasterRechargePackageServiceImpl implements MasterRechargePackageService {
    
    @Resource
    private MasterRechargePackageMapper masterRechargePackageMapper;
    
    @Override
    public int deleteByPrimaryKey(Long id) {
        return masterRechargePackageMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    public int insertSelective(MasterRechargePackage record) {
        return masterRechargePackageMapper.insertSelective(record);
    }
    
    @Override
    public MasterRechargePackage selectByPrimaryKey(Long id) {
        return masterRechargePackageMapper.selectByPrimaryKey(id);
    }
    
    @Override
    public int updateByPrimaryKeySelective(MasterRechargePackage record) {
        return masterRechargePackageMapper.updateByPrimaryKeySelective(record);
    }
    
    /**
     * 修改总站套餐信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R updateMasterRechargePackage(MasterRechargePackage rechargePackage) {
        int num = 0;
        if (rechargePackage.getId() == null) {
            num = insertSelective(rechargePackage);
        } else {
            num = updateByPrimaryKeySelective(rechargePackage);
        }
        if (num == 1) {
            return Rx.success("添加/修改套餐信息---成功");
        } else {
            throw new RuntimeException("添加/修改套餐信息---异常");
        }
    }
    
    @Override
    public int insert(MasterRechargePackage record) {
        return masterRechargePackageMapper.insert(record);
    }
    
    @Override
    public int updateByPrimaryKey(MasterRechargePackage record) {
        return masterRechargePackageMapper.updateByPrimaryKey(record);
    }
    @Override
    public List<MasterRechargePackage> selectByAllAndValid(SysUser user) {
        MasterRechargePackage masterRechargePackage = new MasterRechargePackage();
        if (CommonUtils.isMerchantType(user) || CommonUtils.isApiType(user)) {
            masterRechargePackage.setStatus("1");
        }
        masterRechargePackage.setIsDel("0");
//        masterRechargePackage.setMerchantId(1);
        return masterRechargePackageMapper.selectByAll(masterRechargePackage);
    }
}



