package com.paradise.tomdog.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.News;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.NewsMapper;
import com.paradise.tomdog.sys.service.NewsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Resource
    private NewsMapper newsMapper;

    @Override
    public int deleteByPrimaryKey(Integer id, SysUser user) {
        return newsMapper.deleteByPrimaryKey(id, user.getId());
    }

    @Override
    public int insert(News record) {
        return newsMapper.insert(record);
    }

    @Override
    public int insertSelective(News record) {
        return newsMapper.insertSelective(record);
    }

    @Override
    public News selectByPrimaryKey(Integer id) {
        return newsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(News record) {
        return newsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(News record) {
        return newsMapper.updateByPrimaryKey(record);
    }

    /**
     * 修改商户文章
     */
    @Override
    public R<String> updateNews(SysUser user, News news) {
        int num;
        news.setUpdater(user.getId());
        news.setUpdateTime(new Date());
        if (news.getId() != null) {
            news.setCreator(user.getId());
            news.setCreateTime(new Date());
            num = updateByPrimaryKey(news);
        } else {
            num = insertSelective(news);
        }
        if (num == 1) {
            return Rx.success("添加/修改文章---成功");
        }
        throw new RuntimeException("添加/修改文章---异常");
    }

    @Override
    public List<News> selectByAll(News news) {
        return newsMapper.selectByAll(news);
    }

    @Override
    public PageData selectByAll(News news, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<News> list = newsMapper.selectByAll(news);
        return new PageData(new PageInfo<>(list));
    }

    @Override
    public List<News> selectByAllWithoutContent(News news) {
        return newsMapper.selectByAllWithoutContent(news);
    }
}
