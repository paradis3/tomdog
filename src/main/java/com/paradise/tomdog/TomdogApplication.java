package com.paradise.tomdog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * spring boot 启动类
 *
 * @author Paradise
 */
@SpringBootApplication
@EnableSwagger2
@EnableScheduling
@EnableAsync
public class TomdogApplication {

    public static void main(String[] args) {
        SpringApplication.run(TomdogApplication.class, args);
    }

}
