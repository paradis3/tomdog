package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserPayInfo;

import java.util.List;

public interface SysUserPayInfoService {


    int deleteByPrimaryKey(Long id);

    int insert(SysUserPayInfo record);

    int insertSelective(SysUserPayInfo record);

    SysUserPayInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUserPayInfo record);

    int updateByPrimaryKey(SysUserPayInfo record);
    
    
    int updateByUserId(SysUserPayInfo updated, Long userId);
    
    List<SysUserPayInfo> selectByAll(SysUserPayInfo sysUserPayInfo);
    
    SysUserPayInfo selectOneByAppId(String appId);
    /**
    *修改商户首付款信息
     */
    R updateSysUserPayInfo(SysUserPayInfo info);
    /**
     *查询商户付款信息
     */
     List<SysUserPayInfo> selectByUserIdAndIsDelNot(Long userId, String notIsDel);
    /**
     * 获取付款url
     */
    String getPayUrl(SysUser user, String payNo,SysUserPayInfo sysUserPayInfo);
    /**
     *初始化分站商户收付款信息
     */
    void initMerchantPayInfo(Long MerchantId);
}
