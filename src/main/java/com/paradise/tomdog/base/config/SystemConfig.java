package com.paradise.tomdog.base.config;


import com.paradise.tomdog.base.utils.IdWorkByTwitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * @author lijie
 * @date 2019年12月30日
 */
@Configuration
public class SystemConfig {

    @Bean
    public IdWorkByTwitter idWorkByTwitter(@Value("${bdtop.system.work-id:0}")long workId, @Value("${bdtop.system.data-center-id:0}")long dataCenterId){
        return new IdWorkByTwitter(workId, dataCenterId);
    }

//    @Bean
//    public DB db(@Qualifier("dataSource") DataSource dataSource){
//        return new DB(dataSource, true);
//    }

}
