package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Paradise
 */
@Getter
@Setter
public class BranchRechargeRecordQueryBean {
    
    @NotNull(message = "pageNum不能为空！")
    @ApiModelProperty(example = "1")
    private Integer pageNum;
    
    @NotNull(message = "pageSize不能为空！")
    @ApiModelProperty(example = "5")
    private Integer pageSize;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "查询开始时间")
    private Date beginDate;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "查询结束时")
    private Date endDate;
    
    @ApiModelProperty(value ="用户名/商户名")
    private String name;
    
    @ApiModelProperty(value ="手机号")
    private String telephone;
    
    @ApiModelProperty(value ="充值订单号")
    private String orderNo;
  
    @ApiModelProperty(value ="订单状态0 创建 1待支付 2 支付成功 3支付失败")
    private String status;
    
    @ApiModelProperty(value ="商户id",hidden = true)
    private Long merchantId;
    
    @ApiModelProperty(value ="用户id",hidden = true)
    private Long userId;
    
    
}
