package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysLoginInfo;

public interface SysLoginInfoService {
    
    
    int deleteByPrimaryKey(Long infoId);
    
    int insert(SysLoginInfo record);
    
    int insertSelective(SysLoginInfo record);
    
    SysLoginInfo selectByPrimaryKey(Long infoId);
    
    int updateByPrimaryKeySelective(SysLoginInfo record);
    
    int updateByPrimaryKey(SysLoginInfo record);
    
}


