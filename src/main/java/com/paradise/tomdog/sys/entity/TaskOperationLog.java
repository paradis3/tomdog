package com.paradise.tomdog.sys.entity;

import lombok.Data;

/**
 * @author Paradise
 */
@Data
public class TaskOperationLog {
    /**
     * 自增主键
     */
    private Long id;

    private Long taskId;

    private Long operatorId;

    /**
     * 1 暂停任务
     * 2 取消任务
     * 3 完成任务
     */
    private Integer operateType;

    private Long createTime;

    /**
     * 0 fail
     * 1 success
     */
    private Integer result;

    private String resultMsg;
}