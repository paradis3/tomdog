package com.paradise.tomdog.sys.entity;

import com.paradise.tomdog.rpc.bean.TbShopInfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "淘宝店铺信息")
@Data
public class TbShop {
    private Long id;

    private String url;

    private Integer shopId;

    private String picUrl;

    private String shopName;

    private String seller;

    private Integer fansNum;

    private Long sellerId;

    private Date updateTime;

    public TbShop() {
    }

    public TbShop(String url, TbShopInfo tbShopInfo) {
        this.url = url;
        this.shopId = Integer.valueOf(tbShopInfo.getShopId());
        this.picUrl = tbShopInfo.getPicUrl();
        this.shopName = tbShopInfo.getShopName();
        this.seller = tbShopInfo.getSeller();
        this.fansNum = tbShopInfo.getFansNum();
        this.sellerId = tbShopInfo.getSellerId();
    }
}