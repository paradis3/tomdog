package com.paradise.tomdog.rpc.bean;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 查询淘宝店铺信息
 *
 * @author Paradise
 */
@Getter
@Setter
@Slf4j
public class JdDrQuery extends AbstractLlQueryBean {
    private String darenid;

    public JdDrQuery(String darenid) {
        this.darenid = darenid;
    }

    public static void main(String[] args) {
        JdDrQuery drQuery = new JdDrQuery("419599");
        drQuery.doQuery();
    }

    @Override
    public void initSign() {
        String url = "/api/query_jingdong_daren?appid=1049&darenid=" + this.darenid
                + "&timestamp="
                + this.timestamp + "&ver=4";
        try {
            url = URLEncoder.encode(url, CharsetUtil.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.sign = SecureUtil.md5(url);
    }

    @Override
    public R doQuery() {
        this.initSign();
        HttpResponse httpResponse = HttpUtil
                .createPost("http://www.liuliangjunheng.com/api/query_jingdong_daren")
                .body(this.toJson()).execute();
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            log.info("查询京东达人信息Result：");
            log.info(httpResponse.body());
            JSONObject model = new JSONObject(httpResponse.body()).getJSONObject("model");
            return Rx.success(model);
        }
        return Rx.fail(httpResponse.toString());
    }

    public String toJson() {
        return JSONUtil.toJsonStr(this);
    }
}
