package com.paradise.tomdog.sys.constant;

/**
 * 任务操作类型
 *
 * @author Paradise
 */
public enum Task_Operate_Type {
    /**
     * 暂停任务
     */
    PAUSE(1),
    CANCEL(2),
    COMPLETE(3),
    DELETE(4);

    private int type;

    Task_Operate_Type(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
