package com.paradise.tomdog.rpc.result;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 任务暂停返回结果
 *
 * @author Paradise
 */
@Getter
@Setter
@ToString
public class LlTaskPauseResult implements LlResultInterface {

    @Getter
    @Setter
    @ToString
    public static class Data {
        private String status;
        private String tips;
    }

    private Data data;

    @Override
    public boolean isSuccess() {
        return "1".equals(this.getData().status);
    }

    @Override
    public String getTips() {
        return this.getData().tips;
    }
}
