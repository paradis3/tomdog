package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysMenu;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.List;

/**
 * @author Paradise
 */
public interface SysMenuService {


    int deleteByPrimaryKey(Long id);

    int insert(SysMenu record);

    SysMenu selectByPrimaryKey(Long id);

    int updateByPrimaryKey(SysMenu record);

    /**
     * 查询菜单列表
     *
     * @param roleId   角色id
     * @param isMaster 1总站
     * @return select res
     */
    List<SysMenu> selectByRole(Long roleId, Integer isMaster);

    /**
     * 初始化商户超级管理员菜单权限
     *
     * @param roleId   角色id
     * @param isMaster 1总站
     * @return insert count
     */
    int initSuperRoleMenu(Long roleId, Integer isMaster);

    /**
     * 配置角色菜单
     *
     * @param menuIds     逗号分隔菜单id
     * @param currentUser 操作人
     * @param roleId      角色id
     * @return {@link R}
     */
    R cfgMenu(String menuIds, SysUser currentUser, Long roleId);

    /**
     * 树化
     *
     * @param menuList 菜单列表
     */
    void listToTree(List<SysMenu> menuList);
}
