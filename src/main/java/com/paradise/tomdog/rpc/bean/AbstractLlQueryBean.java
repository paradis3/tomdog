package com.paradise.tomdog.rpc.bean;

import com.paradise.tomdog.base.R;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Paradise
 */
@Getter
@Setter
public abstract class AbstractLlQueryBean {
    String client;
    String format;
    String appid;
    String ver;
    String timestamp;
    String sign;

    AbstractLlQueryBean() {
        this.client = "web";
        this.format = "json";
        this.appid = "1049";
        this.ver = "4";
        this.timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        this.sign = "c58c097f292cd487526364c3f51d8315";
    }

    /**
     * 签名初始化
     */
    public abstract void initSign();

    /**
     * 执行查询
     *
     * @return 查询结果封装 {@link R}
     */
    public abstract R doQuery();
}
