package com.paradise.tomdog.base.security;

import com.paradise.tomdog.base.component.MyToken;
import com.paradise.tomdog.base.utils.JwtTokenUtil;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * JWT登录授权过滤器
 *
 * @author macro
 * @date 2018/4/26
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private SysUserService sysUserService;
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {
        String authHeader = request.getHeader(this.tokenHeader);
        if (authHeader != null && authHeader.startsWith(this.tokenHead) && !"undefined".equals(authHeader)) {
            String authToken = authHeader.substring(this.tokenHead.length());
            String usernameAndDomain = jwtTokenUtil.getUserNameFromToken(authToken);
            if (usernameAndDomain != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(usernameAndDomain);
                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                    SysUser sysUser = sysUserService.selectOneByUsernameAndDomain(usernameAndDomain);
                    MyToken authentication = new MyToken(userDetails, null, userDetails.getAuthorities(),sysUser);
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        chain.doFilter(request, response);
    }
}
