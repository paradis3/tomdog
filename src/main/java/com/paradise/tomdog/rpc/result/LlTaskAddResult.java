package com.paradise.tomdog.rpc.result;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 创建任务接口返回的 Data 数据解析
 *
 * @author Paradise
 */
@Getter
@Setter
@ToString
public class LlTaskAddResult implements LlResultInterface {

    @Override
    public boolean isSuccess() {
        return "1".equals(this.getData().status);
    }

    @Override
    public String getTips() {
        return this.getData().tips;
    }

    @Getter
    @Setter
    @ToString
    public static class Data {
        /**
         * 提示信息
         */
        private String tips;
        /**
         * 通用状态码
         */
        private String status;
        /**
         * 赠送积分余额
         */
        private String score;
        /**
         * 购买积分余额
         */
        private String score2;
        /**
         * 此次下单扣除积分
         */
        private String price;
    }

    private Data data;
}
