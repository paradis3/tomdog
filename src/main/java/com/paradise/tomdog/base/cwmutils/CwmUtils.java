package com.paradise.tomdog.base.cwmutils;

import com.alibaba.fastjson.JSON;
import com.paradise.tomdog.sys.entity.SysUserPayInfo;
import com.paradise.tomdog.sys.mapper.SysUserPayInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.*;

@Component
@Slf4j
public class CwmUtils {
    
    
    @Autowired
    private SysUserPayInfoMapper sysUserPayInfoMapper;
    private static CwmUtils cwmUtils;
    
    public void setSysUserPayInfoMapper(SysUserPayInfoMapper sysUserPayInfoMapper) {
        this.sysUserPayInfoMapper = sysUserPayInfoMapper;
    }
    
    @PostConstruct
    public void init() {
        cwmUtils = this;
        cwmUtils.sysUserPayInfoMapper = this.sysUserPayInfoMapper;
    }
    
    public static String md5(String str) {
        return md5(str, "utf-8");
    }
    
    public static String md5(String str, String encodeing) {
        MessageDigest md5;
        StringBuilder md5StrBuff = new StringBuilder();
        try {
            md5 = MessageDigest.getInstance("MD5");
            if (encodeing != null && !"".equals(encodeing)) {
                md5.update(str.getBytes(encodeing));
            } else {
                md5.update(str.getBytes());
            }
            byte[] domain = md5.digest();
            for (byte b : domain) {
                if (Integer.toHexString(0xFF & b).length() == 1) {
                    md5StrBuff.append("0").append(Integer.toHexString(0xFF & b));
                } else {
                    md5StrBuff.append(Integer.toHexString(0xFF & b));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return md5StrBuff.toString().toLowerCase();
    }
    
    public static String encode(String param) throws UnsupportedEncodingException {
        return URLEncoder.encode(param, "UTF-8");
    }
    
    public static String httpget(String url) {
        return httpget(url, "UTF-8");
    }
    
    public static String httpget(String url, String charset) {
        
        return httpget(url, charset, null);
        
    }
    
    public static String httpget(String url, String charset, Map<String, String> header) {
        BufferedReader br = null;
        String tips = "";
        try {
            URL host = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) host.openConnection();
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(10000);
            if (header != null && !header.isEmpty()) {
                for (Map.Entry<String, String> m : header.entrySet()) {
                    connection.setRequestProperty(m.getKey(), m.getValue());
                }
            }
            
            br = new BufferedReader(new InputStreamReader(connection.getInputStream(), charset));
            String s = "";
            StringBuilder sb = new StringBuilder("");
            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            tips = sb.toString();
            br.close();
            br = null;
            
        } catch (Exception err) {
            err.printStackTrace();
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                
                }
            }
        }
        
        return tips;
        
    }
    
    
    public static String createUrl(String uri, Map<String, String> map) throws UnsupportedEncodingException {
        String secret = map.get("secret");
        StringBuilder para = new StringBuilder();
        StringBuilder param = new StringBuilder();
        List<Map.Entry<String, String>> list = new ArrayList<Map.Entry<String, String>>(map.entrySet());
        // ----------------------按key排序----------------------
        list.sort(Comparator.comparing(Map.Entry::getKey));
        
        // ----------------------排序后获得签名串----------------------
        for (Map.Entry<String, String> m : list) {
            String key = m.getKey();
            if (!"secret".equals(key)) {
                para.append(key).append("=").append(m.getValue()).append("&");
                param.append(key).append("=").append(CwmUtils.encode(m.getValue())).append("&");
            }
        }
        String sign = CwmUtils.md5(CwmUtils.encode(uri + "?" + para + secret));
        return uri + "?" + param + "sign=" + sign;
    }
    
    
    public static Boolean verifyCwmSign(HttpServletRequest request) throws UnsupportedEncodingException {
//        return true;
        log.info("校验财务猫回调签名----开始");
        try {
            Map<String, String> paramMap = CwmUtils.getParamMap(request);
            log.info("接收到的财务猫回调：{}",JSON.toJSONString(paramMap));
            SysUserPayInfo info = cwmUtils.sysUserPayInfoMapper.
                    selectOneByAppIdAndMerchantId(paramMap.get("appid"),Long.valueOf(paramMap.get("extends_param")));
            paramMap.put("secret", info.getAppSecret());
            String fromSign = paramMap.remove("sign");
            String url = CwmUtils.createUrl("/tomdog/callback/rechargeCallBack", paramMap);
            String sign = StringUtils.substringAfterLast(url, "sign=");
            boolean reslut = StringUtils.equals(fromSign, sign);
            log.info("回调签名结果为----{}",reslut);
            return reslut;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Map<String, String> getParamMap(HttpServletRequest request) {
        HashMap<String, String> paramMap = new HashMap<>();
        //获取所有的参数名
        Enumeration<String> paramNames = request.getParameterNames();
        String name = null;
        while (paramNames.hasMoreElements()) {
            name = paramNames.nextElement();
            String value = request.getParameter(name);
            paramMap.put(name, value);
        }
        return paramMap;
    }
    
    
}
