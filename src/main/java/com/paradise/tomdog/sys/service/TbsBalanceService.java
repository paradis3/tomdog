package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.TbsBalance;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;

public interface TbsBalanceService {
    
    
    int insert(TbsBalance record);
    
    TbsBalance selectByPrimaryKey(Long userId);
    
    int updateByPrimaryKey(TbsBalance record);
    
    TbsBalance updateByWithDrawRecord(TbsBalance tbsBalance, TbsWithdrawRecord record);
    
    TbsBalance returnByWithDrawRecord(TbsWithdrawRecord record);
    
    int deleteByPrimaryKey(Long userId);
    
    int insertSelective(TbsBalance record);
    
    int updateByPrimaryKeySelective(TbsBalance record);
}
