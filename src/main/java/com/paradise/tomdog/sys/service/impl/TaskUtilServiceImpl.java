package com.paradise.tomdog.sys.service.impl;

import cn.hutool.json.JSONObject;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.TaskUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Paradise
 */
@Service
@Slf4j
public class TaskUtilServiceImpl implements TaskUtilService {

    @Resource
    private JdGoodsMapper jdGoodsMapper;
    @Resource
    private JdShopMapper jdShopMapper;
    @Resource
    private DyInfoMapper dyInfoMapper;
    @Resource
    private JdDrMapper jdDrMapper;
    @Resource
    private TbArticleMapper tbArticleMapper;

    /**
     * 根据jd itemId 查询商品信息
     *
     * @param itemId 商品id
     * @return {@link JdGoods}
     */
    @Override
    public JdGoods selectByItemId(Long itemId) {
        return jdGoodsMapper.selectByPrimaryKey(itemId);
    }

    @Override
    public void updateJdGoodsInfo(JdGoods record, boolean existFlag) {
        if (existFlag) {
            jdGoodsMapper.updateByPrimaryKey(record);
        } else {
            jdGoodsMapper.insert(record);
        }
    }

    @Override
    public void updateJdShop(JdShop record, boolean existFlag) {
        if (existFlag) {
            jdShopMapper.updateByPrimaryKey(record);
        } else {
            jdShopMapper.insert(record);
        }
    }

    @Override
    public JdShop selectByShopId(String shopId) {
        return jdShopMapper.selectByPrimaryKey(Long.parseLong(shopId));
    }

    @Override
    public DyInfo selectDyInfo(String id) {
        return dyInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public void insertDyInfo(DyInfo dyInfo) {
        dyInfoMapper.insert(dyInfo);
    }

    @Override
    public JdDr selectDrById(Long id) {
        return jdDrMapper.selectByPrimaryKey(id);
    }

    @Override
    public JdDr saveJdDr(Long id, JSONObject jsonObject, boolean existFlag) {
        JdDr dr = new JdDr();
        dr.setId(id);
        dr.setName(jsonObject.getStr("authorName"));
        dr.setFansNum(jsonObject.getInt("followNums"));
        dr.setJson(jsonObject.toString());
        if (existFlag) {
            jdDrMapper.updateByPrimaryKey(dr);
        } else {
            jdDrMapper.insert(dr);
        }
        return jdDrMapper.selectByPrimaryKey(id);
    }

    @Override
    public TbArticle selectArticleById(Long id) {
        return tbArticleMapper.selectByPrimaryKey(id);
    }

    @Override
    public TbArticle saveTbArticle(Long id, JSONObject jsonObject, boolean existFlag) {
        TbArticle tbArticle = new TbArticle();
        tbArticle.setId(id);
        tbArticle.setAuthor(jsonObject.getStr("author"));
        tbArticle.setReadCount(jsonObject.getInt("readCount"));
        tbArticle.setJson(jsonObject.toString());
        if (existFlag) {
            tbArticleMapper.updateByPrimaryKey(tbArticle);
        } else {
            tbArticleMapper.insert(tbArticle);
        }
        return tbArticle;
    }
}
