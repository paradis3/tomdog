package com.paradise.tomdog.base.utils;

import java.util.regex.Pattern;

/**
 * 参数校验工具
 *
 * @author Paradise
 */
public class ParamCheckUtils {

    private static final Pattern TEL_PATTERN = Pattern.compile("^1[3456789][0-9]\\d{4,8}$");

    public static boolean telephoneCheck(String tel) {
        return TEL_PATTERN.matcher(tel).matches();
    }

}
