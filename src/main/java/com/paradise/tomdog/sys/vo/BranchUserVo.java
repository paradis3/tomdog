package com.paradise.tomdog.sys.vo;

import com.paradise.tomdog.sys.entity.SysUser;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 分站用户
 *
 * @author Paradise
 * @see com.paradise.tomdog.sys.entity.SysUser
 */
@Getter
@Setter
public class BranchUserVo extends SysUser {
    /**
     * 上级账户名
     */
    private String pUserName;
    /**
     * 上级真实姓名
     */
    private String pRealName;
    /**
     * 上级推广码
     */
    private String pInviteCode;
    /**
     * 用户等级
     */
    private String levelName;
    /**
     * 推广返比
     */
    private BigDecimal percent;
    /**
     * 商户名称
     */
    private String merchantName;

    /**
     * 商户账户名
     */
    private String merchantUserName;

    /**
     * 商户域名
     */
    private String domain;
    /**
     * 最近登录时间
     */
    private Date loginTime;

    /**
     * 推广积分余额
     */
    private BigDecimal spreadBalance;
    /**
     * 转化率
     */
    private BigDecimal rate;

    /**
     * 推广人数
     */
    private Integer spreadCount;

    /**
     * 充值人数
     */
    private Integer rechargeCount;

    /**
     * 下级充值金额
     */
    private BigDecimal rechargeAmount;

    /**
     * 累计获得推广积分
     */
    private Integer spreadPoints;

    /**
     * 个人累计充值
     */
    private BigDecimal selfRechargeAmount;

    /**
     * 个人累计带来的推广积分
     */
    private Integer parentSpreadPoints;
}
