package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author Paradise
 */
@ApiModel("用户注册校验实体信息")
@Data
public class RegisterParamCheckBean {

    @Length(max = 20, min = 5, message = "用户名长度在5-20个字符之间")
    @ApiModelProperty(value = "用户名")
    private String username;


    @NotBlank(message = "手机号码不能为空")
    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "域名")
    private String domain;

}
