package com.paradise.tomdog.rpc.bean;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import lombok.Getter;
import lombok.Setter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 查询淘宝店铺信息
 *
 * @author Paradise
 */
@Getter
@Setter
public class TbTklQuery extends AbstractLlQueryBean {
    private String key;

    public TbTklQuery(String key) {
        this.key = key;
    }

    @Override
    public void initSign() {
        String url = "/api/query_taokouling?appid=1049&timestamp=" + this.timestamp
                + "&ver=4";
        try {
            url = URLEncoder.encode(url, CharsetUtil.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.sign = SecureUtil.md5(url);
    }

    @Override
    public R doQuery() {
        this.initSign();
        HttpResponse httpResponse = HttpUtil
                .createPost("http://www.liuliangjunheng.com/api/query_taokouling")
                .body(this.toJson()).execute();
        System.out.println(httpResponse.body());
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            return Rx.success(httpResponse.body());
        }
        return Rx.fail(httpResponse.toString());
    }

    public String toJson() {
        return JSONUtil.toJsonStr(this);
    }

}
