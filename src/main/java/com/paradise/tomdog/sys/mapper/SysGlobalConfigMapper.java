package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysGlobalConfig;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.util.List;

@Mapper
public interface SysGlobalConfigMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysGlobalConfig record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysGlobalConfig record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysGlobalConfig selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysGlobalConfig record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysGlobalConfig record);
    
    List<SysGlobalConfig> selectAllByConfigId(@Param("configId") String configId);
}