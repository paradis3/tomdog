package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 余额实体信息
 *
 * @author Paradise
 */
@ApiModel(value = "余额Model")
@Data
public class Balance {
    /**
     * 变更数量，可为负数
     */
    private transient Integer amount;
    /**
     * 余额表自增主键
     */
    @ApiModelProperty(value = "余额表自增主键")
    private Long balanceId;
    /**
     * 业务主键（分站商户主键，API商户主键，用户主键）
     */
    @ApiModelProperty(value = "业务主键（分站商户主键，API商户主键，用户主键）")
    private Long businessId;
    /**
     * 业务类型，对应业务主键
     */
    @ApiModelProperty(value = "业务类型，对应业务主键")
    private Integer type;
    /**
     * 积分余额
     */
    @ApiModelProperty(value = "积分余额")
    private Integer balance;
    /**
     * 乐观锁-版本号
     */
    @ApiModelProperty(value = "乐观锁-版本号")
    private Integer version;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    /**
     * 删除标记
     */
    private int isDel;

    public Balance() {
    }

    /**
     * 初始化构造器
     *
     * @param businessId 业务主键
     * @param type       业务类型
     */
    public Balance(Long businessId, Integer type, Integer balance) {
        this.businessId = businessId;
        this.type = type;
        this.balance = balance;
    }
}