package com.paradise.tomdog.sys.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsBalance;
import com.paradise.tomdog.sys.entity.TbsBalanceRecord;
import com.paradise.tomdog.sys.mapper.TbsBalanceRecordMapper;
import com.paradise.tomdog.sys.service.TbsBalanceRecordService;
import com.paradise.tomdog.sys.service.TbsBalanceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Paradise
 */
@Service
public class TbsBalanceRecordServiceImpl implements TbsBalanceRecordService {
    
    @Resource
    private TbsBalanceRecordMapper tbsBalanceRecordMapper;

    
    @Override
    public int insert(TbsBalanceRecord record) {
        return tbsBalanceRecordMapper.insert(record);
    }
    
    @Override
    public TbsBalanceRecord selectByPrimaryKey(Long id) {
        return tbsBalanceRecordMapper.selectByPrimaryKey(id);
    }
    
    @Override
    public int updateByPrimaryKey(TbsBalanceRecord record) {
        return tbsBalanceRecordMapper.updateByPrimaryKey(record);
    }
    
    
    @Override
    public int deleteByPrimaryKey(Long id) {
        return tbsBalanceRecordMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    public int insertSelective(TbsBalanceRecord record) {
        return tbsBalanceRecordMapper.insertSelective(record);
    }
    
    @Override
    public int updateByPrimaryKeySelective(TbsBalanceRecord record) {
        return tbsBalanceRecordMapper.updateByPrimaryKeySelective(record);
    }
    
    @Override
    public PageData getSpreadRechargePage(Integer pageNum, Integer pageSize, SysUser user) {
        TbsBalanceRecord record = new TbsBalanceRecord();
        record.setUserId(user.getId());
        record.setIsDel("0");
        PageHelper.startPage(pageNum, pageSize);
        return new PageData(new PageInfo<>(tbsBalanceRecordMapper.selectSpreadByPage(record)));
    }
    
    @Override
    public Integer selectSumSpreadPointsByUserId(Long userId) {
        return tbsBalanceRecordMapper.selectSumSpreadPointsByUserId(userId);
    }
    

}


