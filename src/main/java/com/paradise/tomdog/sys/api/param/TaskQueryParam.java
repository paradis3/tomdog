package com.paradise.tomdog.sys.api.param;

import com.paradise.tomdog.rpc.bean.BaseParamBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 任务查询 bean
 *
 * @author Paradise
 */
@EqualsAndHashCode(callSuper = false)
@Api(value = "任务查询参数")
@Data
public class TaskQueryParam extends BaseParamBean implements ApiParamInterface {

    @ApiModelProperty(value = "API用户名", example = "")
    private String username;
    @ApiModelProperty(value = "appId", example = "PFJzDlQP")
    private String appId;
    @ApiModelProperty(value = "时间戳", example = "123456789")
    private Long timestamp;
    @ApiModelProperty("任务ID")
    private String taskId;
    @ApiModelProperty(value = "签名")
    private String sign;

    public TaskQueryParam() {
    }
}
