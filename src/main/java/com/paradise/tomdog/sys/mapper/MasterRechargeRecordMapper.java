package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.MasterRechargeRecord;
import com.paradise.tomdog.sys.entity.MasterRechargeRecordQueryBean;
import com.paradise.tomdog.sys.entity.MasterRechargeRecordWithSysUserWithSysBranchMerchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface MasterRechargeRecordMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(MasterRechargeRecord record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(MasterRechargeRecord record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    MasterRechargeRecord selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(MasterRechargeRecord record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(MasterRechargeRecord record);

    List<MasterRechargeRecord> selectByAll(MasterRechargeRecord masterRechargeRecord);

    List<MasterRechargeRecord> selectAllByUpdateTimeBefore(@Param("minUpdateTime") Date minUpdateTime);

    List<MasterRechargeRecordWithSysUserWithSysBranchMerchant> selectAllByQueryBean(MasterRechargeRecordQueryBean queryBean);

    /**
     * 统计 目标日期 之后的充值记录，充值金额
     *
     * @param createTime 查询开始时间
     * @return 金额，记录数
     */
    Map<String, Object> countByStatusAndPayTime(@Param("createTime") Date createTime);

    /**
     * 统计 目标日期 之后的累计充值金额
     *
     * @param createTime 查询开始区间
     * @return 充值金额 sum
     */
    BigDecimal selectPayAmountByPayTime(@Param("createTime") Date createTime);

    /**
     * 查询30d的充值金额
     *
     * @return ListMap
     */
    List<Map<String, Object>> selectPayAmount();

}