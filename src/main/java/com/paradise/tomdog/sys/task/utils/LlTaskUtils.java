package com.paradise.tomdog.sys.task.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.CheckResults;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.UrlParser;
import com.paradise.tomdog.rpc.bean.*;
import com.paradise.tomdog.rpc.constant.Task_Add_Type;
import com.paradise.tomdog.rpc.result.LlTaskAddResult;
import com.paradise.tomdog.rpc.result.LlTaskCancelResult;
import com.paradise.tomdog.rpc.result.LlTaskPauseResult;
import com.paradise.tomdog.rpc.result.LlTaskQueryResult;
import com.paradise.tomdog.rpc.utils.LlUtils;
import com.paradise.tomdog.sys.constant.TaskTypeTools;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.TbShopMapper;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * 封装 任务相关工具类
 *
 * @author Paradise
 */
@Slf4j
public class LlTaskUtils {
    private static final String USERNAME = "u_1932953";
    private static final Pattern JD_ITEM_PATTERN = Pattern.compile("https://item.jd.com/\\d*.html");
    private static final Pattern JD_SHOP_PATTERN = Pattern.compile("https://\\w*.jd.com/");
    private static final Pattern JD_SHOP_PATTERN_ZY = Pattern.compile("https://mall.jd.com/index-\\d*.html");
    private static final Pattern TB_SHOP_PATTERN = Pattern.compile("https://\\w*.taobao.com\\S*");
    private static final Pattern TM_SHOP_PATTERN = Pattern.compile("https://\\w*.tmall.com\\S*");
    private static final Pattern TB_ITEM_PATTERN = Pattern.compile("https://item.taobao.com/item.htm\\?id=\\d*");
    private static final Pattern TM_ITEM_PATTERN = Pattern.compile("https://detail.tmall.com/item.htm\\?id=\\d*");
    private static final Pattern PDD_ITEM_PATTERN = Pattern.compile("https://mobile.yangkeduo.com/goods.html\\?goods_id=\\d*");
    private static final Pattern PDD_SHOP_PATTERN = Pattern.compile("https://mobile.yangkeduo.com/mall_page.html\\?mall_id=\\d*");
    private static final Pattern DY_URL_PATTERN = Pattern.compile("https://v.douyin.com/\\w{6}/");
    private static final Pattern TB_JHS_URL_PATTERN = Pattern.compile("https://detail.ju.taobao.com/home.htm\\?\\S*");
    private static final Pattern JD_DR_URL_PATTERN = Pattern.compile("https://eco\\.m\\.jd\\.com/content/dr_home/index\\.html\\?authorId=\\d*");

    /**
     * 创建任务时的参数校验方法
     *
     * @param task 任务参数
     * @return {@link R}
     */
    public static R checkTaskParam(LlTask task) {
        Task_Add_Type type = TaskTypeTools.getType(task.getType());
        if (type.equals(Task_Add_Type.NULL)) {
            return Rx.fail("任务类型不正确");
        }
        String[] keys = task.getKeywords().split("[,，;；]");
        // hours 参数格式校验
        CheckResults hoursCheckResult = checkTaskHours(task.getHour());
        if (hoursCheckResult.hasErrors()) {
            return Rx.fail(hoursCheckResult.getMsg());
        }
        CheckResults beginTimeCheckResult = checkBeginTime(task.getBeginTime());
        if (beginTimeCheckResult.hasErrors()) {
            return Rx.fail(beginTimeCheckResult.getMsg());
        }
        Integer calculatorDayCount = getHoursCount(task.getHour());
        log.info("创建任务，单位小时任务量配置，日总计：{}", calculatorDayCount);
        if (!calculatorDayCount.equals(task.getDayCount())) {
            return Rx.fail("每日任务数和单位小时任务配置总计数不一致");
        }
        if ((calculatorDayCount * task.getDays() * keys.length) != (task.getCount())) {
            return Rx.fail("任务数总数和每日任务数*天数不相等");
        }
        // 判断任务类型
        if (TaskTypeTools.needKeyword(type) && StrUtil.isEmpty(task.getKeywords())) {
            return Rx.fail("搜索词不能为空");
        }
        if (TaskTypeTools.needDeptAndBrowseTime(type)) {
            if (StrUtil.isEmpty(task.getBrowseTime())) {
                return Rx.fail("浏览时长不能为空");
            }
            try {
                int bt = Integer.parseInt(task.getBrowseTime());
                if (bt > 180 || bt < 30) {
                    return Rx.fail("浏览时长区间30-180");
                }
            } catch (RuntimeException e) {
                return Rx.fail("浏览时长参数格式不正确");
            }
            if (task.getDepth() == null) {
                return Rx.fail("浏览深度不能为空");
            } else {
                if (task.getDepth() > 2 || task.getDepth() < 0) {
                    task.setDepth(0);
                }
            }
        }
        if (TaskTypeTools.isJdItemTask(type) && !isJdItemUrl(task.getTarget())) {
            return Rx.fail("京东商品链接不正确");
        }
        if (TaskTypeTools.isJdShopTask(type)
                && !isJdShopUrl(task.getTarget())
                && !isJdShopZyUrl(task.getTarget())) {
            return Rx.fail("京东店铺链接不正确");
        }
        if (TaskTypeTools.isTbItemTask(type)) {
            if (!isTbItemUrl(task.getTarget()) && !isTmItemUrl(task.getTarget())) {
                return Rx.fail("淘宝宝贝链接不正确");
            }
        }
        if (TaskTypeTools.isTbShopTask(type)) {
            if (!isTbShopUrl(task.getTarget()) && !isTmShopUrl(task.getTarget())) {
                return Rx.fail("淘宝店铺链接不正确");
            }
            // 过滤商品链接
            if (LlTaskUtils.isTbItemUrl(task.getTarget()) || LlTaskUtils.isTmItemUrl(task.getTarget())) {
                return Rx.fail("店铺链接不正确");
            }
            // 处理店铺链接 - 简化
            String url = task.getTarget();
            task.setTarget(url.substring(0, url.indexOf(".com") + 4));
        }
        if (TaskTypeTools.isPddItemTask(type) && !isPddItemUrl(task.getTarget())) {
            return Rx.fail("拼多多商品链接不正确");
        }
        if (TaskTypeTools.isPddShopTask(type) && !isPddShopUrl(task.getTarget())) {
            return Rx.fail("拼多多店铺链接不正确");
        }
        // 校验淘口令
        // 校验文章口令，链接
        if (TaskTypeTools.isTbArticleTask(TaskTypeTools.getType(task.getType()))) {
            String id = getContentIdFromTarget(task.getTarget());
            if (StrUtil.isEmpty(id)) {
                return Rx.fail("淘宝文章口令或者链接不正确");
            }
            if (!isTky(task.getTarget())) {
                task.setTarget(dealTbArticle(task.getTarget()));
            }
        }
        if (TaskTypeTools.isDyTask(type)) {
            if (!LlTaskUtils.isDyUrl(task.getTarget())) {
                return Rx.fail("抖音链接不正确");
            }
            if (task.getCount() < 100) {
                return Rx.fail("抖音任务数量最少为100");
            }
        }
        if (Task_Add_Type.TB_OPEN_REMAINDER.equals(TaskTypeTools.getType(task.getType()))) {
            if (!isTbJhsUrl(task.getTarget())) {
                return Rx.fail("淘宝聚划算链接不正确");
            }
        }
        if (Task_Add_Type.JD_FOLLOW.equals(TaskTypeTools.getType(task.getType()))) {
            if (!isJdDrUrl(task.getTarget())) {
                return Rx.fail("京东达人链接不正确");
            }
        }
        return Rx.success();
    }

    public static boolean isJdDrUrl(String url) {
        return JD_DR_URL_PATTERN.matcher(url).matches();
    }

    /**
     * 校验任务 每小时任务量配置参数
     *
     * @param hours 配置信息
     * @return {@link CheckResults}
     */
    private static CheckResults checkTaskHours(String hours) {
        String[] arr = hours.split(",");
        int hoursOfDay = 24;
        if (arr.length != hoursOfDay) {
            return new CheckResults(false, "每小时任务量配置格式不正确");
        }
        try {
            Integer count = getHoursCount(hours);
            int maxTaskCount = 9999;
            if (count > maxTaskCount) {
                return new CheckResults(false, "每日任务总数超出限制9999");
            }
        } catch (RuntimeException e) {
            return new CheckResults(false, "每小时任务量配置格式不正确");
        }
        return new CheckResults();
    }

    /**
     * 获取 每小时任务配置总数
     *
     * @param hours 配置信息
     * @return 每日累计总数
     */
    private static Integer getHoursCount(String hours) throws RuntimeException {
        String[] arr = hours.split(",");
        int calculatorCount = 0;
        for (String s : arr) {
            calculatorCount += Integer.parseInt(s);
        }
        return calculatorCount;
    }

    /**
     * 任务 begin_time 格式校验
     *
     * @param beginTime 任务开始时间 yyyy-MM-dd
     * @return true - 通过 false - 校验不通过
     */
    private static CheckResults checkBeginTime(String beginTime) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date parseDate = sdf.parse(beginTime);
            if (parseDate.before(sdf.parse(sdf.format(new Date())))) {
                return new CheckResults(false, "任务开始时间不能小于当前时间");
            }
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, 7);
            if (parseDate.after(calendar.getTime())) {
                return new CheckResults(false, "任务开始时间不能超过当前时间7天");
            }
        } catch (Exception e) {
            log.info(e.getLocalizedMessage(), e);
            return new CheckResults(false, "任务开始时间格式不正确，正确的格式为'yyyy-MM-dd'");
        }
        return new CheckResults();
    }

    public static LlTaskPauseResult pause(LlTask task) {
        TaskPauseBean taskPauseBean = TaskPauseBean.builder()
                .username(USERNAME)
                .id(task.getTaskId())
                .status(0)
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();
        return taskPauseBean.dollResolve("/ll/task_pause?", LlTaskPauseResult.class);
    }

    /**
     * 创建任务
     *
     * @param task 任务信息
     * @return {@link LlTaskAddResult}
     */
    public static LlTaskAddResult create(LlTask task, TbShopMapper tbShopMapper) {
        TaskAddBean addBean = TaskAddBean.builder()
                .username(USERNAME)
                .id(task.getTaskId())
                .count(task.getCount())
                .hour(task.getHour())
                .begin_time(task.getBeginTime())
                .type(task.getType())
                .target(task.getTarget())
                .keyword(task.getKeywords())
                .browse_time(task.getBrowseTime())
                .depth(task.getDepth())
                .shopVisit("no")
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();
        // 抖音任务处理
        if (TaskTypeTools.isDyTask(TaskTypeTools.getType(task.getType()))) {
            addBean.setAweme_id(Long.valueOf(Objects.requireNonNull(getDyVideoId(task.getTarget()))));
            addBean.setTarget("");
        }
        // 淘宝直播任务处理
        if (TaskTypeTools.getType(task.getType()).equals(Task_Add_Type.TB_LIVE_WATCH)) {
            Long liveId = getLiveIdByTkl(task.getTarget());
            if (liveId == null) {
                throw new RuntimeException("淘宝直播id解析为空");
            }
            addBean.setKeyword(liveId.toString());
            addBean.setLiveid(liveId);
            addBean.setTarget("");
        }
        // 淘宝达人关注
        if (TaskTypeTools.getType(task.getType()).equals(Task_Add_Type.TB_LIVE_FOLLOW)) {
            if (isTbShopUrl(addBean.getTarget()) || isTmShopUrl(addBean.getTarget())) {
                TbShop tbShop = tbShopMapper.selectOneByUrl(addBean.getTarget());
                if (tbShop != null && tbShop.getSellerId() != null) {
                    addBean.setUserid(tbShop.getSellerId());
                } else {
                    addBean.setUserid(queryTbShopSellerId(addBean.getTarget()));
                }
            } else {
                addBean.setKeyword(addBean.getTarget());
            }
            addBean.setTarget("");
        }
        // 微淘点赞处理
        if (Task_Add_Type.TB_WT_LIKE.equals(TaskTypeTools.getType(task.getType()))) {
            addBean.setKeyword(addBean.getTarget());
        }
        // 商品点赞任务
        if (Task_Add_Type.TB_ITEM_LIKE.equals(TaskTypeTools.getType(task.getType()))) {
            addBean.setItemid(UrlParser.parse(addBean.getTarget()).get("id"));
            addBean.setKeyword("");
            addBean.setTarget("");
        }
        if (Task_Add_Type.TB_OPEN_REMAINDER.equals(TaskTypeTools.getType(task.getType()))) {
            specialDeal(addBean);
        }
        // 淘宝文章处理，解析ID
        if (TaskTypeTools.isTbArticleTask(TaskTypeTools.getType(task.getType()))) {
            addBean.setTarget(getContentIdFromTarget(addBean.getTarget()));
        }
        return addBean.dollResolve("/ll/task_add?", LlTaskAddResult.class);
    }

    private static void specialDeal(TaskAddBean addBean) {
        String target = addBean.getTarget();
        Map<String, String> map = UrlParser.parse(target);
        String link = "https://item.taobao.com/item.htm?id=" + map.get("item_id");
        addBean.setTarget(link);
        addBean.setJuid(map.get("id"));
        addBean.setItemid(map.get("item_id"));
    }

    private static String getContentIdFromTarget(String target) {
        // 判断是淘口令还是文章链接
        if (isTky(target)) {
            return getContentIdByTkl(target);
        } else {
            String id = UrlParser.parse(target).get("contentId");
            log.info("解析出的文章id为：{}", id);
            return id;
        }
    }

    private static String dealTbArticle(String target) {
        if (target.length() > 255) {
            Map map = UrlParser.parse(target);
            target = map.get("basePath") + "?contentId=" + map.get("contentId");
        }
        return target;
    }

    /**
     * 查询任务
     *
     * @param taskId 任务ID
     * @return {@link LlTaskQueryResult}
     */
    public static LlTaskQueryResult query(String taskId) {
        TaskQueryBean taskQueryBean = TaskQueryBean.builder()
                .username(USERNAME)
                .id(taskId)
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();
        return taskQueryBean.dollResolve("/ll/task_list?", LlTaskQueryResult.class);
    }

    /**
     * 取消任务
     *
     * @param taskId 任务id
     * @return {@link LlTaskCancelResult}
     */
    public static LlTaskCancelResult cancel(String taskId) {
        TaskCancelBean taskCancelBean = TaskCancelBean.builder()
                .username(USERNAME)
                .id(taskId)
                .format("json")
                .ver(5)
                .timestamp(LlUtils.getCurrentSeconds())
                .build();
        return taskCancelBean.dollResolve("/ll/task_cancel?", LlTaskCancelResult.class);
    }

    /**
     * 初始化积分变更记录
     *
     * @param param 参数 {@link BalanceUpdateParam}
     * @return {@link PointsRecord}
     */
    public static PointsRecord initPointsRecord(BalanceUpdateParam param) {
        PointsRecord record = new PointsRecord();
        record.setBusinessId(param.getBusinessId());
        record.setUserType(param.getUserTypeEnum().getType());
        record.setAmount(param.getAmount());
        record.setCreateTime(new Date());
        record.setCreateBy(param.getCreateBy());
        record.setStatus("1");
        record.setIsDel(0);
        record.setRecordType(param.getRecordType().getCode());
        if (StrUtil.isEmpty(param.getRemark())) {
            record.setRemark(param.getRecordType().getDesc());
        } else {
            record.setRemark(param.getRemark());
        }
        record.setTaskId(param.getTaskId());
        record.setRechargeId(param.getRechargeId());
        return record;
    }

    /**
     * 根据淘宝店铺链接查询店铺信息
     *
     * @param url 店铺链接
     * @return {@link ShopResInfo}
     */
    public static ShopResInfo postShopQuery(String url) {
        String res = HttpUtil.post("http://www.liuliangjunheng.com/api/query_taobao_shop", new TbShopQuery(url).toJson());
        log.info("店铺信息：{}", res);
        return JSONUtil.toBean(res, ShopResInfo.class);
    }

    private static Long queryTbShopSellerId(String url) {
        ShopResInfo shopResInfo = LlTaskUtils.postShopQuery(url);
        if (shopResInfo.isSuccess()) {
            return shopResInfo.toTbShopInfo().getSellerId();
        }
        return null;
    }

    /**
     * 调用流量均衡api 查询淘口令信息
     *
     * @param key 淘口令key
     * @return {@link R}
     */
    public static R queryTkl(String key) {
        return new TbTklQuery(key).doQuery();
    }

    public static Long getLiveIdByTkl(String key) {
        key = key.substring(1);
        key = key.substring(0, key.length() - 1);
        try {
            R res = queryTkl(key);
            if (Rx.isSuccess(res)) {
                JSONObject jsonObject = JSONUtil.parseObj(res.getData());
                log.info(jsonObject.toString());
                String url = jsonObject.getJSONObject("model").getStr("url");
                if (StrUtil.isNotBlank(url)) {
                    String id = UrlParser.parse(url).get("id");
                    log.info("解析出的直播id为：{}", id);
                    return Long.parseLong(id);
                }
            }
        } catch (RuntimeException e) {
            return null;
        }
        return null;
    }

    public static String getContentIdByTkl(String key) {
        key = key.substring(1);
        key = key.substring(0, key.length() - 1);
        try {
            R res = queryTkl(key);
            if (Rx.isSuccess(res)) {
                JSONObject jsonObject = JSONUtil.parseObj(res.getData());
                String url = jsonObject.getJSONObject("model").getStr("url");
                if (StrUtil.isNotBlank(url)) {
                    String id = UrlParser.parse(url).get("contentId");
                    log.info("解析出的文章id为：{}", id);
                    return id;
                }
            }
        } catch (RuntimeException e) {
            return null;
        }
        return null;
    }

    /**
     * 调用流量均衡api，查询京东商品信息
     *
     * @param goodsId 商品id
     * @return {@link R}
     */
    public static R queryJdGoods(String goodsId) {
        return new JdGoodsQuery(goodsId).doQuery();
    }

    public static String urlResolver(String url) {
        return url.substring(0, url.indexOf(".com") + 4);
    }

    public static boolean isJdItemUrl(String url) {
        return JD_ITEM_PATTERN.matcher(url).matches();
    }

    public static boolean isJdShopUrl(String url) {
        return JD_SHOP_PATTERN.matcher(url).matches();
    }

    public static boolean isJdShopZyUrl(String url) {
        return JD_SHOP_PATTERN_ZY.matcher(url).matches();
    }

    public static boolean isTbItemUrl(String url) {
        return TB_ITEM_PATTERN.matcher(url).matches();
    }

    public static boolean isTmItemUrl(String url) {
        return TM_ITEM_PATTERN.matcher(url).matches();
    }

    public static boolean isTbShopUrl(String url) {
        return TB_SHOP_PATTERN.matcher(url).matches();
    }

    public static boolean isTmShopUrl(String url) {
        return TM_SHOP_PATTERN.matcher(url).matches();
    }

    public static boolean isPddShopUrl(String url) {
        return PDD_SHOP_PATTERN.matcher(url).matches();
    }

    public static boolean isPddItemUrl(String url) {
        return PDD_ITEM_PATTERN.matcher(url).matches();
    }

    public static boolean isDyUrl(String url) {
        return DY_URL_PATTERN.matcher(url).matches();
    }

    public static boolean isTky(String tkl) {
        tkl = tkl.trim();
        return tkl.startsWith("￥") && tkl.endsWith("￥");
    }

    public static boolean isTbJhsUrl(String url) {
        boolean check = TB_JHS_URL_PATTERN.matcher(url).matches();
        Map<String, String> map = UrlParser.parse(url);
        return check && StrUtil.isNotEmpty(map.get("id")) && StrUtil.isNotBlank(map.get("item_id"));
    }

    public static String getShortCodeFromDyUrl(String url) {
        return url.substring(21, 27);
    }

    /**
     * 查询抖音视频信息
     *
     * @param url 视频分享短链接
     * @return {@link DyInfo}
     */
    public static DyInfo queryDyInfo(String url) {
        if (!isDyUrl(url)) {
            throw new RuntimeException("抖音链接不正确，参考格式为：https://v.douyin.com/pJvcu4/");
        }
        String vId = getDyVideoId(url);
        if (vId == null) {
            throw new RuntimeException("抖音链接不正确");
        }
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new RuntimeException("抖音短视频信息查询出错");
        }
        String title = doc.title();
        String picUrl;
        // 获取抖音短视频缩略图url
        Elements elements = doc.getElementsByClass("video-player");
        String attr = elements.get(0).attr("style");
        picUrl = attr.substring(attr.indexOf("(") + 1, attr.length() - 1);

        elements = doc.getElementsByClass("desc");
        String desc = elements.get(0).text();
        elements = doc.getElementsByClass("name nowrap");
        String author = elements.get(0).text();
        return new DyInfo(getShortCodeFromDyUrl(url), url, title, picUrl, desc, author, null, vId);
    }

    public static String getDyVideoId(String url) {
        HttpResponse res = HttpUtil.createGet(url).execute();
        if (res.getStatus() == HttpStatus.HTTP_MOVED_TEMP) {
            String location = res.header("location");
            if (StrUtil.isNotBlank(location)) {
                return location.substring(location.indexOf("/video/") + 7, location.indexOf("/?region="));
            }
        }
        return null;
    }

    public static void main(String[] args) {
        TaskAddBean addBean = TaskAddBean.builder()
                .username(USERNAME)
                .id(String.valueOf(System.currentTimeMillis()))
                .count(1)
                .hour("0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0")
                .begin_time("2020-04-06")
                .type(Task_Add_Type.TB_ITEM_APP_SEARCH_BT.getType())
                .target("https://item.taobao.com/item.htm?id=571243245029")
                .keyword("创维 55k")
//                https://detail.ju.taobao.com/home.htm?id=10000886260098&item_id=571243245029
//                .itemid(UrlParser.parse("https://detail.ju.taobao.com/home.htm?id=10000886260098&item_id=571243245029").get("item_id"))
//                .juid(UrlParser.parse("https://detail.ju.taobao.com/home.htm?id=10000886260098&item_id=571243245029").get("id"))
                .userid(143845803L)

                .browse_time("90")
                .depth(0)
                .shopVisit("no")
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();

        LlTaskAddResult result = addBean.dollResolve("/ll/task_add?", LlTaskAddResult.class);
        System.out.println(result);

    }

}
