package com.paradise.tomdog.sys.service.impl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import com.paradise.tomdog.sys.entity.SysDictType;
import com.paradise.tomdog.sys.mapper.SysDictTypeMapper;
import com.paradise.tomdog.sys.service.SysDictTypeService;

@Service
public class SysDictTypeServiceImpl implements SysDictTypeService {

    @Resource
    private SysDictTypeMapper sysDictTypeMapper;

    @Override
    public int deleteByPrimaryKey(Long dictId) {
        return sysDictTypeMapper.deleteByPrimaryKey(dictId);
    }

    @Override
    public int insert(SysDictType record) {
        return sysDictTypeMapper.insert(record);
    }

    @Override
    public int insertSelective(SysDictType record) {
        return sysDictTypeMapper.insertSelective(record);
    }

    @Override
    public SysDictType selectByPrimaryKey(Long dictId) {
        return sysDictTypeMapper.selectByPrimaryKey(dictId);
    }

    @Override
    public int updateByPrimaryKeySelective(SysDictType record) {
        return sysDictTypeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysDictType record) {
        return sysDictTypeMapper.updateByPrimaryKey(record);
    }

}
