package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Paradise
 */
@Getter
@Setter
@ApiModel(value = "积分记录查询参数")
public class PointsRecordQueryBean {
    @ApiModelProperty(value = "查询开始时间（时间戳）", example = "1575356306119")
    private Long beginDate;
    @ApiModelProperty(value = "查询结束时间（时间戳）", example = "1575356906119")
    private Long endDate;
    @NotNull
    @ApiModelProperty(example = "1")
    private Integer pageNum;
    @NotNull
    @ApiModelProperty(example = "5")
    private Integer pageSize;
    @ApiModelProperty(value = "积分变更类型")
    private Integer recordType;

    @ApiModelProperty(value = "用户名")
    private String userName;
    @ApiModelProperty(value = "分站商户名")
    private String merchantName;
    @ApiModelProperty(value = "手机号码")
    private String telephone;

    @ApiModelProperty(value = "分站商户ID", hidden = true)
    private Long merchantId;
    @ApiModelProperty(value = "商户ID", hidden = true)
    private Long apiUserId;
    @ApiModelProperty(hidden = true)
    private Long operator;
    @ApiModelProperty(value = "分站用户ID", hidden = true)
    private Long userId;
    @ApiModelProperty(hidden = true)
    private Long businessId;
    @ApiModelProperty(hidden = true)
    private Integer userType;
    @ApiModelProperty(hidden = true)
    private String remark;
}
