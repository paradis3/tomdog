package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysAccount;

import java.util.List;

/**
 * @author Paradise
 */
public interface SysAccountService {

    /**
     * 新增
     *
     * @param account 账户信息
     * @return insert count
     */
    int insert(SysAccount account);

    /**
     * 更新
     *
     * @param account 账户信息
     * @return update count
     */
    int updateByPrimaryKey(SysAccount account);

    /**
     * 查询管理员列表
     *
     * @param roleId  当前角色id
     * @param account 查询信心
     * @return select res
     */
    List<SysAccount> selectAllByRoleId(Long roleId, SysAccount account);

    /**
     * 查询管理员列表
     *
     * @param account 查询信心
     * @return select res
     */
    List<SysAccount> selectAllByMerchantId(SysAccount account);
}
