package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsBalanceRecord;

/**
 * @author Paradise
 */
public interface TbsBalanceRecordService {
    
    /**
     * insert
     *
     * @param record record
     * @return insert count
     */
    int insert(TbsBalanceRecord record);
    
    TbsBalanceRecord selectByPrimaryKey(Long id);
    
    int updateByPrimaryKey(TbsBalanceRecord record);
    
    int deleteByPrimaryKey(Long id);
    
    int insertSelective(TbsBalanceRecord record);
    
    int updateByPrimaryKeySelective(TbsBalanceRecord record);
    
    /**
     * 分页查询推广收益
     *
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @param user     当前用户
     * @return {@link PageData}
     */
    PageData getSpreadRechargePage(Integer pageNum, Integer pageSize, SysUser user);
    
    /**
     * 查询累计推广收益
     *
     * @param userId 用户ID
     * @return 累计推广积分
     */
    Integer selectSumSpreadPointsByUserId(Long userId);
    
}


