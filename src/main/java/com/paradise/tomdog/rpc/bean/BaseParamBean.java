package com.paradise.tomdog.rpc.bean;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Paradise
 */
@Slf4j
public abstract class BaseParamBean {

    /**
     * 获取属性名数组
     *
     * @param obj 对象
     * @return 返回对象属性名数组
     */
    private String[] getFiledNames(Object obj) {
        Field[] fields = obj.getClass().getDeclaredFields();
        String[] fieldNames = new String[fields.length];
        for (int i = 0; i < fields.length; i++) {
            fieldNames[i] = fields[i].getName();
        }
        return fieldNames;
    }

    /**
     * 根据属性名获取属性值
     *
     * @param fieldName 属性名称
     * @param obj       对象
     * @return 返回对应属性的值
     */
    private Object getFieldValueByName(String fieldName, Object obj) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" +
                    firstLetter +
                    fieldName.substring(1);
            Method method = obj.getClass().getMethod(getter);
            return method.invoke(obj);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 将建构的 builder 转为 Map
     *
     * @return 转化后的 Map
     */
    private Map<String, String> toMap() {
        String[] fieldNames = getFiledNames(this);
        Map<String, String> map = new HashMap<>(fieldNames.length);
        for (String name : fieldNames) {
            String value = String.valueOf(getFieldValueByName(name, this));
            if (StrUtil.isNotEmpty(value) && !"null".equals(value)) {
                map.put(name, value);
            }
        }
        return map;
    }

    public Map<String, String> toMapWithoutSign() {
        Map<String, String> map = this.toMap();
        map.remove("sign");
        return map;
    }

    /**
     * 构建签名 Map
     *
     * @param privateKey CmwAPI KEY
     * @return 构建签名后的 Map
     */
    private String createQueryParam(String privateKey, String prefix) {
        String str = packageSign(toMap());
        String toDec = prefix + str + "&" + privateKey;
        String sign = null;
        try {
            String url = URLEncoder.encode(toDec, CharsetUtil.UTF_8);
            sign = SecureUtil.md5(url);
            str = URLDecoder.decode(str, CharsetUtil.UTF_8);
            str = str.replaceAll(" ", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        str = str + "&signkey=" + sign;
        return str;
    }

    public String createSign(String privateKey, String prefix) {
        String str = packageSign(toMapWithoutSign());
        String toDec = prefix + str + "&" + privateKey;
        String sign = null;
        try {
            String url = URLEncoder.encode(toDec, CharsetUtil.UTF_8);
            sign = SecureUtil.md5(url);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sign;
    }


    private String packageSign(Map<String, String> params) {
        // 先将参数以其参数名的字典序升序进行排序
        TreeMap<String, String> sortedParams = new TreeMap<>(params);
        // 遍历排序后的字典，将所有参数按"key=value"格式拼接在一起
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> param : sortedParams.entrySet()) {
            String value = param.getValue();
            if (StrUtil.isEmpty(value)) {
                continue;
            }
            if (first) {
                first = false;
            } else {
                sb.append("&");
            }
            sb.append(param.getKey()).append("=");
            sb.append(value);
        }
        return sb.toString();
    }

    private String urlEncode(String src) {
        try {
            return URLEncoder.encode(src, CharsetUtil.UTF_8).replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return src;
        }
    }

    public String doll(String method) {
        String param = this.createQueryParam("9a42a3adde42f5d236c8994861697d80", method);
        String url = "http://api.lieliu.com:1024" + method + param;
        HttpResponse httpResponse = HttpUtil.createGet(url).execute();
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            log.info("请求响应成功，参数为：{}", param);
        }
        return httpResponse.body();
    }

    public <T> T dollResolve(String method, Class<T> tClass) {
        String res = doll(method);
        log.info("请求响应结果：{}", res);
        return JSONUtil.toBean(res, tClass);
    }

    private String replace(String url) {
        url = url.replaceAll("/", "%2f")
                .replaceAll("\\?", "%3f")
                .replaceAll("=", "%3d")
                .replaceAll("&", "%26");
        return url;
    }
}
