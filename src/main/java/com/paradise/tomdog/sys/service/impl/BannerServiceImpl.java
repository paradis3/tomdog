package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.Banner;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.BannerMapper;
import com.paradise.tomdog.sys.service.BannerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * banner-service 实现类
 *
 * @author Yangjian
 */
@Service
public class BannerServiceImpl implements BannerService {

    @Resource
    private BannerMapper bannerMapper;

    @Override
    public int deleteByPrimaryKey(Integer id, SysUser user) {
        return bannerMapper.deleteByPrimaryKey(id, user.getId());
    }

    @Override
    public int insert(Banner record) {
        return bannerMapper.insert(record);
    }

    @Override
    public int insertSelective(Banner record) {
        return bannerMapper.insertSelective(record);
    }

    @Override
    public Banner selectByPrimaryKey(Integer id) {
        return bannerMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Banner record) {
        return bannerMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Banner record) {
        return bannerMapper.updateByPrimaryKey(record);
    }

    /**
     * 修改商户文章
     */
    @Override
    public R<String> updateBanner(SysUser user, Banner banner) {
        int num;
        banner.setUpdateUser(user.getId().toString());
        banner.setUpdateTime(new Date());
        if (banner.getId() != null) {
            banner.setCreateUser(user.getId().toString());
            banner.setCreateTime(new Date());
            num = updateByPrimaryKey(banner);
        } else {
            num = insertSelective(banner);
        }
        if (num == 1) {
            return Rx.success("添加/修改banner---成功");
        }
        throw new RuntimeException("添加/修改banner---异常");
    }

    @Override
    public List<Banner> selectByAll(Banner banner) {
        return bannerMapper.selectByAll(banner);
    }
}
