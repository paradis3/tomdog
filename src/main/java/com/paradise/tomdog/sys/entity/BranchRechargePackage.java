package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * BranchRechargePackage - 分站商户充值套餐
 *
 * @author Paradise auto-generator
 */
@ApiModel(value = "分站商户充值套餐")
@Data
public class BranchRechargePackage {
    /**
     * 套餐主键自增ID
     */
    @ApiModelProperty(value = "套餐主键自增ID")
    private Long id;

    /**
     * 套餐名称
     */
    @ApiModelProperty(value = "套餐名称")
    @NotBlank(message = "套餐名称不能为空")
    private String packageName;

    /**
     * 套餐售价
     */
    @ApiModelProperty(value = "套餐售价")
    @NotNull(message = "套餐售价不能为空")
    private BigDecimal price;

    /**
     * 套餐积分
     */
    @ApiModelProperty(value = "套餐积分")
    @NotNull(message = "套餐积分不能为空")
    private Integer points;

    /**
     * 赠送积分
     */
    @ApiModelProperty(value = "赠送积分")
    @NotNull(message = "赠送积分不能为空")
    private Integer givePoints;

    /**
     * 分站商户ID
     */
    @ApiModelProperty(value = "分站商户ID")
    @NotNull(message = "分站商户ID不能为空")
    private Long merchantId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间", hidden = true)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人", hidden = true)
    private Long createBy;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人", hidden = true)
    private Long updateBy;

    /**
     * 状态 1代表有效
     */
    @ApiModelProperty(value = "状态 1代表有效")
    @NotBlank(message = "套餐名称不能为空")
    private String status;

    /**
     * 删除标记 1=删除
     */
    @ApiModelProperty(value = "删除标记 1=删除")
    @NotBlank(message = "套餐名称不能为空")
    private String isDel;

    @ApiModelProperty("折扣-展示用")
    @Length(max = 20, message = "折扣字段字符长度最大为20")
    private String discount;
}