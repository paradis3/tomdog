package com.paradise.tomdog.sys.api.param;

import com.paradise.tomdog.rpc.bean.BaseParamBean;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Paradise
 */
@Getter
@Setter
public class UserBalanceParam extends BaseParamBean implements ApiParamInterface {

    @ApiModelProperty(value = "API用户名", example = "123123")
    private String username;
    @ApiModelProperty(value = "appId", example = "PFJzDlQP")
    private String appId;
    @ApiModelProperty(value = "时间戳")
    private Long timestamp;
    @ApiModelProperty(value = "签名")
    private String sign;

    public UserBalanceParam() {
    }
}
