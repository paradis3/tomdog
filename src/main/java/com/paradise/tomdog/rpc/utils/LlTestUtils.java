package com.paradise.tomdog.rpc.utils;

import cn.hutool.http.HttpUtil;
import com.paradise.tomdog.rpc.bean.TaskAddBean;
import com.paradise.tomdog.rpc.bean.TaskQueryBean;
import com.paradise.tomdog.rpc.result.LlTaskAddResult;
import com.paradise.tomdog.rpc.result.LlTaskQueryResult;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Paradise
 */
public class LlTestUtils {
    private static LlTaskAddResult create() {
        TaskAddBean addBean = TaskAddBean.builder()
                .username("u_1932953")
                .id(System.currentTimeMillis() + "123456")
                .count(1)
                .hour("0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0")
                .begin_time("2019-12-16")
                .type(6)
                .target("https://ydtty.taobao.com/")
//                .keyword("")
//                .browse_time("")
//                .depth(0)
//                .shopVisit("no")
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();
        System.out.println(addBean.getId());
        return addBean.dollResolve("/ll/task_add?", LlTaskAddResult.class);
    }

    private static LlTaskQueryResult query(String taskId) {
        TaskQueryBean taskQueryBean = TaskQueryBean.builder()
                .username("u_1932953")
                .id(taskId)
                .format("json")
                .timestamp(LlUtils.getCurrentSeconds())
                .ver(5)
                .build();
        return taskQueryBean.dollResolve("/ll/task_list?", LlTaskQueryResult.class);
    }

    public static void postShopQuery() {
        String res = HttpUtil.post("http://www.liuliangjunheng.com/api/query_taobao_shop",
                "{\"url\":\"https://adidas.tmall.com/shop/view_shop.htm?spm=a1z0k.7386009.1997989141.2.1e6e1437KTe9l9&shop_id=621477621\",\"client\":\"web\",\"format\":\"json\",\"appid\":1049,\"ver\":\"4\",\"timestamp\":1576738962,\"sign\":\"4cc87362bbf77856f24f9fd8e7991219\"}");
        System.out.println(res);
    }

    public static void main(String[] args) {
//        System.out.println(create().toString());
//        System.out.println(query("1576551612011123456"));
//        postShopQuery();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Long beginDate = calendar.getTimeInMillis();
        System.out.println(new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(beginDate));
        System.out.println(System.currentTimeMillis());
        System.out.println(beginDate);
        System.out.println(calendar.get(Calendar.DATE));

    }
}
