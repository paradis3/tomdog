package com.paradise.tomdog.rpc.utils;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import com.paradise.tomdog.base.utils.AppUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Paradise
 */
public class LlUtils {

    private static AtomicLong seconds;
    private static Long millionSeconds;

    /**
     * @return 秒数
     */
    public synchronized static Long getCurrentSeconds(String asp) {
        if (seconds == null) {
            seconds = new AtomicLong();
        }
        if (millionSeconds != null && System.currentTimeMillis() - millionSeconds < 60000) {
            return seconds.get();
        } else {
            millionSeconds = System.currentTimeMillis();
        }
        String url = "http://api.lieliu.com:1024/api/sys_now?format=json";
        HttpResponse response = HttpUtil.createGet(url).execute();
        if (response.getStatus() == HttpStatus.HTTP_OK) {
            String res = response.body();
            long s = Long.parseLong(new JSONObject(new JSONObject(res).getStr("data")).getStr("time"));
            seconds.set(s);
        } else {
            seconds.set(System.currentTimeMillis() / 1000);
        }
        return seconds.get();
    }

    public static Long getCurrentSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    public static String generateTaskId() {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        return timeStamp + AppUtils.getAppId();
    }


    public static void main(String[] args) {
        BigDecimal percent = new BigDecimal(4).divide(new BigDecimal(7), 4, BigDecimal.ROUND_HALF_DOWN);
        System.out.println(percent);
    }

}
