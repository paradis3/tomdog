package com.paradise.tomdog.base.config;

import com.paradise.tomdog.base.utils.RedisUtil;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysSmsInfo;
import com.paradise.tomdog.sys.service.SysBranchMerchantService;
import com.paradise.tomdog.sys.service.SysSmsInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消息初始化
 *
 * @author lj
 * @date 2020/1/3 下午2:54
 */
@Component
@Slf4j
public class SmsConfig implements CommandLineRunner {
    private final SysSmsInfoService sysSmsInfoService;
    private final SysBranchMerchantService sysBranchMerchantService;
    private final RedisUtil redisUtil;

    public SmsConfig(SysSmsInfoService sysSmsInfoService, SysBranchMerchantService sysBranchMerchantService, RedisUtil redisUtil) {
        this.sysSmsInfoService = sysSmsInfoService;
        this.sysBranchMerchantService = sysBranchMerchantService;
        this.redisUtil = redisUtil;
    }

    @Override
    public void run(String... args) {
        log.info("加载短信相关信息----开始");
        try {
            //先清空上一次存储的sms信息
            redisUtil.delAll("sms*");
            //添加新的信息
            SysSmsInfo sysSmsInfo = new SysSmsInfo();
            sysSmsInfo.setStatus("1");
            List<SysSmsInfo> sysSmsInfos = sysSmsInfoService.selectByAll(sysSmsInfo);
            for (SysSmsInfo smsInfo : sysSmsInfos) {
                //redis存入商户相应的短信模板id
                redisUtil.set("sms_temp_" + smsInfo.getMerchantId() + "_" + smsInfo.getServiceId(), smsInfo.getTemplateId());
            }
            SysBranchMerchant sysBranchMerchant = new SysBranchMerchant();
            sysBranchMerchant.setStatus("1");
            List<SysBranchMerchant> sysBranchMerchants = sysBranchMerchantService.selectByAll(sysBranchMerchant);
            for (SysBranchMerchant branchMerchant : sysBranchMerchants) {
                //redis存入商户相应的签名id
                redisUtil.set("sms_sign_" + branchMerchant.getId(), branchMerchant.getSmsSignId());
            }
            log.info("加载短信相关信息----完成");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("加载短信相关信息----失败");
        }
    }

}