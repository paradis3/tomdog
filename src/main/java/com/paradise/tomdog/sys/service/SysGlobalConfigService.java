package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysGlobalConfig;

import java.util.List;

public interface SysGlobalConfigService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insert(SysGlobalConfig record);
    
    int insertSelective(SysGlobalConfig record);
    
    SysGlobalConfig selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(SysGlobalConfig record);
    
    int updateByPrimaryKey(SysGlobalConfig record);
    
    List<SysGlobalConfig> selectAllByConfigId(String configId);
}
