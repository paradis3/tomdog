package com.paradise.tomdog.sys.constant;

/**
 * 积分变更类型枚举
 *
 * @author Paradise
 */
public enum Points_Record_Type {
    //    分站/API商户充值减少、分站/API商户充值增加、分站用户充值减少、分站用户充值增加、短信消费、发起任务积分扣除、取消任务退回
    RECHARGE_ADD("1", "充值增加"),
    SMS_CONSUME("2", "短信消费"),
    TASK_CONSUME("3", "发起任务积分扣除"),
    TASK_CANCEL("4", "取消任务积分退还"),
    MASTER_MODIFY_ADD("5", "总站修改积分-赠送积分"),
    MASTER_MODIFY_MINUS("6", "总站修改积分-扣除积分");

    private String code;

    private String desc;

    Points_Record_Type(String code, String desc) {
        this.desc = desc;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
