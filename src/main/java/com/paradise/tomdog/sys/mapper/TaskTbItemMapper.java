package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TbItemInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 淘宝任务- 宝贝信息
 *
 * @author Paradise
 */
@Mapper
public interface TaskTbItemMapper {
    /**
     * delete by primary key
     *
     * @param taskId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long taskId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbItemInfo record);

    /**
     * select by primary key
     *
     * @param taskId primary key
     * @return object by primary key
     */
    TbItemInfo selectByPrimaryKey(Long taskId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbItemInfo record);
}