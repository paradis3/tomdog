package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPriceDepth;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface TaskPriceDepthMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPriceDepth record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskPriceDepth selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskPriceDepth record);

    /**
     * 查询总站
     *
     * @param type 类型 1随机 2深度
     * @return select res
     */
    TaskPriceDepth selectMasterByType(@Param("type") Integer type);

    /**
     * 查询分站商户 非默认
     *
     * @param type       类型 1随机 2深度
     * @param merchantId 分站商户id
     * @return select res
     */
    TaskPriceDepth selectOneByMerchantId(@Param("merchantId") Long merchantId,
                                         @Param("type") Integer type);

    /**
     * 查询分站商户 配置列表
     *
     * @param merchantId 分站商户id
     * @return select res
     */
    List<TaskPriceDepth> selectByMerchantId(@Param("merchantId") Long merchantId);

    /**
     * 查询分站商户 配置列表
     *
     * @return select res
     */
    List<TaskPriceDepth> selectByIsDefault();


    /**
     * 查询分站商户默认
     *
     * @param type 类型 1随机 2深度
     * @return select res
     */
    TaskPriceDepth selectDefault(@Param("type") Integer type);

}