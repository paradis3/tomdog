package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserPayInfo;
import com.paradise.tomdog.sys.mapper.SysUserPayInfoMapper;
import com.paradise.tomdog.sys.service.SysUserPayInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SysUserPayInfoServiceImpl implements SysUserPayInfoService {
    
    @Resource
    private SysUserPayInfoMapper sysUserPayInfoMapper;
    
    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysUserPayInfoMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    public int insert(SysUserPayInfo record) {
        return sysUserPayInfoMapper.insert(record);
    }
    
    @Override
    public int insertSelective(SysUserPayInfo record) {
        return sysUserPayInfoMapper.insertSelective(record);
    }
    
    @Override
    public SysUserPayInfo selectByPrimaryKey(Long id) {
        return sysUserPayInfoMapper.selectByPrimaryKey(id);
    }
    
    @Override
    public int updateByPrimaryKeySelective(SysUserPayInfo record) {
        return sysUserPayInfoMapper.updateByPrimaryKeySelective(record);
    }
    
    @Override
    public int updateByPrimaryKey(SysUserPayInfo record) {
        return sysUserPayInfoMapper.updateByPrimaryKey(record);
    }
    
    
    @Override
    public int updateByUserId(SysUserPayInfo updated, Long merchantId) {
        return sysUserPayInfoMapper.updateByMerchantId(updated, merchantId);
    }
    @Override
    public List<SysUserPayInfo> selectByAll(SysUserPayInfo sysUserPayInfo) {
        return sysUserPayInfoMapper.selectByAll(sysUserPayInfo);
    }
    /**
     * 修改商户收付款信息
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R updateSysUserPayInfo(SysUserPayInfo info) {
        SysUserPayInfo sysUserPayInfoIsDel = new SysUserPayInfo();
        sysUserPayInfoIsDel.setIsDel("1");
        sysUserPayInfoIsDel.setUpdateTime(new Date());
        sysUserPayInfoIsDel.setUpdateBy(info.getUpdateBy());
        updateByUserId(sysUserPayInfoIsDel, info.getMerchantId());
        info.setId(null);
        int num = insert(info);
        if (num == 1) {
            return Rx.success("添加/修改充值收款信息---成功");
        }
        throw new RuntimeException("添加/修改充值收款信息---异常");
    }
    
    /**
     * 查询商户付款信息
     */
    @Override
    public List<SysUserPayInfo> selectByUserIdAndIsDelNot(Long userId, String notIsDel) {
        return sysUserPayInfoMapper.selectByMerchantIdAndIsDelNot(userId, notIsDel);
    }
    
    /**
     * 获取付款url
     */
    @Override
    public String getPayUrl(SysUser user, String payNo,SysUserPayInfo sysUserPayInfo) {
        //支付页URL拼接方法为（apiHost + payment?id= + 交易流水号）http://www.caiwumao.com/payment?id=2018100008115448672200075541
        return sysUserPayInfo.getAppServer() + "/payment?id=" + payNo;
    }
    
    @Override
    public void initMerchantPayInfo(Long MerchantId) {
    
    }
    
    @Override
    public SysUserPayInfo selectOneByAppId(String appId) {
        return sysUserPayInfoMapper.selectOneByAppId(appId);
    }
}
