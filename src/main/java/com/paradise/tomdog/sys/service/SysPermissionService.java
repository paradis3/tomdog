package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysPermission;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysPermissionService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insert(SysPermission record);
    
    int insertSelective(SysPermission record);
    
    SysPermission selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(SysPermission record);
    
    int updateByPrimaryKey(SysPermission record);
    

    
}
