/**
 * Copyright (C), 2016-2019, XXX有限公司
 * FileName: RechargeController
 * Author:   Administrator
 * Date:     2019/12/27/027 15:17
 * Description: 充值接口
 * History:
 * <author>          <time>          <version>          <desc>
 * simon          修改时间           版本号              描述
 */
package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.UserLevel;
import com.paradise.tomdog.sys.service.SysUserPayInfoService;
import com.paradise.tomdog.sys.service.UserLevelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.security.Principal;

/**
 * 〈用户等级升级返利〉
 *
 * @author Administrator
 * @create 2019/12/27/027
 * @since 1.0.0
 */
@Api(tags = "用户等级(升级返利)接口", description = "用户等级(升级返利)接口")
@RestController
@RequestMapping("/user/level")
@Slf4j
public class UserLevelController {
    @Autowired
    SysUserPayInfoService sysUserPayInfoService;
    @Autowired
    UserLevelService userLevelService;

    @ApiOperation("添加/修改用户等级配置相关信息")
    @PostMapping("/updateLevelInfo")
    @PreAuthorize("hasAuthority('pms:mb:level:update')")
    @ControllerWebLog(name = "添加/修改用户等级相关信息")
    public R updateLevelInfo(@Validated UserLevel userLevel, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isAdminType(user) && !user.getMerchantId().equals(userLevel.getMerchantId())) {
            throw new RuntimeException("分站商户只可设置自家的用户等级相关信息");
        }
        return userLevelService.updateSysUserPayInfo(userLevel, user);
    }

    @ApiOperation("分站商户查询用户等级配置相关信息")
    @GetMapping("/getUserLevelList")
    @PreAuthorize("hasAuthority('pms:bu:level:get')")
    public R getUserLevelList(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return Rx.success(userLevelService.getUserLevelList(user));
    }

    @ApiIgnore
    @ApiOperation("总站分页查询用户等级配置相关信息")
    @GetMapping("/getAllUserLevelList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "pageNum", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "pageSize", defaultValue = "10", paramType = "query")
    })
    @PreAuthorize("hasAuthority('pms:m:level:get')")
    public R getAllUserLevelList(@NotNull(message = "pageNum不能为空！") Integer pageNum,
                                 @NotNull(message = "pageSize不能为空！") Integer pageSize) {
        return Rx.success(userLevelService.getUserLevelPage(pageNum, pageSize));
    }

    @ApiOperation("总站查询商户默认初始化的等级列表")
    @GetMapping("/getDefaultLevelList")
    @PreAuthorize("hasAuthority('pms:m:defaultLevel:get')")
    public R getDefaultLevelList(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return Rx.success(userLevelService.getDefaultLevelList(user));
    }

    @ApiOperation("总站查询商户的等级列表")
    @GetMapping("/getMerchantLevelList")
    @PreAuthorize("hasAuthority('pms:m:merchantLevelList:get')")
    public R getMerchantLevelList(Long merchantId) {
        if (merchantId == null) {
            return Rx.fail("商户ID 不能为空");
        }
        return Rx.success(userLevelService.getMerchantLevelList(merchantId));
    }
}
