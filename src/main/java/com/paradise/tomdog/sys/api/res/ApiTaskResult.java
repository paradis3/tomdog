package com.paradise.tomdog.sys.api.res;

import com.paradise.tomdog.sys.entity.LlTask;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;

/**
 * Api 查询任务详情
 *
 * @author Paradise
 */
@Getter
@Setter
public class ApiTaskResult {
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 任务类型
     */
    private Integer type;
    /**
     * 总发布量
     */
    private Integer count;
    /**
     * 每小时任务执行量
     */
    private String hour;
    /**
     * 任务开始时间 yyyy-MM-dd
     */
    private String beginTime;
    /**
     * 商品链接 or 店铺连接
     */
    private String target;
    /**
     * 搜索关键字
     */
    private String keyword;
    /**
     * 浏览时长
     */
    private String browseTime;
    /**
     * * 0=不浏览* 1=随机浏览* 2=深度浏览
     */
    private Integer depth;

    private String createTime;
    /**
     * 类型描述
     */
    private String typeDesc;
    /**
     * 退回积分
     */
    private Integer backPoints;

    private String status;
    private String statusDesc;

    public ApiTaskResult() {
    }

    public ApiTaskResult(LlTask task, String typeDesc, Integer backPoints, String statusDesc) {
        this.taskId = task.getTaskId();
        this.type = task.getType();
        this.count = task.getCount();
        this.hour = task.getHour();
        this.beginTime = task.getBeginTime();
        this.target = task.getTarget();
        this.keyword = task.getKeywords();
        this.browseTime = task.getBrowseTime();
        this.depth = task.getDepth();
        this.createTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(task.getCreateTime());
        this.typeDesc = typeDesc;
        this.backPoints = backPoints;
        this.status = task.getStatus();
        this.statusDesc = statusDesc;
    }
}
