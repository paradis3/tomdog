package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysRolePermission;

import java.util.List;

public interface SysRolePermissionService{


    int deleteByPrimaryKey(Long id);

    int insert(SysRolePermission record);

    int insertSelective(SysRolePermission record);

    SysRolePermission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRolePermission record);

    int updateByPrimaryKey(SysRolePermission record);
    
    int insertList(List<SysRolePermission> list);

}
