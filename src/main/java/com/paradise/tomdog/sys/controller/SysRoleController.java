package com.paradise.tomdog.sys.controller;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysMenu;
import com.paradise.tomdog.sys.entity.SysRole;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.service.SysMenuService;
import com.paradise.tomdog.sys.service.SysRoleService;
import com.paradise.tomdog.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统参数配置 - 价格表 - 配置 控制器
 *
 * @author Paradise
 */
@Api(tags = "后台管理：角色管理")
@RestController
@RequestMapping("/sys/role/")
@Slf4j
public class SysRoleController {

    private final SysRoleService roleService;
    private final SysUserService userService;
    private final SysMenuService menuService;

    public SysRoleController(SysRoleService roleService, SysUserService userService,
                             SysMenuService menuService) {
        this.roleService = roleService;
        this.userService = userService;
        this.menuService = menuService;
    }

    @ApiOperation("新增角色")
    @PostMapping("/addRole")
    @ControllerWebLog(name = "新增角色")
    @PreAuthorize("hasAuthority('pms:mb:role:addRole')")
    public R addRole(@Validated @RequestBody SysRole role,
                     @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        Long merchantId = currentUser.getMerchantId();
        if (CommonUtils.isAdminType(currentUser)) {
            merchantId = null;
        }
        // 判断 name 是否重复
        SysRole sysRole = roleService.selectByNameAndMerchantId(role.getName(), currentUser.getMerchantId());
        if (sysRole != null) {
            return Rx.fail("角色名称已存在");
        }
        // pid 当前商户的超级管理员
        SysRole superRole = roleService.selectSuperRole(merchantId);
        if (superRole == null) {
            log.error("查询超级管理员角色出错");
            return Rx.fail();
        }
        role.setPid(superRole.getId());
        role.setCreator(currentUser.getId());
        role.setUpdater(currentUser.getId());
        role.setStatus(DictConstant.ENABLE);
        role.setMerchantId(merchantId);
        role.setIsSuper(0);
        if (roleService.insert(role) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @ApiOperation("编辑角色（只能修改名称，描述）")
    @PostMapping("/editRole")
    @ControllerWebLog(name = "编辑角色")
    @PreAuthorize("hasAuthority('pms:mb:role:editRole')")
    public R editRole(@ApiIgnore Principal principal, @Validated @RequestBody SysRole role) {
        if (role.getId() == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        SysRole originRole = roleService.selectByPrimaryKey(role.getId());
        if (!originRole.getName().equals(role.getName())) {
            // 判断 name 是否重复
            SysRole sysRole = roleService.selectByNameAndMerchantId(role.getName(), currentUser.getMerchantId());
            if (sysRole != null) {
                return Rx.fail("角色名称已存在");
            }
        }
        role.setUpdater(currentUser.getId());
        if (roleService.updateByPrimaryKey(role) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @ApiOperation("删除角色（逻辑删除-递归删除）")
    @PostMapping("/delRole")
    @ControllerWebLog(name = "删除角色")
    @PreAuthorize("hasAuthority('pms:mb:role:delRole')")
    public R delRole(@ApiIgnore Principal principal, Long roleId) {
        if (roleId == null) {
            return Rx.fail("参数为空异常");
        }
        SysRole role = roleService.selectByPrimaryKey(roleId);
        if (role.getIsSuper() == 1) {
            return Rx.fail("超级管理员不能删除！");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        int x = roleService.deleteByPrimaryKey(roleId, currentUser.getId());
        if (x > 0) {
            int y = userService.deleteAllByRoleId(roleId, currentUser.getId());
            log.info("关联删除账户信息数量：{}", y);
            return Rx.success("角色删除成功，删除数量：" + x);
        }
        return Rx.fail();
    }

    @ApiOperation("查询角色列表")
    @GetMapping("/getRoleList")
    @PreAuthorize("hasAuthority('pms:mb:role:getRoleList')")
    public R getRoleList(@ApiIgnore Principal principal, String name) {
        SysUser currentUser = CommonUtils.getUser(principal);
        // 默认管理员查看全部角色列表
        List<SysRole> roleList = new ArrayList<>();
        // 总站type
        SysRole role = roleService.selectByPrimaryKey(currentUser.getRoleId());
        if (CommonUtils.isAdminType(currentUser)) {
            // 递归查询下级
            if (role.getIsSuper() != 1) {
                SysRole con = new SysRole();
                con.setIsSuper(0);
                con.setPid(role.getPid());
                con.setName(name);
                roleList = roleService.selectByAll(con);
            } else {
                roleList = roleService.selectAllByPid(currentUser.getRoleId(), name);
            }
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            // 超管查看全部
            if (role.getIsSuper() == 1) {
                roleList = roleService.selectAllByMerchantId(currentUser.getMerchantId(), name);
            } else {
                roleList = roleService.selectAllByPid(currentUser.getRoleId(), name);
            }
        }
        // 过滤超级管理员
        List<SysRole> delList = new ArrayList<>();
        for (SysRole sysRole : roleList) {
            if (new Integer(1).equals(sysRole.getIsSuper())) {
                delList.add(sysRole);
            }
        }
        roleList.removeAll(delList);
        return Rx.success(roleList);
    }

    @ApiOperation("查询菜单列表")
    @GetMapping("/getMenuList")
    @PreAuthorize("hasAuthority('pms:mb:role:getMenuList')")
    public R getMenuList(@ApiIgnore Principal principal, String treeFlag) {
        SysUser currentUser = CommonUtils.getUser(principal);
        Integer isMater = CommonUtils.isAdminType(currentUser) ? 1 : 0;
        List<SysMenu> menuList = menuService.selectByRole(currentUser.getRoleId(), isMater);
        if (StrUtil.isNotEmpty(treeFlag)) {
            menuService.listToTree(menuList);
        }
        return Rx.success(menuList);
    }

    @ApiOperation("查询角色已分配菜单列表")
    @GetMapping("/getRoleMenuList")
    @PreAuthorize("hasAuthority('pms:mb:role:getRoleMenuList')")
    public R getRoleMenuList(@ApiIgnore Principal principal, Long roleId) {
        if (roleId == null) {
            return Rx.fail("角色ID不能为空");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        Integer isMater = CommonUtils.isAdminType(currentUser) ? 1 : 0;
        List<SysMenu> menuList = menuService.selectByRole(roleId, isMater);
        menuService.listToTree(menuList);
        return Rx.success(menuList);
    }

    @ApiOperation("配置角色菜单")
    @PostMapping("/cfgMenu")
    @ControllerWebLog(name = "配置角色菜单")
    @PreAuthorize("hasAuthority('pms:mb:role:cfgMenu')")
    public R cfgMenu(@ApiIgnore Principal principal, Long roleId, String menuIds) {
        if (roleId == null) {
            return Rx.fail("参数为空异常");
        }
        SysRole role = roleService.selectByPrimaryKey(roleId);
        if (role.getIsSuper() == 1) {
            log.info("修改超级管理员菜单");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        return menuService.cfgMenu(menuIds, currentUser, roleId);
    }

}
