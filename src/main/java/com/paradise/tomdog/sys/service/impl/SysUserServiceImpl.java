package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.*;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.BalanceService;
import com.paradise.tomdog.sys.service.SysUserService;
import com.paradise.tomdog.sys.service.UserLevelService;
import com.paradise.tomdog.sys.vo.BranchUserQuery;
import com.paradise.tomdog.sys.vo.BranchUserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户-商户-相关 service 实现
 *
 * @author Paradise
 */
@Slf4j
@Service
public class SysUserServiceImpl implements SysUserService {

    private final UserDetailsService userDetailsService;
    private final JwtTokenUtil jwtTokenUtil;
    private final PasswordEncoder passwordEncoder;

    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysLoginInfoMapper loginInfoMapper;
    @Resource
    private BalanceService balanceService;
    @Resource
    private SysBranchMerchantMapper merchantMapper;
    @Resource
    private SysPasswordLogMapper sysPasswordLogMapper;
    @Autowired
    ApplicationContext context;
    @Autowired
    MobileMsg mobileMsg;
    @Autowired
    UserLevelService userLevelService;
    @Resource
    private SysUserInfoMapper userInfoMapper;
    @Resource
    private UserExpandMapper userExpandMapper;

    public SysUserServiceImpl(UserDetailsService userDetailsService,
                              JwtTokenUtil jwtTokenUtil,
                              PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public int deleteByPrimaryKey(Long userId, Long updateBy) {
        return sysUserMapper.deleteByPrimaryKey(updateBy, null);
    }

    @Override
    public int insert(SysUser record) {
        return sysUserMapper.insert(record);
    }

    @Override
    public int insertSelective(SysUser record) {
        return sysUserMapper.insertSelective(record);
    }

    @Override
    public SysUser selectByPrimaryKey(Long id) {
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(SysUser record) {
        return sysUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysUser record) {
        return sysUserMapper.updateByPrimaryKey(record);
    }

    @Override
    public SysUser register(RegisterBean registerBean) {
        return null;
    }

    @Override
    public R<String> login(String domain, String username, String password, HttpServletRequest request) {
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username + "," + domain);

            if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                throw new BadCredentialsException("用户名或者密码错误");
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails,
                    null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtTokenUtil.generateToken(userDetails, domain);
            this.insertLoginLog(request, username, domain, null);
            return Rx.success(token);
        } catch (AuthenticationException e) {
            log.warn("登录异常:{}", e.getMessage());
            this.insertLoginLog(request, username, domain, e);
            return Rx.fail(e.getMessage());
        }
    }

    /**
     * 记录登录日志
     *
     * @param request  请求信息
     * @param username 用户名
     * @param e        异常 nullable
     */
    private void insertLoginLog(HttpServletRequest request, String username, String domain, AuthenticationException e) {
        UserAgent userAgent = UserAgentUtil.parse(request.getHeader("user-agent"));
        SysLoginInfo loginInfo = SysLoginInfo.builder()
                .browser(userAgent.getBrowser().getName())
                .os(userAgent.getOs().getName())
                .ipaddr(request.getHeader("X-Real-IP"))
                .status("1")
                .domain(domain)
                .userName(username)
                .msg(Rx.success().getMsg())
                .build();
        if (e != null) {
            loginInfo.setStatus("0");
            loginInfo.setMsg("登录异常:{" + e.getMessage() + "}");
        } else {
            SysBranchMerchant merchant = merchantMapper.selectOneByDomain(domain);
            if (merchant != null) {
                SysUser user = sysUserMapper.selectOneByUsernameAndMerchantId(username, merchant.getId());
                if (user != null) {
                    loginInfo.setUid(user.getId());
                }
            }
        }
        loginInfoMapper.insert(loginInfo);
    }

    @Override
    public R changePassword(String newPassword, String password, Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!passwordEncoder.matches(password, user.getPassword())) {
            return Rx.error("密码输入错误");
        }
        if (sysUserMapper.changePassword(user.getId(), passwordEncoder.encode(newPassword), user.getId()) == 1) {
            // 记录密码修改日志
            SysPasswordLog log = new SysPasswordLog(user.getId(), CommonUtils.dealPassword(password),
                    CommonUtils.dealPassword(newPassword), user.getId());
            sysPasswordLogMapper.insert(log);
            return Rx.success(log);
        }
        return Rx.fail();
    }

    @Override
    public R forgetPassword(String newPassword, String telephone, String smsCode, String domain) {
        List<SysUser> sysUsers = selectAllByPhoneAndDomain(telephone, domain);
        if (sysUsers.size() == 0) {
            return Rx.fail("手机号错误,无此用户");
        }
        if (sysUsers.size() > 1) {
            log.error("手机号+域名查出多个账户信息，请排查数据错误");
            DentalCabotClient.sendText("手机号+域名查出多个账户信息，请排查数据错误");
            return Rx.fail("系统错误，请稍后再试");
        }
        SysUser sysUser = sysUsers.get(0);
        //校验验证码
        if (!mobileMsg.checkSms(telephone, smsCode)) {
            log.error("验证码错误");
            return Rx.fail("验证码错误");
        }
        if (sysUserMapper.changePassword(sysUser.getId(), passwordEncoder.encode(newPassword), sysUser.getId()) == 1) {
            // 记录密码修改日志
            SysPasswordLog log = new SysPasswordLog(sysUser.getId(), CommonUtils.dealPassword(newPassword),
                    CommonUtils.dealPassword(newPassword), sysUser.getId());
            sysPasswordLogMapper.insert(log);
            return Rx.success(sysUser.getUsername());
        }
        return Rx.fail();
    }

    @Override
    public SysUser getUserInfo(Long userId) {
        return sysUserMapper.selectByPrimaryKey(userId);
    }

    @Override
    public BranchUserVo getUserInfoDetail(Long userId) {
        return sysUserMapper.selectDetailByUserId(userId);
    }

    @Override
    public R resetPassword(String password, Long id) {
        SysUser user = sysUserMapper.selectByPrimaryKey(id);
        String encodePassword = passwordEncoder.encode(password);
        if (sysUserMapper.changePassword(user.getId(), encodePassword, user.getId()) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public PageData getBranchMerchantUserPage(BranchUserQuery query, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<BranchUserVo> userList = sysUserMapper.selectBranchUserList(query);
        for (BranchUserVo vo : userList) {
            vo.setLoginTime(loginInfoMapper.selectLastLoginTime(vo.getId()));
        }
        return new PageData(new PageInfo<>(userList));
    }

    @Override
    public void exportBranchMerchantUserList(BranchUserQuery query, HttpServletRequest request, HttpServletResponse response) {
        List<BranchUserVo> list = sysUserMapper.selectBranchUserList(query);
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .title("分站用户数据列表")
                .dataList(getDataList(list))
                .rowName(new String[]{"序号", "用户ID", "账号USERNAME", "真实姓名", "手机号码", "会员等级", "提成",
                        "归属分站名称", "注册时间", "推广码", "上级推广码", "上级USERNAME", "上级真实姓名",
                        "积分余额", "推广积分余额", "推广人数", "充值人数", "转化率", "下级累计充值", "个人累计充值"})
                .response(response).request(request)
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public R modifyEnableMerchant(Long id, SysUser currentUser) {
        SysUser user = sysUserMapper.selectByPrimaryKey(id);
        if (user == null || user.getMerchantId() == null) {
            return Rx.fail("分站用户不存在或已被删除");
        }
        if (user.getStatus().equalsIgnoreCase(DictConstant.DISABLE)) {
            user.setStatus(DictConstant.ENABLE);
        } else {
            user.setStatus(DictConstant.DISABLE);
        }
        if (sysUserMapper.updateStatusById(user.getStatus(), id, currentUser.getId()) == 1) {
            return Rx.success(sysUserMapper.selectByPrimaryKey(id));
        }
        return Rx.fail();
    }

    @Override
    public R modifyEnableAccount(Long id, SysUser currentUser) {
        SysUser user = sysUserMapper.selectByPrimaryKey(id);
        if (user == null) {
            return Rx.fail("管理员账户不存在或已被删除");
        }
        if (user.getStatus().equalsIgnoreCase(DictConstant.DISABLE)) {
            user.setStatus(DictConstant.ENABLE);
        } else {
            user.setStatus(DictConstant.DISABLE);
        }
        if (sysUserMapper.updateStatusById(user.getStatus(), id, currentUser.getId()) == 1) {
            return Rx.success(sysUserMapper.selectByPrimaryKey(id));
        }
        return Rx.fail();
    }

    @Override
    public R changeBranchMerchantUserPassword(Long id, String password, SysUser currentUser) {
        SysUser user = sysUserMapper.selectByPrimaryKey(id);

        if (user == null || user.getMerchantId() == null) {
            return Rx.fail("分站用户不存在或已被删除");
        }
        if (CommonUtils.isMerchantType(currentUser) && !currentUser.getMerchantId().equals(user.getMerchantId())) {
            return Rx.fail("不允许访问");
        }
        int x = sysUserMapper.changePassword(id, passwordEncoder.encode(password), currentUser.getId());
        if (x == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public BranchUserVo selectBranchMerchantUser(Long id) {
        return sysUserMapper.selectBranchMerchantUser(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public synchronized R modifyBranchMerchantUserPoints(Long id, Integer type, Integer amount, SysUser currentUser, String remark) {
        SysUser user = sysUserMapper.selectByPrimaryKey(id);
        if (user == null || user.getMerchantId() == null) {
            return Rx.fail("分站用户不存在或已被删除");
        }
        if (CommonUtils.isMerchantType(currentUser) && !currentUser.getMerchantId().equals(user.getMerchantId())) {
            return Rx.fail("不允许访问");
        }
        Points_Record_Type recordType;
        if (type.equals(DictConstant.MINUS)) {
            amount = -amount;
            recordType = Points_Record_Type.MASTER_MODIFY_MINUS;
        } else {
            recordType = Points_Record_Type.MASTER_MODIFY_ADD;
        }
        BalanceUpdateParam param = BalanceUpdateParam.builder().remark(remark)
                .businessId(id).userTypeEnum(UserTypeEnum.MERCHANT_USER_TYPE)
                .recordType(recordType).amount(amount).createBy(currentUser.getId())
                .build();
        return balanceService.updateBalance(param);
    }

    @Override
    public SysUser selectByInvitationCode(String uid) {
        if (StrUtil.isBlank(uid)) {
            return null;
        }
        try {
            return sysUserMapper.selectByInvitationCode(Integer.valueOf(uid));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public R registerBranchUser(RegisterBean registerBean, SysUser puser) {
        SysUser sysUser = new SysUser();
        sysUser.setStatus(DictConstant.ENABLE);
        sysUser.setRoleId(null);
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(registerBean.getDomain());
        if (merchant != null) {
            sysUser.setMerchantId(merchant.getId());
        } else {
            log.error("根据域名{}查询商户信息不存在", registerBean.getDomain());
            return Rx.fail("根据域名{" + registerBean.getDomain() + "}查询商户信息不存在");
        }
        //查询此商户是否有相同用户名的用户
        if (this.selectAllByUsernameAndMerchantId(registerBean.getUsername(), merchant.getId()).size() > 0) {
            return Rx.fail("用户名已存在");
        }
        //查询此商户是否有相同手机号的用户
        if (this.selectAllByPhoneAndMerchantId(registerBean.getTelephone(), merchant.getId()).size() > 0) {
            return Rx.fail("手机号已存在");
        }
        // 查询上级推广人
        if (puser != null) {
            // 判断域名和推广人是否一致
            if (!puser.getMerchantId().equals(merchant.getId())) {
                log.info("邀请码和域名不属于同一商户");
            } else {
                sysUser.setSuperiorId(puser.getId());
            }
        }
        // 将密码进行加密操作
        String encodePassword = passwordEncoder.encode(registerBean.getPassword());
        sysUser.setPassword(encodePassword);
        sysUser.setUsername(registerBean.getUsername());
        // 校验验证码
        if (!mobileMsg.checkSms(registerBean.getTelephone(), registerBean.getSmsCode())) {
            log.error("验证码错误");
            return Rx.fail("验证码错误");
        }
        sysUser.setTelephone(registerBean.getTelephone());
        sysUser.setInvitationCode(AppUtils.getInvitationCode());
        sysUser.setUserType(UserTypeEnum.MERCHANT_USER_TYPE.getType());
        sysUser.setStatus(DictConstant.ENABLE);
        // 初始化用户等级列表
        UserLevel defaultUserLevel = userLevelService.getDefaultUserLevel(sysUser.getMerchantId());
        sysUser.setUserLevel(defaultUserLevel.getId());
        sysUserMapper.insert(sysUser);
        if (sysUser.getId() != null) {
            initUserExpand(sysUser);
        }
        return Rx.success(sysUser);
    }

    private void initUserExpand(SysUser sysUser) {
        log.info(">>> 初始化推广信息拓展信息表");
        try {
            // 初始化推广信息表
            userExpandMapper.insert(new UserExpand(sysUser.getId()));
            // 更新上级用户推广人数
            if (sysUser.getSuperiorId() != null) {
                userExpandMapper.addSpreadCountByUid(sysUser.getSuperiorId());
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            DentalCabotClient.sendText(e.getLocalizedMessage());
        }
        log.info("初始化以及更新操作完成 <<<");
    }


    @Override
    public SysUser selectByLoginBean(LoginBean loginBean) {
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(loginBean.getDomain());
        if (merchant == null) {
            return null;
        }
        return sysUserMapper.selectByUsernameType(loginBean.getUsername(),
                merchant.getId(), UserTypeEnum.MERCHANT_USER_TYPE.getType());
    }

    @Override
    public SysUser selectOneByUsernameAndDomain(String usernameAndDomain) {
        String[] strArr = usernameAndDomain.split(",");
        String domain = null;
        if (strArr.length > 1) {
            domain = usernameAndDomain.split(",")[1];
        }
        String username = usernameAndDomain.split(",")[0];
        return selectOneByUsernameAndDomain(username, domain);
    }

    @Override
    public SysUser selectOneByUsernameAndDomain(String username, String domain) {
        // 查询商户
        List<SysUser> sysUsers = selectAllByUsernameAndDomain(username, domain);
        if (sysUsers.size() == 1) {
            return sysUsers.get(0);
        }
        return null;
    }

    @Override
    public List<SysUser> selectAllByUsernameAndMerchantId(String username, Long merchantId) {
        // 查询商户
        SysUser sysUser = new SysUser();
        sysUser.setUsername(username);
        sysUser.setMerchantId(merchantId);
        return sysUserMapper.selectByAll(sysUser);
    }

    @Override
    public List<SysUser> selectAllByUsernameAndDomain(String username, String domain) {
        /*----------------本地测试使用----------------------------*/
        List<SysUser> sysUsers = new ArrayList<>();
        if ("admin".equals(username) && ("localhost:8080".equals(domain) || "localhost:8081".equals(domain))) {
            sysUsers.add(sysUserMapper.selectByPrimaryKey(1L));
            return sysUsers;
        }
        if ("branch".equals(username) && ("localhost:8080".equals(domain) || "localhost:8081".equals(domain))) {
            sysUsers.add(sysUserMapper.selectByPrimaryKey(5L));
            return sysUsers;
        }
        if ("branch_user1".equals(username) && ("localhost:8080".equals(domain) || "localhost:8081".equals(domain))) {
            sysUsers.add(sysUserMapper.selectByPrimaryKey(8L));
            return sysUsers;
        }
        if ("branch_user2".equals(username) && ("localhost:8080".equals(domain) || "localhost:8081".equals(domain))) {
            sysUsers.add(sysUserMapper.selectByPrimaryKey(9L));
            return sysUsers;
        }
        if ("api".equals(username) && ("localhost:8080".equals(domain) || "localhost:8081".equals(domain))) {
            sysUsers.add(sysUserMapper.selectByPrimaryKey(6L));
            return sysUsers;
        }
        /*----------------本地测试使用----------------------------*/
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(domain);
        if (merchant != null && merchant.getId() != 1) {
            return selectAllByUsernameAndMerchantId(username, merchant.getId());
        } else {
            return sysUserMapper.selectByUsernameMerIsNull(username);
        }
    }

    @Override
    public List<SysUser> selectAllByPhoneAndMerchantId(String telephone, Long merchantId) {
        SysUser sysUser = new SysUser();
        sysUser.setTelephone(telephone);
        sysUser.setMerchantId(merchantId);
        return sysUserMapper.selectByAll(sysUser);
    }

    @Override
    public List<SysUser> selectAllByPhoneAndDomain(String telephone, String domain) {
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(domain);
        if (merchant != null) {
            return selectAllByPhoneAndMerchantId(telephone, merchant.getId());
        } else if (StringUtils.isBlank(domain)) {
            return sysUserMapper.selectAllByPhoneAndDomainIsNull(telephone);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public R getInvitationUserList(SysUser user, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        BranchUserQuery query = new BranchUserQuery();
        query.setSuperiorId(user.getId());
        List<BranchUserVo> list = sysUserMapper.selectBranchUserList(query);
        return Rx.success(new PageData(new PageInfo<>(list)));
    }

    @Override
    public Integer getInvitationUserCount(Long userId) {
        return sysUserMapper.countBySuperiorId(userId);
    }

    @Override
    public R updateUserTelephone(String telephone, String code, SysUser user) {
        // 验证码校验逻辑
        if (!ParamCheckUtils.telephoneCheck(telephone)) {
            return Rx.fail("手机号码格式不正确");
        }
        if (telephone.equals(user.getTelephone())) {
            return Rx.fail("新手机号码和原来的手机号码相同");
        }
        if (sysUserMapper.selectOneByTelephoneAndMerchantId(telephone, user.getMerchantId()) != null) {
            return Rx.fail("手机号码: " + telephone + "已存在");
        }
        //校验验证码
        if (!mobileMsg.checkSms(user.getTelephone(), code)) {
            log.error("验证码错误");
            return Rx.fail("验证码错误");
        }
        sysUserMapper.updateTelephoneById(telephone, user.getId());
        return Rx.success();
    }

    @Override
    public SysLoginInfo getLastLoginInfo(String username, String domain) {
        return loginInfoMapper.selectLastLoginInfo(username, domain);
    }

    @Override
    public R configCsQrCode(Long id, String picUrl, SysUser user) {
        SysUserInfo userInfo = userInfoMapper.selectByPrimaryKey(id);
        if (userInfo == null) {
            userInfoMapper.insert(new SysUserInfo(id, picUrl, user.getId()));
        } else {
            userInfo.setQrUrl(picUrl);
            userInfo.setUpdateBy(user.getId());
            userInfoMapper.updateByPrimaryKey(userInfo);
        }
        return Rx.success();
    }

    @Override
    public String getCsQrCodeUrl(Long id) {
        SysBranchMerchant merchant = merchantMapper.selectByPrimaryKey(id);
        if (merchant == null) {
            return "";
        }
        return merchant.getCsUrl();
    }

    @Override
    public SysUser selectMerchantSuperAdmin(String username, Long merchantId) {
        return sysUserMapper.selectMerchantSuperAdmin(username, merchantId);
    }

    @Override
    public int deleteAllByRoleId(Long roleId, Long id) {
        return sysUserMapper.deleteAllByRoleId(roleId, id);
    }

    //    "序号", "用户ID", "账号USERNAME", "真实姓名", "手机号码", "会员等级", "提成",
//                        "归属分站名称", "注册时间", "推广码", "上级推广码", "上级USERNAME", "上级真实姓名",
//                        "积分余额", "推广积分余额", "推广人数", "充值人数", "转化率", "累计充值"
    private List<Object[]> getDataList(List<BranchUserVo> branchUserVoList) {
        List<Object[]> resultList = new ArrayList<>();
        Object[] arr;
        int index = 1;
        for (BranchUserVo branchUserVo : branchUserVoList) {
            arr = new Object[20];
            arr[0] = index++;
            arr[1] = branchUserVo.getId();
            arr[2] = branchUserVo.getUsername();
            arr[3] = branchUserVo.getRealName();
            arr[4] = branchUserVo.getTelephone();
            arr[5] = branchUserVo.getLevelName();
            arr[6] = branchUserVo.getPercent();
            arr[7] = branchUserVo.getMerchantName();
            arr[8] = DateFormatUtils.format(branchUserVo.getRegistrationTime());
            arr[9] = branchUserVo.getInvitationCode();
            arr[10] = branchUserVo.getPInviteCode();
            arr[11] = branchUserVo.getPUserName();
            arr[12] = branchUserVo.getPRealName();
            arr[13] = branchUserVo.getUserPoints();
            arr[14] = branchUserVo.getSpreadBalance();
            arr[15] = branchUserVo.getSpreadCount();
            arr[16] = branchUserVo.getRechargeCount();
            arr[17] = branchUserVo.getRate();
            arr[18] = branchUserVo.getRechargeAmount();
            arr[19] = branchUserVo.getSelfRechargeAmount();
            resultList.add(arr);
        }
        return resultList;
    }


}
