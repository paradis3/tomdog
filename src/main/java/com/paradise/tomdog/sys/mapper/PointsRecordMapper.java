package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.PointsRecord;
import com.paradise.tomdog.sys.entity.PointsRecordQueryBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 积分变动记录Mapper
 *
 * @author Paradise
 */
@Mapper
public interface PointsRecordMapper {
    /**
     * delete by primary key
     *
     * @param recordId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long recordId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(PointsRecord record);

    /**
     * select by primary key
     *
     * @param recordId primary key
     * @return object by primary key
     */
    PointsRecord selectByPrimaryKey(Long recordId);

    /**
     * select by condition
     *
     * @param pointsRecord 查询条件
     * @return select result
     */
    List<PointsRecord> selectByAll(PointsRecordQueryBean pointsRecord);

    /**
     * 关联查询 分站商户积分记录
     *
     * @param pointsRecord 查询条件
     * @return select result
     */
    List<PointsRecord> selectMerchantRecord(PointsRecordQueryBean pointsRecord);

    /**
     * 关联查询 API商户积分记录
     *
     * @param pointsRecord 查询条件
     * @return select result
     */
    List<PointsRecord> selectApiRecord(PointsRecordQueryBean pointsRecord);

    /**
     * 关联查询 分站用户积分记录
     *
     * @param pointsRecord 查询条件
     * @return select result
     */
    List<PointsRecord> selectUserRecord(PointsRecordQueryBean pointsRecord);


    /**
     * 根据任务ID和积分变更类型查询积分变更数量
     *
     * @param taskId     任务id
     * @param recordType 记录类型
     * @return 变更数量
     */
    PointsRecord selectByTaskIdAndTypeNotMerchant(@Param("taskId") String taskId,
                                                  @Param("recordType") String recordType);

    /**
     * 根据任务ID和积分变更类型查询积分变更数量
     *
     * @param taskId     任务id
     * @param recordType 记录类型
     * @param userType   用户类型
     * @return 变更数量
     */
    PointsRecord selectByTaskIdAndType(@Param("taskId") String taskId,
                                       @Param("recordType") String recordType,
                                       @Param("userType") Integer userType);

    /**
     * 条件查询积分变更数量
     *
     * @param businessId 业务ID
     * @param userType   用户类型
     * @param taskId     任务ID
     * @param recordType 记录类型
     * @return amount
     */
    Integer selectAmountByCondition(@Param("businessId") Long businessId,
                                    @Param("userType") Integer userType,
                                    @Param("taskId") String taskId,
                                    @Param("recordType") String recordType);


}