package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.TbShop;
import com.paradise.tomdog.sys.mapper.TbShopMapper;
import com.paradise.tomdog.sys.service.TbShopService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Paradise
 */
@Service
public class TbShopServiceImpl implements TbShopService {

    @Resource
    private TbShopMapper tbShopMapper;

    @Override
    public int insert(TbShop record) {
        if (record.getId() == null) {
            return tbShopMapper.insert(record);
        } else {
            return tbShopMapper.updateByPrimaryKey(record);
        }
    }

    @Override
    public TbShop selectByUrl(String url) {
        return tbShopMapper.selectOneByUrl(url);
    }

}
