package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysUserApi;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface SysUserApiMapper {
    /**
     * delete by primary key
     *
     * @param id       primaryKey
     * @param updateBy updateBy
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("id") Long id,
                           @Param("updateBy") Long updateBy);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysUserApi record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysUserApi record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysUserApi selectByPrimaryKey(Long id);

    /**
     * 根据 userId 查询 api user info
     *
     * @param userId userId
     * @return select result
     */
    SysUserApi selectByUserId(Long userId);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysUserApi record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysUserApi record);

    /**
     * 条件查询列表
     *
     * @param sysUserApi 查询条件
     * @return result list
     */
    List<SysUserApi> selectByAll(SysUserApi sysUserApi);

    /**
     * 根据 username 查询 Api user
     *
     * @param username 用户名
     * @return select result
     */
    SysUserApi selectByUsername(String username);

    /**
     * 查询数量
     *
     * @return 未删除的API商户数量
     */
    Long countByIsDel();
    

}