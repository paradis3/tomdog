package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TbShop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 淘宝店铺
 *
 * @author Paradise
 */
@Mapper
public interface TbShopMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbShop record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TbShop selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbShop record);

    /**
     * 根据url 查询
     *
     * @param url 淘宝店铺url
     * @return select result
     */
    TbShop selectOneByUrl(@Param("url") String url);


}