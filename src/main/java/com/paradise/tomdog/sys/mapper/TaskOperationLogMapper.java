package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskOperationLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Paradise
 */
@Mapper
public interface TaskOperationLogMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskOperationLog record);
}