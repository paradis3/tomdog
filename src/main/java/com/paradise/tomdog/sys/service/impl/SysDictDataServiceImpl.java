package com.paradise.tomdog.sys.service.impl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import com.paradise.tomdog.sys.entity.SysDictData;
import com.paradise.tomdog.sys.mapper.SysDictDataMapper;
import com.paradise.tomdog.sys.service.SysDictDataService;

@Service
public class SysDictDataServiceImpl implements SysDictDataService {

    @Resource
    private SysDictDataMapper sysDictDataMapper;

    @Override
    public int deleteByPrimaryKey(Long dictCode) {
        return sysDictDataMapper.deleteByPrimaryKey(dictCode);
    }

    @Override
    public int insert(SysDictData record) {
        return sysDictDataMapper.insert(record);
    }

    @Override
    public int insertSelective(SysDictData record) {
        return sysDictDataMapper.insertSelective(record);
    }

    @Override
    public SysDictData selectByPrimaryKey(Long dictCode) {
        return sysDictDataMapper.selectByPrimaryKey(dictCode);
    }

    @Override
    public int updateByPrimaryKeySelective(SysDictData record) {
        return sysDictDataMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(SysDictData record) {
        return sysDictDataMapper.updateByPrimaryKey(record);
    }

}
