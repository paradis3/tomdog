package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "com.paradise.tomdog.sys.entity.TaskRefund")
@Data
public class TaskRefund {
    private Long refundId;

    /**
     * 任务id
     */
    @ApiModelProperty(value = "任务id")
    private String taskId;

    @ApiModelProperty(value = "处理状态：0未处理；1：已处理；")
    private String status;

    /**
     * 猎流返回code
     */
    @ApiModelProperty(value = "猎流返回code")
    private String statusCode;

    /**
     * 猎流返回的描述
     */
    @ApiModelProperty(value = "猎流返回的描述")
    private String statusDesc;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    private Date createTime;
}