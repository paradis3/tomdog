package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.TbItemInfo;
import com.paradise.tomdog.sys.mapper.TaskTbItemMapper;
import com.paradise.tomdog.sys.service.TaskTbItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TaskTbItemServiceImpl implements TaskTbItemService {

    @Resource
    private TaskTbItemMapper taskTbItemMapper;

    @Override
    public int deleteByPrimaryKey(Long taskId) {
        return taskTbItemMapper.deleteByPrimaryKey(taskId);
    }

    @Override
    public int insert(TbItemInfo record) {
        return taskTbItemMapper.insert(record);
    }

    @Override
    public TbItemInfo selectByPrimaryKey(Long taskId) {
        return taskTbItemMapper.selectByPrimaryKey(taskId);
    }

    @Override
    public int updateByPrimaryKey(TbItemInfo record) {
        return taskTbItemMapper.updateByPrimaryKey(record);
    }

}
