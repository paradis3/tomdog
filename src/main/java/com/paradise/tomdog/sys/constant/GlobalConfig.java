package com.paradise.tomdog.sys.constant;

/**
 * 删除状态枚举
 *
 * @author Paradise
 */
public enum GlobalConfig {
    /**
     * 短信扣积分配置id
     */
    SMS("1", "未删除");

    private String code;
    private String name;

    GlobalConfig(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
