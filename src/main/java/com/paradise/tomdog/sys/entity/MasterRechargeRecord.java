package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-MasterRechargeRecord")
@Data
public class MasterRechargeRecord {
    /**
     * 充值ID
     */
    @ApiModelProperty(value = "充值ID")
    private Long id;

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 用户所属的商户id
     */
    @ApiModelProperty(value = "用户所属的商户id")
    private Long merchantId;

    /**
     * 充值积分
     */
    @ApiModelProperty(value = "充值积分")
    private Integer rechargePoints;

    /**
     * 支付金额
     */
    @ApiModelProperty(value = "支付金额")
    private BigDecimal payAmount;

    /**
     * 1：支付宝；2：微信支付 3：QQ支付
     */
    @ApiModelProperty(value = "1：支付宝；2：微信支付 3：QQ支付")
    private String payType;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    /**
     * 支付流水号
     */
    @ApiModelProperty(value = "支付流水号")
    private String payNo;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private Date payTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 套餐ID
     */
    @ApiModelProperty(value = "套餐ID")
    private Long packageId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private Long updateBy;

    /**
     * 订单状态（0 创建 1待支付 2 支付成功 3支付失败）
     */
    @ApiModelProperty(value = "订单状态（0 创建 1待支付 2 支付成功 3支付失败）")
    private String status;

    /**
     * 删除标记 1=删除
     */
    @ApiModelProperty(value = "删除标记 1=删除")
    private String isDel;

    @ApiModelProperty(value = "总积分余额")
    private Integer balance;
}