package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.service.SysMenuService;
import com.paradise.tomdog.sys.service.SysUserApiService;
import com.paradise.tomdog.sys.service.SysUserService;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Paradise
 */
@Api(tags = "后台管理：系统管理接口")
@RestController
@RequestMapping("/sys")
@Slf4j
public class MasterUserController {

    @Value("${jwt.tokenHead}")
    private String tokenHead;
    private final SysUserService sysUserService;
    private final SysUserApiService userApiService;
    private final SysMenuService menuService;

    @Autowired
    public MasterUserController(SysUserService sysUserService, SysUserApiService userApiService,
                                SysMenuService menuService) {
        this.sysUserService = sysUserService;
        this.userApiService = userApiService;
        this.menuService = menuService;
    }

    @ApiIgnore
    @PostMapping("/register")
    @ApiOperation("注册废弃接口")
    @ControllerWebLog
    public R registry(@ApiParam(value = "用户名密码") @RequestBody RegisterBean registerBean) {
        return Rx.success(sysUserService.register(registerBean));
    }

    @ApiOperation(value = "登录以后返回token")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    @ControllerWebLog
    public R login(@ApiParam(value = "用户名密码") @RequestBody RegisterBean registerBean,
                   HttpServletRequest request) {
        R<String> loginRes = sysUserService.login(registerBean.getDomain(), registerBean.getUsername(), registerBean.getPassword(), request);
        if (!Rx.isSuccess(loginRes)) {
            return loginRes;
        }
        SysUser userInfo = sysUserService.selectOneByUsernameAndDomain(registerBean.getUsername(), registerBean.getDomain());
        if (CommonUtils.isMerchantUserType(userInfo)) {
            return Rx.fail("账号不存在");
        }
        userInfo.setPassword(null);
        if (CommonUtils.isAdminType(userInfo)) {
            userInfo.setMerchantId(1L);
        }
        Map<String, Object> tokenMap = new HashMap<>(3);
        tokenMap.put("token", loginRes.getData());
        tokenMap.put("tokenHead", tokenHead);
        tokenMap.put("userInfo", userInfo);
        return Rx.success(tokenMap);
    }

    @ApiOperation(value = "管理员修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newPassword", value = "新密码", defaultValue = "newPassword"),
            @ApiImplicitParam(name = "password", value = "旧密码", defaultValue = "password")})
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    @ResponseBody
    @ControllerWebLog
    @PreAuthorize("hasAuthority('pms:mba:sys:changePassword')")
    public R changePassword(@RequestParam("password") String password,
                            @RequestParam("newPassword") String newPassword,
                            @ApiIgnore Principal principal) {
        if (StringUtils.isBlank(password)) {
            return Rx.error("密码不能为空");
        }
        return sysUserService.changePassword(newPassword, password, principal);
    }

    @ApiIgnore
    @ApiOperation(value = "管理员重置密码（禁用）")
    @ApiImplicitParams({@ApiImplicitParam(name = "password", value = "新密码", defaultValue = "password")})
    @RequestMapping(value = "/changePassword2", method = RequestMethod.POST)
    @ResponseBody
    public R changePassword2(String password, Long id) {
        if (StringUtils.isBlank(password)) {
            return Rx.error("密码不能为空");
        }
        return sysUserService.resetPassword(password, id);
    }

    @ApiIgnore
    @ApiOperation("获取用户所有权限")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "用户id")})
    @RequestMapping(value = "/permission/{id}", method = RequestMethod.GET)
    @ResponseBody
    public R getPermissionList(@PathVariable Long id) {
        List<SysPermission> permissionList = sysUserService.getPermissionList(id);
        return Rx.success(permissionList);
    }

    @ApiOperation("拉取用户信息")
    @GetMapping(value = "/info")
    @ResponseBody
    public R getUserInfo(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        Integer isMater = CommonUtils.isAdminType(currentUser) ? 1 : 0;
        List<SysMenu> menuList = menuService.selectByRole(currentUser.getRoleId(), isMater);
        menuService.listToTree(menuList);
        SysUser userInfo = sysUserService.getUserInfo(CommonUtils.getUserId(principal));
        if (CommonUtils.isAdminType(userInfo)) {
            userInfo.setMerchantId(1L);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("menuList", menuList);
        map.put("userInfo", userInfo);
        return Rx.success(map);
    }

    @ApiOperation("获取API用户信息")
    @GetMapping(value = "/getApiInfo")
    @ResponseBody
    @ControllerWebLog(name = "后台获取Api商户信息")
    public R getApiInfo(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (!CommonUtils.isApiType(currentUser)) {
            return Rx.permissionDenied();
        }
        SysUserApi api = userApiService.selectByUserId(currentUser.getId());
        return Rx.success(api);
    }

    @ApiOperation("重置API商户信息-ApiKey,secret")
    @PostMapping(value = "/resetApiInfo")
    @ResponseBody
    @ControllerWebLog(name = "重置API商户信息-ApiKey,secret")
    public R resetApiInfo(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (!CommonUtils.isApiType(currentUser)) {
            return Rx.permissionDenied();
        }
        return userApiService.resetApiKeySecret(currentUser);
    }

}
