package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author Paradise
 */
@Getter
@Setter
@ApiModel
public class SysUserPayInfoQueryBean {
    @NotNull
    @ApiModelProperty(example = "1")
    private Integer pageNum;
    @NotNull
    @ApiModelProperty(example = "5")
    private Integer pageSize;
    
    @ApiModelProperty(value = "查询开始时间（时间戳）", example = "1575356306119")
    private Long beginDate;
    @ApiModelProperty(value = "查询结束时间（时间戳）", example = "1575356906119")
    private Long endDate;
    @ApiModelProperty(value = "1: 加积分；0：减积分", example = "1")
    private Integer type;


    @ApiModelProperty(value = "分站商户ID", hidden = true)
    private Long merchantId;
    @ApiModelProperty(value = "CmwAPI 商户ID", hidden = true)
    private Long apiUserId;

    @ApiModelProperty(hidden = true)
    private Long operator;

    @ApiModelProperty(value = "分站用户ID", hidden = true)
    private Long userId;
}
