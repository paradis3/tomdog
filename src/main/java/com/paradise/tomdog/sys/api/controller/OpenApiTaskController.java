package com.paradise.tomdog.sys.api.controller;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.exception.ApiException;
import com.paradise.tomdog.base.utils.AppUtils;
import com.paradise.tomdog.rpc.utils.LlUtils;
import com.paradise.tomdog.sys.api.param.*;
import com.paradise.tomdog.sys.api.res.ApiTaskResult;
import com.paradise.tomdog.sys.constant.*;
import com.paradise.tomdog.sys.entity.LlTask;
import com.paradise.tomdog.sys.entity.SysUserApi;
import com.paradise.tomdog.sys.service.LlTaskService;
import com.paradise.tomdog.sys.service.PointsRecordService;
import com.paradise.tomdog.sys.service.SysUserApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 开放 API
 *
 * @author Paradise
 */
@Api
@Slf4j
@RestController
@RequestMapping("/api")
public class OpenApiTaskController {

    @Value("${apiTestFlag}")
    private boolean apiTestFlag;

    private LlTaskService taskService;
    private SysUserApiService userApiService;
    private PointsRecordService pointsRecordService;

    @Autowired
    public void setTaskService(LlTaskService taskService) {
        this.taskService = taskService;
    }

    @Autowired
    public void setUserApiService(SysUserApiService userApiService) {
        this.userApiService = userApiService;
    }

    /**
     * API 创建任务
     *
     * @param task 任务参数
     * @return R
     */
    @ApiOperation("创建任务")
    @GetMapping("/task/create")
    public R createTask(TaskCreateParam task) throws ApiException {
        R<SysUserApi> res;
        try {
            res = this.check(task);
            if (Rx.isApiSuccess(res)) {
                if (StrUtil.isEmpty(task.getTaskId())) {
                    return Rx.apiFail("taskId 不能为空");
                }
                return taskService.apiCreateTask(task, res.getData());
            } else {
                return res;
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new ApiException("创建任务异常", e);
        }
    }

    private boolean validateTimestamp(Long timestamp) {
        return Math.abs(LlUtils.getCurrentSeconds() - timestamp) / 60 < 10;
    }

    @ApiOperation("取消任务")
    @GetMapping("/task/cancel")
    public R cancelTask(TaskCancelParam param) throws ApiException {
        // 前置处理、参数校验、时间戳、验签
        // 根据 username 查询 api - user
        R<SysUserApi> res;
        try {
            res = this.check(param);
            if (Rx.isApiSuccess(res)) {
                if (StrUtil.isEmpty(param.getTaskId())) {
                    return Rx.apiFail("taskId 不能为空");
                }
                LlTask task = taskService.selectByTaskId(param.getTaskId());
                // 判断任务状态，只能是进行中的任务
                if (!task.getStatus().equals(Task_Status.ING.getCode())) {
                    return Rx.apiFail("任务状态不是进行中");
                }
                SysUserApi userApi = res.getData();
                if (!task.getUserId().equals(userApi.getUserId())) {
                    return Rx.apiFail("任务不存在");
                }
                return taskService.cancelApiTask(task);
            } else {
                return res;
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new ApiException("取消任务异常", e);
        }
    }

    @ApiOperation("查询任务")
    @GetMapping("/task/query")
    public R queryTask(TaskQueryParam param) throws ApiException {
        // 根据 username 查询 api - user
        R res;
        try {
            res = this.check(param);
            if (Rx.isApiSuccess(res)) {
                if (StrUtil.isEmpty(param.getTaskId())) {
                    return Rx.apiFail("taskId 不能为空");
                }
                // 返回任务状态 进行中、申请取消中、已取消（积分退还情况）、部分完成（积分退回情况）、全部完成
                LlTask task = taskService.selectByTaskId(param.getTaskId());
                SysUserApi userApi = (SysUserApi) res.getData();
                if (task == null || !task.getUserId().equals(userApi.getUserId())) {
                    return Rx.apiFail("任务不存在");
                }
                ApiTaskResult result = new ApiTaskResult(task, TaskTypeTools.getType(task.getType()).getName(),
                        0, getStatusDesc(task.getStatus()));
                if (task.getStatus().equals(Task_Status.PART_COMPLETED.getCode())) {
                    // 查询退回积分 通过 taskId 查询 记录表
                    Integer backPoints = pointsRecordService.getAmountByCondition(userApi.getId(), UserTypeEnum.API_TYPE,
                            task.getTaskId(), Points_Record_Type.TASK_CANCEL);
                    result.setBackPoints(backPoints);
                } else if (task.getStatus().equals(Task_Status.CANCELED.getCode())) {
                    result.setBackPoints(task.getCostPoints());
                }
                return Rx.apiSuccess("任务查询成功", result);
            } else {
                return res;
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new ApiException("查询任务异常", e);
        }
    }

    private String getStatusDesc(String code) {
        for (Task_Status status : Task_Status.values()) {
            if (status.getCode().equals(code)) {
                return status.getName();
            }
        }
        return "";
    }

    @ApiOperation("获取系统时间戳")
    @GetMapping("/timestamp")
    public R timestamp() {
        return Rx.apiSuccess("获取成功", LlUtils.getCurrentSeconds());
    }

    @ApiOperation("获取API商户积分余额")
    @GetMapping("/balance")
    public R balance(UserBalanceParam param) throws ApiException {
        R res;
        try {
            res = this.check(param);
            if (Rx.isApiSuccess(res)) {
                SysUserApi userApi = (SysUserApi) res.getData();
                return Rx.apiSuccess("获取成功", userApiService.getApiUserBalance(userApi.getId()));
            } else {
                return res;
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            throw new ApiException("获取API商户积分余额异常", e);
        }
    }

    /**
     * 参数校验方法
     *
     * @param param api 参数接口
     * @return {@link R} 包含 SysUserApi -data
     */
    private R<SysUserApi> check(ApiParamInterface param) {
        // 根据 username 查询 api - user
        SysUserApi userApi = userApiService.selectByUsername(param.getUsername());
        if (userApi == null) {
            return Rx.apiFail("username 不存在");
        }
        if (userApi.getStatus().equals(DictConstant.DISABLE)) {
            return Rx.apiFail("账户已被禁用");
        }
        if (param.getTimestamp() == null) {
            return Rx.apiFail("时间戳为空");
        } else if (!validateTimestamp(param.getTimestamp())) {
            return Rx.apiFail("时间戳不正确或已过期");
        }
        String sign = param.createSign(userApi.getAppSecret(), "");
        log.info("Sign:{}", sign);
        if (!param.getSign().equals(sign)) {
            // 生成八位混淆码
            if (apiTestFlag) {
                return Rx.apiFail("签名错误" + AppUtils.getInvitationCode() + sign);
            }
            return Rx.fail("签名错误");
        }
        return Rx.apiSuccess("", userApi);
    }

    @Autowired
    public void setPointsRecordService(PointsRecordService pointsRecordService) {
        this.pointsRecordService = pointsRecordService;
    }
}
