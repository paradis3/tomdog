package com.paradise.tomdog.sys.controller;

import cn.hutool.core.util.StrUtil;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.SysUserEdit;
import com.paradise.tomdog.sys.entity.UserLevel;
import com.paradise.tomdog.sys.service.SysUserService;
import com.paradise.tomdog.sys.service.UserLevelService;
import com.paradise.tomdog.sys.vo.BranchUserQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.security.Principal;

/**
 * 总站：分站用户管理 controller
 *
 * @author Paradise
 */
@RestController
@Slf4j
@Api(tags = "后台管理：分站用户管理")
@Validated
@RequestMapping("/sys/branchUser/")
public class MasterBranchMerchantUserController {

    private final SysUserService userService;
    private final UserLevelService userLevelService;

    public MasterBranchMerchantUserController(SysUserService userService, UserLevelService userLevelService) {
        this.userService = userService;
        this.userLevelService = userLevelService;
    }

    @GetMapping("/getBranchMerchantUserPage")
    @ApiOperation("分页查询分站用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", defaultValue = "5", paramType = "query")
    })
    @PreAuthorize("hasAuthority('pms:mb:branchUser:getBranchMerchantUserPage')")
    public R getBranchMerchantUserPage(BranchUserQuery query, @ApiIgnore Principal principal,
                                       @NotNull(message = "pageNum 不能为空") Integer pageNum,
                                       @NotNull(message = "pageSize 不能为空") Integer pageSize) {
        // 如果当前登录用户是分站商户，只查询自己分站下的用户信息
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isMerchantType(currentUser)) {
            query.setMerchantId(currentUser.getMerchantId());
        }
        PageData pageData = userService.getBranchMerchantUserPage(query, pageNum, pageSize);
        return Rx.success(pageData);

    }

    @GetMapping("/exportBranchMerchantUserList")
    @ApiOperation("查询导出分站用户列表")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:exportBranchMerchantUserList')")
    public void exportBranchMerchantUserList(BranchUserQuery query, HttpServletRequest request,
                                             @ApiIgnore Principal principal,
                                             HttpServletResponse response) {
        // 如果当前登录用户是分站商户，只查询自己分站下的用户信息
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isMerchantType(currentUser)) {
            query.setMerchantId(currentUser.getMerchantId());
        }
        userService.exportBranchMerchantUserList(query, request, response);
    }

    @ApiOperation(("查询分站用户详情"))
    @ApiImplicitParams(@ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"))
    @GetMapping("/getBranchMerchantUser")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:getBranchMerchantUser')")
    public R getBranchMerchantUser(@NotNull(message = "分站用户id不能为空") Long id) {
        return Rx.success(userService.selectBranchMerchantUser(id));
    }

    @ApiOperation("启用、禁用分站用户（状态切换）")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"))
    @PostMapping("/enableBranchMerchantUser")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:enableBranchMerchantUser')")
    public R enableBranchMerchantUser(@NotNull(message = "分站用户id不能为空") Long id,
                                      @ApiIgnore Principal principal) {
        return userService.modifyEnableMerchant(id, CommonUtils.getUser(principal));
    }

    @ApiOperation("修改分站用户密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"),
            @ApiImplicitParam(name = "password", paramType = "query", defaultValue = "new_pass_word"),
    })
    @PostMapping("/changeBranchMerchantUserPassword")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:changeBranchMerchantUserPassword')")
    public R changeBranchMerchantUserPassword(@NotNull(message = "分站用户id不能为空") Long id,
                                              @NotBlank(message = "密码不能为空")
                                              @Length(max = 20, min = 5, message = "密码长度5-20位字符") String password,
                                              @ApiIgnore Principal principal) {
        return userService.changeBranchMerchantUserPassword(id, password, CommonUtils.getUser(principal));
    }

    @ApiOperation("修改分站用户积分")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "修改类型：1 增加积分；0 减少积分；", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "修改积分数量", defaultValue = "2", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "API商户 ID", defaultValue = "201", paramType = "query")
    })
    @PostMapping("/modifyBranchMerchantUserPoints")
    @ControllerWebLog
    @PreAuthorize("hasAuthority('pms:mb:branchUser:modifyBranchMerchantUserPoints')")
    public R modifyBranchMerchantUserPoints(@NotNull(message = "type 不能为空") Integer type,
                                            @NotNull(message = "amount 不能为空") Integer amount,
                                            @NotNull(message = "id 不能为空") Long id,
                                            @NotBlank(message = "备注不能为空") String remark,
                                            @ApiIgnore Principal principal) {
        if (!(type.equals(DictConstant.ADD) || type.equals(DictConstant.MINUS))) {
            return Rx.fail("type 参数格式不正确");
        }
        if (amount <= 0) {
            return Rx.fail("amount 参数格式不正确");
        }
        return userService.modifyBranchMerchantUserPoints(id, type, amount, CommonUtils.getUser(principal), remark);
    }

    @ApiIgnore
    @ApiOperation("配置分站用户客服二维码")
    @PostMapping("/configCsQrCode")
    @ControllerWebLog(name = "配置分站用户客服二维码")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:configCsQrCode')")
    public R configCsQrCode(Long id, String picUrl, @ApiIgnore Principal principal) {
        if (id == null) {
            return Rx.fail("分站用户ID为空");
        }
        if (StrUtil.isBlank(picUrl)) {
            return Rx.fail("二维码图片URL为空");
        }
        return userService.configCsQrCode(id, picUrl, CommonUtils.getUser(principal));
    }


    @ApiOperation("修改分站用户资料")
    @PostMapping(value = "/modifyUserInfo")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:modifyUserInfo')")
    @ControllerWebLog
    public R modifyUserInfo(@Validated @RequestBody SysUserEdit userEdit,
                            @ApiIgnore Principal principal) {
        SysUser sysUser = CommonUtils.getUser(principal);
        log.info("分站商户{}修改用户ID={}资料", sysUser.getUsername(), userEdit.getId());
        log.info(userEdit.toString());
        if (userEdit.getId() == null) {
            return Rx.fail("参数为空异常");
        }
        if (!ParamCheckUtils.telephoneCheck(userEdit.getTelephone())) {
            return Rx.fail("手机号码格式不正确");
        }
        SysUser targetUser = userService.selectByPrimaryKey(userEdit.getId());
        boolean notAllow = CommonUtils.isMerchantType(sysUser) && !targetUser.getMerchantId().equals(sysUser.getMerchantId());
        if (!CommonUtils.isMerchantUserType(targetUser) || notAllow) {
            return Rx.fail("不允许访问");
        }
        // 校验等级是否正确 level 查询不到，非总站并且不是当前商户创建的等级
        UserLevel level = userLevelService.selectByPrimaryKey(userEdit.getUserLevel());
        boolean illegal = level == null ||
                (!CommonUtils.isAdminType(sysUser) && !level.getMerchantId().equals(sysUser.getMerchantId()));
        if (illegal) {
            log.error("传入了不属于该用户的等级ID");
            return Rx.fail("用户等级参数错误，请重新选择");
        }
        int x = userService.updateByPrimaryKeySelective(userEdit.toUser());
        if (x == 1) {
            return Rx.success();
        } else {
            return Rx.fail();
        }
    }

    @ApiOperation("获取用户信息详情")
    @PostMapping(value = "/getUserInfo")
    @PreAuthorize("hasAuthority('pms:mb:branchUser:modifyUserInfo')")
    public R getUserInfo(Long id) {
        if (id == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser user = userService.selectByPrimaryKey(id);
        if (user == null || !CommonUtils.isMerchantUserType(user)) {
            return Rx.fail("不允许访问");
        }
        return Rx.success(user);
    }
}
