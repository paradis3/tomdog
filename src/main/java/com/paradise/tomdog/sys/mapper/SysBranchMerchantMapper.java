package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface SysBranchMerchantMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysBranchMerchant record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysBranchMerchant selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysBranchMerchant record);

    List<SysBranchMerchant> selectByAll(SysBranchMerchant sysBranchMerchant);

    List<SysBranchMerchant> selectAllByUpdateTimeAfter(@Param("minUpdateTime") Date minUpdateTime);

    /**
     * 条件查询列表
     *
     * @param sysBranchMerchant 查询条件
     * @return List
     */
    List<SysBranchMerchant> selectByPage(SysBranchMerchant sysBranchMerchant);

    /**
     * 根据domain 查询分站商户信息
     *
     * @param domain 域名-分站
     * @return {@linkplain SysBranchMerchant}
     */
    SysBranchMerchant selectOneByDomain(@Param("domain") String domain);

    /**
     * select by tel not del limit-1
     *
     * @param telephone tel
     * @return select result
     */
    SysBranchMerchant selectOneByTelephone(@Param("telephone") String telephone);

    /**
     * 查询未删除的分站商户数量
     *
     * @return 未删除的分站商户数量
     */
    Long countBy();
}