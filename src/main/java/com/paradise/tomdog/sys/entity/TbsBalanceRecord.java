package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 推广积分余额变动记录
 *
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.TbsBalanceRecord")
@Data
public class TbsBalanceRecord {
    /**
     * 记录表自增主键
     */
    @ApiModelProperty(value = "记录表自增主键")
    private Long id;
    
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    @NotNull(message = "用户id不能为空")
    private Long userId;
    
    /**
     * 积分数量
     */
    @ApiModelProperty(value = "积分数量")
    @NotNull(message = "积分数量")
    @Min(value = 0)
    private Integer amount;
    
    /**
     * 类型：1：返利 2：提现 3提现失败退回
     */
    @ApiModelProperty(value = "类型：1：返利 2：提现 3提现失败退回")
    private String type;
    
    /**
     * 关联id
     */
    @ApiModelProperty(value = "关联id")
    private Long sourceId;
    
    /**
     * 删除标记 1删除
     */
    @ApiModelProperty(value = "删除标记 1删除")
    private String isDel;
    
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",hidden = true)
    private Long createBy;
    
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",hidden = true)
    private Date createTime;



    public TbsBalanceRecord(Long userId, Integer amount, String type) {
        this.userId = userId;
        this.amount = amount;
        this.type = type;
        this.createTime = new Date();
    }

    public TbsBalanceRecord() {
    }
}