package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.MasterRechargePackage;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.List;

public interface MasterRechargePackageService {
    
    
    int deleteByPrimaryKey(Long id);
    
    int insertSelective(MasterRechargePackage record);
    
    MasterRechargePackage selectByPrimaryKey(Long id);
    
    int updateByPrimaryKeySelective(MasterRechargePackage record);
    
    R updateMasterRechargePackage(MasterRechargePackage rechargePackage);
    
    int insert(MasterRechargePackage record);
    
    int updateByPrimaryKey(MasterRechargePackage record);
    
    List<MasterRechargePackage> selectByAllAndValid(SysUser user);
}


