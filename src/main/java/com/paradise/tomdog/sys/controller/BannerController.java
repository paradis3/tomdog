package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.Banner;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.SysBranchMerchantMapper;
import com.paradise.tomdog.sys.service.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

/**
 * @author Paradise
 */
@Api(tags = "banner相关接口")
@RestController
@RequestMapping("/banner")
@Slf4j
public class BannerController {

    private final BannerService bannerService;
    @Resource
    private SysBranchMerchantMapper merchantMapper;

    public BannerController(BannerService bannerService) {
        this.bannerService = bannerService;
    }

    @ApiOperation("添加或修改Banner")
    @PostMapping("/updateBanner")
    @PreAuthorize("hasAuthority('pms:mb:banner:update')")
    public R<String> updateSmsInfo(Banner info, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isAdminType(user)) {
            info.setMerchantId(user.getMerchantId());
        }
        return bannerService.updateBanner(user, info);
    }

    @ApiOperation("查看banner列表--管理端用")
    @PostMapping("/getBanner2")
    public R<List<Banner>> getBanner2(Long merchantId, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        Banner banner = new Banner();
        if (!CommonUtils.isAdminType(user)) {
            banner.setMerchantId(user.getMerchantId());
        } else {
            banner.setMerchantId(merchantId);
        }
        List<Banner> newsList = bannerService.selectByAll(banner);
        return Rx.success(newsList);
    }

    @ApiOperation("查看banner列表--页面用")
    @PostMapping("/getBanner")
    public R<List<Banner>> getBanner(String domain) {
        Banner banner = new Banner();
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(domain);
        if (merchant != null) {
            banner.setMerchantId(merchant.getId());
        } else {
            log.error("根据域名{}查询商户信息不存在", domain);
            return Rx.fail("根据域名{" + domain + "}查询商户信息不存在");
        }
        banner.setIsEnable(DictConstant.ENABLE);
        List<Banner> newsList = bannerService.selectByAll(banner);
        return Rx.success(newsList);
    }

    @ApiOperation("查看Banner详细信息")
    @PostMapping("/getBannerInfo")
    public R<List<Banner>> getNewsInfo(Integer id) {
        Banner banner = new Banner();
        banner.setId(id);
        List<Banner> newsList = bannerService.selectByAll(banner);
        return Rx.success(newsList);
    }

    @ApiOperation("删除banner")
    @PostMapping("/del")
    @ControllerWebLog(name = "删除banner")
    public R del(Integer id, @ApiIgnore Principal principal) {
        if (id == null) {
            return Rx.fail("ID 不能为空");
        }
        if (bannerService.selectByPrimaryKey(id) == null) {
            return Rx.fail("banner 不存在");
        }
        if (bannerService.deleteByPrimaryKey(id, CommonUtils.getUser(principal)) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

}

