package com.paradise.tomdog.sys.entity;

import lombok.Data;

import java.util.List;

/**
 * 任务详情实体
 *
 * @author Paradise
 */
@Data
public class TaskDetail {

    private LlTask task;
    private List<TaskItem> itemList;
    private TbItemInfo taskTbItem;
}
