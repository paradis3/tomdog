package com.paradise.tomdog.base.security;

import com.paradise.tomdog.base.component.AdminUserDetails;
import com.paradise.tomdog.base.utils.ParamCheckUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysPermission;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.SysPermissionMapper;
import com.paradise.tomdog.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.List;

/**
 * SpringSecurity的配置
 *
 * @author macro
 * @date 2018/4/26
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysPermissionMapper sysPermissionMapper;
    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;
    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf()// 由于使用的是JWT，我们这里不需要csrf
                .disable()
                .cors().and()
                .sessionManagement()// 基于token，所以不需要session
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        // 允许对于网站静态资源的无授权访问
                        "/",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js",
                        "/swagger-resources/**",
                        "/v2/api-docs/**",
                        "/**/showPic/**"
                )
                .permitAll()
                // 对登录注册要允许匿名访问
                .antMatchers("/sys/login", "/user/registerParamCheck", "/callback/rechargeCallBack",
                        "/user/forgetPassword", "/sys/branchMerchant/getICPAndCopyright",
                        "/user/login", "/user/register", "/msg/getNormalCode", "/banner/getBanner", "/news/getNewsList",
                        "/user/changePassword2", "/news/getNewsInfo", "/banner/getBannerInfo")
                .permitAll()
                .antMatchers("/**/api/**")
                .permitAll()
                //跨域请求会先进行一次options请求
                .antMatchers(HttpMethod.OPTIONS)
                .permitAll()
                .anyRequest()// 除上面外的所有请求全部需要鉴权认证
                .authenticated();
        // 禁用缓存
        httpSecurity.headers().cacheControl();
        // 添加JWT filter
        httpSecurity.addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        //添加自定义未授权和未登录结果返回
        httpSecurity.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public UserDetailsService userDetailsService() {
        //获取登录用户信息
        return (usernameAndDomain) -> {
            String[] strArr = usernameAndDomain.split(",");
            String domain = null;
            if (strArr.length > 1) {
                domain = usernameAndDomain.split(",")[1];
            }
            String username = usernameAndDomain.split(",")[0];
            SysUser sysUser = sysUserService.selectOneByUsernameAndDomain(username, domain);
            if (sysUser != null) {
                if (sysUser.getIsDel() == 1) {
                    throw new UsernameNotFoundException("账号已被禁用");
                }
                if (DictConstant.DISABLE.equals(sysUser.getStatus())) {
                    throw new UsernameNotFoundException("账号已被禁用");
                }
                List<SysPermission> permissionList = sysPermissionMapper.getPermissionListByRoleId(Long.valueOf(sysUser.getUserType()));
                return new AdminUserDetails(sysUser, permissionList);
            }
            // 判断是不是手机号
            if (ParamCheckUtils.telephoneCheck(username)) {
                List<SysUser> userList = sysUserService.selectAllByPhoneAndDomain(username, domain);
                if (!userList.isEmpty()) {
                    SysUser user = userList.get(0);
                    return new AdminUserDetails(user,
                            sysPermissionMapper.getPermissionListByRoleId(Long.valueOf(user.getUserType())));
                }
            }
            throw new UsernameNotFoundException("商户已被禁用或删除，或者用户名不存在");
        };
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void setSysUserService(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    @Autowired
    public void setRestfulAccessDeniedHandler(RestfulAccessDeniedHandler restfulAccessDeniedHandler) {
        this.restfulAccessDeniedHandler = restfulAccessDeniedHandler;
    }

    @Autowired
    public void setRestAuthenticationEntryPoint(RestAuthenticationEntryPoint restAuthenticationEntryPoint) {
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
    }
}
