package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.TaskPriceBrowserTimeMapper;
import com.paradise.tomdog.sys.mapper.TaskPriceBtnMapper;
import com.paradise.tomdog.sys.mapper.TaskPriceDepthMapper;
import com.paradise.tomdog.sys.service.TaskPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 系统参数配置 - 价格表 - 配置 控制器
 *
 * @author Paradise
 */
@Api(tags = "后台管理：系统参数配置")
@RestController
@RequestMapping("/sys/cfg/")
@Slf4j
public class SystemCfgController {

    private final TaskPriceService taskPriceService;
    @Resource
    private TaskPriceBrowserTimeMapper priceBrowserTimeMapper;
    @Resource
    private TaskPriceDepthMapper taskPriceDepthMapper;
    @Resource
    private TaskPriceBtnMapper taskPriceBtnMapper;


    public SystemCfgController(TaskPriceService taskPriceService) {
        this.taskPriceService = taskPriceService;
    }

    @ApiOperation("查询任务价格表")
    @GetMapping("/getTaskPriceList")
    public R getTaskPriceList(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        List<TaskPrice> priceList = new ArrayList<>();
        if (CommonUtils.isAdminType(currentUser) || CommonUtils.isApiType(currentUser)) {
            priceList = taskPriceService.selectAllByMerchantId(null);
        }
        if (CommonUtils.isMerchantType(currentUser) || CommonUtils.isMerchantUserType(currentUser)) {
            priceList = taskPriceService.selectAllByMerchantId(currentUser.getMerchantId());
        }
        return Rx.success(priceList);
    }

    @ApiOperation("分站商户查询任务成本价格表")
    @GetMapping("/getTaskPriceListForMerchant")
    public R getTaskPriceListForMerchant(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        List<TaskPrice> priceList;
        if (CommonUtils.isMerchantType(currentUser)) {
            priceList = taskPriceService.selectAllByMerchantId(null);
        } else {
            return Rx.fail("不允许访问");
        }
        return Rx.success(priceList);
    }

    @ApiOperation("保存任务价格配置")
    @PostMapping("/modifyTaskPrice")
    public R modifyTaskPrice(@RequestBody TaskPrice[] taskPriceArr, @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (CommonUtils.isApiType(currentUser) || CommonUtils.isMerchantUserType(currentUser)) {
            return Rx.permissionDenied();
        }
        int x = taskPriceService.updatePriceCfg(taskPriceArr, currentUser);
        if (x == taskPriceArr.length) {
            return Rx.success();
        }
        log.error("任务价格配置更新异常");
        return Rx.success();
    }

    @ApiIgnore
    @ApiOperation("查询时间阶梯配置")
    @GetMapping("/getTaskBrowserTimeCfg")
    public R getTaskBrowserTimeCfg(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        List<TaskPriceBrowserTime> list = new ArrayList<>();
        if (CommonUtils.isAdminType(currentUser) || CommonUtils.isApiType(currentUser)) {
            list = priceBrowserTimeMapper.selectAllByMerchantId(null, 1);
        }
        if (CommonUtils.isMerchantType(currentUser) || CommonUtils.isMerchantUserType(currentUser)) {
            list = priceBrowserTimeMapper.selectAllByMerchantId(currentUser.getMerchantId(), 0);
            if (list.isEmpty()) {
                list = priceBrowserTimeMapper.selectAllByMerchantId(null, 0);
            }
        }
        return Rx.success(list);
    }

    @ApiOperation("浏览时长价格配置")
    @PostMapping("/cfgTaskPriceBrowserTime")
    public R cfgTaskPriceBrowserTime(@ApiIgnore Principal principal, Integer price) {
        if (price == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        // 总站配置 价格
        if (CommonUtils.isAdminType(currentUser)) {
            TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectMaster();
            taskPriceBtn.setPrice(price);
            taskPriceBtnMapper.updateByPrimaryKey(taskPriceBtn);
        } else if (CommonUtils.isMerchantType(currentUser)) {
            TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectOneByMerchantId(currentUser.getMerchantId());
            if (taskPriceBtn == null) {
                taskPriceBtnMapper.insert(new TaskPriceBtn(currentUser.getMerchantId(), price, 0, 0));
            } else {
                taskPriceBtn.setPrice(price);
                taskPriceBtnMapper.updateByPrimaryKey(taskPriceBtn);
            }
        } else {
            return Rx.fail("不允许访问");
        }
        return Rx.success();
    }

    @ApiOperation("查询浏览时长价格")
    @GetMapping("/getTaskPriceBrowserTime")
    public R getTaskPriceBrowserTime(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        // 总站配置 价格
        if (CommonUtils.isAdminType(currentUser)) {
            TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectMaster();
            return Rx.success(taskPriceBtn);
        } else if (CommonUtils.isMerchantType(currentUser)) {
            TaskPriceBtn taskPriceBtn = taskPriceBtnMapper.selectOneByMerchantId(currentUser.getMerchantId());
            if (taskPriceBtn == null) {
                taskPriceBtn = taskPriceBtnMapper.selectDefault();
            }
            return Rx.success(taskPriceBtn);
        } else {
            return Rx.fail("不允许访问");
        }
    }

    @ApiOperation("查询浏览深度价格")
    @GetMapping("/getTaskPriceDepth")
    public R getTaskPriceDepth(@ApiIgnore Principal principal, Integer type) {
        if (type == null) {
            return Rx.fail("参数为空异常");
        }
        if (type < 1 || type > 2) {
            return Rx.fail("参数只能为随机浏览（1）或者固定浏览（2）");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        // 总站配置 价格
        if (CommonUtils.isAdminType(currentUser)) {
            TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectMasterByType(type);
            return Rx.success(taskPriceDepth);
        } else if (CommonUtils.isMerchantType(currentUser)) {
            TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectOneByMerchantId(currentUser.getMerchantId(), type);
            if (taskPriceDepth == null) {
                taskPriceDepth = taskPriceDepthMapper.selectDefault(type);
            }
            return Rx.success(taskPriceDepth);
        } else {
            return Rx.fail("不允许访问");
        }
    }


    @ApiOperation("浏览深度价格配置")
    @PostMapping("/cfgTaskPriceDepth")
    public R cfgTaskPriceDepth(@ApiIgnore Principal principal, Integer price, Integer type) {
        if (price == null || type == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        // 总站配置 价格
        if (CommonUtils.isAdminType(currentUser)) {
            TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectMasterByType(type);
            taskPriceDepth.setPrice(price);
            taskPriceDepth.setType(type);
            taskPriceDepthMapper.updateByPrimaryKey(taskPriceDepth);
            return Rx.success(taskPriceDepth);
        } else if (CommonUtils.isMerchantType(currentUser)) {
            TaskPriceDepth taskPriceDepth = taskPriceDepthMapper.selectOneByMerchantId(currentUser.getMerchantId(), type);
            if (taskPriceDepth == null) {
                taskPriceDepthMapper.insert(new TaskPriceDepth(currentUser.getMerchantId(), type, price, 0, 0));
            } else {
                taskPriceDepth.setPrice(price);
                taskPriceDepth.setType(type);
                taskPriceDepthMapper.updateByPrimaryKey(taskPriceDepth);
            }
            return Rx.success(taskPriceDepthMapper.selectOneByMerchantId(currentUser.getMerchantId(), type));
        } else {
            return Rx.fail("不允许访问");
        }
    }

}
