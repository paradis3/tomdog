package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.News;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.mapper.SysBranchMerchantMapper;
import com.paradise.tomdog.sys.service.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Api(tags = "文章相关接口")
@RestController
@RequestMapping("/news")
@Slf4j
public class NewsController {
    @Autowired
    private NewsService newsService;
    @Resource
    private SysBranchMerchantMapper merchantMapper;

    @ApiOperation("添加或修改文章")
    @PostMapping("/updateNews")
    @PreAuthorize("hasAuthority('pms:mb:news:update')")
    @ControllerWebLog(name = "添加或修改文章")
    public R<String> updateNews(@RequestBody News info, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isAdminType(user)) {
            info.setMerchantId(user.getMerchantId());
        }
        info.setPublishTime(new Date());
        return newsService.updateNews(user, info);
    }

    @ApiOperation("查看文章信息列表--管理端用")
    @PostMapping("/getNewsList2")
    public R<PageData> getNewsList(Long merchantId, Integer type,
                                   Integer pageNum, Integer pageSize,
                                   @ApiIgnore Principal principal) {
        if (pageNum == null || pageSize == null) {
            return Rx.fail("参数为空异常");
        }
        SysUser user = CommonUtils.getUser(principal);
        News news = new News();
        if (CommonUtils.isAdminType(user)) {
            news.setMerchantId(merchantId);
        } else {
            news.setMerchantId(user.getMerchantId());
        }
        if (type != null) {
            news.setType(type);
        }
        return Rx.success(newsService.selectByAll(news, pageNum, pageSize));
    }

    @ApiOperation("查看文章信息列表--页面显示用")
    @PostMapping("/getNewsList")
    public R<List<News>> getNewsList(String type, String domain) {
        News news = new News();
        // 查询商户
        SysBranchMerchant merchant = merchantMapper.selectOneByDomain(domain);
        if (merchant != null) {
            news.setMerchantId(merchant.getId());
        } else {
            log.error("根据域名{}查询商户信息不存在", domain);
            return Rx.fail("根据域名{" + domain + "}查询商户信息不存在");
        }
        news.setStatus(0);
        if (StringUtils.isNotEmpty(type)) {
            news.setType(Integer.valueOf(type));
        }
        List<News> newsList = newsService.selectByAll(news);
        return Rx.success(newsList);
    }

    @ApiOperation("查看文章详细信息")
    @PostMapping("/getNewsInfo")
    public R<List<News>> getNewsInfo(Integer id) {
        News news = new News();
        news.setId(id);
        List<News> newsList = newsService.selectByAll(news);
        return Rx.success(newsList);
    }

    @ApiOperation("逻辑删除文章")
    @PostMapping("/delNews")
    @ControllerWebLog(name = "逻辑删除文章")
    public R delNews(Integer id, @ApiIgnore Principal principal) {
        if (id == null) {
            return Rx.fail("参数为空异常");
        }
        if (newsService.deleteByPrimaryKey(id, CommonUtils.getUser(principal)) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }
}

