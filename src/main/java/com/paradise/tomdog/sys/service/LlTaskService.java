package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.api.param.TaskCreateParam;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.vo.TaskQuery;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 任务 service 接口
 *
 * @author Paradise
 */
public interface LlTaskService {

    /**
     * 根据主键查询
     *
     * @param id 主键
     * @return {@link LlTask}
     */
    LlTask selectByPrimaryKey(Long id);

    /**
     * 根据taskId查询
     *
     * @param taskId taskId
     * @return {@link LlTask}
     */
    LlTask selectByTaskId(String taskId);

    /**
     * 分页查询 任务列表
     *
     * @param query       查询条件 {@link TaskQuery}
     * @param pageNum     PAGE_NUM
     * @param pageSize    PAGE_SIZE
     * @param currentUser 当前登录用户
     * @return {@link PageData}
     */
    PageData selectTaskPage(TaskQuery query, Integer pageNum, Integer pageSize, SysUser currentUser);

    /**
     * 导出 任务列表Excel
     *
     * @param query       查询条件
     * @param request     请求
     * @param response    响应
     * @param currentUser 当前登录用户
     */
    void exportTaskList(TaskQuery query, HttpServletRequest request, HttpServletResponse response, SysUser currentUser);

    /**
     * 修改任务状态
     *
     * @param id     任务主键ID
     * @param status 目标状态
     * @param user   修改人
     * @return {@link R}
     */
    R changeTaskStatus(Long id, String status, SysUser user);

    /**
     * 暂停任务
     *
     * @param id   任务主键id
     * @param user 修改人
     * @return {@link R}
     */
    R taskPause(Long id, SysUser user);


    /**
     * 取消任务
     *
     * @param id   任务主键id
     * @param user 修改人
     * @return {@link R}
     */
    R taskCancel(Long id, SysUser user);

    /**
     * 完成任务
     *
     * @param id   任务主键id
     * @param user 修改人
     * @return {@link R}
     */
    R taskComplete(Long id, SysUser user);

    /**
     * 查询任务
     *
     * @param id 任务主键id
     * @return {@link R}
     */
    R taskQuery(Long id);

    /**
     * 创建任务
     *
     * @param task 任务信息
     * @param user 登录人
     * @return {@link R}
     */
    R createTask(LlTask task, SysUser user);

    /**
     * 修改任务信息
     *
     * @param task 任务信息
     * @param user 当前登录人
     * @return {@link R}
     */
    R updateTask(LlTask task, SysUser user);


    /**
     * 发布任务
     *
     * @param id   任务主键
     * @param user 登录人
     * @return {@link R}
     */
    R publishTask(Long id, SysUser user);

    /**
     * 根据任务主键查询任务详情
     *
     * @param id          任务主键id
     * @param currentUser 当前登录用户
     * @return {@link TaskDetail}
     */
    TaskDetail getTaskDetail(Long id, SysUser currentUser);

    /**
     * 查询任务的分站积分消耗情况
     *
     * @param id   任务主键
     * @param user 操作人
     * @return {@link R}
     */
    R queryMerchantCostPoints(Long id, SysUser user);

    /**
     * api 商户调用接口创建任务
     *
     * @param task    任务参数
     * @param userApi API 用户
     * @return {@link R}
     */
    R apiCreateTask(TaskCreateParam task, SysUserApi userApi);

    /**
     * 取消任务
     *
     * @param task 任务
     * @return {@link R}
     */
    R cancelApiTask(LlTask task);

    /**
     * 取消任务退款处理
     *
     * @param taskRefund 取消任务
     * @return {@link R}
     */
    R resolveRefund(TaskRefund taskRefund);

    /**
     * 用户删除任务
     *
     * @param id   任务id
     * @param user 操作人
     * @return {@link R}
     */
    R deleteTask(Long id, SysUser user);

    /**
     * 查询任务数量
     *
     * @param status      任务状态
     * @param currentUser 当前人
     * @param type        任务类型
     * @return 任务数量
     */
    Integer queryTaskCount(String status, SysUser currentUser, String type);

    /**
     * 查询全部数据 - 任务数量
     *
     * @param currentUser 当前用户
     * @return 查询结果 Map
     */
    Map<String, Integer> queryAllTaskCount(SysUser currentUser);
}
