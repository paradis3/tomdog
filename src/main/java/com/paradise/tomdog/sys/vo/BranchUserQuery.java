package com.paradise.tomdog.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 分站用户查询 参数封装
 *
 * @author Paradise
 */
@Getter
@Setter
@ApiModel
public class BranchUserQuery {
    @ApiModelProperty(value = "姓名")
    private String realName;
    @ApiModelProperty(value = "username")
    private String username;
    @ApiModelProperty(value = "等级ID", example = "201")
    private Long userLevelId;
    @ApiModelProperty(value = "手机号码")
    private String telephone;
    @ApiModelProperty(value = "上级姓名")
    private String pUsername;
    @ApiModelProperty(value = "分站商户id", example = "201")
    private Long merchantId;
    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(hidden = true)
    private Long superiorId;
}
