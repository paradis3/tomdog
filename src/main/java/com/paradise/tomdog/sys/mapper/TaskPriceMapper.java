package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPrice;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 任务价格配置
 *
 * @author Paradise
 */
@Mapper
public interface TaskPriceMapper {
    /**
     * delete by primary key
     *
     * @param priceId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long priceId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPrice record);

    /**
     * select by primary key
     *
     * @param priceId primary key
     * @return object by primary key
     */
    TaskPrice selectByPrimaryKey(Long priceId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskPrice record);

    /**
     * 更新价格
     *
     * @param record record
     * @return update count
     */
    int updatePriceByPrimaryKey(TaskPrice record);

    /**
     * 查询总站价格表
     *
     * @return Map
     */
    @MapKey("type")
    Map<Integer, TaskPrice> selectAllByIsMaster();

    /**
     * 根据分站商户ID 查询 分站任务基础价格表
     *
     * @param merchant 商户ID
     * @return Map
     */
    Map<Integer, Integer> selectAllByMerchantId(@Param("merchant") Long merchant);

    /**
     * 根据type 和分站商户ID 查询价格
     *
     * @param type       任务类型
     * @param merchantId 分站商户ID
     * @return PRICE
     */
    Integer selectPriceByTypeAndMerchantId(@Param("type") Integer type,
                                           @Param("merchantId") Long merchantId);

    /**
     * 查询价格表
     *
     * @param merchantId 商户ID，总站价格传NULL
     * @param isMaster   是否总站 1=是
     * @return 价格表
     */
    List<TaskPrice> selectAllByMerchantIdAndIsMaster(@Param("merchantId") Long merchantId,
                                                     @Param("isMaster") Integer isMaster);

    /**
     * 条件更新任务价格配置
     *
     * @param taskPrice 任务价格
     * @return update count
     */
    int updateByTypeAndMerchantIdAndIsMaster(@Param("taskPrice") TaskPrice taskPrice);

    /**
     * 初始化分站商户价格表的方法
     *
     * @param merchantId 分站商户ID
     * @return insert count
     */
    int initInsertCfg(@Param("merchantId") Long merchantId);
}