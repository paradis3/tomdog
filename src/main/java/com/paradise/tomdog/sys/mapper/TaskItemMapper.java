package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 子任务 mapper
 *
 * @author Paradise
 */
@Mapper
public interface TaskItemMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskItem record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskItem selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskItem record);

    /**
     * 根据任务主键删除子任务
     *
     * @param taskId 任务主键
     * @return delete count
     */
    int deleteByTaskId(@Param("taskId") Long taskId);

    /**
     * 根据任务主键查询子任务列表
     *
     * @param taskId 任务主键
     * @return select result
     */
    List<TaskItem> selectByTaskId(@Param("taskId") Long taskId);

    /**
     * 更新任务完成数量
     *
     * @param updatedCompletedCount 任务完成数量
     * @param id                    任务主键ID
     * @return update count
     */
    int updateCompletedCountById(@Param("updatedCompletedCount") Integer updatedCompletedCount,
                                 @Param("id") Long id);

    /**
     * 完成任务 子任务
     *
     * @param id     子任务主键ID
     * @param status 完成状态
     * @return update count
     */
    int completeTask(@Param("id") Long id,
                     @Param("status") String status);


    /**
     * 更新子任务状态
     *
     * @param status 目标状态
     * @param id     任务id
     * @return update count
     */
    int updateStatusByTaskId(@Param("status") String status,
                             @Param("id") Long id);
}