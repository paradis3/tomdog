package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface SysRoleMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysRole record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysRole record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysRole selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysRole record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysRole record);

    /**
     * 逻辑删除 - 递归
     *
     * @param id      待删除角色id
     * @param updater 更新人
     * @return update count
     */
    int deleteAllById(@Param("id") Long id,
                      @Param("updater") Long updater);

    /**
     * 查询下级角色列表（包含自身）
     *
     * @param pid        角色id
     * @param merchantId 分站商户id
     * @param name       角色名称
     * @return select res
     */
    List<SysRole> selectAllByPidAndMerchantId(@Param("pid") Long pid,
                                              @Param("merchantId") Long merchantId,
                                              @Param("name") String name);

    /**
     * 查询超级管理员
     *
     * @param merchantId 分站商户ID，null - 查询总站
     * @return select res
     */
    SysRole selectSuperRole(@Param("merchantId") Long merchantId);

    /**
     * 查询
     *
     * @param name       name
     * @param merchantId 商户id
     * @return select res
     */
    SysRole selectByNameAndMerchantId(@Param("name") String name,
                                      @Param("merchantId") Long merchantId);

    List<SysRole> selectByAll(SysRole sysRole);


}