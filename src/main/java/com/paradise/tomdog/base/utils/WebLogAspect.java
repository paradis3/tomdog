package com.paradise.tomdog.base.utils;

import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.paradise.tomdog.base.ControllerWebLog;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


/**
 * @author Paradise
 */
@Slf4j
@Aspect
@Component
public class WebLogAspect {


    /**
     * 以 controller 包下定义的所有请求为切入点
     **/
    @Pointcut("execution(public * com.paradise.tomdog.*.controller.*.*(..))")
    public void webLog() {
    }

    /**
     * 在切点之前织入
     *
     * @param joinPoint joinPoint
     */
    @Before("webLog() && @annotation(controllerWebLog)")
    public void doBefore(JoinPoint joinPoint, ControllerWebLog controllerWebLog) {
        // 开始打印请求日志
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        // 打印请求相关参数
        log.info("========================================== Start ==========================================");
        if (StrUtil.isNotEmpty(controllerWebLog.name())) {
            log.info("Name           : {}", controllerWebLog.name());
        }
        // 打印请求 url
        log.info("URL            : {}", request.getRequestURL().toString());
        // 打印 Http method
        log.info("HTTP Method    : {}", request.getMethod());
        // 打印调用 controller 的全路径以及执行方法
//        log.info("Class Method   : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        // 打印请求的 IP
//        log.info("IP             : {}", request.getRemoteAddr());
        // Real IP
        log.info("Real IP        : {}", request.getHeader("X-Real-IP"));
        // 打印请求入参 # 屏蔽掉 request 参数
        Object[] objects = joinPoint.getArgs();
        for (Object o : objects) {
            if (o instanceof Principal) {
                log.info("当前用户：{}", CommonUtils.getUser((Principal) o).getUsername());
                continue;
            }
            if (o instanceof HttpServletRequest) {
                continue;
            }
            log.info("Request Args   : {}", new Gson().toJson(o));
        }
    }

    /**
     * 在切点之后织入
     */
    @After("webLog() && @annotation(controllerWebLog)")
    public void doAfter(ControllerWebLog controllerWebLog) {
        log.info("=========================================== End ===========================================");
        // 每个请求之间空一行
        log.info("");
    }

    /**
     * 环绕
     *
     * @param proceedingJoinPoint proceedingJoinPoint
     * @return Object o
     * @throws Throwable e
     */
    @Around("webLog() && @annotation(controllerWebLog)")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint, ControllerWebLog controllerWebLog) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        // 打印出参
        log.info("Response Args  : {}", new Gson().toJson(result));
        // 执行耗时
        log.info("Time-Consuming : {} ms", System.currentTimeMillis() - startTime);
        return result;
    }

}

