package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.DyInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 抖音信息mapper
 *
 * @author Paradise
 */
@Mapper
public interface DyInfoMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(String id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(DyInfo record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    DyInfo selectByPrimaryKey(String id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(DyInfo record);
}