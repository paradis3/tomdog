package com.paradise.tomdog.rpc.bean;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.rpc.result.jd.ShopInfo;
import com.paradise.tomdog.sys.entity.JdGoods;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 查询淘宝店铺信息
 *
 * @author Paradise
 */
@Getter
@Setter
@Slf4j
public class JdGoodsQuery extends AbstractLlQueryBean {
    private String goodsid;

    public JdGoodsQuery(String goodsId) {
        this.goodsid = goodsId;
    }

    @Override
    public void initSign() {
        String url = "/api/query_jingdong_goods?appid=1049&goodsid=" + this.goodsid
                + "&timestamp="
                + this.timestamp + "&ver=4";
        try {
            url = URLEncoder.encode(url, CharsetUtil.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.sign = SecureUtil.md5(url);
    }

    @Override
    public R doQuery() {
        this.initSign();
        HttpResponse httpResponse = HttpUtil
                .createPost("http://www.liuliangjunheng.com/api/query_jingdong_goods")
                .body(this.toJson()).execute();
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            log.info("查询京东商品信息Result：");
            log.info(httpResponse.body());
            JSONObject model = new JSONObject(httpResponse.body()).getJSONObject("model");
            Long id = model.get("id", Long.class);
            ShopInfo shopInfo = model.getJSONObject("shopInfo").get("shop", ShopInfo.class);
            if (shopInfo == null || shopInfo.getShopId() == 0) {
                return Rx.fail("JD商品信息不存在");
            }
            String name = model.getStr("wname");
            String chatUrl = model.getStr("chatUrl");
            JdGoods jdGoods = new JdGoods(id, name, chatUrl, shopInfo.getName(),
                    shopInfo.getShopId(), shopInfo.getFollowCount());
            return Rx.success(jdGoods);
        }
        return Rx.fail(httpResponse.toString());
    }

    public String toJson() {
        return JSONUtil.toJsonStr(this);
    }

}
