package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-News")
@Data
public class News {
    @ApiModelProperty(value = "")
    private Integer id;

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 类别
     */
    @ApiModelProperty(value = "类别")
    private Integer type;

    /**
     * 0:发布 1：未发布
     */
    @ApiModelProperty(value = "0:发布 1：未发布")
    private Integer status;

    /**
     * 发布日期
     */
    @ApiModelProperty(value = "发布日期", hidden = true)
    private Date publishTime;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片")
    private String image;

    /**
     * 关键词
     */
    @ApiModelProperty(value = "关键词")
    private String keyWord;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;

    /**
     * 内容
     */
    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "缩略图")
    private String thumbUrl;

    @ApiModelProperty(value = "标签")
    private String label;

    /**
     * 所属分站id
     */
    @ApiModelProperty(value = "所属分站id")
    private Long merchantId;

    @ApiModelProperty(hidden = true)
    private Long creator;

    @ApiModelProperty(hidden = true)
    private Date createTime;

    @ApiModelProperty(hidden = true)
    private Long updater;

    @ApiModelProperty(hidden = true)
    private Date updateTime;
    @ApiModelProperty(hidden = true)
    private Integer isDel;
}