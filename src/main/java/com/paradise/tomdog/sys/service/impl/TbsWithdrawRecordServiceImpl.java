package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.base.utils.MobileMsg;
import com.paradise.tomdog.sys.constant.Withdraw_Status;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsBalance;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecordWithSysUser;
import com.paradise.tomdog.sys.mapper.TbsWithdrawRecordMapper;
import com.paradise.tomdog.sys.service.TbsBalanceService;
import com.paradise.tomdog.sys.service.TbsWithdrawRecordService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class TbsWithdrawRecordServiceImpl implements TbsWithdrawRecordService {
    
    @Resource
    private TbsWithdrawRecordMapper tbsWithdrawRecordMapper;
    @Resource
    private TbsBalanceService tbsBalanceService;
    
    @Override
    public int deleteByPrimaryKey(Long id) {
        return tbsWithdrawRecordMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    public int insert(TbsWithdrawRecord record) {
        return tbsWithdrawRecordMapper.insert(record);
    }
    
    @Override
    public int insertSelective(TbsWithdrawRecord record) {
        return tbsWithdrawRecordMapper.insertSelective(record);
    }
    
    @Override
    public TbsWithdrawRecord selectByPrimaryKey(Long id) {
        return tbsWithdrawRecordMapper.selectByPrimaryKey(id);
    }
    
    @Override
    public int updateByPrimaryKeySelective(TbsWithdrawRecord record) {
        return tbsWithdrawRecordMapper.updateByPrimaryKeySelective(record);
    }
    
    @Override
    public int updateByPrimaryKey(TbsWithdrawRecord record) {
        return tbsWithdrawRecordMapper.updateByPrimaryKey(record);
    }
    
    /**
     * 创建发起提现请求记录
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R createPointsWithdrawRecord(SysUser user, TbsWithdrawRecord record) {
        //获取用户余额
        TbsBalance tbsBalance = tbsBalanceService.selectByPrimaryKey(user.getId());
        if (tbsBalance == null || tbsBalance.getSpreadBalance().compareTo(record.getAmount()) < 0) {
            return Rx.fail("邀请积分不足,无法提现");
        }
        record.setId(null);
        record.setUserId(user.getId());
        record.setAmount(-record.getAmount());
        record.setCreateBy(user.getId());
        record.setCreateTime(new Date());
        record.setWithdrawCode(MobileMsg.getSmsCode(6));
        record.setIsDel("0");
        record.setStatus(Withdraw_Status.WITHDRAW_CREATE.getCode());
        tbsWithdrawRecordMapper.insertSelective(record);
        //更新推广积分余额和记录
        tbsBalanceService.updateByWithDrawRecord(tbsBalance,record);
        return Rx.success("提现发起成功！", record);
    }
    
    /**
     * 查询提现记录
     */
    @Override
    public PageData pagePointsWithdrawRecord(Integer pageNum, Integer pageSize, TbsWithdrawRecord record) {
        PageHelper.startPage(pageNum, pageSize);
        record.setIsDel("0");
        List<TbsWithdrawRecordWithSysUser> tbsWithdrawRecordWithSysUsers = tbsWithdrawRecordMapper.selectByAllJoinUser(record);
        return new PageData(new PageInfo<>(tbsWithdrawRecordWithSysUsers));
    }
    /**
     * 审批提现记录
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public R auditPointsWithdraw(TbsWithdrawRecord record) {
        if (record.getId()==null) {
            return Rx.fail("参数错误");
        }
        tbsWithdrawRecordMapper.updateByPrimaryKeySelective(record);
        //如果提现失败，返回用户推广积分
        if (StringUtils.equals(record.getStatus(),Withdraw_Status.WITHDRAW_FAIL.getCode())) {
            TbsWithdrawRecord tbsWithdrawRecord = tbsWithdrawRecordMapper.selectByPrimaryKey(record.getId());
            tbsBalanceService.returnByWithDrawRecord(tbsWithdrawRecord);
        }
        return Rx.success("审批成功");
    }
    /**
     * 导出提现列表单
     */
    @Override
    public void exportPointsWithdraw(TbsWithdrawRecord record, SysUser user, HttpServletRequest request,
                                           HttpServletResponse response) {
        // 根据条件查询分站商户充值记录
        record.setIsDel("0");
        List<TbsWithdrawRecordWithSysUser> list = tbsWithdrawRecordMapper.selectByAllJoinUser(record);
        String[] rowNames = new String[]{"序号", "用户名","提取积分数量", "真实姓名", "提现账户", "联系方式", "提现码","提现状态",
                "时间","备注"};
        String title = "提现记录数据列表";
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .request(request).response(response).title(title)
                .rowName(rowNames)
                .dataList(getDataList(list))
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }
    
    /**
     * 解析Excel需要的数据列表
     * @param list 记录数据
     */
    private List<Object[]> getDataList(List<TbsWithdrawRecordWithSysUser> list) {
        List<Object[]> resultList = new ArrayList<>();
        int index = 1;
        for (TbsWithdrawRecordWithSysUser record : list) {
            Object[] arr = new Object[10];
            arr[0] = index++;
            arr[1] = record.getSysUser().getUsername();
            arr[2] = record.getAmount();
            arr[3] = record.getRealName();
            arr[4] = record.getWithdrawAccount();
            arr[5] = record.getContactWay();
            arr[6] = record.getWithdrawCode();
            arr[7] = Withdraw_Status.getNameBycode(record.getStatus());
            arr[8] = DateFormatUtils.format(record.getCreateTime());
            arr[9] = record.getRemark();
            resultList.add(arr);
        }
        return resultList;
    }
}

