package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-JdDr")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class JdDr {
    @ApiModelProperty(value = "")
    private Long id;

    @ApiModelProperty(value = "")
    private String name;

    @ApiModelProperty(value = "")
    private Integer fansNum;

    @ApiModelProperty(value = "")
    private String json;

    @ApiModelProperty(value = "")
    private Date updateTime;
}