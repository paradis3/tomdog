/**
 * Copyright (C), 2016-2019, XXX有限公司
 * FileName: RechargeController
 * Author:   Administrator
 * Date:     2019/12/27/027 15:17
 * Description: 充值接口
 * History:
 * <author>          <time>          <version>          <desc>
 * simon          修改时间           版本号              描述
 */
package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;
import com.paradise.tomdog.sys.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.Date;

/**
 * 〈用户提现接口〉
 * @author Administrator
 * @create 2019/12/27/027
 * @since 1.0.0
 */
@Api(tags = "用户提现接口", description = "用户提现接口")
@RestController
@RequestMapping("/user/withdraw")
@Slf4j
public class UserWithdrawController {
    @Autowired
    SysUserPayInfoService sysUserPayInfoService;
    @Autowired
    UserLevelService userLevelService;
    @Autowired
    TbsBalanceRecordService tbsBalanceRecordService;
    @Autowired
    TbsWithdrawRecordService tbsWithdrawRecordService;
    @Autowired
    TbsBalanceService tbsBalanceService;
    
    @ApiOperation("用户推广余额")
    @PostMapping("/getTbsPoints")
    @PreAuthorize("hasAuthority('pms:u:tbsPoints:get')")
    public R getTbsPoints(@ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return Rx.success(tbsBalanceService.selectByPrimaryKey(user.getId()));
    }
    
    @ApiOperation("用户发起提现")
    @PostMapping("/pointsWithdraw")
    @PreAuthorize("hasAuthority('pms:u:tbsPoints:withdraw')")
    @ControllerWebLog(name = "用户发起提现")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "amount",value = "提取积分数量",paramType = "query"),
            @ApiImplicitParam(name = "type",value = "收款方式：1支付宝", paramType = "query"),
            @ApiImplicitParam(name = "withdrawAccount",value = "提现账户", paramType = "query"),
            @ApiImplicitParam(name = "realName",value = "真实姓名", paramType = "query"),
            @ApiImplicitParam(name = "contactWay",value = "联系方式", paramType = "query"),
            })
    public R pointsWithdraw(@ApiIgnore @Validated TbsWithdrawRecord record, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return tbsWithdrawRecordService.createPointsWithdrawRecord(user, record);
    }
    
    @ApiOperation("分页查看用户提现列表")
    @PostMapping("/pagePointsWithdraw")
    @PreAuthorize("hasAuthority('pms:bu:tbsPoints:withdraw:page')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页数", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "页码", defaultValue = "10", paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "真实姓名", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "contactWay", value = "联系方式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "withdrawAccount", value = "提现账户", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "status", value = "0 提现中 1 失败 2 成功", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "withdrawCode", value = "提现码", paramType = "query", dataType = "String"),
    })
    public R pagePointsWithdraw(@NotNull(message = "pageNum不能为空！") Integer pageNum,
                                @NotNull(message = "pageSize不能为空！") Integer pageSize,
                                @ApiIgnore TbsWithdrawRecord record, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        record.setMerchantId(user.getMerchantId());
        if (CommonUtils.isMerchantType(user)) {
            record.setMerchantId(user.getMerchantId());
        } else if (CommonUtils.isMerchantUserType(user)) {
            record.setUserId(user.getId());
        }
        PageData pageData = tbsWithdrawRecordService.pagePointsWithdrawRecord(pageNum, pageSize, record);
        return Rx.success(pageData);
    }
    
    @ApiOperation("导出分站用户提现列表")
    @GetMapping("/exportPointsWithdraw")
    @PreAuthorize("hasAuthority('pms:mbu:tbsPoints:withdraw:export')")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "realName", value = "真实姓名", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "contactWay", value = "联系方式", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "withdrawAccount", value = "提现账户", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "status", value = "0 提现中 1 失败 2 成功", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "withdrawCode", value = "提现码", paramType = "query", dataType = "String"),
    })
    public void exportBranchRechargeOrderPage(@ApiIgnore TbsWithdrawRecord record, @ApiIgnore Principal principal,
                                              @ApiIgnore HttpServletResponse response, @ApiIgnore HttpServletRequest request) {
        SysUser currentUser = CommonUtils.getUser(principal);
        tbsWithdrawRecordService.exportPointsWithdraw(record, currentUser, request, response);
    }
    
    @ApiOperation("审核用户提现")
    @PostMapping("/auditPointsWithdraw")
    @PreAuthorize("hasAuthority('pms:b:tbsPoints:withdraw:audit')")
    @ControllerWebLog(name = "审核用户提现")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "status", value = "0 提现中 1 失败 2 成功", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "remark", value = "备注（例如：支付宝流水号）", paramType = "query", dataType = "String"),
    })
    public R auditPointsWithdraw(@ApiIgnore TbsWithdrawRecord record, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        record.setMerchantId(user.getMerchantId());
        record.setUpdateBy(user.getId());
        record.setUpdateTime(new Date());
        return tbsWithdrawRecordService.auditPointsWithdraw(record);
    }
}
