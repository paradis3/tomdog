package com.paradise.tomdog.sys.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 任务查询参数封装
 *
 * @author Paradise
 */
@Getter
@Setter
@ApiModel(value = "TaskQueryVo", description = "任务查询参数")
public class TaskQuery {

    @ApiModelProperty(value = "任务类型")
    private Integer type;
    @ApiModelProperty(value = "任务状态")
    private String status;
    @ApiModelProperty(value = "查询开始时间（时间戳）")
    private Long beginDate;
    @ApiModelProperty(value = "查询结束时间（时间戳）")
    private Long endDate;
    @ApiModelProperty(value = "任务ID")
    private String taskId;
    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "店铺链接")
    private String target;
    @ApiModelProperty(value = "搜索词")
    private String keyword;
    @ApiModelProperty(value = "手机号")
    private String telephone;
}
