package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TbsBalance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 分站用户推广积分余额
 *
 * @author Paradise
 */
@Mapper
public interface TbsBalanceMapper {
    /**
     * delete by primary key
     *
     * @param userId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long userId);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TbsBalance record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(TbsBalance record);
    
    /**
     * select by primary key
     *
     * @param userId primary key
     * @return object by primary key
     */
    TbsBalance selectByPrimaryKey(Long userId);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(TbsBalance record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TbsBalance record);
    
    /**
     * 乐观锁更新 推广积分余额
     *
     * @param amount  变更数量
     * @param version 版本
     * @param userId  用户ID
     * @return update count
     */
    int updateSpreadBalanceByUserId(@Param("amount") Integer amount,
                                    @Param("version") Integer version,
                                    @Param("userId") Long userId);
}