package com.paradise.tomdog.sys.constant;

/**
 * 任务状态枚举
 *
 * @author Paradise
 */
public enum Recharge_Pay_Type {
    /**
     * 未发布
     */
    ALI_PAY("1", "支付宝"),
    WX_PAY("2", "微信支付"),
    OTHER("3", "其它");

    private String code;
    private String name;

    Recharge_Pay_Type(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
