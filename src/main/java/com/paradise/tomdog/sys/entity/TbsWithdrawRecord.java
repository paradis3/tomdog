package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.Transient;

@ApiModel(value = "com-paradise-tomdog-sys-entity-TbsWithdrawRecord")
@Data
public class TbsWithdrawRecord {
    /**
     * 记录表自增主键
     */
    @ApiModelProperty(value = "记录表自增主键")
    private Long id;
    
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long userId;
    
    /**
     * 提取积分数量
     */
    @ApiModelProperty(value = "提取积分数量")
    private Integer amount;
    
    /**
     * 收款方式：1支付宝
     */
    @ApiModelProperty(value = "收款方式：1支付宝 ")
    private String type;
    
    /**
     * 真实姓名
     */
    @ApiModelProperty(value = "真实姓名")
    private String realName;
    
    /**
     * 提现账户
     */
    @ApiModelProperty(value = "提现账户")
    private String withdrawAccount;
    
    /**
     * 联系方式
     */
    @ApiModelProperty(value = "联系方式")
    private String contactWay;
    
    /**
     * 提现码
     */
    @ApiModelProperty(value = "提现码")
    private String withdrawCode;
    
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
    
    /**
     * 0 提现中 1 失败 2 成功
     */
    @ApiModelProperty(value = " 0 提现中 1 失败 2 成功 ")
    private String status;
    
    /**
     * 删除标记 1删除
     */
    @ApiModelProperty(value = "删除标记 1删除")
    private String isDel;
    
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private Long createBy;
    
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    private Long updateBy;
    
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    
    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private transient Long merchantId;
}