package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysAccount;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.vo.BranchUserQuery;
import com.paradise.tomdog.sys.vo.BranchUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface SysUserMapper {
    /**
     * delete by primary key
     *
     * @param id       primaryKey
     * @param updateBy updateBy user_id
     * @return deleteCount
     */
    int deleteByPrimaryKey(@Param("id") Long id,
                           @Param("updateBy") Long updateBy);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysUser record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysUser record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysUser selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysUser record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysUser record);

    /**
     * 根据用户名查询启用的用户信息
     *
     * @param username username
     * @return 单个用户信息
     */
    @Deprecated
    SysUser selectOneByUsername(@Param("username") String username);

    /**
     * 根据用户ID查询启用的用户信息-分站用户详情
     *
     * @param userId 用户名
     * @return {@link BranchUserVo}
     */
    BranchUserVo selectDetailByUserId(@Param("userId") Long userId);

    /**
     * 修改密码
     *
     * @param id             用户id
     * @param encodePassword 加密后的密码
     * @param updateBy       更新人
     * @return 处理密码
     */
    int changePassword(@Param("id") Long id,
                       @Param("encodePassword") String encodePassword,
                       @Param("updateBy") Long updateBy);

    /**
     * 根据手机号码查询
     *
     * @param telephone 手机号码
     * @return select result
     */
    SysUser selectOneByTelephone(@Param("telephone") String telephone);

    /**
     * 根据手机号码查询
     *
     * @param telephone  手机号码
     * @param merchantId 分站商户id
     * @return select result
     */
    SysUser selectOneByTelephoneAndMerchantId(@Param("telephone") String telephone,
                                              @Param("merchantId") Long merchantId);

    /**
     * 条件查询
     *
     * @param sysUser 查询
     * @return select result
     */
    List<SysUser> selectByAll(SysUser sysUser);

    /**
     * 条件查询 分站用户
     *
     * @param query 查询条件
     * @return select result
     */
    List<BranchUserVo> selectBranchUserList(BranchUserQuery query);

    /**
     * 修改状态
     *
     * @param updatedStatus 目标状态
     * @param id            主键
     * @param updateBy      修改人
     * @return update count
     */
    int updateStatusById(@Param("updatedStatus") String updatedStatus,
                         @Param("id") Long id,
                         @Param("updateBy") Long updateBy);


    /**
     * 查询分站用户详情
     *
     * @param id 主键
     * @return {@link BranchUserVo}
     */
    BranchUserVo selectBranchMerchantUser(Long id);

    /**
     * 根据邀请码查询用户信息
     *
     * @param invitationCode 邀请码、推广链接code
     * @return {@link SysUser}
     */
    SysUser selectByInvitationCode(@Param("invitationCode") Integer invitationCode);

    /**
     * 查询推广用户列表
     *
     * @param superiorId 推广人id
     * @return select result
     */
    List<SysUser> selectBySuperiorId(@Param("superiorId") Long superiorId);

    /**
     * 查询推广用户数量
     *
     * @param superiorId 推广人id
     * @return select count
     */
    Integer countBySuperiorId(@Param("superiorId") Long superiorId);

    /**
     * 修改绑定手机号码
     *
     * @param updatedTelephone 新手机号码
     * @param id               用户主键ID
     * @return update count
     */
    int updateTelephoneById(@Param("updatedTelephone") String updatedTelephone,
                            @Param("id") Long id);

    /**
     * 升级用户
     *
     * @param updatedUserLevel 新的level
     * @param id               用户主键ID
     * @return update count
     */
    int updateUserLevelById(@Param("updatedUserLevel") Integer updatedUserLevel,
                            @Param("id") Long id);

    /**
     * 同步更新 分站商户账户信息
     *
     * @param id        id
     * @param telephone tel
     * @param username  username
     * @return update count
     */
    int updateByBranchMerchant(@Param("id") Long id,
                               @Param("telephone") String telephone,
                               @Param("username") String username);

    /**
     * 根据用户名和分站id为空获取用户
     *
     * @param username 用户名
     * @return select result
     */
    List<SysUser> selectByUsernameMerIsNull(String username);

    /**
     * 根据用户名和分站id为空获取用户
     */
    List<SysUser> selectAllByPhoneAndDomainIsNull(String telephone);

    /**
     * 查询统计数量
     *
     * @param userType   用户类型 3 = 分站商户用户
     * @param merchantId 商户Id
     * @return 统计数量
     */
    Long countByUserTypeAndMerchantId(@Param("userType") Integer userType,
                                      @Param("merchantId") Long merchantId);

    /**
     * 登录查询
     *
     * @param username   用户名
     * @param merchantId 商户ID
     * @param userType   用户类型
     * @return {@link SysUser}
     */
    SysUser selectByUsernameType(@Param("username") String username,
                                 @Param("merchantId") Long merchantId,
                                 @Param("userType") Integer userType);

    /**
     * 查询分站商户 默认管理员
     *
     * @param username   分站商户账户
     * @param merchantId 商户id
     * @return {@link SysUser}
     */
    SysUser selectMerchantSuperAdmin(@Param("username") String username,
                                     @Param("merchantId") Long merchantId);

    /**
     * 逻辑删除角色下的账户 - 递归
     *
     * @param roleId 角色id
     * @param id     操作人id
     * @return update count
     */
    int deleteAllByRoleId(@Param("roleId") Long roleId, @Param("id") Long id);

    /**
     * 新增
     *
     * @param account 账户信息
     * @return insert count
     */
    int insertAccount(SysAccount account);

    /**
     * 更新
     *
     * @param account 账户信息
     * @return update count
     */
    int updateAccount(SysAccount account);

    /**
     * 查询管理员列表
     *
     * @param roleId  当前角色id
     * @param account 查询信心
     * @return select res
     */
    List<SysAccount> selectAllByRoleId(@Param("roleId") Long roleId,
                                       @Param("account") SysAccount account);

    /**
     * 查询登录时的 userId
     *
     * @param username   username
     * @param merchantId 商户id
     * @return {@link SysUser}
     */
    SysUser selectOneByUsernameAndMerchantId(@Param("username") String username,
                                             @Param("merchantId") Long merchantId);


}