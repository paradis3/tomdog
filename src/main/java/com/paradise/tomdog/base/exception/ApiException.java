package com.paradise.tomdog.base.exception;

/**
 * API 调用异常
 *
 * @author Paradise
 */
public class ApiException extends Exception {
    public ApiException() {
        super();
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }
}
