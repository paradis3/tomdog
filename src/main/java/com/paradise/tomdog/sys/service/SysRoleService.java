package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleService {


    /**
     * 逻辑递归删除角色
     *
     * @param roleId 角色id
     * @param userId 当前用户id
     * @return del count
     */
    int deleteByPrimaryKey(Long roleId, Long userId);

    /**
     * 新增
     *
     * @param record r
     * @return inset count
     */
    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    /**
     * 更新角色信息
     *
     * @param record r
     * @return update count
     */
    int updateByPrimaryKey(SysRole record);

    /**
     * 根据角色id 查询下级递归 包含
     *
     * @param roleId 角色id
     * @param name   角色名称
     * @return select res
     */
    List<SysRole> selectAllByPid(@Param("roleId") Long roleId, String name);

    /**
     * 查询分站商户下全部
     *
     * @param merchantId 分站商户id
     * @param name       角色名称
     * @return select res
     */
    List<SysRole> selectAllByMerchantId(@Param("merchantId") Long merchantId, String name);

    /**
     * 查询超级管理员
     *
     * @param merchantId 分站商户ID，null - 查询总站
     * @return select res
     */
    SysRole selectSuperRole(Long merchantId);

    /**
     * 查询
     *
     * @param name       name
     * @param merchantId 商户id
     * @return select res
     */
    SysRole selectByNameAndMerchantId(String name, Long merchantId);

    List<SysRole> selectByAll(SysRole role);

}
