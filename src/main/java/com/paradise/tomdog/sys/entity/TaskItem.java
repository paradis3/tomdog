package com.paradise.tomdog.sys.entity;

import lombok.Data;

/**
 * 子任务 实体
 *
 * @author Paradise
 */
@Data
public class TaskItem {
    /**
     * 子任务主键
     */
    private Long id;

    /**
     * 任务表主键
     */
    private Long taskId;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 子任务id
     */
    private String taskItemId;

    /**
     * 状态
     */
    private String status;

    /**
     * 任务总数
     */
    private Integer totalCount;

    /**
     * 完成总数
     */
    private Integer completedCount;

    /**
     * 备注
     */
    private String remark;
}