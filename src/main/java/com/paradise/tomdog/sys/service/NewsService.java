package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.News;
import com.paradise.tomdog.sys.entity.SysUser;

import java.util.List;

public interface NewsService {

    int deleteByPrimaryKey(Integer id, SysUser user);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);

    R<String> updateNews(SysUser user, News news);

    List<News> selectByAll(News news);

    PageData selectByAll(News news, Integer pageNo, Integer pageSize);

    List<News> selectByAllWithoutContent(News news);
}
