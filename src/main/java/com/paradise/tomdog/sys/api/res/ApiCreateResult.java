package com.paradise.tomdog.sys.api.res;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Paradise
 */
@Getter
@Setter
@AllArgsConstructor
public class ApiCreateResult {
    private Integer costPoints;
    private Integer balance;
}
