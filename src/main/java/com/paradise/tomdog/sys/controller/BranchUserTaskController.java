package com.paradise.tomdog.sys.controller;

import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.base.utils.UrlParser;
import com.paradise.tomdog.rpc.bean.*;
import com.paradise.tomdog.sys.constant.Task_Status;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.service.LlTaskService;
import com.paradise.tomdog.sys.service.TaskUtilService;
import com.paradise.tomdog.sys.service.TbShopService;
import com.paradise.tomdog.sys.task.utils.LlTaskUtils;
import com.paradise.tomdog.sys.vo.TaskQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * 分站用户：任务模块接口
 *
 * @author Paradise
 */
@Api(tags = "分站用户：任务模块接口")
@Slf4j
@RestController
@RequestMapping("/branch/userTask")
@Validated
public class BranchUserTaskController {

    private final TaskUtilService taskUtilService;

    private final LlTaskService taskService;
    private final TbShopService tbShopService;
    @Value("${llRefreshDay}")
    private int refreshDays;

    public BranchUserTaskController(LlTaskService taskService,
                                    TbShopService tbShopService,
                                    TaskUtilService taskUtilService) {
        this.taskService = taskService;
        this.tbShopService = tbShopService;
        this.taskUtilService = taskUtilService;
    }

    @ApiOperation("查询淘宝店铺信息")
    @GetMapping("/queryTbShopInfo")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryTbShopInfo')")
    public R queryTbShopInfo(String url) {
        log.info("url:{}", url);
        try {
            if (!LlTaskUtils.isTmShopUrl(url) && !LlTaskUtils.isTbShopUrl(url)) {
                return Rx.fail("店铺链接不正确");
            }
            // 过滤商品链接
            if (LlTaskUtils.isTbItemUrl(url) || LlTaskUtils.isTmItemUrl(url)) {
                return Rx.fail("店铺链接不正确");
            }
            url = LlTaskUtils.urlResolver(url);
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            return Rx.fail("店铺链接格式不正确");
        }
        // 先根据 url 查询数据库是否存在
        TbShop tbShop = tbShopService.selectByUrl(url);
        boolean existFlag = tbShop != null;
        if (existFlag && CommonUtils.compareDate(tbShop.getUpdateTime(), refreshDays)) {
            return Rx.success(tbShop);
        }
        ShopResInfo shopResInfo = LlTaskUtils.postShopQuery(url);
        if (shopResInfo.isSuccess()) {
            TbShopInfo tbShopInfo = shopResInfo.toTbShopInfo();
            TbShop shop = new TbShop(url, tbShopInfo);
            if (existFlag) {
                shop.setId(tbShop.getId());
            }
            tbShopService.insert(shop);
            return Rx.success(tbShopInfo);
        }
        return Rx.error("店铺信息查询失败", shopResInfo);
    }

    public static Map<String, Object> parseTklRes(String json) {
        JSONObject object;
        try {
            object = JSONUtil.parseObj(json).getJSONObject("model");
        } catch (Exception e) {
            return null;
        }
        String templateId = object.getStr("templateId");
        if (!"item".equalsIgnoreCase(templateId)) {
            return null;
        }
        String id = UrlParser.parse(object.getStr("url")).get("id");
        Map<String, Object> resMap = new HashMap<>();
        Map<String, String> modelMap = new HashMap<>();
        modelMap.put("url", "https://item.taobao.com/item.htm?id=" + id);
        modelMap.put("content", object.getStr("content"));
        modelMap.put("picUrl", object.getStr("picUrl"));
        modelMap.put("password", object.getStr("password"));
        modelMap.put("ownerName", object.getStr("ownerName"));
        modelMap.put("templateId", object.getStr("templateId"));
        modelMap.put("price", object.getStr("price"));
        modelMap.put("ownerFace", object.getStr("ownerFace"));
        resMap.put("model", modelMap);
        return resMap;
    }

    @ApiOperation("查询淘宝淘口令信息")
    @GetMapping("/queryTkl")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryTkl')")
    public R queryTkl(String key) {
        TbTklQuery query = new TbTklQuery(key);
        query.initSign();
        HttpResponse httpResponse = HttpUtil
                .createPost("http://www.liuliangjunheng.com/api/query_taokouling")
                .body(query.toJson()).execute();
        if (httpResponse.getStatus() == HttpStatus.HTTP_OK) {
            Map<String, Object> resMap = parseTklRes(httpResponse.body());
            if (resMap != null) {
                return Rx.success(JSONUtil.toJsonStr(resMap));
            }
            return Rx.success(httpResponse.body());
        }
        return Rx.fail(httpResponse.toString());
    }

    @ApiOperation("查询京东商品信息")
    @GetMapping("/queryJdItem")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryJdItem')")
    public R queryJdItem(Long itemId) {
        // 查询数据库
        JdGoods jdGoods = taskUtilService.selectByItemId(itemId);
        boolean existFlag = jdGoods != null;
        if (existFlag && CommonUtils.compareDate(jdGoods.getUpdateTime(), refreshDays)) {
            return Rx.success(jdGoods);
        }
        R res = LlTaskUtils.queryJdGoods(String.valueOf(itemId));
        if (Rx.isSuccess(res)) {
            jdGoods = (JdGoods) res.getData();
            taskUtilService.updateJdGoodsInfo(jdGoods, existFlag);
            return Rx.success(jdGoods);
        }
        return res;
    }

    @ApiOperation("查询京东达人信息")
    @GetMapping("/queryJdDr")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryJdDr')")
    public R queryJdDr(Long id) {
        // 查询数据库
        JdDr dr = taskUtilService.selectDrById(id);
        boolean existFlag = dr != null;
        if (existFlag && CommonUtils.compareDate(dr.getUpdateTime(), refreshDays)) {
            return Rx.success(dr.getJson());
        }
        JdDrQuery drQuery = new JdDrQuery(String.valueOf(id));
        R res = drQuery.doQuery();
        if (Rx.isSuccess(res)) {
            JSONObject jsonObject = (JSONObject) res.getData();
            dr = taskUtilService.saveJdDr(id, jsonObject, existFlag);
            return Rx.success(dr.getJson());
        }
        return res;
    }

    @ApiOperation("查询淘宝文章信息")
    @GetMapping("/queryTbArticle")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryTbArticle')")
    public R queryTbArticle(Long id) {
        // 查询数据库
        TbArticle article = taskUtilService.selectArticleById(id);
        boolean existFlag = article != null;
        if (existFlag && CommonUtils.compareDate(article.getUpdateTime(), refreshDays)) {
            return Rx.success(article.getJson());
        }
        TbArticleQuery articleQuery = new TbArticleQuery(String.valueOf(id));
        R res = articleQuery.doQuery();
        if (Rx.isSuccess(res)) {
            JSONObject jsonObject = (JSONObject) res.getData();
            article = taskUtilService.saveTbArticle(id, jsonObject, existFlag);
            return Rx.success(article.getJson());
        }
        return res;
    }

    @ApiOperation("查询京东店铺信息")
    @GetMapping("/queryJdShop")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryJdShop')")
    public R queryJdShop(String shopId) {
        // 查询数据库
        JdShop jdShop = taskUtilService.selectByShopId(shopId);
        boolean existFlag = jdShop != null;
        if (existFlag && CommonUtils.compareDate(jdShop.getUpdateTime(), refreshDays)) {
            return Rx.success(jdShop);
        }
        R res = new JdShopQuery(shopId).doQuery();
        if (Rx.isSuccess(res)) {
            jdShop = (JdShop) res.getData();
            taskUtilService.updateJdShop(jdShop, existFlag);
            return Rx.success(jdShop);
        }
        return res;
    }

    @ApiOperation("查询抖音短视频信息")
    @GetMapping("/queryDyInfo")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryDyInfo')")
    public R queryDyInfo(String url) {
        if (!LlTaskUtils.isDyUrl(url)) {
            return Rx.fail("抖音短视频链接格式不正确");
        }
        // 查询数据库
        String id = LlTaskUtils.getShortCodeFromDyUrl(url);
        DyInfo dyInfo = taskUtilService.selectDyInfo(id);
        if (dyInfo != null) {
            return Rx.success(dyInfo);
        }
        dyInfo = LlTaskUtils.queryDyInfo(url);
        taskUtilService.insertDyInfo(dyInfo);
        return Rx.success(dyInfo);
    }

    @ApiOperation("查询拼多多商品信息")
    @GetMapping("/queryPddItemInfo")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryPddItemInfo')")
    public R queryPddItemInfo(String goodsId) {
        PddGoodsQuery query = new PddGoodsQuery(goodsId);
        query.initSign();
        return query.doQuery();
    }

    @ApiOperation("查询拼多多店铺信息")
    @GetMapping("/queryPddShopInfo")
    @PreAuthorize("hasAuthority('pms:mbau:task:queryPddShopInfo')")
    public R queryPddShopInfo(String shopId) {
        PddShopQuery query = new PddShopQuery(shopId);
        query.initSign();
        return query.doQuery();
    }

    /**
     * 新增任务 tb
     * 1.参数校验
     * 2.链接分析
     * 3.子任务处理
     *
     * @param task 任务实体
     * @return {@link R}
     */
    @ApiOperation("新增任务")
    @PostMapping("/createTask")
    @PreAuthorize("hasAuthority('pms:u:task:createTask')")
    @ControllerWebLog(name = "新增任务")
    public R createTask(@Validated({Default.class}) @RequestBody LlTask task,
                        @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(user)) {
            return Rx.permissionDenied();
        }
        return taskService.createTask(task, user);
    }

    @ApiOperation("编辑任务")
    @PostMapping("/updateTask")
    @PreAuthorize("hasAuthority('pms:u:task:updateTask')")
    public R updateTask(@Validated({Default.class, LlTask.Edit.class}) @RequestBody LlTask task,
                        @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(user)) {
            return Rx.permissionDenied();
        }
        return taskService.updateTask(task, user);
    }

    @ApiOperation("查询任务详情")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", required = true, defaultValue = "201", paramType = "query"))
    @GetMapping("/getTaskDetail")
    @PreAuthorize("hasAuthority('pms:u:task:getTaskDetail')")
    public R getTaskDetail(@NotNull(message = "ID不能为空") Long id, @ApiIgnore Principal principal) {
        TaskDetail detail = taskService.getTaskDetail(id, CommonUtils.getUser(principal));
        return Rx.success(detail);
    }

    @ApiOperation("发布任务")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", defaultValue = "201", required = true, paramType = "query")})
    @PostMapping("/publishTask")
    @PreAuthorize("hasAuthority('pms:u:task:publishTask')")
    @ControllerWebLog(name = "发布任务")
    public R publishTask(@NotNull(message = "ID不能为空") Long id,
                         @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(user)) {
            return Rx.permissionDenied();
        }
        return taskService.publishTask(id, user);
    }

    @ApiOperation("删除任务")
    @PostMapping("/deleteTask")
    @PreAuthorize("hasAuthority('pms:u:task:deleteTask')")
    @ControllerWebLog(name = "删除任务")
    public R deleteTask(@NotNull(message = "ID不能为空") Long id, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(user)) {
            return Rx.permissionDenied();
        }
        return taskService.deleteTask(id, user);
    }

    @ApiOperation("分页查询任务列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", defaultValue = "1"),
            @ApiImplicitParam(name = "pageSize", defaultValue = "10")
    })
    @GetMapping("/getTaskPage")
    @PreAuthorize("hasAuthority('pms:u:task:getTaskPage')")
    public R getTaskPage(TaskQuery query, Integer pageNum, Integer pageSize,
                         @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        if (user == null) {
            return Rx.error("服务器错误");
        }
        query.setUserId((user.getId()));
        PageData pageData = taskService.selectTaskPage(query, pageNum, pageSize, user);
        return Rx.success(pageData);
    }

    @ApiOperation("取消任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/taskCancel")
    @PreAuthorize("hasAuthority('pms:u:task:taskCancel')")
    public R taskCancel(@NotNull(message = "任务主键不能为空") Long id, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        LlTask task = taskService.selectByPrimaryKey(id);
        if (task == null || !task.getStatus().equals(Task_Status.ING.getCode())
                || !task.getUserId().equals(user.getId())) {
            return Rx.error("不允许的操作");
        }
        return taskService.taskCancel(id, user);
    }

    @ApiIgnore
    @ApiOperation("查询任务（用于同步任务状态|非查询任务详情）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/taskQuery")
    public R taskQuery(@NotNull(message = "任务主键不能为空") Long id) {
        return taskService.taskQuery(id);
    }

    @ApiOperation("查询任务积分消耗")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/queryCostPoints")
    @PreAuthorize("hasAuthority('pms:u:task:queryCostPoints')")
    public R queryMerchantCostPoints(@NotNull(message = "任务主键不能为空") Long id,
                                     @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return taskService.queryMerchantCostPoints(id, user);
    }

    @ApiIgnore
    @ApiOperation("暂停任务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "任务主键ID", defaultValue = "210", paramType = "query")
    })
    @GetMapping("/taskPause")
    public R taskPause(@NotNull(message = "任务主键不能为空") Long id, @ApiIgnore Principal principal) {
        SysUser user = CommonUtils.getUser(principal);
        return taskService.taskPause(id, user);
    }

    @ApiIgnore
    @ApiOperation("查询导出任务列表Excel")
    @GetMapping("/exportTaskList")
    public void exportTaskList(TaskQuery query,
                               @ApiIgnore Principal principal,
                               HttpServletRequest request,
                               HttpServletResponse response) {
        query.setUserId(CommonUtils.getUser(principal).getId());
        taskService.exportTaskList(query, request, response, CommonUtils.getUser(principal));
    }

    @ApiOperation("查询任务数量")
    @GetMapping("/queryTaskCount")
    @PreAuthorize("hasAuthority('pms:u:task:queryTaskCount')")
    public R<Integer> queryTaskCount(@ApiIgnore Principal principal,
                                     @RequestParam String type,
                                     @RequestParam String status) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(currentUser)) {
            return Rx.permissionDenied();
        }
        return Rx.success(taskService.queryTaskCount(status, currentUser, type));
    }

    @ApiOperation("查询全部任务数量-统计")
    @GetMapping("/queryAllTaskCount")
    @PreAuthorize("hasAuthority('pms:u:task:queryAllTaskCount')")
    public R<Map<String, Integer>> queryAllTaskCount(@ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        if (!CommonUtils.isMerchantUserType(currentUser)) {
            return Rx.permissionDenied();
        }
        return Rx.success(taskService.queryAllTaskCount(currentUser));
    }
}
