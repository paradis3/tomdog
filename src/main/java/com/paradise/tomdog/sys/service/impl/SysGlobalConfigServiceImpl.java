package com.paradise.tomdog.sys.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.paradise.tomdog.sys.entity.SysGlobalConfig;
import com.paradise.tomdog.sys.mapper.SysGlobalConfigMapper;
import com.paradise.tomdog.sys.service.SysGlobalConfigService;

import java.util.List;

@Service
public class SysGlobalConfigServiceImpl implements SysGlobalConfigService {
    
    @Resource
    private SysGlobalConfigMapper sysGlobalConfigMapper;
    
    @Override
    public int deleteByPrimaryKey(Long id) {
        return sysGlobalConfigMapper.deleteByPrimaryKey(id);
    }
    
    @Override
    public int insert(SysGlobalConfig record) {
        return sysGlobalConfigMapper.insert(record);
    }
    
    @Override
    public int insertSelective(SysGlobalConfig record) {
        return sysGlobalConfigMapper.insertSelective(record);
    }
    
    @Override
    public SysGlobalConfig selectByPrimaryKey(Long id) {
        return sysGlobalConfigMapper.selectByPrimaryKey(id);
    }
    
    @Override
    public int updateByPrimaryKeySelective(SysGlobalConfig record) {
        return sysGlobalConfigMapper.updateByPrimaryKeySelective(record);
    }
    
    @Override
    public int updateByPrimaryKey(SysGlobalConfig record) {
        return sysGlobalConfigMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public List<SysGlobalConfig> selectAllByConfigId(String configId) {
        return sysGlobalConfigMapper.selectAllByConfigId(configId);
    }
}
