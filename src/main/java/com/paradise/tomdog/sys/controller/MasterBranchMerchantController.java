package com.paradise.tomdog.sys.controller;

import com.paradise.tomdog.base.ControllerWebLog;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TaskPermissionMerchant;
import com.paradise.tomdog.sys.service.SysBranchMerchantService;
import com.paradise.tomdog.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Paradise
 */
@Api(tags = "总站：分站商户管理")
@RestController
@RequestMapping("/sys/branchMerchant")
@Slf4j
@Validated
public class MasterBranchMerchantController {
    private final SysBranchMerchantService branchMerchantService;
    private final SysUserService userService;

    @Autowired
    public MasterBranchMerchantController(SysBranchMerchantService branchMerchantService,
                                          SysUserService userService) {
        this.branchMerchantService = branchMerchantService;
        this.userService = userService;
    }

    @ApiOperation("新增分站商户")
    @PostMapping("/addBranchMerchant")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:addBranchMerchant')")
    public R addBranchMerchant(@Validated({Default.class, SysBranchMerchant.Add.class})
                               @RequestBody SysBranchMerchant branchMerchant,
                               @ApiIgnore Principal principal) {
        // 参数格式校验 使用框架封装的方法
        // 存在性校验 - 手机号，邮箱，账号
        SysUser currentUser = CommonUtils.getUser(principal);
        if (StringUtils.contains(branchMerchant.getDomain(), ",")) {
            return Rx.fail("域名含有非法字符");
        }
        if (StringUtils.equalsAny(branchMerchant.getUsername(), "admin", "branch", "api", "branch_user1", "branch_user2")) {
            return Rx.fail("用户名不能为admin/branch/api/user");
        }
        return branchMerchantService.addBranchMerchant(branchMerchant, currentUser);
    }


    /**
     * 修改分站商户资料
     *
     * @param branchMerchant 分站商户
     * @param principal      登录信息
     * @return 修改结果
     * 变更：权限，分站商户维护自己资料
     */
    @ApiOperation("修改分站商户")
    @PostMapping("/modifyBranchMerchant")
    @PreAuthorize("hasAuthority('pms:mb:branchMerchant:modifyBranchMerchant')")
    @ControllerWebLog(name = "修改分站商户")
    public R modifyBranchMerchant(@Validated({Default.class, SysBranchMerchant.Edit.class})
                                  @RequestBody SysBranchMerchant branchMerchant,
                                  @ApiIgnore Principal principal) {
        if (branchMerchant.getId() == null) {
            return Rx.fail("商户ID参数为空");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        return branchMerchantService.modifyBranchMerchant(branchMerchant, currentUser);
    }

    @ApiOperation("删除分站商户")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"))
    @DeleteMapping("/delBranchMerchant")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:delBranchMerchant')")
    public R delBranchMerchant(@NotNull(message = "分站商户id不能为空") Long id, @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return branchMerchantService.deleteById(id, currentUser);
    }

    @ApiOperation(("查询分站商户详情"))
    @ApiImplicitParams(@ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"))
    @GetMapping("/getBranchMerchant")
    @PreAuthorize("hasAuthority('pms:mb:branchMerchant:getBranchMerchant')")
    public R getBranchMerchant(@NotNull(message = "分站商户id不能为空") Long id) {
        return Rx.success(branchMerchantService.selectById(id));
    }

    @ApiOperation("启用、禁用分站商户（状态切换）")
    @ApiImplicitParams(@ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"))
    @PostMapping("/enableBranchMerchant")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:enableBranchMerchant')")
    public R enableBranchMerchant(@NotNull(message = "分站商户id不能为空") Long id,
                                  @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        return branchMerchantService.modifyEnableMerchant(id, currentUser);
    }

    @ApiOperation("修改分站商户管理员密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", paramType = "query", defaultValue = "201"),
            @ApiImplicitParam(name = "password", paramType = "query", defaultValue = "new_pass_word"),
    })
    @PostMapping("/changeBranchMerchantPassword")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:changeBranchMerchantPassword')")
    @ControllerWebLog(name = "修改分站商户管理员密码")
    public R changeBranchMerchantPassword(@NotNull(message = "分站商户id不能为空") Long id,
                                          @NotBlank(message = "密码不能为空")
                                          @Length(max = 20, min = 5, message = "密码长度5-20位字符") String password,
                                          @ApiIgnore Principal principal) {
        SysUser currentUser = CommonUtils.getUser(principal);
        // 根据分站商户id 查询默认管理员
        SysBranchMerchant branchMerchant = branchMerchantService.selectById(id);
        if (branchMerchant == null) {
            return Rx.fail("分站商户查询异常，操作失败");
        }
        SysUser user = userService.selectMerchantSuperAdmin(branchMerchant.getUsername(), branchMerchant.getId());
        if (user == null) {
            log.info("查询分站商户默认管理员异常");
            return Rx.fail("分站商户管理员信息查询异常，操作失败");
        }
        return branchMerchantService.changePassword(user.getId(), password, currentUser);
    }

    @ApiOperation("分页查询分站商户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "pageNum", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "pageSize", defaultValue = "10", paramType = "query")
    })
    @GetMapping("/getBranchMerchantPage")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:getBranchMerchantPage')")
    public R getBranchMerchantPage(String username, String name,
                                   @NotNull(message = "pageNum为空！") Integer pageNum,
                                   @NotNull(message = "pageSize为空！") Integer pageSize) {
        SysBranchMerchant branchMerchant = new SysBranchMerchant();
        branchMerchant.setUsername(username);
        branchMerchant.setName(name);
        return Rx.success(branchMerchantService.selectByPage(branchMerchant, pageNum, pageSize));
    }

    @ApiOperation("查询有效的分站商户列表")
    @GetMapping("/getBranchMerchantList")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:getBranchMerchantList')")
    public R getBranchMerchantList() {
        SysBranchMerchant sysBranchMerchant = new SysBranchMerchant();
        sysBranchMerchant.setStatus("1");
        sysBranchMerchant.setPid(1L);
        return Rx.success(branchMerchantService.selectByAll(sysBranchMerchant));
    }

    @ApiOperation("查询导出分站商户列表")
    @GetMapping("/exportBranchMerchantList")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:exportBranchMerchantList')")
    public void exportBranchMerchantList(String username, String name,
                                         HttpServletResponse response,
                                         HttpServletRequest request) {
        SysBranchMerchant branchMerchant = new SysBranchMerchant();
        branchMerchant.setUsername(username);
        branchMerchant.setName(name);
        branchMerchantService.exportBranchMerchantList(branchMerchant, request, response);
    }

    @ApiOperation("修改分站商户积分")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "修改类型：1 增加积分；0 减少积分；", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(name = "amount", value = "修改积分数量", defaultValue = "2", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "API商户 ID", defaultValue = "201", paramType = "query")
    })
    @PostMapping("/modifyBranchMerchantPoints")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:exportBranchMerchantList')")
    public R modifyBranchMerchantPoints(@NotNull(message = "type 不能为空") Integer type,
                                        @NotNull(message = "amount 不能为空") Integer amount,
                                        @NotNull(message = "id 不能为空") Long id,
                                        @NotBlank(message = "备注不能为空") String remark,
                                        @ApiIgnore Principal principal) {
        if (!type.equals(DictConstant.ADD) && !type.equals(DictConstant.MINUS)) {
            return Rx.fail("type 参数格式不正确");
        }
        if (amount <= 0) {
            return Rx.fail("amount 参数格式不正确");
        }
        SysUser currentUser = CommonUtils.getUser(principal);
        return branchMerchantService.modifyBranchMerchantPoints(id, type, amount, currentUser, remark);
    }

    @ApiOperation("查询分站商户任务权限列表")
    @ApiImplicitParams({@ApiImplicitParam(name = "merchantId", value = "分站商户ID", defaultValue = "201")})
    @GetMapping("/getTaskPermissionList")
    @PreAuthorize("hasAuthority('pms:mb:branchMerchant:getTaskPermissionList')")
    public R getTaskPermissionList(Long merchantId) {
        List<TaskPermissionMerchant> permissionMerchantList = branchMerchantService.getTaskPermissionList(merchantId);
        return Rx.success(permissionMerchantList);
    }

    @ApiOperation("修改分站商户任务权限")
    @PostMapping("/modifyMerchantTaskPermission")
    @PreAuthorize("hasAuthority('pms:m:branchMerchant:modifyMerchantTaskPermission')")
    public R modifyMerchantTaskPermission(@RequestBody TaskPermissionMerchant[] permissionList) {
        return branchMerchantService.modifyMerchantTaskPermission(permissionList);
    }


    @ApiOperation("查询分站商户版权和备案信息以及Logo路径")
    @PostMapping("/getICPAndCopyright")
    public R getIcpAndCopyright(@RequestBody String domain) {
        Map<String, String> map = new HashMap<>(16);
        SysBranchMerchant branchMerchant = branchMerchantService.selectByDomain(domain);
        if (branchMerchant == null) {
            return Rx.fail("无法根据域名查询商户信息");
        }
        map.put("copyright", branchMerchant.getCopyright());
        map.put("icp", branchMerchant.getIcp());
        map.put("homeLogo", branchMerchant.getHomeLogoUrl());
        map.put("loginLogo", branchMerchant.getLoginLogoUrl());
        map.put("smallLogo", branchMerchant.getSmallLogoUrl());
        map.put("merchantTitle", branchMerchant.getMerchantTitle());
        map.put("merchantName", branchMerchant.getName());
        map.put("homePhotoUrl", branchMerchant.getHomePhotoUrl());
        map.put("csUrl", branchMerchant.getCsUrl());
        map.put("expandJson", branchMerchant.getExpandJson());
        return Rx.success(map);
    }
}
