package com.paradise.tomdog.sys.constant;

/**
 * 用户类型枚举
 *
 * @author Paradise
 */
public enum UserTypeEnum {
    //0 超管 1 总站 2 分站商户 3 分站用户 4 API商户
    ADMIN_TYPE(1),
    MERCHANT_TYPE(2),
    MERCHANT_USER_TYPE(3),
    API_TYPE(4);

    private int type;

    UserTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
