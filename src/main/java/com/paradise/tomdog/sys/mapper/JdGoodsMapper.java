package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.JdGoods;
import org.apache.ibatis.annotations.Mapper;

/**
 * 京东商品mapper
 *
 * @author Paradise
 */
@Mapper
public interface JdGoodsMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(JdGoods record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    JdGoods selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(JdGoods record);
}