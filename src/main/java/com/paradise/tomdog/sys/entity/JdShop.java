package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ApiModel(value = "com-paradise-tomdog-sys-entity-JdShop")
@Getter
@Setter
@ToString
public class JdShop {
    /**
     * 京东店铺id
     */
    @ApiModelProperty(value = "京东店铺id")
    private Long shopId;

    /**
     * 店铺链接
     */
    @ApiModelProperty(value = "店铺链接")
    private String url;

    /**
     * 店铺名称
     */
    @ApiModelProperty(value = "店铺名称")
    private String shopName;

    /**
     * 店铺关注人数
     */
    @ApiModelProperty(value = "店铺关注人数")
    private Integer followCount;

    /**
     * 店铺logo 图片链接
     */
    @ApiModelProperty(value = "店铺logo 图片链接")
    private String logoUrl;
    @ApiModelProperty(value = "")
    private Date createTime;
    @ApiModelProperty(value = "")
    private Date updateTime;

    public JdShop() {
    }

    public JdShop(Long shopId, String url, String shopName, Integer followCount, String logoUrl) {
        this.shopId = shopId;
        this.url = url;
        this.shopName = shopName;
        this.followCount = followCount;
        this.logoUrl = logoUrl;
    }
}