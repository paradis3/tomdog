package com.paradise.tomdog.sys.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author Paradise
 */
@Getter
@Setter
@ToString
public class SysUserInfo {
    /**
     * 用户表主键
     */
    private Long userId;

    /**
     * 客服二维码url
     */
    private String qrUrl;

    private Date updateTime;

    private Long updateBy;

    public SysUserInfo() {
    }

    public SysUserInfo(Long userId, String qrUrl, Long updateBy) {
        this.userId = userId;
        this.qrUrl = qrUrl;
        this.updateBy = updateBy;
    }
}