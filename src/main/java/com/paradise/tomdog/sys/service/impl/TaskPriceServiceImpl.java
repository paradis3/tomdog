package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.base.utils.CommonUtils;
import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TaskPrice;
import com.paradise.tomdog.sys.mapper.TaskPriceMapper;
import com.paradise.tomdog.sys.service.TaskPriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 任务价格表实现类
 *
 * @author Paradise
 */
@Service
public class TaskPriceServiceImpl implements TaskPriceService {

    @Resource
    private TaskPriceMapper taskPriceMapper;

    @Override
    public int insert(TaskPrice record) {
        return taskPriceMapper.insert(record);
    }

    @Override
    public TaskPrice selectByPrimaryKey(Long priceId) {
        return taskPriceMapper.selectByPrimaryKey(priceId);
    }

    @Override
    public int updateByPrimaryKey(TaskPrice record) {
        return taskPriceMapper.updateByPrimaryKey(record);
    }

    @Override
    public int updatePriceByPrimaryKey(TaskPrice record) {
        return taskPriceMapper.updatePriceByPrimaryKey(record);
    }

    @Override
    public int updatePriceCfg(TaskPrice[] taskPrices, SysUser currentUser) {
        // 总站，更新自己的价格表； type is_master=1
        // 分站商户，先判断是否有价格表，如果没有，先初始化，再更新
        int count = 0;
        if (CommonUtils.isAdminType(currentUser)) {
            for (TaskPrice taskPrice : taskPrices) {
                if (taskPriceMapper.updateByTypeAndMerchantIdAndIsMaster(taskPrice) == 1) {
                    count++;
                }
            }
        }
        if (CommonUtils.isMerchantType(currentUser)) {
            List<TaskPrice> taskPriceList = taskPriceMapper.selectAllByMerchantIdAndIsMaster(currentUser.getMerchantId(), 0);
            if (taskPriceList.isEmpty()) {
                taskPriceMapper.initInsertCfg(currentUser.getMerchantId());
            }
            for (TaskPrice taskPrice : taskPrices) {
                taskPrice.setMerchantId(currentUser.getMerchantId());
                if (taskPriceMapper.updateByTypeAndMerchantIdAndIsMaster(taskPrice) == 1) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public List<TaskPrice> selectAllByMerchantId(Long merchantId) {
        if (merchantId == null) {
            return taskPriceMapper.selectAllByMerchantIdAndIsMaster(null, 1);
        }
        List<TaskPrice> taskPriceList = taskPriceMapper.selectAllByMerchantIdAndIsMaster(merchantId, 0);
        if (taskPriceList.isEmpty()) {
            // 未配置，则查询默认配置
            taskPriceList = taskPriceMapper.selectAllByMerchantIdAndIsMaster(null, 0);
        }
        return taskPriceList;
    }

}

