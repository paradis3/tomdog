package com.paradise.tomdog.rpc.constant;

/**
 * 任务添加 - 类型 枚举
 *
 * @author Paradise
 */

public enum Task_Add_Type {
    // 空类型
    NULL(-1, ""),
    // 0=APP搜索,
    TB_ITEM_APP_SEARCH_BT(0, "淘宝APP搜索"),
    // 1=PC搜索
    TB_ITEM_PC_SEARCH_BT(1, "淘宝PC搜索"),
    // 2=直访店铺,
    TB_SHOP_DIRECT_SHOP_BT(2, "淘宝直访店铺"),
    // 3=直访商品,
    TB_ITEM_DIRECT_BT(3, "淘宝直访商品"),
    //    6=淘宝店铺收藏
    TB_SHOP_COLLECT(6, "淘宝店铺收藏"),
    //    7=淘宝商品收藏
    TB_ITEM_COLLECT(7, "淘宝商品收藏"),
    //    9=淘宝商品搜索收藏任务
    TB_ITEM_SEARCH_COLLECT_BT(9, "淘宝商品搜索收藏任务"),
    //    10=淘宝商品直接加购任务
    TB_ITEM_DIRECT_CAR(10, "淘宝商品直接加购任务"),
    //    11=淘宝商品搜索加购任务
    TB_ITEM_SEARCH_CAR_BT(11, "淘宝商品搜索加购任务"),
    //    12=淘宝直播关注（达人关注）任务
    TB_LIVE_FOLLOW(12, "淘宝直播关注（达人关注）任务"),
    // 13 = 微淘点赞任务
    TB_WT_LIKE(13, "淘宝微淘点赞任务"),
    //    14=淘宝直播观看人数任务
    TB_LIVE_WATCH(14, "淘宝直播观看人数任务"),
    //    15=淘宝开团提醒任务
    TB_OPEN_REMAINDER(15, "淘宝开团提醒任务"),
    //    17=淘宝商品点赞任务
    TB_ITEM_LIKE(17, "淘宝商品点赞任务"),
    //    20=淘宝淘抢购任务
    TB_TAO_BUY(20, "淘宝淘抢购任务"),
    //    21=淘宝微淘达人文章阅读任务 - 阅读次数
    TB_ARTICLE_READ_TIME(21, "淘宝微淘达人文章阅读任务 - 阅读次数"),
    //    22=淘宝微淘达人文章阅读任务 - 阅读人数
    TB_ARTICLE_READ_PEOPLE(22, "淘宝微淘达人文章阅读任务 - 阅读人数"),
    //    23=淘宝微淘达人文章阅读任务 - 引导进店
    TB_ARTICLE_READ_SHOP(23, "淘宝微淘达人文章阅读任务 - 引导进店"),
    //    24=淘宝直播热度任务
    TB_LIVE_HOT(24, "淘宝直播热度任务"),
    //    18=淘宝直播间加购任务
    TB_LIVE_CAR(18, "淘宝直播间加购任务"),

    //    70=京东流量任务
    JD_ITEM_SEARCH_BT(70, "京东流量任务"),
    //    72=京东店铺收藏任务
    JD_SHOP_COLLECT(72, "京东店铺收藏任务"),
    //    71=京东商品直接收藏任务
    JD_ITEM_DIRECT_COLLECT(71, "京东商品直接收藏任务"),
    //    77=京东商品搜索收藏任务
    JD_ITEM_SEARCH_COLLECT_BT(77, "京东商品搜索收藏任务"),
    //    73=京东商品直接加购任务
    JD_ITEM_DIRECT_CAR(73, "京东商品直接加购任务"),
    //    78=京东商品搜索加购任务
    JD_ITEM_SEARCH_CAR_BT(78, "京东商品搜索加购任务"),
    //    74=京东达人关注任务
    JD_FOLLOW(74, "京东达人关注任务"),
    //    75=京东预约任务
    JD_RESERVATION(75, "京东预约任务"),
    //    76=京东秒杀提醒任务
    JD_SPIKE_REMAINDER(76, "京东秒杀提醒任务"),

    //    31抖音粉丝(100起)
    DY_FANS(31, "抖音粉丝"),
    //32抖音点赞(100起)
    DY_LIKE(32, "抖音点赞"),
    //33抖音播放(1000起)
    DY_PLAY(33, "抖音播放"),
    //34抖音分享(100起)
    DY_SHARE(34, "抖音分享"),
    //35抖音评论(10起)
    DY_COMMENT(35, "抖音评论"),

    //    90 = 拼多多流量任务
    PDD_ITEM_LL_BT(90, "拼多多流量任务"),
    //    91 = 拼多多商品搜索收藏任务
    PDD_ITEM_SEARCH_COLLECT_BT(91, "拼多多商品搜索收藏任务"),
    //    92 = 拼多多商品直接收藏任务
    PDD_ITEM_DIRECT_COLLECT(92, "拼多多商品直接收藏任务"),
    //    93 = 拼多多商品店铺收藏任务
    PDD_SHOP_COLLECT(93, "拼多多商品店铺收藏任务");

    private String name;

    private int type;

    Task_Add_Type(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }
}
