package com.paradise.tomdog.rpc.result.jd;

import lombok.Getter;
import lombok.Setter;

/**
 * 京东商品信息
 *
 * @author Paradise
 */
@Getter
@Setter
public class JdItemInfo {
    private ShopInfo shopInfo;
    private String name;
    private String chatUrl;
    private Long id;

    public JdItemInfo(ShopInfo shopInfo, String name, String chatUrl, Long id) {
        this.shopInfo = shopInfo;
        this.name = name;
        this.chatUrl = chatUrl;
        this.id = id;
    }

    @Override
    public String toString() {
        return "JdItemInfo{" +
                "shopInfo=" + shopInfo +
                ", wname='" + name + '\'' +
                ", chatUrl='" + chatUrl + '\'' +
                ", id=" + id +
                '}';
    }
}
