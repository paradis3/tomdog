package chatbot;

import chatbot.message.Message;
import chatbot.message.TextMessage;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpStatus;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * @author dzhang
 */
@Slf4j
public class DentalCabotClient {

    private static final String WEB_HOOK_PREFIX = "https://oapi.dingtalk.com/robot/send?access_token=";
    private static final String DEFAULT_TOKEN = "34225fea20aa8989813e6a951f1f8339cb37202da400c5a18f9bc862b5ba9a76";

    private static SendResult push(String token, Message message) {
        SendResult sendResult = new SendResult();
        HttpResponse response = HttpUtil.createPost(WEB_HOOK_PREFIX + token)
                .body(message.toJsonString())
                .execute();
        if (response.getStatus() == HttpStatus.HTTP_OK) {
            String result = (response.body());
            JSONObject obj = JSONObject.parseObject(result);
            Integer errCode = obj.getInteger("errcode");
            sendResult.setErrorCode(errCode);
            sendResult.setErrorMsg(obj.getString("errmsg"));
            sendResult.setIsSuccess(errCode.equals(0));
        }
        return sendResult;
    }

    public static void sendMessage(String token, Message message) {
        SendResult result = push(WEB_HOOK_PREFIX + token, message);
        if (!result.isSuccess()) {
            log.error("钉钉机器人推送异常： " + result.getErrorMsg());
        }
    }

    public static void sendText(String msg, String token) {
        sendText(msg, token, false);
    }

    public static void sendText(String msg) {
        sendText(msg, DEFAULT_TOKEN, false);
    }

    public static void sendText(String msg, String token, boolean atAll) {
        SendResult result = push(token, new TextMessage("流量均衡业务告警： " + msg, new ArrayList<>(), atAll));
        if (!result.isSuccess()) {
            log.error("钉钉机器人推送异常： " + result.getErrorMsg());
        }
    }

    public static void main(String[] args) {
        sendText("Hello-world2!", "34225fea20aa8989813e6a951f1f8339cb37202da400c5a18f9bc862b5ba9a76", true);
    }

}


