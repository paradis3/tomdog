package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPermissionMerchant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 分站商户任务权限配置
 *
 * @author Paradise
 */
@Mapper
public interface TaskPermissionMerchantMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPermissionMerchant record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskPermissionMerchant selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(TaskPermissionMerchant record);

    /**
     * 初始化 CmwAPI 权限
     *
     * @param merchantId api 商户ID
     * @return insert count
     */
    int initPermission(@Param("merchantId") Long merchantId);

    /**
     * 查询 分站商户任务权限
     *
     * @param merchantId 分站商户id
     * @return select result
     */
    List<TaskPermissionMerchant> selectAllByMerchantId(@Param("merchantId") Long merchantId);

    /**
     * 查询是否有权限
     *
     * @param taskCode   任务类型
     * @param merchantId 分站商户ID
     * @return true 存在 false 不存在
     */
    boolean selectByTaskCodeAndMerchantId(@Param("taskCode") String taskCode,
                                          @Param("merchantId") Long merchantId);

}