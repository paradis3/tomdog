package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysDictType;

public interface SysDictTypeService {


    int deleteByPrimaryKey(Long dictId);

    int insert(SysDictType record);

    int insertSelective(SysDictType record);

    SysDictType selectByPrimaryKey(Long dictId);

    int updateByPrimaryKeySelective(SysDictType record);

    int updateByPrimaryKey(SysDictType record);

}
