package com.paradise.tomdog.sys.service.impl;

import chatbot.DentalCabotClient;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.base.utils.DateFormatUtils;
import com.paradise.tomdog.base.utils.ExcelExportUtils;
import com.paradise.tomdog.global.SystemConfig;
import com.paradise.tomdog.sys.constant.DictConstant;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.mapper.*;
import com.paradise.tomdog.sys.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Paradise
 */
@Slf4j
@Service
public class SysBranchMerchantServiceImpl implements SysBranchMerchantService {

    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private SysBranchMerchantMapper sysBranchMerchantMapper;
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private BalanceMapper balanceMapper;
    @Resource
    private BalanceService balanceService;
    @Resource
    private UserLevelService userLevelService;
    @Resource
    private SysUserPayInfoService sysUserPayInfoService;
    @Resource
    private BranchRechargePackageService branchRechargePackageService;
    @Resource
    private TaskPermissionMerchantMapper taskPermissionMerchantMapper;
    @Resource
    private SysSmsInfoService sysSmsInfoService;
    @Resource
    private SysRoleMapper roleMapper;
    @Resource
    private SysMenuMapper menuMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R addBranchMerchant(SysBranchMerchant record, SysUser currentUser) {
        record.setName(record.getMerchantTitle());
        if (sysBranchMerchantMapper.selectOneByTelephone(record.getTelephone()) != null) {
            return Rx.fail("分站商户手机号码：" + record.getTelephone() + "已被其他商户绑定");
        }
        if (StrUtil.isNotEmpty(record.getDomain())) {
            if (sysBranchMerchantMapper.selectOneByDomain(record.getDomain()) != null) {
                return Rx.fail("分站商户域名: " + record.getDomain() + " 已被其他商户绑定");
            }
        }
        // 新增分站商户信息
        record.setCreateBy(currentUser.getId());
        record.setUpdateBy(currentUser.getId());
        record.setPid(currentUser.getMerchantId());
        record.setUpdateTime(new Date());
        record.setStatus("1");
        record.setIsDel(0);
        if (StringUtils.isNotEmpty(record.getSmsSignId())) {
            record.setSmsSignId(record.getSmsSignId().replaceAll(" ", ""));
        }
        sysBranchMerchantMapper.insert(record);
        if (record.getId() == null) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Rx.fail();
        }
        // 初始化积分余额
        balanceMapper.insert(new Balance(record.getId(), UserTypeEnum.MERCHANT_TYPE.getType(), 0));
        // 初始化分站商户权限
        taskPermissionMerchantMapper.initPermission(record.getId());
        // 初始化分站商户等级（升级、返利）列表
        List<UserLevel> userLevels = userLevelService.initMerchantLevel(record.getId());
        // 初始化分站商户收付款信息
        sysUserPayInfoService.initMerchantPayInfo(record.getId());
        //初始化短信签名模板信息(暂未写，可与总站共用)
        sysSmsInfoService.initSmsInfo(record.getId());
        // 初始化分站商户套餐信息
        branchRechargePackageService.initMerchantPackage(record.getId());
        // 初始化新增默认角色
        Long roleId = initMerchantSuperRole(record.getId(), currentUser.getId());
        if (roleId != null) {
            // 新增分站默认管理员账户信息
            SysUser user = initSysUser(record, currentUser.getId(), roleId);
            if (sysUserMapper.insert(user) == 1) {
                return Rx.success(record);
            }
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return Rx.fail();
    }

    private SysUser initSysUser(SysBranchMerchant record, Long userId, Long roleId) {
        SysUser user = new SysUser();
        user.setPassword(passwordEncoder.encode(SystemConfig.DEFAULT_PASSWORD));
        user.setUsername(record.getUsername());
        user.setMerchantId(record.getId());
        user.setRegistrationSource("新增商户创建默认管理员账户");
        user.setRegistrationTime(new Date());
        user.setTelephone(record.getTelephone());
        user.setUserType(UserTypeEnum.MERCHANT_TYPE.getType());
        user.setStatus(DictConstant.ENABLE);
        user.setCreator(userId);
        user.setUpdater(userId);
        // 分站角色 - 设定
        user.setRoleId(roleId);
        return user;
    }

    private Long initMerchantSuperRole(Long merchantId, Long userId) {
        SysRole role = new SysRole();
        role.setIsSuper(1);
        role.setMerchantId(merchantId);
        role.setStatus(DictConstant.ENABLE);
        role.setUpdater(userId);
        role.setCreator(userId);
        role.setName("超级管理员");
        role.setDescription("新增分站创建默认超级管理员角色");
        if (roleMapper.insert(role) == 1) {
            // 初始化角色的菜单权限
            menuMapper.initSuperRoleMenu(role.getId(), 0);
            return role.getId();
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R modifyBranchMerchant(SysBranchMerchant branchMerchant, SysUser currentUser) {
        SysBranchMerchant sourceInfo = sysBranchMerchantMapper.selectByPrimaryKey(branchMerchant.getId());
        branchMerchant.setName(branchMerchant.getMerchantTitle());
        if (branchMerchant.getId().equals(currentUser.getMerchantId())) {
            log.info("商户正在自行修改资料");
            log.info("修改前资料：{}", sourceInfo.toString());
            branchMerchant.setTelephone(sourceInfo.getTelephone());
            branchMerchant.setUsername(sourceInfo.getUsername());
            log.info("修改后资料：{}", branchMerchant.toString());
        }
        branchMerchant.setUpdateBy(currentUser.getId());
        branchMerchant.setUpdateTime(new Date());

        // 如果手机号码 or username 被修改，同步更新 sys_user
        boolean changed = !sourceInfo.getUsername().equals(branchMerchant.getUsername())
                || !sourceInfo.getTelephone().equals(branchMerchant.getTelephone());
        if (changed) {
            SysUser user = sysUserMapper.selectMerchantSuperAdmin(sourceInfo.getUsername(), branchMerchant.getId());
            if (user != null) {
                int x = sysUserMapper.updateByBranchMerchant(user.getId(), branchMerchant.getTelephone(), branchMerchant.getUsername());
                if (x != 1) {
                    log.error("商户修改信息，同步更新账户信息失败");
                    return Rx.fail();
                }
            }
        }
        if (StringUtils.isNotEmpty(branchMerchant.getSmsSignId())) {
            branchMerchant.setSmsSignId(branchMerchant.getSmsSignId().replaceAll(" ", ""));
        }
        if (sysBranchMerchantMapper.updateByPrimaryKeySelective(branchMerchant) == 1) {
            return Rx.success(branchMerchant);
        }
        TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        return Rx.fail();
    }

    @Override
    public R deleteById(Long id, SysUser currentUser) {
        SysBranchMerchant branchMerchant = new SysBranchMerchant();
        branchMerchant.setId(id);
        branchMerchant.setIsDel(1);
        branchMerchant.setUpdateBy(currentUser.getId());
        branchMerchant.setUpdateTime(new Date());
        if (sysBranchMerchantMapper.updateByPrimaryKeySelective(branchMerchant) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public PageData selectByPage(SysBranchMerchant branchMerchant, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<SysBranchMerchant> branchMerchantList = sysBranchMerchantMapper.selectByPage(branchMerchant);
        return new PageData(branchMerchantList, new PageInfo<>(branchMerchantList).getTotal());
    }

    @Override
    public R changePassword(Long id, String password, SysUser currentUser) {
        if (1 == sysUserMapper.changePassword(id, passwordEncoder.encode(password), currentUser.getId())) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public R modifyEnableMerchant(Long id, SysUser currentUser) {
        SysBranchMerchant merchant = sysBranchMerchantMapper.selectByPrimaryKey(id);
        if (merchant == null) {
            return Rx.fail();
        }
        SysBranchMerchant branchMerchant = new SysBranchMerchant();
        branchMerchant.setId(id);
        branchMerchant.setUpdateBy(currentUser.getId());
        if (merchant.getStatus().equals(DictConstant.DISABLE)) {
            branchMerchant.setStatus(DictConstant.ENABLE);
        } else if (merchant.getStatus().equals(DictConstant.ENABLE)) {
            branchMerchant.setStatus(DictConstant.DISABLE);
        }
        branchMerchant.setUpdateTime(new Date());
        if (sysBranchMerchantMapper.updateByPrimaryKeySelective(branchMerchant) == 1) {
            return Rx.success();
        }
        return Rx.fail();
    }

    @Override
    public SysBranchMerchant selectById(Long id) {
        return sysBranchMerchantMapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R modifyBranchMerchantPoints(Long id, Integer type, Integer amount, SysUser currentUser, String remark) {
        SysBranchMerchant merchant = sysBranchMerchantMapper.selectByPrimaryKey(id);
        if (merchant == null) {
            return Rx.fail("分站商户不存在或已被删除");
        }
        Points_Record_Type recordType;
        if (type.equals(DictConstant.MINUS)) {
            amount = -amount;
            recordType = Points_Record_Type.MASTER_MODIFY_MINUS;
        } else {
            recordType = Points_Record_Type.MASTER_MODIFY_ADD;
        }
        BalanceUpdateParam param = BalanceUpdateParam.builder()
                .businessId(id).userTypeEnum(UserTypeEnum.MERCHANT_TYPE).remark(remark)
                .recordType(recordType).amount(amount).createBy(currentUser.getId())
                .build();
        R res = balanceService.updateBalance(param);
        if (!Rx.isSuccess(res)) {
            return res;
        }
        return res;
    }

    @Override
    public void exportBranchMerchantList(SysBranchMerchant branchMerchant,
                                         HttpServletRequest request,
                                         HttpServletResponse response) {
        String fileName = "分店商户导出列表";
        String[] rowNames = new String[]{"分站商户ID", "USERNAME", "分账商户名称", "积分余额", "绑定域名", "创建时间", "备注信息"};
        List<SysBranchMerchant> branchMerchantList = sysBranchMerchantMapper.selectByPage(branchMerchant);
        ExcelExportUtils.ExcelExportCfg cfg = ExcelExportUtils.ExcelExportCfg.builder()
                .request(request).response(response)
                .dataList(getExcelDataList(branchMerchantList))
                .rowName(rowNames)
                .sheetName(fileName)
                .title(fileName)
                .build();
        try {
            ExcelExportUtils.exportData(cfg);
        } catch (Exception e) {
            DentalCabotClient.sendText(e.getLocalizedMessage());
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public List<TaskPermissionMerchant> getTaskPermissionList(Long merchantId) {
        return taskPermissionMerchantMapper.selectAllByMerchantId(merchantId);
    }

    @Override
    public R modifyMerchantTaskPermission(TaskPermissionMerchant[] permissionList) {
        for (TaskPermissionMerchant permission : permissionList) {
            if (taskPermissionMerchantMapper.updateByPrimaryKey(permission) != 1) {
                return Rx.fail();
            }
        }
        return Rx.success();
    }

    @Override
    public SysBranchMerchant selectByDomain(String domain) {
        return sysBranchMerchantMapper.selectOneByDomain(domain);
    }

    private List<Object[]> getExcelDataList(List<SysBranchMerchant> branchMerchantList) {
        List<Object[]> resultList = new ArrayList<>();
        Object[] arr;
        for (SysBranchMerchant chant : branchMerchantList) {
            arr = new Object[7];
            arr[0] = chant.getId();
            arr[1] = chant.getUsername();
            arr[2] = chant.getName();
            arr[3] = chant.getPoints();
            arr[4] = chant.getDomain();
            arr[5] = DateFormatUtils.format(chant.getCreateTime());
            arr[6] = chant.getRemark();
            resultList.add(arr);
        }
        return resultList;
    }

    @Override
    public List<SysBranchMerchant> selectByAll(SysBranchMerchant sysBranchMerchant) {
        sysBranchMerchant.setIsDel(0);
        return sysBranchMerchantMapper.selectByAll(sysBranchMerchant);
    }

    @Override
    public List<SysBranchMerchant> selectAllByUpdateTimeAfter(Date minUpdateTime) {
        return sysBranchMerchantMapper.selectAllByUpdateTimeAfter(minUpdateTime);
    }
}
