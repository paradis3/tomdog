/*
 * Copyright (C), 2016-2020, XXX有限公司
 * FileName: PermissionGenerateUtil
 * Author:   Administrator
 * Date:     2020/1/7/007 16:03
 * Description: 角色权限生成器
 * History:
 * <author>          <time>          <version>          <desc>
 * lj          2020-2-13 10:56:28           版本号              描述
 */
package com.paradise.tomdog.base.utils;

import com.paradise.tomdog.sys.entity.SysPermission;
import com.paradise.tomdog.sys.entity.SysRolePermission;
import com.paradise.tomdog.sys.mapper.SysPermissionMapper;
import com.paradise.tomdog.sys.service.SysPermissionService;
import com.paradise.tomdog.sys.service.SysRolePermissionService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 〈角色权限生成器〉
 *
 * @author Administrator
 * @date 2020/1/7/007
 * @since 1.0.0
 */
@Slf4j
@Component
public class PermissionGenerateUtil implements ApplicationListener {
    @Autowired
    SysPermissionService sysPermissionService;
    @Autowired
    SysPermissionMapper sysPermissionMapper;
    @Autowired
    SysRolePermissionService sysRolePermissionService;
    /**
     * 扫描的包名
     */
    private static final String BASE_PACKAGE = "com.paradise.tomdog.*.controller";
    private static final String RESOURCE_PATTERN = "/**/*.class";

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        try {
            //获取已有的权限列表
            List<SysPermission> sysPermissions = sysPermissionMapper.selectAllValue();
            List<String> sysPermissionsCollect = sysPermissions.stream().map(SysPermission::getValue).collect(Collectors.toList());
            String pattern = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + ClassUtils.convertClassNameToResourcePath(BASE_PACKAGE) + RESOURCE_PATTERN;
            Resource[] resources = resourcePatternResolver.getResources(pattern);
            MetadataReaderFactory readerFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
            for (Resource resource : resources) {
                if (resource.isReadable()) {
                    MetadataReader reader = readerFactory.getMetadataReader(resource);
                    //扫描到的class
                    String className = reader.getClassMetadata().getClassName();
                    Class<?> clazz = Class.forName(className);
                    //判断类的Api注解
                    RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                    String mappingValueBase = "";
                    if (requestMapping != null) {
                        mappingValueBase = ((String[]) requestMapping.annotationType().getDeclaredMethod("value", new Class[0])
                                .invoke(requestMapping, new Object[]{}))[0];
                    }
                    //获取所有方法
                    for (Method declaredMethod : clazz.getDeclaredMethods()) {
                        SysPermission sysPermission = new SysPermission();
                        //判断是否有PreAuthorize注解
                        PreAuthorize preAuthorize = declaredMethod.getAnnotation(PreAuthorize.class);
                        if (preAuthorize != null) {
                            String preAuthorizeValue = (String) preAuthorize.annotationType().getDeclaredMethod("value", new Class[0])
                                    .invoke(preAuthorize, new Object[]{});
                            sysPermission.setValue(preAuthorizeValue.split("'")[1]);
                            if (sysPermissionsCollect.contains(sysPermission.getValue())) {
                                continue;
                            }
                        } else {
                            //如果权限未设置跳过此次循环
                            continue;
                        }
                        //判断是否有ApiOperation注解
                        ApiOperation apiOperation = declaredMethod.getAnnotation(ApiOperation.class);
                        if (apiOperation != null) {
                            String apiOperationValue = (String) apiOperation.annotationType().getDeclaredMethod("value", new Class[0])
                                    .invoke(apiOperation, new Object[]{});
                            sysPermission.setName(apiOperationValue);
                        }
                        //判断是否有PostMapping注解
                        String mappingValue = "";
                        PostMapping postMapping = declaredMethod.getAnnotation(PostMapping.class);
                        if (postMapping != null) {
                            mappingValue = ((String[]) postMapping.annotationType().getDeclaredMethod("value", new Class[0])
                                    .invoke(postMapping, new Object[]{}))[0];
                        } else {
                            //判断是否有getMapping注解
                            GetMapping getMapping = declaredMethod.getAnnotation(GetMapping.class);
                            if (postMapping != null) {
                                mappingValue = ((String[]) getMapping.annotationType().getDeclaredMethod("value", new Class[0])
                                        .invoke(getMapping, new Object[]{}))[0];
                            } else {
                                //判断是否有DeleteMapping注解
                                DeleteMapping delMapping = declaredMethod.getAnnotation(DeleteMapping.class);
                                if (delMapping != null) {
                                    mappingValue = ((String[]) delMapping.annotationType().getDeclaredMethod("value", new Class[0])
                                            .invoke(delMapping, new Object[]{}))[0];
                                } else {
                                    //判断是否有RequestMapping注解
                                    RequestMapping restMapping = declaredMethod.getAnnotation(RequestMapping.class);
                                    if (restMapping != null) {
                                        mappingValue = ((String[]) restMapping.annotationType().getDeclaredMethod("value", new Class[0])
                                                .invoke(restMapping, new Object[]{}))[0];
                                    }
                                }
                            }
                        }
                        sysPermission.setUri(mappingValueBase + mappingValue);
                        sysPermission.setPid(0L);
                        sysPermission.setType("1");
                        sysPermission.setStatus("1");
                        sysPermission.setCreator(1L);
                        sysPermission.setCreateTime(new Date());
                        sysPermission.setUpdater(1L);
                        sysPermission.setUpdateTime(new Date());
                        this.doInsert(sysPermission);
                    }
                }
            }
        } catch (Exception e) {
            log.error("读取class失败", e);
        }
    }

    private void doInsert(SysPermission sysPermission) {
        try {
            sysPermissionService.insert(sysPermission);
            //初始化权限  1 总站 2 分站商户 3 分站用户 4 API商户的 权限
            String permissionValue = sysPermission.getValue().split(":")[1];
            List<SysRolePermission> sysRolePermissionArrayList = new ArrayList<>();
            if (StringUtils.contains(permissionValue, "m")) {
                SysRolePermission sysRolePermission = new SysRolePermission();
                sysRolePermission.setPermissionId(sysPermission.getId());
                sysRolePermission.setRoleId(1L);
                sysRolePermissionArrayList.add(sysRolePermission);
            }
            if (StringUtils.contains(permissionValue, "b")) {
                SysRolePermission sysRolePermission = new SysRolePermission();
                sysRolePermission.setPermissionId(sysPermission.getId());
                sysRolePermission.setRoleId(2L);
                sysRolePermissionArrayList.add(sysRolePermission);
            }
            if (StringUtils.contains(permissionValue, "u")) {
                SysRolePermission sysRolePermission = new SysRolePermission();
                sysRolePermission.setPermissionId(sysPermission.getId());
                sysRolePermission.setRoleId(3L);
                sysRolePermissionArrayList.add(sysRolePermission);
            }
            if (StringUtils.contains(permissionValue, "a")) {
                SysRolePermission sysRolePermission = new SysRolePermission();
                sysRolePermission.setPermissionId(sysPermission.getId());
                sysRolePermission.setRoleId(4L);
                sysRolePermissionArrayList.add(sysRolePermission);
            }
            sysRolePermissionService.insertList(sysRolePermissionArrayList);
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }
}