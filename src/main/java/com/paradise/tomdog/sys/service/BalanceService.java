package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.Balance;
import com.paradise.tomdog.sys.entity.BalanceUpdateParam;

/**
 * 余额相关操作的接口封装
 *
 * @author Paradise
 */
public interface BalanceService {
    /**
     * 查询余额
     *
     * @param businessId   业务主键
     * @param userTypeEnum 类型，参见 {@link UserTypeEnum}
     * @return 积分余额
     */
    Integer queryBalance(Long businessId, UserTypeEnum userTypeEnum);

    /**
     * 增加积分 增加并记录
     *
     * @param businessId   业务主键
     * @param userTypeEnum 类型，参见 {@link UserTypeEnum}
     * @param amount       数量
     * @param recordType   记录类型 {@linkplain Points_Record_Type}
     * @return update count 成功=1
     */
    R<Balance> updateBalance(Long businessId, UserTypeEnum userTypeEnum, int amount, Points_Record_Type recordType);

    /**
     * 更新短信消费积分
     *
     * @param businessId 业务主键
     * @return 更新结果
     */
    R<Balance> updateBalanceForSms(Long businessId);

    /**
     * 修改积分并记录
     *
     * @param param 构造更新参数 注意参数的完整性
     * @return {@link R}
     */
    R<Balance> updateBalance(BalanceUpdateParam param);


}

