package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.SysUserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * user info
 *
 * @author Paradise
 */
@Mapper
public interface SysUserInfoMapper {
    /**
     * delete by primary key
     *
     * @param userId primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long userId);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysUserInfo record);

    /**
     * select by primary key
     *
     * @param userId primary key
     * @return object by primary key
     */
    SysUserInfo selectByPrimaryKey(Long userId);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysUserInfo record);

}