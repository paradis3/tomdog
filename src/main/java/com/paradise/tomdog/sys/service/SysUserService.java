package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.base.PageData;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.vo.BranchUserQuery;
import com.paradise.tomdog.sys.vo.BranchUserVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Paradise
 */
public interface SysUserService {

    /**
     * 逻辑删除
     *
     * @param userId   账户id
     * @param updateBy 更新人
     * @return update count
     */
    int deleteByPrimaryKey(Long userId, Long updateBy);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    /**
     * 用户注册
     *
     * @param registerBean 注册实体
     * @return 返回注册后的实体类信息
     */
    SysUser register(RegisterBean registerBean);

    /**
     * 用户登录
     *
     * @param username username
     * @param password password
     * @param domain   当前域名
     * @param request  请求信息
     * @return {@link R}
     */
    R<String> login(String domain, String username, String password, HttpServletRequest request);

    /**
     * 修改密码
     *
     * @param newPassword 新密码
     * @param password    旧密码
     * @param principal   principal
     * @return 修改结果
     */
    R changePassword(String newPassword, String password, Principal principal);

    /**
     * 修改密码
     *
     * @param newPassword 新密码
     * @param telephone   手机号码
     * @param smsCode     短信验证码
     * @param domain      域名
     * @return 修改结果
     */
    R forgetPassword(String newPassword, String telephone, String smsCode, String domain);

    /**
     * 根据用户名 查询用户信息
     *
     * @param userId username
     * @return sysUser info
     */
    SysUser getUserInfo(Long userId);

    /**
     * 查询分站用户详细信息
     *
     * @param userId 用户名
     * @return {@link BranchUserVo}
     */
    BranchUserVo getUserInfoDetail(Long userId);

    default List<SysPermission> getPermissionList(Long id) {
        return new ArrayList<>();
    }

    /**
     * 重置用户账号密码
     *
     * @param password 新密码
     * @param id       账户id
     * @return {@linkplain R}
     */
    R resetPassword(String password, Long id);

    /**
     * 分页查询 分站用户列表
     *
     * @param query    查询参数
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @return {@linkplain PageData}
     */
    PageData getBranchMerchantUserPage(BranchUserQuery query, Integer pageNum, Integer pageSize);

    /**
     * 查询导出 分站用户 Excel
     *
     * @param query    查询实体
     * @param request  请求
     * @param response 响应
     */
    void exportBranchMerchantUserList(BranchUserQuery query, HttpServletRequest request, HttpServletResponse response);

    /**
     * 修改 分站用户状态
     *
     * @param id          分站用户主键
     * @param currentUser 当前操作人
     * @return {@linkplain R}
     */
    R modifyEnableMerchant(Long id, SysUser currentUser);

    /**
     * 修改 管理员状态
     *
     * @param id          用户主键
     * @param currentUser 当前操作人
     * @return {@linkplain R}
     */
    R modifyEnableAccount(Long id, SysUser currentUser);

    /**
     * 修改 分站用户 密码
     *
     * @param id          分站用户id
     * @param password    新密码
     * @param currentUser 当前登录人
     * @return {@link R}
     */
    R changeBranchMerchantUserPassword(Long id, String password, SysUser currentUser);

    /**
     * 查询 分站用户详情
     *
     * @param id 分站用户id
     * @return {@link BranchUserVo}
     */
    BranchUserVo selectBranchMerchantUser(Long id);

    /**
     * 修改分站用户积分
     *
     * @param id          分站用户主键
     * @param type        修改类型 1+/0-
     * @param amount      修改数量
     * @param currentUser 修改人
     * @param remark      备注
     * @return {@link R}
     */
    R modifyBranchMerchantUserPoints(Long id, Integer type, Integer amount, SysUser currentUser, String remark);

    /**
     * 根据邀请码查询用户
     *
     * @param uid 邀请码
     * @return {@link SysUser}
     */
    SysUser selectByInvitationCode(String uid);

    /**
     * 分站用户注册 （带推广链接）
     *
     * @param registerBean 注册信息
     * @return {@link R}
     */
    R registerBranchUser(RegisterBean registerBean, SysUser puser);

    /**
     * 根据用户名和分站id查询用户信息
     *
     * @param username 用户名
     * @return {@link SysUser}
     */
    List<SysUser> selectAllByUsernameAndMerchantId(String username, Long merchantId);

    /**
     * 根据用户名和分站域名查询用户信息
     *
     * @param username 用户名
     * @param domain   域名信息
     * @return {@link SysUser}
     */
    SysUser selectOneByUsernameAndDomain(String username, String domain);

    /**
     * 根据登录信息查询分站用户
     *
     * @param loginBean 登录信息
     * @return {@link SysUser}
     */
    SysUser selectByLoginBean(LoginBean loginBean);

    /**
     * 根据用户名和分站id查询用户信息
     *
     * @param usernameAndDomain
     * @return
     */
    SysUser selectOneByUsernameAndDomain(String usernameAndDomain);

    /**
     * 根据用户名和分站信息查询用户信息
     *
     * @param username
     * @param domain
     * @return
     */
    List<SysUser> selectAllByUsernameAndDomain(String username, String domain);

    /**
     * 根据电话号码和分站id查询用户信息
     *
     * @param telephone
     */
    List<SysUser> selectAllByPhoneAndMerchantId(String telephone, Long merchantId);

    /**
     * 根据电话号码和分站信息查询用户信息
     *
     * @param telephone 手机号码
     * @param domain    分站域名
     * @return sysUserList
     */
    List<SysUser> selectAllByPhoneAndDomain(String telephone, String domain);

    /**
     * 查询推广用户列表
     *
     * @param user     当前用户
     * @param pageNum  pageNum
     * @param pageSize pageSize
     * @return {@link R}
     */
    R getInvitationUserList(SysUser user, Integer pageNum, Integer pageSize);

    /**
     * 查询推广用户数
     *
     * @param userId 当前用户
     * @return 推广用户数
     */
    Integer getInvitationUserCount(Long userId);

    /**
     * 修改绑定手机号码
     *
     * @param telephone 新手机号码
     * @param code      验证码
     * @param user      当前用户
     * @return {@link R}
     */
    R updateUserTelephone(String telephone, String code, SysUser user);

    /**
     * 查询上次登录信息
     *
     * @return {@link SysLoginInfo}
     */
    SysLoginInfo getLastLoginInfo(String username, String domain);

    /**
     * 配置分站用户专属客服二维码
     *
     * @param id     分站用户id
     * @param picUrl 二维码图片
     * @param user   当前用户
     * @return {@link R}
     */
    R configCsQrCode(Long id, String picUrl, SysUser user);

    /**
     * 查询专属客服二维码图片
     *
     * @param id 分站商户 id
     * @return url
     */
    String getCsQrCodeUrl(Long id);

    /**
     * 查询分站商户默认管理员
     *
     * @param username   分站商户账户
     * @param merchantId 分站商户id
     * @return {@link SysUser}
     */
    SysUser selectMerchantSuperAdmin(String username, Long merchantId);

    /**
     * 逻辑删除角色下的账户 - 递归
     *
     * @param roleId 角色id
     * @param id     操作人id
     * @return update count
     */
    int deleteAllByRoleId(Long roleId, Long id);
}
