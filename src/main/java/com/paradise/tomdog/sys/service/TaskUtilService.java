package com.paradise.tomdog.sys.service;

import cn.hutool.json.JSONObject;
import com.paradise.tomdog.sys.entity.*;

/**
 * 任务相关工具
 *
 * @author Paradise
 */
public interface TaskUtilService {

    /**
     * 根据jd itemId 查询商品信息
     *
     * @param itemId 商品id
     * @return {@link JdGoods}
     */
    JdGoods selectByItemId(Long itemId);

    /**
     * 更新jd info
     *
     * @param record    jd info
     * @param existFlag 存在更新，否则插入
     */
    void updateJdGoodsInfo(JdGoods record, boolean existFlag);

    /**
     * 更新jd 店铺信息
     *
     * @param record    新的店铺信息
     * @param existFlag 存在更新，否则插入
     */
    void updateJdShop(JdShop record, boolean existFlag);

    /**
     * 根据京东店铺id查询信息
     *
     * @param shopId jd 店铺id
     * @return {@link JdShop}
     */
    JdShop selectByShopId(String shopId);

    /**
     * 查询抖音信息
     *
     * @param id 主键
     * @return {@link DyInfo}
     */
    DyInfo selectDyInfo(String id);

    /**
     * 新增抖音视频信息
     *
     * @param dyInfo 抖音信息
     */
    void insertDyInfo(DyInfo dyInfo);

    /**
     * 查询达人信息
     *
     * @param id 达人id
     * @return {@link JdDr}
     */
    JdDr selectDrById(Long id);

    /**
     * @param id         达人id
     * @param jsonObject json信息
     * @param existFlag  是否存在
     * @return {@link JdDr}
     */
    JdDr saveJdDr(Long id, JSONObject jsonObject, boolean existFlag);

    /**
     * 根据文章id 查询信息
     *
     * @param id 文章id
     * @return {@link TbArticle}
     */
    TbArticle selectArticleById(Long id);

    /**
     * 保存淘宝文章
     *
     * @param id         文章id
     * @param jsonObject json
     * @param existFlag  是否存在
     * @return {@link TbArticle}
     */
    TbArticle saveTbArticle(Long id, JSONObject jsonObject, boolean existFlag);
}
