package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysDictData;

public interface SysDictDataService {


    int deleteByPrimaryKey(Long dictCode);

    int insert(SysDictData record);

    int insertSelective(SysDictData record);

    SysDictData selectByPrimaryKey(Long dictCode);

    int updateByPrimaryKeySelective(SysDictData record);

    int updateByPrimaryKey(SysDictData record);

}
