package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com-paradise-tomdog-sys-entity-SysSmsInfo")
@Data
public class SysSmsInfo {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    
    /**
     * 关联商户id
     */
    @ApiModelProperty(value = "关联商户id")
    private Long merchantId;
    
    /**
     * 业务名
     */
    @ApiModelProperty(value = "业务名")
    private String serviceName;
    
    /**
     * 业务id
     */
    @ApiModelProperty(value = "业务id")
    private String serviceId;
    
    /**
     * 模板id
     */
    @ApiModelProperty(value = "模板id")
    private String templateId;
    
    /**
     * 模板内容
     */
    @ApiModelProperty(value = "模板内容")
    private String templateContent;
    
    /**
     * 状态：1启用 0禁用
     */
    @ApiModelProperty(value = "状态：1启用 0禁用")
    private String status;
    
    /**
     * 是否删除：1删除
     */
    @ApiModelProperty(value = "是否删除：1删除")
    private String isDel;
    
    @ApiModelProperty(hidden = true)
    private Long createBy;
    
    @ApiModelProperty(hidden = true)
    private Long updateBy;
    
    @ApiModelProperty(hidden = true)
    private Date createTime;
    
    @ApiModelProperty(hidden = true)
    private Date updateTime;
}