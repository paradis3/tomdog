package com.paradise.tomdog.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.paradise.tomdog.base.cwmutils.CmwAPI;
import com.paradise.tomdog.sys.entity.BranchRechargeRecord;
import com.paradise.tomdog.sys.entity.MasterRechargeRecord;
import com.paradise.tomdog.sys.entity.SysUserPayInfo;
import com.paradise.tomdog.sys.mapper.SysUserPayInfoMapper;
import com.paradise.tomdog.sys.service.BranchRechargeRecordService;
import com.paradise.tomdog.sys.service.MasterRechargeRecordService;
import com.paradise.tomdog.sys.service.SysBranchMerchantService;
import com.paradise.tomdog.sys.service.SysSmsInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 充值主动扫描任务
 */
@Component
@Slf4j
public class RechargeJob {

    @Autowired
    SysSmsInfoService sysSmsInfoService;
    @Autowired
    SysBranchMerchantService sysBranchMerchantService;
    @Autowired
    private MasterRechargeRecordService masterRechargeRecordService;
    @Autowired
    private BranchRechargeRecordService branchRechargeRecordService;
    @Autowired
    private SysUserPayInfoMapper sysUserPayInfoMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;


    /**
     * 每10分钟执行一次，查询待支付的订单信息
     */
    @Scheduled(fixedDelay = 600000) //上一次执行完毕时间点之后60秒再执行
    public void rechargeQuery() {
        log.info("执行查询待支付的订单信息任务----开始");
        try {
            Date dateMaster = new Date(System.currentTimeMillis() - 1200000);
            List<MasterRechargeRecord> masterRechargeRecords = masterRechargeRecordService.selectAllByUpdateTimeBefore(dateMaster);
            //查询用户商户付款信息
            SysUserPayInfo sysUserPayInfoMaster = sysUserPayInfoMapper.selectOneByMerchantId(1L);
            for (MasterRechargeRecord masterRechargeRecord : masterRechargeRecords) {
                Map<String, String> queryParam = new HashMap<>();
                //商户订单号
                queryParam.put("orderid", masterRechargeRecord.getOrderNo());
                String resultStr = CmwAPI.orderQuery(queryParam, sysUserPayInfoMaster);
                log.info("第三方支付查询返回结果：{}", resultStr);
                JSONObject resultStrJson = JSON.parseObject(resultStr);
                if (resultStrJson != null) {
                    JSONObject resultDataStrJson = resultStrJson.getJSONObject("data");
                    if (resultDataStrJson != null) {
                        String status = resultDataStrJson.getString("status");
                        if (StringUtils.equalsAny(status, "1")) {
                            //发送给kafka
                            kafkaTemplate.send("cwmCallBack", resultDataStrJson.toJSONString());
                        } else {
                            masterRechargeRecord.setUpdateTime(new Date());
                            masterRechargeRecord.setUpdateBy(1L);
                            //置为失败
                            masterRechargeRecord.setStatus("3");
                            //更新充值记录
                            masterRechargeRecordService.updateByPrimaryKey(masterRechargeRecord);
                        }
                    }
                }
            }
            Date dateBranch = new Date(System.currentTimeMillis() - 1200000);
            List<BranchRechargeRecord> branchRechargeRecords = branchRechargeRecordService.selectAllByUpdateTimeBefore(dateBranch);
            for (BranchRechargeRecord branchRechargeRecord : branchRechargeRecords) {
                //查询用户商户付款信息
                SysUserPayInfo sysUserPayInfoBranch = sysUserPayInfoMapper.selectOneByMerchantId(branchRechargeRecord.getMerchantId());
                Map<String, String> queryParam = new HashMap<>();
                //商户订单号
                queryParam.put("orderid", branchRechargeRecord.getOrderNo());
                String resultStr = CmwAPI.orderQuery(queryParam, sysUserPayInfoBranch);
                log.info("第三方支付查询返回结果：{}", resultStr);
                JSONObject resultStrJson = JSON.parseObject(resultStr);
                if (resultStrJson != null) {
                    JSONObject resultDataStrJson = resultStrJson.getJSONObject("data");
                    if (resultDataStrJson != null) {
                        String status = resultDataStrJson.getString("status");
                        if (StringUtils.equalsAny(status, "1")) {
                            //发送给kafka
                            kafkaTemplate.send("cwmCallBack", resultDataStrJson.toJSONString());
                        } else {
                            branchRechargeRecord.setUpdateTime(new Date());
                            branchRechargeRecord.setUpdateBy(1L);
                            //置为失败
                            branchRechargeRecord.setStatus("3");
                            //更新充值记录
                            branchRechargeRecordService.updateByPrimaryKey(branchRechargeRecord);
                        }
                    }
                }
            }
            log.info("查询待支付的订单信息任务----结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("查询待支付的订单信息任务----失败");
        }
    }
}
