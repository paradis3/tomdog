package com.paradise.tomdog.rpc.result;

/**
 * @author Paradise
 */
public interface LlResultInterface {
    /**
     * 是否成功
     *
     * @return boolean true - success
     */
    boolean isSuccess();

    /**
     * 获得Tips
     *
     * @return Tips
     */
    String getTips();
}
