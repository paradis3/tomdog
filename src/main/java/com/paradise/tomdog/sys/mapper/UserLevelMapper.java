package com.paradise.tomdog.sys.mapper;
import java.util.Date;

import com.paradise.tomdog.sys.entity.UserLevel;
import org.apache.ibatis.annotations.Mapper;import org.apache.ibatis.annotations.Param;import java.math.BigDecimal;import java.util.List;


/**
 * 用户等级
 *
 * @author Paradise
 */
@Mapper
public interface UserLevelMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Integer id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(UserLevel record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(UserLevel record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    UserLevel selectByPrimaryKey(Integer id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(UserLevel record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(UserLevel record);
    
    /**
     * 条件查询全部等级列表
     *
     * @param userLevel 查询条件
     * @return select result
     */
    List<UserLevel> selectByAll(UserLevel userLevel);
    
    
    /**
     * 查询是否能升级
     *
     * @param payAmount    充值金额
     * @param currentLevel 当前等级 level
     * @return select result limit 1
     */
    UserLevel selectCanUpdateLevel(@Param("merchantId") Long merchantId,@Param("payAmount") BigDecimal payAmount,
                                   @Param("currentLevel") Integer currentLevel);
    
    /**
     * 查询是否能升级
     *
     * @param totalPoints  累计充值积分
     * @param currentLevel 当前等级 level
     * @return select result limit 1
     */
    UserLevel selectCanUpdateLevelByPoints(@Param("totalPoints") Integer totalPoints,
                                           @Param("currentLevel") Integer currentLevel);
}