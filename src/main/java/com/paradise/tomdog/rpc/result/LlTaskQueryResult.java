package com.paradise.tomdog.rpc.result;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Paradise
 */
@Data
@ToString
public class LlTaskQueryResult implements LlResultInterface {

    @Override
    public boolean isSuccess() {
        return "1".equals(this.getData().status);
    }

    @Override
    public String getTips() {
        return this.getData().tips;
    }

    @Getter
    @Setter
    @ToString
    public static class Data {
        /**
         * 本次查询时间总数
         */
        private Integer all;
        /**
         * 通用状态码
         */
        private String status;
        /**
         * 提示信息
         */
        private String tips;
        private Integer count;
        private Integer success;
        private Integer success_all;
        private List list;

        public Integer getCompletedCount() {
            Item item = this.list.getL().get(0);
            return item.getC() - item.getE();
        }

        public Integer getStatusCode() {
            return this.list.getL().get(0).getS();
        }

        public String getStatusDesc() {
            return this.list.getL().get(0).getM();
        }

    }

    @Getter
    @Setter
    @ToString
    public static class List {
        private java.util.List<Item> l;
    }

    @Getter
    @Setter
    public static class Item {
        /**
         * 任务id
         */
        private String i;
        /**
         * 旺旺、宝贝id、店铺id
         */
        private String a;
        /**
         * 搜索关键字
         */
        private String k;
        /**
         * 任务量
         */
        private Integer c;
        /**
         * 单价
         */
        private Integer p;
        /**
         * 剩余量
         */
        private Integer e;
        /**
         * 任务状态
         */
        private Integer s;
        /**
         * 任务状态描述
         */
        private String m;
        /**
         * 任务发布时间
         */
        private String t;
        /**
         * 任务每小时分配量
         */
        private String h;
        /**
         * 任务开始时间
         */
        private String b;

        private String browse_time;

        private String depth;

        private String name;

        @Override
        public String toString() {
            return "Item{" +
                    "i='" + i + '\'' +
                    ", a='" + a + '\'' +
                    ", k='" + k + '\'' +
                    ", 任务量=" + c +
                    ", 单价=" + p +
                    ", 剩余量=" + e +
                    ", 状态=" + s +
                    ", m='" + m + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    private Data data;
}
