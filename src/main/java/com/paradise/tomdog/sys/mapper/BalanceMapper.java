package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.Balance;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 余额Mapper
 *
 * @author Paradise
 */
@Mapper
public interface BalanceMapper {
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(Balance record);

    /**
     * select by primary key
     *
     * @param balanceId primary key
     * @return object by primary key
     */
    Balance selectByPrimaryKey(Long balanceId);

    /**
     * 根据业务主键查询余额
     *
     * @param businessId 业务主键
     * @param type       {@link com.paradise.tomdog.base.utils.CommonUtils}
     * @return {@link Balance}
     */
    Balance selectByBusinessIdAndType(@Param("businessId") Long businessId,
                                      @Param("type") Integer type);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(Balance record);

    /**
     * update record with cas by business_id
     * 注意 amount 参数必填，可为负数；balance参数无意义
     *
     * @param balance the updated record
     * @return update count
     */
    int updateByBusinessIdWithCas(Balance balance);

    /**
     * update record with cas by balance_id
     * 注意 amount 参数必填，可为负数；balance参数无意义
     *
     * @param balance the updated record
     * @return update count
     */

    int updateByBalanceIdWithCas(Balance balance);
}