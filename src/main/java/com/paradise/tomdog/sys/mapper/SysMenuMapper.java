package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.base.R;
import com.paradise.tomdog.sys.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface SysMenuMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysMenu record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysMenu selectByPrimaryKey(Long id);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysMenu record);

    /**
     * 查询菜单列表
     *
     * @param roleId   角色id
     * @param isMaster 1 总站 0 分钟商户
     * @return select res
     */
    List<SysMenu> selectByRole(@Param("roleId") Long roleId,
                               @Param("isMaster") Integer isMaster);

    /**
     * 初始化商户超级管理员菜单权限
     *
     * @param roleId   角色id
     * @param isMaster 1 总站 0 分钟商户
     * @return insert count
     */
    int initSuperRoleMenu(@Param("roleId") Long roleId,
                          @Param("isMaster") Integer isMaster);

    /**
     * 配置角色菜单
     *
     * @param ids      逗号分隔菜单id
     * @param updateBy 操作人
     * @param roleId   角色id
     * @return {@link R}
     */
    int cfgMenu(@Param("ids") Long[] ids, @Param("roleId") Long roleId, @Param("updateBy") Long updateBy);

    /**
     * 删除原有配置
     *
     * @param roleId 角色id
     * @return del count
     */
    int deleteCfgByRoleId(@Param("roleId") Long roleId);
}