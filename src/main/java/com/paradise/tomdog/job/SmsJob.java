package com.paradise.tomdog.job;

import com.paradise.tomdog.base.utils.RedisUtil;
import com.paradise.tomdog.sys.entity.SysBranchMerchant;
import com.paradise.tomdog.sys.entity.SysSmsInfo;
import com.paradise.tomdog.sys.service.SysBranchMerchantService;
import com.paradise.tomdog.sys.service.SysSmsInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 定时任务
 *
 * @author Paradise
 */
@Component
@Slf4j
public class SmsJob {

    @Autowired
    SysSmsInfoService sysSmsInfoService;
    @Autowired
    SysBranchMerchantService sysBranchMerchantService;
    @Autowired
    RedisUtil redisUtil;

    /**
     * 每3分钟执行一次，查询需修改的sms模板签名信息
     * 上一次执行完毕时间点之后60秒再执行
     */
    @Scheduled(fixedDelay = 180_000)
    public void smsQuery() {
        log.info("执行短信配置更新定时任务----开始");
        try {
            Date date = new Date(System.currentTimeMillis() - 1200_000);
            List<SysSmsInfo> sysSmsInfos = sysSmsInfoService.selectAllByUpdateTimeAfter(date);
            for (SysSmsInfo smsInfo : sysSmsInfos) {
                if (StringUtils.equals(smsInfo.getStatus(), "0") || StringUtils.equals(smsInfo.getIsDel(), "1")) {
                    redisUtil.delete("sms_temp_" + smsInfo.getMerchantId() + "_" + smsInfo.getServiceId(), smsInfo.getTemplateId());
                } else {
                    //redis存入商户相应的短信模板id
                    redisUtil.set("sms_temp_" + smsInfo.getMerchantId() + "_" + smsInfo.getServiceId(), smsInfo.getTemplateId());
                    log.info("修改已加载短信模板信息----{}", smsInfo.getTemplateId());
                }
            }
            List<SysBranchMerchant> sysBranchMerchants = sysBranchMerchantService.selectAllByUpdateTimeAfter(date);
            for (SysBranchMerchant branchMerchant : sysBranchMerchants) {
                if (StringUtils.equals(branchMerchant.getStatus(), "0") || branchMerchant.getIsDel() == 1) {
                    redisUtil.delete("sms_sign_" + branchMerchant.getId(), branchMerchant.getSmsSignId());
                } else {
                    //redis存入商户相应的签名id
                    redisUtil.set("sms_sign_" + branchMerchant.getId(), branchMerchant.getSmsSignId());
                    log.info("修改已加载短信签名信息----{}", branchMerchant.getSmsSignId());
                }
            }
            log.info("执行短信配置更新定时任务----结束");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("执行短信配置更新定时任务----失败");
        }
    }
}
