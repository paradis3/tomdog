package com.paradise.tomdog.base.utils;

import cn.hutool.core.util.StrUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 解析url 参数
 *
 * @author Paradise
 */
public class UrlParser {

    public static Map<String, String> parse(String url) {
        url = url.trim();
        Map<String, String> map = new HashMap<>();
        if (StrUtil.isBlank(url)) {
            return map;
        }
        String[] urlParts = url.split("\\?");
        //没有参数
        if (urlParts.length == 1) {
            return map;
        }
        //有参数
        String[] params = urlParts[1].split("&");
        for (String param : params) {
            String[] keyValue = param.split("=");
            map.put(keyValue[0], keyValue[1]);
        }
        map.put("basePath", urlParts[0]);
        return map;
    }
}
