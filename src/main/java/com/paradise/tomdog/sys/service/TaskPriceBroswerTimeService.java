package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.TaskPriceBrowserTime;

public interface TaskPriceBroswerTimeService {


    int deleteByPrimaryKey(Long id);

    int insert(TaskPriceBrowserTime record);

    TaskPriceBrowserTime selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TaskPriceBrowserTime record);

}
