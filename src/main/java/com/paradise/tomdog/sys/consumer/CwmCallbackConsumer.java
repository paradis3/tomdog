package com.paradise.tomdog.sys.consumer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.paradise.tomdog.base.R;
import com.paradise.tomdog.base.Rx;
import com.paradise.tomdog.sys.constant.Points_Record_Type;
import com.paradise.tomdog.sys.constant.Recharge_Status;
import com.paradise.tomdog.sys.constant.UserTypeEnum;
import com.paradise.tomdog.sys.entity.*;
import com.paradise.tomdog.sys.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author lj
 */
@Slf4j
@Component
public class CwmCallbackConsumer {
    @Autowired
    private MasterRechargeRecordService masterRechargeRecordService;
    @Autowired
    private BranchRechargeRecordService branchRechargeRecordService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private BalanceService balanceService;
    @Autowired
    private SysUserApiService sysUserApiService;
    @Autowired
    private KafkaTemplate kafkaTemplate;

    /**
     * 处理财务猫充值回调
     */
    @KafkaListener(topics = {"cwmCallBack"})
    @Transactional(rollbackFor = Exception.class)
    public void handleCallback(ConsumerRecord<String, String> record) {
        log.info("topic={},accessKey={},value={}", record.topic(), record.key(), record.value());
        if (StringUtils.isEmpty(record.value())) {
            return;
        }
        JSONObject json = JSON.parseObject(record.value());
        log.info("支付回调-参数{}", json);
        if (json == null) {
            return;
        }
        //商户订单号
        String orderid = json.getString("orderid");
        //1：收款成功；0：待支付；-1：二维码生成中；11：二维码生成失败；12：订单超时未支付'
        String status = json.getString("status");
        //支付金额 单位：元。精确小数点后2位
        String facevalue = json.getString("facevalue");
        //财务猫平台生成的唯一支付流水订单号
        String id = json.getString("id");
        //时间戳
        String timestamp = json.getString("timestamp");
        //扩展字段代表使用商户ID
        String extendsParam = json.getString("extends_param");
        BalanceUpdateParam balanceUpdateParam;
        //如果扩展字段 不是1则是分站商户
        if (StringUtils.equals(extendsParam, "1")) {
            balanceUpdateParam = dealUserRecharge(orderid, id, timestamp, status);
        } else {
            balanceUpdateParam = dealMerchantRecharge(orderid, id, timestamp, status);
        }
        if (balanceUpdateParam != null) {
            //更新余额和相应记录
            R<Balance> balanceR = balanceService.updateBalance(balanceUpdateParam);
            if (!Rx.isSuccess(balanceR)) {
                //余额更新失败发送到kafka
                kafkaTemplate.send("rechargeFailList", JSON.toJSONString(balanceUpdateParam));
            }
        }
    }

    private BalanceUpdateParam dealUserRecharge(String orderid, String id, String timestamp, String status) {
        BalanceUpdateParam balanceUpdateParam = null;
        MasterRechargeRecord masterRecordQuery = new MasterRechargeRecord();
        masterRecordQuery.setOrderNo(orderid);
        masterRecordQuery.setIsDel("0");
        masterRecordQuery.setStatus(Recharge_Status.UN_PAYED.getCode());
        List<MasterRechargeRecord> masterRecords = masterRechargeRecordService.selectByAll(masterRecordQuery);
        if (masterRecords.size() > 0) {
            MasterRechargeRecord masterRechargeRecord = masterRecords.get(0);
            SysUser user = sysUserService.getUserInfo(masterRechargeRecord.getUserId());
            if (user == null) {
                log.info("此{}用户此删除或不存在", masterRechargeRecord.getUserId());
                return null;
            }
            masterRechargeRecord.setUpdateTime(new Date());
            masterRechargeRecord.setUpdateBy(1L);
            masterRechargeRecord.setPayNo(id);
            if (StringUtils.isNotBlank(timestamp)) {
                masterRechargeRecord.setPayTime(new Date());
            }
            if (StringUtils.equalsAny(status, "1")) {
                masterRechargeRecord.setStatus("2");
                //余额积分对象更新
                balanceUpdateParam = BalanceUpdateParam.builder().createBy(user.getId()).amount(masterRechargeRecord.getRechargePoints())
                        .recordType(Points_Record_Type.RECHARGE_ADD).rechargeId(masterRechargeRecord.getId())
                        .build();
                if (user.getMerchantId() != null) {
                    balanceUpdateParam.setBusinessId(user.getMerchantId());
                    balanceUpdateParam.setUserTypeEnum(UserTypeEnum.MERCHANT_TYPE);
                    Integer balance = balanceService.queryBalance(user.getMerchantId(), UserTypeEnum.MERCHANT_TYPE);
                    masterRechargeRecord.setBalance(balance + masterRechargeRecord.getRechargePoints());
                } else {
                    SysUserApi sysUserApi = sysUserApiService.selectByUserId(masterRechargeRecord.getUserId());
                    balanceUpdateParam.setBusinessId(sysUserApi.getId());
                    balanceUpdateParam.setUserTypeEnum(UserTypeEnum.API_TYPE);
                    Integer balance = balanceService.queryBalance(sysUserApi.getId(), UserTypeEnum.API_TYPE);
                    masterRechargeRecord.setBalance(balance + masterRechargeRecord.getRechargePoints());
                }
            } else if (StringUtils.equalsAny(status, "11", "12")) {
                masterRechargeRecord.setStatus("3");
            }
            //更新充值记录
            masterRechargeRecordService.updateByPrimaryKey(masterRechargeRecord);
        }
        return balanceUpdateParam;
    }

    private BalanceUpdateParam dealMerchantRecharge(String orderid, String id, String timestamp, String status) {
        BalanceUpdateParam balanceUpdateParam = null;
        BranchRechargeRecord branchRecordQuery = new BranchRechargeRecord();
        branchRecordQuery.setOrderNo(orderid);
        branchRecordQuery.setIsDel("0");
        branchRecordQuery.setStatus(Recharge_Status.UN_PAYED.getCode());
        List<BranchRechargeRecord> branchRecords = branchRechargeRecordService.selectByAll(branchRecordQuery);
        if (branchRecords.size() > 0) {
            BranchRechargeRecord branchRechargeRecord = branchRecords.get(0);
            SysUser user = sysUserService.getUserInfo(branchRechargeRecord.getUserId());
            if (user == null) {
                log.info("此{}用户此删除或不存在", branchRechargeRecord.getUserId());
                return null;
            }
            branchRechargeRecord.setPayNo(id);
            if (StringUtils.isNotBlank(timestamp)) {
                branchRechargeRecord.setPayTime(new Date());
            }
            branchRechargeRecord.setUpdateTime(new Date());
            branchRechargeRecord.setUpdateBy(1L);
            if (StringUtils.equalsAny(status, "1")) {
                branchRechargeRecord.setStatus(Recharge_Status.PAY_SUCCESS.getCode());
                // 查询余额，计算充值完成后的积分余额
                Integer balance = balanceService.queryBalance(branchRechargeRecord.getUserId(), UserTypeEnum.MERCHANT_USER_TYPE);
                balance += branchRechargeRecord.getRechargePoints();
                branchRechargeRecord.setBalance(balance);
                //余额积分对象更新
                balanceUpdateParam = BalanceUpdateParam.builder().createBy(branchRechargeRecord.getUserId()).amount(branchRechargeRecord.getRechargePoints())
                        .recordType(Points_Record_Type.RECHARGE_ADD).rechargeId(branchRechargeRecord.getId()).businessId(branchRechargeRecord.getUserId())
                        .userTypeEnum(UserTypeEnum.MERCHANT_USER_TYPE)
                        .build();
            } else if (StringUtils.equalsAny(status, "11", "12")) {
                branchRechargeRecord.setStatus(Recharge_Status.PAY_FAIL.getCode());
            }
            //更新充值记录
            branchRechargeRecordService.updateByPrimaryKey(branchRechargeRecord);
            //用户升级
            branchRechargeRecordService.userLevelHandler(branchRechargeRecord, user);
            //用户返利
            branchRechargeRecordService.userSpreadPointsHandler(branchRechargeRecord, user);
        }
        return balanceUpdateParam;
    }
}
    
