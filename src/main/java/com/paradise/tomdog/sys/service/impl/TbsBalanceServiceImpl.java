package com.paradise.tomdog.sys.service.impl;

import com.paradise.tomdog.sys.entity.TbsBalance;
import com.paradise.tomdog.sys.entity.TbsBalanceRecord;
import com.paradise.tomdog.sys.entity.TbsWithdrawRecord;
import com.paradise.tomdog.sys.mapper.TbsBalanceMapper;
import com.paradise.tomdog.sys.mapper.TbsBalanceRecordMapper;
import com.paradise.tomdog.sys.service.TbsBalanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Slf4j
public class TbsBalanceServiceImpl implements TbsBalanceService {

    @Resource
    private TbsBalanceMapper tbsBalanceMapper;
    @Resource
    private TbsBalanceRecordMapper tbsBalanceRecordMapper;

    @Override
    public int insert(TbsBalance record) {
        return tbsBalanceMapper.insert(record);
    }

    @Override
    public TbsBalance selectByPrimaryKey(Long userId) {
        TbsBalance tbsBalance = tbsBalanceMapper.selectByPrimaryKey(userId);
        if (tbsBalance == null) {
            tbsBalance = new TbsBalance();
            tbsBalance.setSpreadBalance(0);
        }
        return tbsBalance;
    }

    @Override
    public int updateByPrimaryKey(TbsBalance record) {
        return tbsBalanceMapper.updateByPrimaryKey(record);
    }

    /**
     * 提现申请同步更新推广积分和余额
     *
     * @param tbsBalance
     * @param record
     * @return
     */
    @Override
    public TbsBalance updateByWithDrawRecord(TbsBalance tbsBalance, TbsWithdrawRecord record) {
        // 修改用户推广积分余额
        log.info("推广人当前推广积分余额信息{}", tbsBalance);
        tbsBalanceMapper.updateSpreadBalanceByUserId(record.getAmount(),
                tbsBalance.getVersion(), tbsBalance.getUserId());
        TbsBalance afterTbsBalance = tbsBalanceMapper.selectByPrimaryKey(tbsBalance.getUserId());
        log.info("推广人最新积分余额信息{}", afterTbsBalance);

        // 记录推广积分提现记录
        TbsBalanceRecord tbsBalanceRecord = new TbsBalanceRecord(tbsBalance.getUserId(), record.getAmount(), "2");
        tbsBalanceRecord.setIsDel("0");
        tbsBalanceRecord.setSourceId(record.getId());
        tbsBalanceRecord.setCreateTime(new Date());
        tbsBalanceRecord.setCreateBy(tbsBalance.getUserId());
        tbsBalanceRecordMapper.insert(tbsBalanceRecord);
        return afterTbsBalance;
    }

    /**
     * 提现失败同步返回推广积分和余额
     *
     * @param record
     * @return
     */
    @Override
    public TbsBalance returnByWithDrawRecord(TbsWithdrawRecord record) {
        //查询用户推广余额
        TbsBalance tbsBalance = tbsBalanceMapper.selectByPrimaryKey(record.getUserId());
        log.info("推广人当前推广积分余额信息{}", tbsBalance);
        // 修改用户推广积分余额
        tbsBalanceMapper.updateSpreadBalanceByUserId(-record.getAmount(),
                tbsBalance.getVersion(), tbsBalance.getUserId());
        TbsBalance afterTbsBalance = tbsBalanceMapper.selectByPrimaryKey(tbsBalance.getUserId());
        log.info("推广人最新积分余额信息{}", afterTbsBalance);
        // 记录推广积分提现失败记录
        TbsBalanceRecord tbsBalanceRecord = new TbsBalanceRecord(tbsBalance.getUserId(), -record.getAmount(), "3");
        tbsBalanceRecord.setIsDel("0");
        tbsBalanceRecord.setSourceId(record.getId());
        tbsBalanceRecord.setCreateTime(new Date());
        tbsBalanceRecord.setCreateBy(tbsBalance.getUserId());
        tbsBalanceRecordMapper.insert(tbsBalanceRecord);
        return afterTbsBalance;
    }

    @Override
    public int deleteByPrimaryKey(Long userId) {
        return tbsBalanceMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int insertSelective(TbsBalance record) {
        return tbsBalanceMapper.insertSelective(record);
    }

    @Override
    public int updateByPrimaryKeySelective(TbsBalance record) {
        return tbsBalanceMapper.updateByPrimaryKeySelective(record);
    }
}

