package com.paradise.tomdog.sys.entity;

public class MasterRechargeRecordWithSysUserWithSysBranchMerchant extends MasterRechargeRecord {
    private SysUser sysUser;

    private SysBranchMerchant sysBranchMerchant;

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public SysBranchMerchant getSysBranchMerchant() {
        return sysBranchMerchant;
    }

    public void setSysBranchMerchant(SysBranchMerchant sysBranchMerchant) {
        this.sysBranchMerchant = sysBranchMerchant;
    }
}