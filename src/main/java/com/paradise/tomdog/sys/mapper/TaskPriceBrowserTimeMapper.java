package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.TaskPriceBrowserTime;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务浏览时长阶梯价格表 Mapper
 *
 * @author Paradise
 */
@Deprecated
@Mapper
public interface TaskPriceBrowserTimeMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(TaskPriceBrowserTime record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    TaskPriceBrowserTime selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(TaskPriceBrowserTime record);

    /**
     * 查询阶梯价格表
     *
     * @param merchantId 商户id 总站或者默认配置 传 null
     * @param isMaster   1=总站 0 分站商户
     * @return 阶梯价格表
     */
    @Deprecated
    List<TaskPriceBrowserTime> selectAllByMerchantId(@Param("merchantId") Long merchantId,
                                                     @Param("isMaster") Integer isMaster);


    /**
     * 查询阶梯价格表
     *
     * @param browserTime 浏览时长
     * @param merchantId  商户ID
     * @return 价格表
     */
    List<TaskPriceBrowserTime> selectByBrowserTimeForMerchant(@Param("browserTime") Integer browserTime,
                                                              @Param("merchantId") Long merchantId);

    /**
     * 查询总站阶梯价格表
     *
     * @param browserTime 浏览时长
     * @return 价格表
     */
    List<TaskPriceBrowserTime> selectByBrowserTimeForMaster(@Param("browserTime") Integer browserTime);
}