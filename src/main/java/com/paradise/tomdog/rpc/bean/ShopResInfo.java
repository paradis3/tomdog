package com.paradise.tomdog.rpc.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 店铺信息查询返回
 *
 * @author Paradise
 */
@Getter
@Setter
@ToString
public class ShopResInfo {
    private int status;
    private Model model;
    private String asyntime;

    public boolean isSuccess() {
        return this.status == 1;
    }

    public TbShopInfo toTbShopInfo() {
        return new TbShopInfo(this.model.picUrl, Integer.valueOf(this.model.indexData.fansNum),
                this.model.indexData.sellerNick, this.model.title, this.model.indexData.shopId, this.model.getSellerId());
    }

    @Getter
    @Setter
    private static class Model {
        private String id;
        private String nick;
        private Long sellerId;
        private String title;
        private String picUrl;
        private String rankType;
        private String rankNum;
        private IndexData indexData;
    }

    @Getter
    @Setter
    private static class IndexData {
        private String fansNum;
        private String shopId;
        private String sellerNick;
        private String shopName;
        private String shopLogo;
    }

}
