package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 密码修改记录
 *
 * @author Paradise
 */
@ApiModel(value = "com.paradise.tomdog.sys.entity.SysPasswordLog")
@Data
public class SysPasswordLog {
    /**
     * 日志主键
     */
    @ApiModelProperty(value = "日志主键")
    private Long logId;
    /**
     * 用户主键
     */
    @ApiModelProperty(value = "用户主键")
    private Long userId;
    /**
     * 旧密码
     */
    @ApiModelProperty(value = "旧密码")
    private String prePassword;
    /**
     * 新密码
     */
    @ApiModelProperty(value = "新密码")
    private String newPassword;
    private Long createBy;
    private Long createTime;

    public SysPasswordLog(Long userId, String prePassword, String newPassword, Long createBy) {
        this.userId = userId;
        this.prePassword = prePassword;
        this.newPassword = newPassword;
        this.createBy = createBy;
        this.createTime = System.currentTimeMillis();
    }
}