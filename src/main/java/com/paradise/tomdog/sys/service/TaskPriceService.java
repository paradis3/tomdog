package com.paradise.tomdog.sys.service;

import com.paradise.tomdog.sys.entity.SysUser;
import com.paradise.tomdog.sys.entity.TaskPrice;

import java.util.List;

public interface TaskPriceService {

    /**
     * insert
     *
     * @param record record
     * @return insert count
     */
    int insert(TaskPrice record);

    /**
     * select
     *
     * @param priceId 价格配置主键
     * @return {@link TaskPrice}
     */
    TaskPrice selectByPrimaryKey(Long priceId);

    /**
     * update
     *
     * @param record record
     * @return update count
     */
    int updateByPrimaryKey(TaskPrice record);

    /**
     * update
     *
     * @param record record
     * @return update count
     */
    int updatePriceByPrimaryKey(TaskPrice record);

    /**
     * 批量更新价格配置
     *
     * @param taskPrices  任务价格配置信息
     * @param currentUser 当前登录人
     * @return update count
     */
    int updatePriceCfg(TaskPrice[] taskPrices, SysUser currentUser);

    /**
     * 查询价格表
     *
     * @param merchantId 商户ID，总站传null
     * @return 价格表
     */
    List<TaskPrice> selectAllByMerchantId(Long merchantId);
}

