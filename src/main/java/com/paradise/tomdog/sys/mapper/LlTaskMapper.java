package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.LlTask;
import com.paradise.tomdog.sys.vo.TaskQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Paradise
 */
@Mapper
public interface LlTaskMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);

    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(LlTask record);

    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(LlTask record);

    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    LlTask selectByPrimaryKey(Long id);

    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(LlTask record);

    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(LlTask record);

    /**
     * 根据 taskQuery 查询
     *
     * @param query 查询条件
     * @return select result
     */
    List<LlTask> selectByTaskQuery(TaskQuery query);


    /**
     * 分站商户查询任务列表
     *
     * @param query      查询条件
     * @param merchantId 分站商户
     * @return select result
     */
    List<LlTask> selectByTaskQueryAndMerchantId(@Param("query") TaskQuery query,
                                                @Param("merchantId") Long merchantId);

    /**
     * 根据 llTask 查询
     *
     * @param llTask 查询条件
     * @return select result
     */

    List<LlTask> selectByAll(LlTask llTask);

    /**
     * 修改任务状态
     *
     * @param updatedStatus 目标状态
     * @param id            任务id
     * @param releaseTime   发布时间，可以为空
     * @return update count
     */
    int updateStatusById(@Param("updatedStatus") String updatedStatus,
                         @Param("id") Long id,
                         @Param("releaseTime") Long releaseTime);

    /**
     * 根据状态查询任务列表
     *
     * @param status 任务状态
     * @return select result
     * @see com.paradise.tomdog.sys.constant.Task_Status
     */
    List<LlTask> selectByStatus(@Param("status") String status);

    /**
     * 更新已完成任务数量
     *
     * @param updatedCompletedCount 最新数据
     * @param id                    任务主键ID
     * @return update count
     */
    int updateCompletedCountById(@Param("updatedCompletedCount") Integer updatedCompletedCount,
                                 @Param("id") Long id);


    /**
     * 任务完成
     *
     * @param id     任务主键ID
     * @param status 任务完成状态
     * @return update count
     */
    int completeTask(@Param("id") Long id,
                     @Param("status") String status);

    /**
     * 根据 taskId 查询任务信息
     *
     * @param taskId 任务Id
     * @return select result
     */
    LlTask selectOneByTaskId(@Param("taskId") String taskId);

    /**
     * 统计任务数据
     *
     * @param merchantId 分站商户ID
     * @param taskType   任务类型
     * @param beginDate  查询任务开始时间
     * @return 任务数量
     */
    Long countByCondition(@Param("merchantId") Long merchantId,
                          @Param("taskType") Integer taskType,
                          @Param("beginDate") Long beginDate);


    /**
     * 根据任务状态和用户 id 查询任务 数量
     *
     * @param status   任务状态
     * @param userId   用户id
     * @param typeList 任务类型
     * @return 任务数量
     */
    Integer countByStatusAndUserId(@Param("status") String status,
                                   @Param("userId") Long userId,
                                   @Param("typeList") List<Integer> typeList);
}