package com.paradise.tomdog.rpc.constant;

/**
 * 通用状态码 - 枚举
 *
 * @author Paradise
 */

public enum Sms_Service_Type {
    /**
     * 注册/修改密码/修改手机号
     */
    NORMALCODE("1", "注册/修改密码/修改手机号");


    Sms_Service_Type(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
