package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author Paradise
 */
@ApiModel(value = "角色实体")
@Data
public class SysRole {
    /**
     * 角色主键
     */
    @ApiModelProperty(value = "角色主键", example = "1")
    private Long id;

    @ApiModelProperty(value = "上级角色id", example = "1")
    private Long pid;

    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    @Size(max = 10, min = 2, message = "角色名称长度2-10个字符")
    private String name;

    /**
     * 角色描述
     */
    @ApiModelProperty(value = "角色描述")
    @Size(max = 50, message = "角色描述最多50个字符")
    private String description;

    /**
     * 角色状态，启用，禁用，删除
     */
    @ApiModelProperty(value = "角色状态，启用，禁用，删除", hidden = true)
    private String status;

    @ApiModelProperty(value = "商户ID", example = "201", hidden = true)
    private Long merchantId;

    @ApiModelProperty(hidden = true)
    private Long creator;

    @ApiModelProperty(hidden = true)
    private Date createTime;

    @ApiModelProperty(hidden = true)
    private Long updater;

    @ApiModelProperty(hidden = true)
    private Date updateTime;

    @ApiModelProperty(hidden = true)
    private Integer isSuper;
}