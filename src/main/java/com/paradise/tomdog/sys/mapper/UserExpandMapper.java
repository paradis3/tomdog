package com.paradise.tomdog.sys.mapper;

import com.paradise.tomdog.sys.entity.UserExpand;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by Mybatis Generator 2020/03/09
 */
@Mapper
public interface UserExpandMapper {
    int deleteByPrimaryKey(Long uid);

    int insert(UserExpand record);

    UserExpand selectByPrimaryKey(Long uid);

    int updateByPrimaryKeySelective(UserExpand record);

    /**
     * 用户注册 增加推广人数+1
     *
     * @param uid 上级uid
     * @return update count
     */
    int addSpreadCountByUid(@Param("uid") Long uid);

}