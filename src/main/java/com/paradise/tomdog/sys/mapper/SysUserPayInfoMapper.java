package com.paradise.tomdog.sys.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.paradise.tomdog.sys.entity.SysUserPayInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserPayInfoMapper {
    /**
     * delete by primary key
     *
     * @param id primaryKey
     * @return deleteCount
     */
    int deleteByPrimaryKey(Long id);
    
    /**
     * insert record to table
     *
     * @param record the record
     * @return insert count
     */
    int insert(SysUserPayInfo record);
    
    /**
     * insert record to table selective
     *
     * @param record the record
     * @return insert count
     */
    int insertSelective(SysUserPayInfo record);
    
    /**
     * select by primary key
     *
     * @param id primary key
     * @return object by primary key
     */
    SysUserPayInfo selectByPrimaryKey(Long id);
    
    /**
     * update record
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKeySelective(SysUserPayInfo record);
    
    /**
     * update record selective
     *
     * @param record the updated record
     * @return update count
     */
    int updateByPrimaryKey(SysUserPayInfo record);
    

    List<SysUserPayInfo> selectByMerchantIdAndIsDelNot(@Param("merchantId") Long merchantId,
                                                       @Param("notIsDel") String notIsDel);
    
    
    int updateByMerchantId(@Param("updated") SysUserPayInfo updated, @Param("merchantId") Long merchantId);
    
    SysUserPayInfo selectOneByMerchantId(@Param("merchantId")Long merchantId);

	
    List<SysUserPayInfo> selectByAll(SysUserPayInfo sysUserPayInfo);
    
    SysUserPayInfo selectOneByAppId(@Param("appId")String appId);

    SysUserPayInfo selectOneByAppIdAndMerchantId(@Param("appId")String appId,@Param("merchantId")Long merchantId);

	
	

	
}