package com.paradise.tomdog.sys.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel(value = "com.paradise.tomdog.sys.entity.SysUserPayInfo")
@Data
public class SysUserPayInfo {
    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", hidden = true)
    private Long id;

    /**
     * 关联账号id
     */
    @ApiModelProperty(value = "关联账号id")
    @NotNull(message = "关联账号id不能为空")
    private Long merchantId;

    /**
     * 支付方式：1财务猫
     */
    @ApiModelProperty(value = "支付方式：1财务猫")
    @NotBlank(message = "支付方式 不能为空")
    private String payPattern;

    /**
     * API服务器地址
     */
    @ApiModelProperty(value = "API服务器地址")
    @NotBlank(message = "API服务器地址 不能为空")
    private String appServer;

    /**
     * APP_ID
     */
    @ApiModelProperty(value = "APP_ID")
    @NotBlank(message = "APP_ID 不能为空")
    private String appId;

    /**
     * APP_SECRET
     */
    @ApiModelProperty(value = "APP_SECRET")
    @NotBlank(message = "APP_SECRET 不能为空")
    private String appSecret;

    /**
     * 状态：1启用 0禁用
     */
    @ApiModelProperty(value = "状态：1启用 0禁用")
    @NotBlank(message = "status状态不能为空")
    private String status;

    /**
     * 是否删除：1删除
     */
    @ApiModelProperty(value = "是否删除：1删除")
    @NotBlank(message = "isDel是否删除不能为空")
    private String isDel;

    @ApiModelProperty(value = "null", hidden = true)
    private Long createBy;

    @ApiModelProperty(value = "null", hidden = true)
    private Long updateBy;

    @ApiModelProperty(value = "null", hidden = true)
    private Date createTime;

    @ApiModelProperty(value = "null", hidden = true)
    private Date updateTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}